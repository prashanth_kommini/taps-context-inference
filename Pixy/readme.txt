build.xml					: ant based build file for Pixy and TAPS
LogFile.txt					: Log file with debug messages to help understand the code
run-taps.pl					: perl file to run TAPS

build						: contains classes after the build 
class

config:						: folder consisting of SQL sink definitions. 
config.txt
harmless_server_vars.txt
mem.pl						: controls jvm max and starting memory configuration options 
model_file.txt
model_sqlsanit.txt
model_sql.txt
model_xsssanit.txt
model_xss.txt
sinks_file.txt
sinks_sql_sample.txt
sinks_sqlsanit.txt
sinks_sql.txt				: extend this if you want to add more PHP functions as SQL sinks 
sinks_xss_sample.txt
sinks_xsssanit.txt
sinks_xss.txt

doc:						various licenses
cup.txt
eclipse-usage.txt
eclipsing.txt
jflex.txt
licenses
LICENSE.txt
readme.txt

graphs:						folder contains DOT graphs for various artifacts (dependency graphs, CFG, derivation tress before and after transformation)
addwhere.dot
calledby_session1.php.txt
includes_session1.php.txt
session1.php__beforeSymEval___1__DerivationT.dot: DOT representation of derivation tree before transformation
session1.php__transformedDT_2__DerivationT.dot	: DOT representation of derivation tree after transformation
sql1__depGraph.dot.dot	: DOT represenation of dependency graph for each sink 
sql3__depGraph.dot.dot
sql.dot					: DOT representation of Control Flow Graph of function sql 
t1.dot
t2.dot

lib:						Pixy dependencies 
at
jauto
java_cup
junit.jar
org

scripts:				scripts for dumping CFG/AST and SQLIA and XSS detection
quick_cfg.pl			: dumps CFGs for functions
run-sql.pl				
run-xss.pl
tree.pl					: dumps Abstract syntax tree for a program 

src:					source folder for Pixy and TAPS
at						: pixy codebase (augmented)
edu						: UIC specific TAPS codebase 
sql92					: augmented SQL parser

test:
at

testharness:			contains various cases TAPS handles and doesnt handle
onlineHarness.php				: several test cases (pass, fail)
onlineHarness.php__transformed	: transformed version of above test cases
session1.php					: examples used for session1 
session1.php__transformed
