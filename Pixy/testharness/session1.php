<?php
// Tool for Automatically Preparing SQL Queries (TAPS) Test Harness

t1();
function t1 () {
	$x = 1; 
	if ( $x == 1 ) { 
		$q = "select * from X "; 
	} else { 
		$q = "delete from X "; 
	}
	$q = addWhere( $q );
	
	sql ( $q ); 
}

$y = 1; 
function addWhere( $q ){
	if ( $y == 1 ) {
		$u = $_GET['user'];
		$q = $q . " where uid = " . $u; 
	} else { 
		$g = $_GET['group'];
		$q = $q . " where gid = " . $g; 
	}

	return $q; 
}


t2();
function t2 () {
	$g = $_GET['group'];
	$u = $_GET['user'];
	$q = "select * from X where uid = " . $u; 
	sql ( $q ); 
}

function sql($query){

	@mysql_query( $query ) ; 
}


/* ---------------- helper methods -------- */
// Prior to PS transformation a really useful step could be converting
// all mysql_ based calls to mysqli_ interface.
// Tools exist to perform such a transformation automatically 
// http://forge.mysql.com/wiki/Converting_to_MySQLi
// note that the above transformation does not convert to prepared statement. 

// generate a PREPARE statement, bind data arguments, and execute
function executePS($ps, $psArgs){
    global $mysqli;
    $stmt = $mysqlih->prepare($ps);
    if( $stmt ) {
        if(!empty($psArgs) && count($psArgs)){
            $types = str_repeat("s", count($psArgs));
            $args[0] = &$stmt; 
            $args[1] = $types;
            if(is_array($psArgs))
                foreach( $psArgs as $arg)
                    $args[] = $arg;
            else
                $args[] = $psArgs;
            call_user_func_array('mysqli_stmt_bind_param', $args);
        }
     
        $result = $stmt->execute();
        if($stmt -> num_rows > 0) //for result set returning queries
            $result = $stmt->result_metadata();
    } else {
        //$error_str = " SISL : Error code - "$mysqli->error." PS creation failed  for \n \t \t $ps, 
        //with arguments - $psArgs "; 
        //@error_log($error_str, 0);
    }
    return $result;
}

// merge query arguments lists 
function merge_helper()
{
    $retArray = array();
    $args = func_get_args();        // get all args
    $iElem = count($args);
    for($i = 0; $i < $iElem; $i++)
    {
        $ar = $args[$i];
        // if arg is yet not set, dont use "" value
        if(!isset($ar)) continue;   
        
        // if arg is an array, use array_merge 
        // otherwise simply append
        if(is_array($ar))           
            $retArray = array_merge( $retArray, $ar);
        else
            $retArray[] = $ar;      
        }
    return $retArray;
}

// initialize the object to generate PREPARE statements
function initPSConn()
{
    //  mysqli provides the prepared statement
    // no procedural equivalent like mysql_query();
   $mysqlih = new mysqli('localhost', 'username', 'password', 'db');
   if(mysqli_connect_errno()) {
      echo "Connection Failed: " . mysqli_connect_error();
      exit();
   }
}

// cleanup
function destroyPSConn()
{
   $mysqlih -> close();   
}

?>

