<?php
// Tool for Automatically Preparing SQL Queries (TAPS) Test Harness
//
// This PHP file contains functions to execute queries and TAPS 
// intends to transform them to use PREPARE statements instead. TAPS 
// considers all query sinks that are called from main PHP function 
// for transformation. Each function given below presents a scenario, 
// and is invoked from the main function (body) of the PHP file. The 
// running example of the paper and formal section are also given 
// below (test11() and test19()).
//
// transformed code uses helper methods
//  executePS(sql query with placeholder ?, argument list), and 
// merge_helper(argument list1, argument list2, ...)
// Code of helper methods is given at the end of the test harness.
// For a variable $x, its query argument list is stored in 
// variable $x__args.

//========================== Test 1 ================================
// a straight line code constructs and executes a delete query 
// expected result : transformation succeeds

test1();      
function test1()
{
    $a = $_GET['user'];
    $query = "delete from test1 where u = '".$a."'";
    @mysql_query($query);
}

//========================== Test 2 =================================
// SQL sink in this test can execute two queries. TAPS analyzes both 
// the control flows. As changes required to transform are contained 
// in the then and the else blocks, it does not find any conflicts in 
// transforming these flows. Test13 provides an example where 
// conditional statement causes a conflict and also provides 
// resolution of the conflict through code motion.
// expected result TAPS : transformation succeeds

test2();        
function test2()
{
    $u = $_GET['user']; 
    $p = $_GET['password'];
    $i = 0;    
    if($i == 1)
    {
        $query = " select * from test2 where u1 = '" . $u ;
        $query .= "'  or p1 = '" . $p . "'"; 
    }
    else
    {
        $query = " delete from test2 where u = '" . $u . "'";    
    }   
    @mysql_query($query);    
}

//========================== Test 3 =================================
// In the following case, a partial query is constructed in a loop.
// The loop body is loop-free and all $query is modified through left
// recursive statements. Further, $searchWords[$i] is unmodified in 
// the loop body. This loop body passes the restrictions imposed by 
// TAPS, is summarized as (or kw = '$searchWords[$i]')* and is 
// transformed. 
// expected result TAPS : transforms
test3();        
function test3()
{
    // expected user input searchWord1,searchWord2,searchWord3 
    $search = $_GET['search'];
    $searchWords = explode(",", $search);
    $i = 0;
    $query = "select * from test3 where kw=''";
    
    for($i = 0; $i < $count($searchWords); $i++)
    {
        $query .= " or kw = '";
        $query .= $searchWords[$i];
        $query .= "' ";
    }

    @mysql_query($query);
}

//========================== Test 4 =================================
// The following case presents a loop that does not pass loop 
// restrictions imposed by TAPS and is not transformed.
// expected result TAPS : reject
test4();
function test4()
{
    // expected user input command,searchWord1,searchWord2,searchWord3 
    $search = $_GET['search'];
    $searchWords = explode(",", $search);
    $i = 0;
    $query = "select * from test4 where kw=''";
    
	for($i = 0; $i < $count($searchWords); $i  )
	{
		if($i != 0)		// skip the command
		{
			$query .= "or kw = '";
			$query .= $searchWords[$i];
			$query .= "' ";
		}
	}

    @mysql_query($query);
}



//========================== Test 6 =================================
// The following function has two infeasible paths. These paths 
// generate malformed SQL queries. TAPS ignores these warnings for the 
// case below. As discussed in the paper, such warnings require a 
// developer to qualify them as caused by infeasible paths, and hence 
// ignore them.
// expected result TAPS : transforms with ignored warnings caused by
// infeasible paths.

test6();        
function test6()
{
    $u = $_GET['x'];
    $i = 0; 

    if($i == 0)
    {
        $q = "select ";      // flow1 
    }    
    else
    {
        $q = "delete";      // flow2
    }

    // do some processing
    
    if($i == 0)
    {
        $q = $q. " * from test6 where uid = '$u'";   // unreachable from flow2 
    }
    else
    {
		// unreachable from flow1 : malformed query
        $q = $q. " from test6 where uid = '$u'"; 
    }
    
    @mysql_query($q);
}

//========================== Test 7 =================================
// This test case generates a query with numeric and string data 
// arguments. Also, note that the numeric value $p3 is computed using 
// a numeric user input.  Expanding the symbolic expression of $p3 
// will introduce * operator in the symbolic query thus may 
// potentially alter the SQL query structure (introduce operators of 
// PHP program into the SQL query). TAPS handles this by stopping 
// symbolic expansion at numeric LHS $p3.
// expected result TAPS : transforms
test7();        
function test7()
{
    $u = $_GET['user'];
    $p1 = (int) $_GET['number1'];
    $p2 = $_GET['alternate'];
    
    $p3 = $p1 * 20;
    
    $q = "select * from test7 where u = '$u' ". " and p = $p1 " . " or p = $p3";
    @mysql_query($q);
}

//========================== Test 8 ==================================
// TAPS relies on symbolic evaluation to learn the structure of a SQL 
// query. The following function provides a case where fixed partial 
// queries are stored in an array. This array is indexed with constant 
// indices. Given the availability of array indices statically, TAPS 
// successfully learns the query structures and transform the program. 
// Note that a similar test case 9 that uses dynamic array indices 
// leads to malformed SQL queries. 
// expected result TAPS : transforms
test8();        
function test8()
{
    $queries = array("select * from test8 where uid=", 
        "delete from test8 where uid = ");

    $i = $_GET['user_choice'];
    $u = $_GET['username'];
    switch($i)
    {
        case 0:
            $q = $queries[0]. " '$u' ";
            break;
        case 1:
            $q = $queries[1]. " '$u' ";
            break;
        default:
            return;
    }

    @mysql_query($q);
}

//========================== Test 9 =================================
// Unlike the previous case, in the following program partial queries 
// from the array are selected based on a user supplied input. For 
// such cases a static analysis techniques cannot learn the complete 
// symbolic structure of the query. TAPS does not transform this 
// program.
// expected result TAPS : rejects
test9();        
function test9()
{
    $queries = array("select * from test9 where uid=", 
        "delete from test9 where uid = ");

    $i = $_GET['user_choice'];
    $u = $_GET['username'];
    if($i != 0 || $i != 1)
        return;

    $q = $queries[$i] . " '$u' ";
    @mysql_query($q);
}

//========================== Test 5 =================================
// Queries are constructed in and executed in multiple functions. This 
// case underscores two aspects: TAPS's ability to enumerate flows in 
// an inter-procedural fashion and passing/receiving arguments list 
// to/from functions.
// expected result TAPS : transforms
test5();        
function test5()
{
    $u = $_GET['username'];
    $p = $_GET['password'];
	
	// The following function computes and returns a query to be 
	// executed. The transformed version should also receive any 
	// data arguments for this query.
    $query = test5_computeQuery($u, $p);

    @mysql_query($query);
}

// this should be transformed and should return 
// the arguments mappings of placeholder ?s in the query 
// strings as $u and $p.
function test5_computeQuery($u, $p)
{
    // following invocation simulates input validation/filtering.
    // note that TAPS allows inputs to be processed in arbitrary 
    // ways. $u below appears in a string context in query $q, and 
    // can contain any arbitrary symbolic expression i.e., TAPS 
    // allows arbitrary processing of user inputs and identifies 
    // processed values as arguments to queries. 
    $u = test5_Filter($u);

    $q = "select * from test5 where uid = '$u'"." and password='$p'";

    // the following function receives a partial query.
    // in the transformed version this function must also receive 
    // arguments mapping to the ? in the partial query. This is 
    // simulated by setting a global argument variable for each 
	// formal parameter of the function.
    test5_inter($q);

    // the following statement returns a partial query to invoker.
    // in the transformed version this statement should also return
    // arguments mapping to the ? in the query. This is simulated by
 	// setting a global variable for return value of each function.
    return $q;
}

function test5_inter($q)
{
    $address = $_GET['test5'];
	
	$q .= " and address='$address'";

	mysql_query($q);
}

// this function process user input $u, adds slashes 
// and constant strings to it. However, the resulting value is 
// used in a string data context, and identifies a single data 
// argument. The following function is not transformed by TAPS.
function test5_Filter($u)
{
    $u = addslashes($u);
	$u = " add a single quote '' some more ".$u;
	return $u;
}




//========================== Test 10 ================================
// This test presents robustness in extracting data arguments 
// correctly. A string data context is enclosed by string delimiters. 
// TAPS allows escaped quotes to appear in program added constants 
// and ensures that it does not introduce any // unescaped quotes in 
// symbolic expressions.  
// expected result TAPS : transforms
test10_1();     
function test10_1()
{
    $u = "O'Reilly";
    $u1 = addslashes($u);
    $q = "select * from test10_1 where uid = '$u1'";
    @mysql_query($q);
}

// hard coded data contains escaped quotes - '' 
// expected result TAPS : transforms
test10_2();     
function test10_2()
{
    $u = "O''Reilly";
    $q = "select * from test10_2 where uid = '$u'";
    @mysql_query($q);
}

// dynamic data is escaped e.g., with addslashes
// expected result TAPS : transforms
test10_3();     
function test10_3()
{
    $u = $_GET['username'];
    $u = addslashes($u);
    $u = " $u . some static data ";
    $q = "select * from test10_3 where uid = '$u'";
    @mysql_query($q);
}

//========================== Test 11 ===============================
// This is the running example from the paper. 
// expected result TAPS : transforms
test11(1);       
function test11($i)
{
    $u = $_GET['x'];
    
    $q1 = "select * from test11 where uid LIKE '%";
    $q2 = addslashes($u);
	$q3 = "%' order by Y";
    
    $q = $q1.$q2.$q3;
    @mysql_query($q);
}   

//========================== Test 12 ================================
// a variant of the above case
// expected result TAPS : transforms
test12(1);      
function test12($i)
{
    $u = $_GET['x'];
    
    $q1 = "select * from test12 where uid = '$u' "."or uid = ";
    if($i == 0)
    {
        $q2 = "'%".$u."%'";
    }
    else
    {
        $q2 = "'admin'";
    }

    $q = $q1.$q2;
//    $q = "select * from X where uid=$q2";
    @mysql_query($q);
}

//========================== Test 13  ===============================
// in below example $q2 causes conflict in transformation immediately
// after the if-then statement. In the then branch, it's value 
// contains SQL code, whereas else branch contains purely data. 
// test13_conresolve shows an equivalent code (can be generated by 
// code motion) that resolves this conflict.
// expected result TAPS : rejects
test13_conflict();       // example of conflict
function test13_conflict($i)
{
    $id = $_GET['id'];
    $name = $_GET['name'];
    
    $q1 = "select * from test13_conflict where uid = ";
    if(isset($name))
    {
        $q2 = $id ." or uname = '$name'";
    }
    else
    {
        $q2 = $id;
    }

    $q = $q1.$q2;
    @mysql_query($q);
}

// the primary reason for conflict is the multiple values of 
// $q2 reaching at the computation of $q. In Single Static
// assignment (SSA) format, this fact is captured by a PHI function
// that assigns value (either from the then branch or else branch) 
// to a unique variable which is then used in the computation of $q.   
// In limited cases, code motion can resolve such computation by 
// relocating these computations to contributors of PHI function's. 
// expected result TAPS : transforms
test13_conresolve();    // conflict resolution by code motion
function test13_conresolve($i)
{
    $id = $_GET['id'];
    $name = $_GET['name'];
    
    $q1 = "select * from test13_conresolve where uid = ";
    if(isset($name))
    {
        // ---- in SSA, $q2_then contributed to the PHI function for 
		// $q2.
        // $q2_then = "'$u' or u ='admin'"; 
        $q2 = $id. " or uname ='$name'";

        // ---- $q2 was used in computation of $q, that resulted in 
		// conflict. 
        $q = $q1.$q2;
    }
    else
    {
        // ---- in SSA, $q2_else contributed to the PHI function for 
		// $q2.
        // $q2_else = "'admin'";
        $q2 = "$id";
        
        // code motion for conflict resolution 
        $q = $q1.$q2;
    }
    
// --- in SSA, value of $q2 is explicitly flowing from either the then 
// or the else branch 
//----- $q2 = PHI( $q2_then, $q2_else)

// following line has been relocated to the then and else branches, 
// which were contributing to the PHI assignment of $q2. 
//    $q = $q1.$q2;
  
    @mysql_query($q);
}

//========================== Test 14 ===========================
// in this case an unreachable sql sink is not transformed. 
// expected result TAPS : transforms the reachable sink
test14();
function test14()
{
    $u = $_GET['x'];
    $q = "select * from test14 where uid = '$u'";
    executeQ($q);
    //@mysql_query($q);
}

function test14_unreachable()
{
    $q = "try me not";
    executeQ($q);
}

function executeQ($q)
{
    @mysql_query($q);
}

//========================== Test 15 =================================
// This test case presents a scenario where query processing may be 
// interleaved with other operations e.g., HTML creation. This is 
// encountered in the real applications frequently.
// expected result TAPS : transforms 
test15(100);    
function test15($i)
{
    $a = "something";

    $q = " select * from test15 where ";
    
	// non query processing
    for($j = 0; $j < $i; $j++)
    {
        if($j = 10)
        {
            for($k = 0; $k < 20; $k++)
                $d .= " hello";
        }
    }

	$q .= " uid = $a";

	// non query processing
    for($j = 0; $j < $i; $j++)
    {
        for($k = 0; $k < 20; $k++)
        	$d .= " $j.$k";
    }
    
    $q .= " or uid = '$i'";
    
	@mysql_query($q);
}

//========================== Test 16 ================================
// this case computes a query with multiple loops that pass TAPS loop 
// restrictions
// expected result TAPS : transforms
test16();       
function test16()
{
    $table = "test16";
    $data = array("column1"=>"yy", "column2"=>"zz");
    $where = array("wherecol1" => "data1", "wherecol2" => "data2");
	
    $keys = array_keys($data); 
    $values = array_values($data);
    $qSet = " $keys[0] = '$values[0]' ";
    $count = count($data);
    for($i = 1; $i < $count; $i++)
    {
        $qSet .= ", ";
        $qSet .= $keys[$i];
        $qSet .= " = '";
        $qSet .= $values[$i]; 
        $qSet .= "' ";        
    }    

    $keys = array_keys($where);
    $values = array_values($where);
    $count = count($where);
    $qWhere = " $keys[0] = '$values[0]' "; 
    for($i = 1; $i < $count; $i++)
    {
        $qWhere .= " AND ";
        $qWhere .= $keys[$i];
        $qWhere .= " = '";
        $qWhere .= $values[$i];
        $qWhere .= "' ";
    }
     
    $query = " UPDATE $table SET ". $qSet . " WHERE ". $qWhere;
       
	@mysql_query( $query );
}

//========================== Test 17 ================================
// The following case presents a summarization of implode PHP 
// function. implode function of PHP, appends array members separating 
// them with the a glue string specified in the arguments. In the TAPS 
// evaluation one instance of implode was summarized and successfully 
// transformed. 
// expected result TAPS : transforms
test17();       
function test17()
{
    $table = "test17";
    $data = array("column1"=>"yy", "column2"=>"zz", "column3"=>"xx");
    $data = array("column1"=>"yy");
    newInsert($table, $data);   
}

function newInsert($table, $data)
{
// 		following is the original code with implode functions
//		$fields = array_keys($data);
//		$query = "INSERT INTO $table (`" ; 
//		$query .= implode('`,`',$fields) ;
//		$query .= "`) VALUES ('";
//		$query .= implode("','",$data)."')");
//		mysql_query($query);

	// implode summarization starts here =================
	    $keys = array_keys($data);
	    $values = array_values($data);
	    $qInto = $keys[0]; 
	    $qValues = "'".$values[0]."'";

	    $count = count($data);
        for($i = 1; $i < $count; $i++)
        {
	        $qInto .= " , ";
	        $qInto .= $keys[$i];
	        $qValues .= ", '";
	        $qValues .= $values[$i];
	        $qValues .= "'";
        }    
	// implode summarization ends here ==================
        
        $query = " INSERT INTO $table ( ". $qInto ;
		$query .= " ) VALUES ( ". $qValues . " )";      
        @mysql_query($query);
}

//========================== Test 18 ================================
// Here keyword LIKE is added through multiple statements. TAPS learns 
// the LIKE keyword correctly, as it combines the symbolic expressions 
// to reason about the SQL queries.  
// expected result TAPS : transforms
test18();       
function test18()
{
 	$a = $_GET['user'];
   	$query = "delete from student where name LI";
   	$query = "$query"."KE '%$a%'";
    @mysql_query($query);
}

//========================== Test 19 ================================
// this is the running example from the formal section of the paper 
// with a slight difference. The paper example uses "+" operator. To 
// avoid problems with encodeURI() that treats + appearing in URLs 
// specially (implemented with ajax) following example uses numeric 
// operator "*".
// expected result : transformation succeeds
test19();      
function test19()
{
    $q = "select * from employee where salary = ";
    $x1 = $_GET['salary'];
    $x2 = $_GET['bonus'];
	$x = $x1 . " * ";
	$x = $x . $x2;
	$q = $q . $x;
    @mysql_query($q);
}

/* ---------------- helper methods -------- */
// Prior to PS transformation a really useful step could be converting
// all mysql_ based calls to mysqli_ interface.
// Tools exist to perform such a transformation automatically 
// http://forge.mysql.com/wiki/Converting_to_MySQLi
// note that the above transformation does not convert to prepared statement. 

// generate a PREPARE statement, bind data arguments, and execute
function executePS($ps, $psArgs){
    global $mysqli;
    $stmt = $mysqlih->prepare($ps);
    if( $stmt ) {
        if(!empty($psArgs) && count($psArgs)){
            $types = str_repeat("s", count($psArgs));
            $args[0] = &$stmt; 
            $args[1] = $types;
            if(is_array($psArgs))
                foreach( $psArgs as $arg)
                    $args[] = $arg;
            else
                $args[] = $psArgs;
            call_user_func_array('mysqli_stmt_bind_param', $args);
        }
     
        $result = $stmt->execute();
        if($stmt -> num_rows > 0) //for result set returning queries
            $result = $stmt->result_metadata();
    } else {
        //$error_str = " SISL : Error code - "$mysqli->error." PS creation failed  for \n \t \t $ps, 
        //with arguments - $psArgs "; 
        //@error_log($error_str, 0);
    }
    return $result;
}

// merge query arguments lists 
function merge_helper()
{
    $retArray = array();
    $args = func_get_args();        // get all args
    $iElem = count($args);
    for($i = 0; $i < $iElem; $i++)
    {
        $ar = $args[$i];
        // if arg is yet not set, dont use "" value
        if(!isset($ar)) continue;   
        
        // if arg is an array, use array_merge 
        // otherwise simply append
        if(is_array($ar))           
            $retArray = array_merge( $retArray, $ar);
        else
            $retArray[] = $ar;      
        }
    return $retArray;
}

// initialize the object to generate PREPARE statements
function initPSConn()
{
    //  mysqli provides the prepared statement
    // no procedural equivalent like mysql_query();
   $mysqlih = new mysqli('localhost', 'username', 'password', 'db');
   if(mysqli_connect_errno()) {
      echo "Connection Failed: " . mysqli_connect_error();
      exit();
   }
}

// cleanup
function destroyPSConn()
{
   $mysqlih -> close();   
}

?>

