package edu.uic.rites.sisl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.concurrent.ConcurrentHashMap;
//import java.util.concurrent.CopyOnWriteArrayList;

import at.ac.tuwien.infosys.www.pixy.Dumper;
import at.ac.tuwien.infosys.www.pixy.MyOptions;
import at.ac.tuwien.infosys.www.pixy.analysis.dep.DepGraph;
import at.ac.tuwien.infosys.www.pixy.conversion.Cfg;
import at.ac.tuwien.infosys.www.pixy.conversion.CfgEdge;
import at.ac.tuwien.infosys.www.pixy.conversion.TacFormalParam;
import at.ac.tuwien.infosys.www.pixy.conversion.TacFunction;
import at.ac.tuwien.infosys.www.pixy.conversion.TacPlace;
import at.ac.tuwien.infosys.www.pixy.conversion.Variable;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNode;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeAssignArray;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeAssignBinary;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeAssignRef;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeAssignSimple;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeAssignUnary;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeBasicBlock;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeCallBuiltin;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeCallRet;

public class SSATransformer {

	/*
	 * Given a depGraph
	 * 	a. find out functions that contain statements of this depgraph
	 *  b. for each such function apply SSA transformation algorithm by Cryton et al. 1991 ACMTOPLAS
	 *  -- more references to understand it better in the method 
	 */
	public static void transform(DepGraph depGraph) 
    {
		// get the functions traversed by the dependency graph
		ArrayList<CfgNode> allDepNodes = DepGraph.getAllCfgNodes(depGraph.getNodes(), true);
		ArrayList<Cfg> allFns = DepGraph.getFunctions(allDepNodes);
	
		
		// collect dependency graph nodes present in each control flow graph
		LinkedHashMap<Cfg, ArrayList<CfgNode>> 
			allFnDepen = new LinkedHashMap<Cfg, ArrayList<CfgNode>>();
		
		for(Cfg cfg : allFns)
		{
			ArrayList<CfgNode> depNodes = DepGraph.getNodesInDepd(cfg, allDepNodes);
			allFnDepen.put(cfg, depNodes);
		}
	
//		for(Map.Entry<Cfg, CopyOnWriteArrayList<CfgNode>> entry : allFnDepen.entrySet())
//		{
//			
//	    	Cfg cfg = entry.getKey();
//	    	String fName = Cfg.getFunction(cfg.getHead()).getName();
//	    	if(cfg.isSSATransformed())
//	    	{
//		    	D.pv(D.D_TEMP, " Skipping SSA transformation for function - " + fName);
//	    	}
//	    	else	
//	    	{
//	    		D.pv(D.D_TEMP, " doing SSA transformation for function - " + fName);
//	    		cfg.setSSATransformed();
//	    	}
//		}
		
    	// phi analysis must be done for each variable
    	// we follow the algorithm by Cryton et al. 1991 ACMTOPLAS
    	// 1. Compute Dominance Frontiers from dominator tree. 
    	// 2. insert phi functions 
    	// 3. rename the variables
		//
		// pseudo code from 
		// http://llvm.cs.uiuc.edu/~vadve/CS526/public_html/Notes-Spring2006/
		// http://www.cs.colostate.edu/~mstrout/CS553Fall06/slides/lecture17-SSA.pdf
		// (example and diagrams in second set of slides are particularly helpful in 
		//  understanding)

		// Let the Cfg enable faster versions of post and pre order
		Cfg.bEnableBuffering = true;

		for(Map.Entry<Cfg, ArrayList<CfgNode>> entry : allFnDepen.entrySet())
		{
	    	Cfg cfg = entry.getKey();
	    	transformSSACfg(cfg);
		}	
		Cfg.bEnableBuffering = false;
    }

	/*
	 *  the following method implements algorithm given by Cryton et al. 1991 ACMTOPLAS.
	 *  Steps: 
    	// 1. Compute Dominance Frontiers from dominator tree. 
    	// 2. insert phi functions 
    	// 3. rename the variables
		// pseudo code from 
		// http://llvm.cs.uiuc.edu/~vadve/CS526/public_html/Notes-Spring2006/
		// http://www.cs.colostate.edu/~mstrout/CS553Fall06/slides/lecture17-SSA.pdf
		// (example and diagrams in second set of slides are particularly helpful in 
		//  understanding)
	 */
    public static void transformSSACfg(Cfg cfg)
    {
    	ConcurrentHashMap<Variable, ArrayList<CfgNode>> iteratedDomF = null;

    	String fName = Cfg.getFunction(cfg.getHead()).getName();
    	if(cfg.isSSATransformed())
    	{
	    	D.pv(D.D_TEMP, " Skipping SSA transformation for function - " + fName);
	    	return;
    	}
    	else	
    	{
    		D.pr(" doing SSA transformation for function - " + fName);
    		cfg.setSSATransformed();
    	}

    	D.pr(" Computing dominator tree....");
    	enterDummyParamNodes(cfg);
    	
    	computeDominatorTree(cfg);
    	D.pr("....DONE....Computing Dominance frontiers....");
    	
    	computeDominanceFrontiers(cfg);
    	D.pr("....DONE....Computing Iterated Dominance frontiers....");    		

    	iteratedDomF = computeIteratedDominanceFrontiers(cfg, cfg.getAll());
    	D.pr("....DONE....Renaming variables now.... iteratedDomF size - " + 
    			iteratedDomF.size());

    	renamePhiVars(cfg.getHead(), iteratedDomF);
    	D.pv(D.D_TEMP, "....DONE....Removing empty Phi nodes now....");
    	
    	removeEmptyPhiEntries(cfg);
    	
    	TacFunction tf = cfg.getFunction(cfg.getHead());
    	String strFName = MyOptions.graphPath;
    	// dump normal dot cfg
	      Dumper.dumpSSABegin();
	      Dumper.dumpDot(tf,  strFName, true);
	      Dumper.dumpSSAEnd();
            
        // dump SSA dot cfg
//        Dumper.dumpSSABegin();
//        Dumper.dumpDot(tf, strFName, true);
//        Dumper.dumpSSAEnd();
    }

	private static void removeEmptyPhiEntries(Cfg cfg) {

		for(CfgNode  cn : cfg.getAll())
		{
			LinkedHashMap <Variable, ArrayList<Variable>> phis = cn.getPhiNodes();
			if(phis == null)
				continue; 

			ArrayList<Variable> toAdjust = new ArrayList<Variable>();
			for(Map.Entry<Variable, ArrayList<Variable>> en : phis.entrySet())
			{
				Variable phiLhs = en.getKey();
				ArrayList<Variable> phiRhs = en.getValue();
				if(phiRhs != null && phiRhs.size() != 0 && phiRhs.contains(phiLhs))
				{
					toAdjust.add(phiLhs);
					D.pv(D.D_TEMP, " found a phi node " +
							"with repeated lhs ----- " + phiLhs + " rhs -" + phiRhs);
				}
			}
			
			for(Variable v : toAdjust)
			{
				phis.get(v).remove(v);
			}
			
			ArrayList<Variable> toRemove = new ArrayList<Variable>();
			for(Map.Entry<Variable, ArrayList<Variable>> en : phis.entrySet())
			{
				Variable phiLhs = en.getKey();
				ArrayList<Variable> phiRhs = en.getValue();
				if(phiRhs == null || phiRhs.size() == 0)
				{
					D.pv(D.D_TEMP, " found an empty phi node with empty rhs ----- " + phiLhs + " rhs -" + phiRhs);
					toRemove.add(phiLhs);
				}
			}
			
			for(Variable v  : toRemove)
			{
				phis.remove(v);
			}
		}            	
	}

	public static void renamePhiVars(
    	CfgNode entry,
    	ConcurrentHashMap<Variable, ArrayList<CfgNode>> iteratedDomF)
	{	    
    	ArrayList<Variable> transVars = new ArrayList<Variable>();
    	
    	// variable is original, and Stack<Variable> contains currently 
    	// visible variable value 
    	LinkedHashMap<Variable, Stack<Variable>> 
    		stacks = new LinkedHashMap<Variable, Stack<Variable>>();
    	
    	LinkedHashMap<Variable, Integer> 
			indices = new LinkedHashMap<Variable, Integer>();
    	
    	// initialization algo
//    	Stacks - an array of stacks, one for each original variable V
//    	Stacks[V] = subscript of most recent definition of V. Initially, Stacks[V] = EmptyStack, ∀ V
//    	Counters - an array of counters, one for each original variable
//    	Counters[V] = number of assignments to V processed, Initially. Counters[V] = 0, ∀ V
        for (Map.Entry<Variable, ArrayList<CfgNode>> dfi : iteratedDomF.entrySet()) 
        {
        	Variable v = dfi.getKey();
 //       	if(dfi.getValue().isEmpty())
 //       		continue; 
        	
        	stacks.put(v, new Stack<Variable>());
        	indices.put(v, 0);
        	transVars.add(v);     
        	D.pd(D.D_TEMP, "######### vars to be transformed - " + v.getName());
        }

        
        // remember who contributed unique SSA LHS 
        // so that we can link phi nodes to the values reaching at them
        LinkedHashMap<Variable, CfgNode> vToCfn = new LinkedHashMap<Variable, CfgNode>();
        
        // for each entry point call the recurseRename
    	recurseRename(stacks, indices, entry, transVars, vToCfn);        
        return;
	}

    public static void recurseRename(
    	LinkedHashMap<Variable, Stack<Variable>> stacks, 
    	LinkedHashMap<Variable, Integer> indices, 
    	CfgNode block,
    	ArrayList<Variable> transVars,
    	LinkedHashMap<Variable, CfgNode> vToCfn)  
    {
    	// algorithm: W
		//  if b previously visited return
		//  for each φ-function p in b
		// 	 GenName(LHS(p)) and replace v with vi, where i=Top(Stack[v])
		//  for each statement s in b (in order)
		//  	for each variable v ∈ RHS(s)
		//  		replace v by vi, where i = Top(Stacks[v])
		//  	for each variable v ∈ LHS(s)
		//  		GenName(v) and replace v with vi, where i=Top(Stack[v])
		//  
		//  for each s ∈ succ(b) (in CFG)
		//  	j ← position in s’s φ-function corresponding to block b
		//  	for each φ-function p in s
		//  		replace the jth operand of RHS(p) by vi, where i = Top(Stack[v])
		//  for each s ∈ child(b) (in DT)
		//  	Rename(s)
		//  for each φ-function or statement t in b
		//  	for each vi ∈ LHS(t)
		//  		Pop(Stack[v])        
    	
    	if(block.isRenamed())
    		return;
    	else
    		block.setRenamed();
    	
    	D.pf(D.D_TEMP, ": Recurse : " + block.toString());
    	// remember stacks where we pushed; pop later
    	ArrayList<Variable> pushedStacks = new ArrayList<Variable>();
    	
    	renamePhiLHSInBlock(block, stacks, indices, transVars, pushedStacks, vToCfn);

        if (block instanceof CfgNodeBasicBlock) 
        {
    	    CfgNodeBasicBlock bb = (CfgNodeBasicBlock) block;
    	    for (CfgNode cnIb : bb.getContainedNodes())
    	    {
    	    	D.pf(D.D_SSA, "......RENAMING......1." + cnIb.toString());
    	    	renameVarsUsedInStmt(cnIb, stacks, indices, transVars);
    	    	genNameLHSStmt(cnIb, stacks, indices, transVars, pushedStacks, vToCfn);
    	    }
        }
        else
        {
	    	D.pf(D.D_SSA, "......RENAMING......." + block.toString());
	    	renameVarsUsedInStmt(block, stacks, indices, transVars);
	    	genNameLHSStmt(block, stacks, indices, transVars, pushedStacks, vToCfn);
        }
        
        for(CfgNode cfgSuccs : block.getSuccessorsNoBlocks())
        {	
        	renamePhiRHSInSuccBlocks(block, cfgSuccs, stacks, indices, transVars, vToCfn);
        }
        
        for(CfgNode domSuccs : block.getImdDominated())
        {
        	recurseRename(stacks, indices, domSuccs, transVars, vToCfn);
        }

        // now remove the stack entries for Phi and assign stmt
        for(Variable pushed : pushedStacks)
        {
        	D.pv(D.D_SSA, "Popping stack for - " + pushed.getName());
        	stacks.get(pushed).pop();
        }
	}

    public static void
    renamePhiRHSInSuccBlocks(
    	CfgNode blockCurrent,
		CfgNode blockEntrySucc, 
		LinkedHashMap<Variable, Stack<Variable>> stacks, 
		LinkedHashMap<Variable, Integer> indices, 
		ArrayList<Variable> transVars,
		LinkedHashMap<Variable, CfgNode> vToCfn)
    {
    	LinkedHashMap<Variable, 
    		// LinkedHashMap<CfgNode, Variable>> phis = null;
    		ArrayList<Variable>> phis = null;
    	
    	LinkedHashMap<Variable, CfgNode> contributedBy = null;
    	
    	while(true)
    	{
	    	phis = blockEntrySucc.getPhiNodes();
	    	contributedBy = blockEntrySucc.getPhiContributors();
	    	if(phis != null)
	    	{	    	
		    	D.pi(D.D_SSA, "Deciding args for phi functions at node - " + blockEntrySucc.toString());
		        for(Map.Entry<Variable, 
		        		// LinkedHashMap<CfgNode, Variable>>
		        		ArrayList<Variable>>
		        	phi : phis.entrySet()) 
		        {
		        	Variable v = phi.getKey();
		        	Variable vOrig = v.getOrig();
		        	if(vOrig != null)
		        		v = vOrig; 
		        	
		        	//LinkedHashMap<CfgNode, Variable> phiArgs = phi.getValue();
		        	Integer index = null;
		        	Stack<Variable> st = stacks.get(v); 
		        	if( st != null && !st.isEmpty())
		        	{
		        		// index = stacks.get(v).peek();
		        		Variable vNew = st.peek();  
		        		D.pi(D.D_SSA, "Phi func should have " + vNew.getName() + " arg");
		        		ArrayList<Variable> phiArgs = phi.getValue();
		        		if(!phiArgs.contains(vNew))
		        		{
		        			phiArgs.add(vNew);
		        			
		        			// remember who contributed to the phi definition
		        			CfgNode by = vToCfn.get(vNew);
		        			blockEntrySucc.addPhiContributors(vNew, by);
		        			
		        			// also mark the contributor, where it contributed
		        			// these are used in transforming loops later on
		        			by.addContributedTo(vNew, blockEntrySucc);
			        		D.pi(D.D_SSA, " contributed by " + by.toString());
		        		}
		        	}
		        }
	    	}
	    	
	        List<CfgNode> succs = blockEntrySucc.getSuccessors();
	        if(succs.size() != 1)
	        	break;
	        
	        blockEntrySucc = succs.get(0);
    	}
    }
    
    public static void
    renamePhiLHSInBlock(
		CfgNode blockEntry, 
		LinkedHashMap<Variable, Stack<Variable>> stacks, 
		LinkedHashMap<Variable, Integer> indices, 
		ArrayList<Variable> transVars,
		ArrayList<Variable> pushedStacks,
		LinkedHashMap<Variable, CfgNode> vToCfn)
    {
    	LinkedHashMap<Variable, ArrayList<Variable>> 
    		phis = blockEntry.getPhiNodes();

    	if(phis == null)
    		return;

    	LinkedHashMap<Variable, ArrayList<Variable>> 
			newPhis = new LinkedHashMap<Variable, ArrayList<Variable>>();

    	for (Map.Entry<Variable, ArrayList<Variable>> 
        	phi : phis.entrySet()) 
        {
        	Variable v = phi.getKey();
        	Integer counter = indices.get(v);

			String name = "";
			if(v.isArrayElement())
				name = Variable.makeSSAName(v, "_SSA", counter);
			else
				name = v.getName() + "_SSA" + counter;

//	    	//String name = v.getName() + "_SSA" + counter;
//        	String name = Variable.makeSSAName(v, "_SSA", counter);

        	Variable vDup = v.dup(name);
			vDup.setOrig(v);

			newPhis.put(vDup, phi.getValue());
			stacks.get(v).push(vDup);
			vToCfn.put(vDup, blockEntry);
			
	    	counter++;

			D.pv(D.D_SSA, "\t \t LHS - pushed - true || variable " + v + " renamed to " + vDup.getName() + 
					" in Phi for " + blockEntry.toString());
			indices.put(v, counter);
			pushedStacks.add(v);
        }
        
        blockEntry.setPhiNodes(newPhis);    	
    }
    
    public static void
    	genNameLHSStmt(
			CfgNode cfn, 
			LinkedHashMap<Variable, Stack<Variable>> stacks, 
			LinkedHashMap<Variable, Integer> indices, 
			ArrayList<Variable> transVars,
			ArrayList<Variable> pushedStacks,
			LinkedHashMap<Variable, CfgNode> varToCfg)
    {
		Variable v = cfn.getLeft();
		
		// Do not rename the return values
///		if(v.getName().startsWith("_superglobals.ret"))
	//		return;
		
		
		// find if the var is to be renamed
		if(transVars.contains(v))
		{
			Integer counter = indices.get(v);
			String name = "";

			// check if renamed var is return variable for this function
			// ----> NO: if so, we need to change the return var in Cfg
			TacFunction tf = cfn.getEnclosingFunction();
			D.pv(D.D_SSA, " tacfunction - " + tf.getName() + " returns - " + tf.getRetVar().getName());
			if(tf.getRetVar().equals(v))
			{
				D.pr(D.D_SSA, " There can be only one return nam from functions, do not make SSA copies!!!!!!!!!!!!!!!!!!!!");
//				tf.setRetVar(vDup);
				return;
			}

			if(v.isArrayElement())
				name = Variable.makeSSAName(v, "_SSA", counter);
			else
				name = v.getName() + "_SSA" + counter;
	    	//String name = Variable.makeSSAName(v, "_SSA" , counter);

			Variable vDup = v.dup(name);
			vDup.setOrig(v);
			cfn.replaceVariable(0, vDup);

			//			cfn.setLeft(vDup);
			
			stacks.get(v).push(vDup);
			varToCfg.put(vDup, cfn);
	    	counter++;

			D.pv(D.D_SSA,"\t \t LHS - pushed - true || variable " + v + " renamed to " + vDup.getName() + 
					" in " + cfn.toString());
			indices.put(v, counter);
			pushedStacks.add(v);
		}    	
		
    }
    
    
	public  static void 
		renameVarsUsedInStmt(
			CfgNode cfn, 
			LinkedHashMap<Variable, Stack<Variable>> stacks, 
			LinkedHashMap<Variable, Integer> indices, 
			ArrayList<Variable> transVars)
	{				
		ArrayList<Variable> vAl = cfn.getRightToRename();
		if(vAl == null) 
			return;
		
		for(Variable v : vAl)
		{
			// find if the var is to be renamed
			if(transVars.contains(v))
			{
				// check if the stack is empty
				// possible when functions assign to return variables
				// as there is no explicit assignment statement, there
				// may not be any explicit definition of the temporary
				Stack<Variable> st = stacks.get(v);
				if(st.isEmpty())
					continue;
				
				Variable ssaVar = st.peek();
				cfn.rightRename(v, ssaVar, true);
				ssaVar.setOrig(v);
				// cfn.addSSAVar(ssaVar, v);
				D.pv(D.D_SSA, "\t\t variable " + v + " renamed to " + ssaVar.getName() + 
						" in " + cfn.toString());
			}
		}
	}
    
//    procedure GenName(Variable V)
//    1. i ← Counters[V]
//    2. replace V by Vi
//    3. push i onto Stacks[V]
//    4. Counters[V] ← i + 1
    public static String renVar(Variable v, Stack<Variable>  stack, Integer counter)
    {
    	String name = v.getName() + "_SSA" + counter;

		Variable vDup = v.dup(name);
		vDup.setOrig(v);
    	// v.setSSAName(name);
//    	d("before pushing counter for variable - " + v.getName() + " = " + counter);
    	stack.push(vDup);
    	counter++;
//    	d("after pushing counter for variable - " + v.getName() + " = " + counter);    	
       	return name; 
    }
    
    
    // insert phi functions 
	public static void insertPhiFunctions(
			ConcurrentHashMap<Variable, ArrayList<CfgNode>> iteratedDomF)
	{	    
        for (Map.Entry<Variable, ArrayList<CfgNode>> dfi : iteratedDomF.entrySet()) 
        {
        	ArrayList<CfgNode> phiNodes = dfi.getValue();
        	Variable v = dfi.getKey();
        	for(CfgNode cphi : phiNodes) 
        	{
        		D.pi(D.D_SSA, "Add phi for variable " + v.getName() + 
        			" at cfg node - " + cphi.toString());        		
        	}      	
        }
        
        return;
	}

    // iterate the cfg nodes present in the toIterate list 
    // with DominanceFrontiers computed for cfg nodes. 
    public static ConcurrentHashMap<Variable, ArrayList<CfgNode>> 
    	computeIteratedDominanceFrontiers(
    		Cfg cfg, ArrayList<CfgNode> toIterate)
    {
    	boolean bChanged = true; 
    	ArrayList<CfgNode> init = null;
    	
    	// algorithm : 
    	// Dominance frontiers for a set of nodes
    	// Extend the dominance frontier mapping from nodes to sets of nodes:
    	//	DF(L) = Union ( DF(X) ) for each X in L
    	//	The iterated dominance frontier DF+(L) is the limit of the sequence:
    	//	DF1 = DF(L)
    	//	DFi+1 = DF(L U DFi)
    	
    	// get all cfgNodes that assign to same variable in this
    	// control flow graph
    	// set L
    	
    	//
        ConcurrentHashMap<ArrayList<CfgNode>, Variable> assignedVars = 
        	getAllAssignedVars(toIterate);

        // store iterated dominance frontiers
        ConcurrentHashMap<Variable, ArrayList<CfgNode>> iteratedDomF = 
        	new ConcurrentHashMap<Variable, ArrayList<CfgNode>>();
        
        for (Map.Entry<ArrayList<CfgNode>, Variable> dfi : assignedVars.entrySet()) 
        {
        	bChanged = true;
//        	d("Computing iterated dominance frontier for " + 
//        			dfi.getValue().getName());
        	init = dfi.getKey();

        	ArrayList<CfgNode> iterationI = computeDominanceFrontier(init, cfg);
        	
        	ArrayList<CfgNode> iterationT = 
        		new ArrayList<CfgNode>(init);
        	
        	iterationT.addAll(iterationI);
        	
        	ArrayList<CfgNode> iterationNext = null;
        	while ( bChanged )
        	{
        		iterationNext = computeDominanceFrontier(iterationT, cfg);
        		if(iterationI.containsAll(iterationNext) && 
        				iterationNext.containsAll(iterationI))
        		{
//        			d("no new addition");
        			bChanged = false; 
        			continue;
        		}    		
        		else
        		{
        			for(CfgNode  cnt : iterationNext)
        			{
        				if(!iterationT.contains(cnt))
        					iterationT.add(cnt);
        			}
        			
        			iterationI = iterationNext;
        		}
        		
//            	for(CfgNode cf : iterationNext)
//            	{
//            		d("$$$$$ Iteration1 found ....." + cf.toString());
//            	}
        		// START HERE
        	}
        	
        	iteratedDomF.put(dfi.getValue(), iterationI);
//        	for(CfgNode cf : init)
//        	{
//        		d("######### ....predecessors ....." + cf.toString());
//        	}

        	for(CfgNode cPhiInsert : iterationI)
        	{
        		ArrayList<Variable> phiArgs = new ArrayList<Variable>();
    		
//	    		ArrayList<CfgNode> preds = new ArrayList<CfgNode>();
//	    		preds.addAll(init);
//	    		phiArgs.put(preds, new ArrayList<Variable>());
        		//phiArgs.(new ArrayList<Variable>());
        		
        		// temporaries do not need Phi...
        		// for an ideal SSA transformation, disable this check
        		// for temporary variables, however, it bloats the SSA IR
        		// size
        		Variable vPhi = dfi.getValue();
        		if(!vPhi.isTemp())
        			cPhiInsert.addPhiNode(vPhi, phiArgs);
        	}
        	
//        	for(CfgNode cf : iterationI)
//        	{
//        		d("######### Iterated frontier ....." + cf.toString());
//        	}
//        	d(".....................>>>>Done");
        }
        
    	D.pv(D.D_SSA, "Size of iterated domF = " + iteratedDomF.size());
    	return iteratedDomF;
    }
    
	public static void computeDominanceFrontiers(Cfg cfg)
	{
		// DF(x) need to be computed for each node in a bottom-up fashion
		// perform BFS on dominator tree and reverse the nodes to get the bottom up order. 
		D.pv(D.D_SSA, " Function - " + cfg.getFunction(cfg.getHead()).getName() + 
			" BFS order for - dominator tree ");
		
		for(CfgNode cfn : cfg.dominatorsBF())
		{
			//System.out.println("\t\t " + cfn.toString());
			ArrayList<CfgNode> cn = 
				new ArrayList<CfgNode>();
			cn.add(cfn);
			cfn.setDominanceFrontier(computeDominanceFrontier(cn, cfg));
		}		
	}
	
	public static ArrayList<CfgNode>
		computeDominanceFrontier(ArrayList<CfgNode> cfnSet, Cfg inCfg)
	{
		// algorithm 
		// X is a set of nodes. and accordingly succ(X) is the set of successors
		// and idom(X) is a set of immediately dominated nodes.
		// this common algorithm is used for DF(single node) as well as to iterate
		// the DFs. 
		//
		// DF(X) ← ∅
		//
		// for each Y ∈ succ(X) /* local */
		// if idom(Y) not X then
		// DF(X) ← DF(X) UNION {Y}
		//
		// for each Z ∈ children(X) /* up */
		// for each Y ∈ DF(Z)
		// if idom(Y) not X then
		// DF(X) ← DF(X) UNION {Y}
		
		ArrayList<CfgNode> domF = new ArrayList<CfgNode>();

		ArrayList<CfgNode> cfnSetIDoms = new ArrayList<CfgNode>();
		ArrayList<CfgNode> cfnSetSuccs = new ArrayList<CfgNode>();

		// get all nodes immediately dominated by set X
		// get all nodes successeors of nodes in set X
		D.pd(D.D_SSA, "Computing DF for - ");

		for(CfgNode cnMember : cfnSet)
		{
			for(CfgNode cnt : cnMember.getImdDominated())
				if(!cfnSetIDoms.contains(cnt))
					cfnSetIDoms.add(cnt);
			
			for(CfgNode cnt : cnMember.getSuccessorsNoBlocks())
				if(!cfnSetSuccs.contains(cnt))
					cfnSetSuccs.add(cnt);

		}
		
		for(CfgNode succ : cfnSetSuccs)
		{
			if(!cfnSetIDoms.contains(succ))
			{
				//D.pd(D.D_SSA, "......\t\t added succ - " + succ.toString());
				if(!domF.contains(succ))
					domF.add(succ);
			}
			else
			{
				//d("$$$$ ignored succ - " + succ.toString());
			}
		}
		
		for(CfgNode child : cfnSetIDoms)
		{
			for(CfgNode childDominated : child.getDominanceFrontier())
			{
				if(!cfnSetIDoms.contains(childDominated))
				{
					//D.pd(D.D_SSA, "..\t\t added from child DF - " + childDominated.toString());
					if(!domF.contains(childDominated))
						domF.add(childDominated);
					//System.out.println("........");
				}
				else
				{	
					//d("$$$$ ignored from child DF - " + childDominated.toString());
				}
			}
		}
		
		return domF;
	}
	
	/*
	 * @param cfg: for formal parameters add dummy cfg nodes
	 * 	formal = formal; 
	 *  these are used in derivation tree construction -- replace rhs formal with the argument
	 *  being passed to the function call.
	 */
    private static void enterDummyParamNodes(Cfg cfg) {

    	CfgNode head = cfg.getHead();

    	TacFunction tf = cfg.getFunction(head);
    	List<TacFormalParam> formals = tf.getParams();
    	if(formals == null || formals.size() == 0)
    		return; 
//    	CfgNode succ = head.getSuccessorsNoBlocks().get(0);
    	
//    	D.pv(D.D_TEMP, "Preds - " + succ.getPredecessors());
//    	D.pv(D.D_TEMP, " succ of head - " + head.getSuccessors());
//    	D.pv(D.D_TEMP, " succssors = " + succ.getSuccessors());
//    	D.pv(D.D_TEMP, " successors no blocks - " + succ.getSuccessorsNoBlocks());
    	
    	List<CfgNode> headSuccs = head.getSuccessors();
    	if(headSuccs.size() > 1)
    		throw new Error(" SSA Analysis - unhandled case of multiple successors " +
    				"for the entry node -" + headSuccs);
    	
    	CfgNode headSucc = headSuccs.get(0);
    	boolean bHeadBlock = true;
    	if(! (headSucc instanceof CfgNodeBasicBlock)) 
    	{
    		//throw new Error(" SSA Analysis - unhandled case - head successor is" +
    		//		" not a basic block - " +  headSuccs);
    		bHeadBlock = false;
    	}
	
    	if(bHeadBlock)
    	{
	    	CfgNodeBasicBlock headBlock = (CfgNodeBasicBlock) headSucc;
	    	CfgNode prevFirst = headBlock.getContainedNodes().get(0);
	    	// now enter dummy nodes to enable correct SSA transformation

	    	for(TacFormalParam formal : formals){
				TacPlace rightTmp = null;
				Variable vLeft = formal.getVariable();
				D.pv(D.D_TEMP, " ################## formal - " + vLeft);
	
				CfgNodeAssignSimple cnau = 
					new CfgNodeAssignSimple(vLeft, vLeft, headSucc.getParseNode());
				cnau.setAlwaysProcess();
				CfgEdge edge = new CfgEdge ( cnau, prevFirst );
				prevFirst.addInEdge(edge);
				cnau.addOutEdge(0, edge);
				cnau.setEnclosingFunction(tf);
			
				headBlock.addNodeAt(0, cnau);
				
				prevFirst = cnau;
			}
    	}
    	else
    	{
    		// make a basic block for all the formal params 
    		// and then 
    		// (a) set its successor to prev node
    		// (b) set prev nodes predecessor to it. 

    		CfgNodeBasicBlock headBlock = null;
	    	// now enter dummy nodes to enable correct SSA transformation
	    	
    		// ParseNode pn = headSucc.getParseNode();
    		CfgNode prevNode = null;
	    	for(TacFormalParam formal : formals){

	    		Variable vLeft = formal.getVariable();
				CfgNodeAssignSimple cnau = 
					new CfgNodeAssignSimple(vLeft, vLeft, headSucc.getParseNode());
				cnau.setAlwaysProcess();
				
				if(prevNode != null)
				{
					CfgEdge edge = new CfgEdge ( cnau, prevNode );
					prevNode.addInEdge(edge);
					cnau.addOutEdge(0, edge);
					headBlock.addNodeAt(0, cnau);
				}
				
				prevNode = cnau;
				cnau.setEnclosingFunction(tf);
				
				if(headBlock == null)
				{
					headBlock = new CfgNodeBasicBlock(cnau);
					cnau.setEnclosingBasicBlock(headBlock);
				
					List<CfgEdge> prevHeadSuccIns = headSucc.getInEdges();
					CfgEdge[] prevHeadOuts = head.getOutEdges();
					
					if(prevHeadSuccIns.size() != 1)
						throw new Error(" SSA unhandled for multiple in edges " +
								"to head successor - " + headSucc);
					
					if(prevHeadOuts[0] != null && prevHeadOuts[1] != null )
						throw new Error(" SSA unhandled for multiple outs from " +
								"the entry node - " + prevHeadOuts);
					
					CfgEdge prevIn = prevHeadSuccIns.get(0);
					CfgEdge headOut = prevHeadOuts[0];
//					CfgNode cnPrevSucc = headSucc.getOutEdges();
					
					CfgEdge newIn = new CfgEdge(prevIn.getSource(), headBlock);
					CfgEdge newToSucc = new CfgEdge(headBlock, prevIn.getDest());
					CfgEdge newHeadOut = new CfgEdge(head, headBlock);
					
					head.addOutEdge(0, newHeadOut);
					
					headBlock.addInEdge(newIn);
					headBlock.addOutEdge(0, newToSucc);
					
					headSucc.removeInEdge(prevIn.getSource());
					headSucc.addInEdge(newToSucc);
					
					
					headBlock.setEnclosingFunction(tf);
				}
	    	}
	    	
			headBlock.informEnclosedNodes();
    	}


    }
	
    // adds immediate dominates links between nodes.
    // cfg contains control flow graph as well as dominator tree
    // follow appropriate links in cfg nodes.
    public static void computeDominatorTree(Cfg cfg)
    {
    	LinkedList<CfgNode> dfPreOrder = cfg.dfPreOrder();
    	
    	D.pr(".............................computing dominates.....");
    	// algorithm {Aho et al.} 
    	// dominates(X) = {all nodes - nodeX - reachableWithoutX} 
    	// compute set of nodes dominated by each node first
    	int iTotal = dfPreOrder.size();
    	D.pr(D.D_TEMP, "........total nodes in preorder -  " + iTotal);
		for(CfgNode c : dfPreOrder)
		{
			if(--iTotal%500 == 0)
				D.pr("\t\t ......remaining.............." + iTotal);
            if (c instanceof CfgNodeBasicBlock) 
            {
        	    CfgNodeBasicBlock bb = (CfgNodeBasicBlock) c;
        	    for (CfgNode cnIb : bb.getContainedNodes())        	    
        	    	computeDominates(cnIb, cfg);
            }
            else
            {
            	// compute nodes that c dominates and set in the object
            	computeDominates(c, cfg);
            }
		}

		
		D.pr(".............................computing immediately dominates.....");
		iTotal = dfPreOrder.size();
    	D.pr(D.D_TEMP, "........total nodes in preorder -  " + iTotal);
		
		// now compute nodes that X immediately dominates
		// algorithm 
		// immD(X) = { dominates(X) - {Union of nodes that dominates(X) dominate}}
    	for (CfgNode c : dfPreOrder) 
    	{
    		if(--iTotal % 100 == 0)
    			D.pr("\t\t ......remaining.............." + iTotal);

    		ArrayList<CfgNode> imm = null;
            if (c instanceof CfgNodeBasicBlock) 
            {
        	    CfgNodeBasicBlock bb = (CfgNodeBasicBlock) c;
        	    D.pr(".........number of nodes in BB " + bb.getContainedNodes().size());
        	    for (CfgNode cnIb : bb.getContainedNodes())
        	    {
        	    	imm = getImdDominated(cnIb);
        	    	cnIb.setImdDominated(imm);
        	    	//D.pi(D.D_SSA, "node - " + cnIb.toString());
    	    		//for(CfgNode cni : imm)
    	    		//	D.pi(D.D_SSA, " \t\t in block immediately dominates " + cni.toString());
        	    }
            }
            else
            {
	    		imm = getImdDominated(c);
	    		c.setImdDominated(imm);
	       		D.pi(D.D_SSA, "node - " + c.toString());
	    	//	for(CfgNode cni : imm)
	    	//	{
	    	//		D.pi(D.D_SSA, " \t\t immediately dominates " + cni.toString());
	    	//	}
            }
        }
    }

    private static void computeDominates(CfgNode c, Cfg cfg)
    {
		LinkedList<CfgNode> reacW = cfg.reachableWithoutX(c);
		ArrayList<CfgNode> dominates = cfg.getAll();
		dominates.removeAll(reacW);
		dominates.remove(c);
		c.setDominates(dominates);	

//		d("Node in CFG : " + c.toString() + "\n \t\t dominates - \n");
//		for(CfgNode r : dominates)
		{
//			d("\t\t\t " + r.toString());
		}
    }
        
    public static ArrayList<CfgNode> getImdDominated(
		CfgNode cfn)
	{
		List<CfgNode> dominates = cfn.getDominates();
//		ArrayList<CfgNode> childDominated = new 
	//		ArrayList<CfgNode> ();
		ArrayList<CfgNode> immDom = new 
			ArrayList<CfgNode>(dominates);	

		
		for(CfgNode child : dominates)
		{
			List<CfgNode> lc = child.getDominates();
			if(lc != null)
			{
				//childDominated.addAll(lc);
				immDom.removeAll(lc);
			}
		}
		
		//immDom.removeAll(childDominated);

		return new ArrayList<CfgNode>(immDom);
	}

	// for all variables get a list of CfgNodes where they are assigned
	// LHS of these stmts become inputs to the phi functions in 
	// SSA
    public static ConcurrentHashMap<ArrayList<CfgNode>, Variable> 
    	getAllAssignedVars(ArrayList<CfgNode> cfgNodes)
    {
		ConcurrentHashMap<ArrayList<CfgNode>, Variable> varGroups = 
			new ConcurrentHashMap<ArrayList<CfgNode>, Variable>();
    		
		ConcurrentHashMap<CfgNode, Variable> allVars = 
			new ConcurrentHashMap<CfgNode, Variable>();

		for(CfgNode cfn : cfgNodes)
    	{
    		Variable v = getVar(cfn);
    		if(v != null)
    		{
    			D.pi(D.D_SSA, "assigned - " + v.getName() + 
    				" in cfg node - " + cfn);
    			allVars.putIfAbsent(cfn, v);
    		}
    	}
		
    	ArrayList<CfgNode> al = new ArrayList<CfgNode>();
    	ArrayList<Object> visited = new ArrayList<Object>();
		// now iterate through above list and group
        for (Map.Entry<CfgNode, Variable> var : allVars.entrySet()) 
        {
        	if(visited.contains(var))
        		continue; 
        	else
        		visited.add(var);
        	
        	Variable vto = var.getValue();
        	CfgNode cto = var.getKey();
        	
        	al.clear();
        	al.add(cto);
        	
        	D.pv(D.D_SSA, "collecting common CFG nodes for variable - " + vto.getName());
            for (Map.Entry<CfgNode, Variable> varT : allVars.entrySet()) 
            {
            	if(varT.equals(var) || visited.contains(varT))
            		continue; 
            	
            	Variable vtoT = varT.getValue();
            	CfgNode ctoT = varT.getKey();

            	if(cto.equals(ctoT) && vto.equals(vtoT)) 
            		continue; 
            	
            	if(vtoT.equals(vto))
            	{
            		al.add(ctoT);
            		//allVars.remove(ctoT, vtoT);
            		visited.add(varT);
            	}
            }

            // debug
//            for(CfgNode ctmp : al)
  //          	D.pv(D.D_SSA, "\t\t\t CFG node - " + ctmp.toString());
            
            ArrayList<CfgNode> vtoNodes = 
            	new ArrayList<CfgNode>();
            vtoNodes.addAll(al);
            varGroups.putIfAbsent(vtoNodes, vto);	
        }
        
    	return varGroups;
    }

    public static Variable getVar(CfgNode cfgn)
    {
    	
    	if(cfgn instanceof CfgNodeAssignArray ||
    		cfgn instanceof CfgNodeAssignBinary || 
    		cfgn instanceof CfgNodeAssignSimple || 
    		cfgn instanceof CfgNodeAssignUnary)
    	{
    		return cfgn.getLeft();
    	}
    	else 
    	if(cfgn instanceof CfgNodeAssignRef)
    	{
    		D.pf(D.D_TEMP, "Handle references... properly " + cfgn);
    		return cfgn.getLeft();
    	}
    	else
    	if(cfgn instanceof CfgNodeCallRet)
    	{
    		CfgNodeCallRet cncr = (CfgNodeCallRet) cfgn;
    		//D.pi(D.D_SSA, "!!!!!!!!!!!! nodecall ret handle this - " + cncr);
    		D.pi(D.D_SSA, "!!!!!!!!!!!! return var - " + cncr.getRetVar());
    		return cncr.getRetVar();
    	}
    	else
    	if(cfgn instanceof CfgNodeCallBuiltin)
    	{
    		CfgNodeCallBuiltin cncr = (CfgNodeCallBuiltin) cfgn;
    		
    		D.pi(D.D_SSA, "!!!!!!!!!!!! handle this - " + cncr);
    		D.pi(D.D_SSA, "!!!!!!!!!!!! return var - " + cncr.getTempVar());
    		return cncr.getLeft();
    	}
    	else
    	{
    		D.pi(D.D_SSA, " No assigned var in - " + cfgn);    		
    	}
    		
    	return null;
    }

}
