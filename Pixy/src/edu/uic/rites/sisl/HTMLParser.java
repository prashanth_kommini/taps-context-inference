package edu.uic.rites.sisl;

import java.util.*;

import org.jsoup.Jsoup;
import org.jsoup.nodes.*;
import org.apache.commons.lang3.StringUtils;

public class HTMLParser {
	
	//	This class variable holds the list of contexts that are supported
	public static List<String> contexts = Arrays.asList(								
		"PCDATA",
		"RCDATA", 
		"TAGNAME",
		"ATTRNAME",
		"QUOTEDATTRVAL",
		"UNQUOTEDATTRVAL"
	);
	
	private ArrayList<String> vars;
	public void setVars(ArrayList<String> queryVars){ this.vars = queryVars; }

	/*
	 *	This is an instance variable that maps PHP variables [Keys] in the HTML
	 *	to contexts [Values] from the contexts class variable
	 */
	public HashMap<String, String> varsContextMap = new HashMap<String, String>();
	public HashMap<String, String> getVarContextsMap(){ return this.varsContextMap; }

	/*
	 *	Wrapper for Jsoup.parse()
	 *	can parse string
	 */
	public void parse(String htmlToParse){
		Document doc = Jsoup.parse(htmlToParse);
		String htmlString = doc.toString();
		System.out.println(htmlString);
		this.setVarContexts(doc);
		this.dumpVarsContextMap();
	}

	/*
	 *	Main context inference driver function which
	 *	fills out the instance variable varsContextMap for each variable with its context
	 */
	public void setVarContexts(Document doc){
		List<Element> elements = doc.select("*");
		for(String var: this.vars){
			this.varsContextMap.put(var, "UNKNOWN");
		}
		for(String var: this.varsContextMap.keySet()){
			System.out.println("TRYING TO FIND CONTEXT FOR VARIABLE: " + var);
			for(Element element: elements){
				System.out.println("Element is: " + element.tagName());
				if(StringUtils.containsIgnoreCase(element.tagName(), var)){
					this.varsContextMap.put(var, "TAGNAME");
				}
				int i = 0;
				Attributes attributes = element.attributes();
				for(Attribute attribute: attributes){
					i++;
					System.out.println("Attribute " + i + " Name is: " + attribute.getKey()+ " Value is: " + attribute.getValue() );
					String attrName = attribute.getKey();
					String attrValue = attribute.getValue();
					if(StringUtils.containsIgnoreCase(attrName, var)){
						this.varsContextMap.put(var, "ATTRNAME");
					}
					if(StringUtils.containsIgnoreCase(attrValue, var)){
						if(attrValue.startsWith("\"") || attrValue.startsWith("\'") || attrValue.startsWith("'") ){
							this.varsContextMap.put(var, "QUOTEDATTRVAL");
						}
						else{
							this.varsContextMap.put(var, "UNQUOTEDATTRVAL");
						}
					}
				}
				
				String scriptText = "";
				for (DataNode node : element.dataNodes()) {
		            scriptText += node.getWholeData();
		        }
				System.out.println("Data in element is : "+ scriptText);
				System.out.println("Element own Text is: " + element.ownText()); 
				if( element.tagName().equalsIgnoreCase("textarea") &&  StringUtils.containsIgnoreCase(element.ownText(), var)){
					this.varsContextMap.put(var, "RCDATA");
				}
				else if( element.tagName().equalsIgnoreCase("script") && StringUtils.containsIgnoreCase(scriptText, var) ){
					this.varsContextMap.put(var, "RCDATA");
				}				
				else if( StringUtils.containsIgnoreCase(element.ownText(), var)){
					this.varsContextMap.put(var, "PCDATA");
				}
			}
		}

	}
	
	public void dumpVarsContextMap(){
		if(!this.varsContextMap.isEmpty()){
			System.out.println("VarContextMap is not empty");
			System.out.println(this.varsContextMap.toString());
		}			
		else{
			System.out.println("HashMap Empty");
		}
	}	
}