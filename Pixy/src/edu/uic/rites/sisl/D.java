/**
 * 
 */
package edu.uic.rites.sisl;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;

//import at.ac.tuwien.infosys.www.pixy.DepClientInfo;
import at.ac.tuwien.infosys.www.pixy.Dumper;
import at.ac.tuwien.infosys.www.pixy.MyOptions;
//import at.ac.tuwien.infosys.www.pixy.MyOptions;
//import at.ac.tuwien.infosys.www.pixy.Utils;
import at.ac.tuwien.infosys.www.pixy.analysis.inter.*;
import at.ac.tuwien.infosys.www.pixy.conversion.*;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.*;
//import at.ac.tuwien.infosys.www.pixy.sanit.FSAAutomaton;
//import at.ac.tuwien.infosys.www.pixy.sanit.SanitAnalysis;
import at.ac.tuwien.infosys.www.pixy.analysis.dep.*;

/**
 * Debugging utilities
 * @author prithvi
 * borrows code from various infosys utils
 */
public class D 
{
	// debug various modules
	public static final int D_SSA = 1;
	public static final int D_DT = 2; 
	public static final int D_SQL = 3; 
	public static final int D_DG = 4;
	public static final int D_PATH = 5; 
	public static final int D_PS = 6;
	public static final int D_TR = 7;
	public static final int D_PIXY = 8;
	
	public static final int D_TEMP = 99;
	public static final int D_SUM = 9;
	
	public static final int D_INTRA = 10;
	public static final int D_INTER = 11;
	public static final int D_SAV = 12;
	public static final int D_CBV  = 13;


	public static final int LAST_ENTRY = 100;

	
	// control the log verboseness
	public static final int LAST_LEVEL = 200;

	public static final int VERBOSE = 100;
	public static final int DEBUG = 101;
	public static final int IMPORTANT = 102;
	public static final int SUMMARY = 103;
	public static final int FATAL = 104;
	
	
	private static int m_debug = D_TR;
	private static int m_mLevel = IMPORTANT;
	
	public static void setDebugModule(int debug)
	{ 
		if(debug > LAST_ENTRY || debug < 0)
		{
			System.out.println(" Invalid debug module " + debug);
			System.exit(1);
		}
		
		m_debug = debug;
	}

	public static void setDebugLevel(int debug)
	{
		if(debug > LAST_LEVEL || debug < 0)
		{
			System.out.println(" Invalid debug level " + debug + " setting Summary");
			m_mLevel = SUMMARY;
		}
	}
	
	private static PrintWriter logto;
	private static void initLog()
	{
		 boolean bOverwrite = true; 
		 String logFile = MyOptions.pixy_home + "/LogFile.txt";
		 logto = Transformation.getOutStream(logFile, bOverwrite);
		 logto.println(" ================ Created new log entry ============= ");
	}
	
	private static void p(String msg){ D.p("", msg);};
	private static void p(String pad, String msg)
	{
		if(logto == null)
		{
			initLog();
		}
		
		// System.out.println(pad + " " + msg);
		logto.println(pad + " " + msg);
		logto.flush();
	}
	
	public static void _p(int moduleId, String msg, int level)
	{
//		if((moduleId >= m_debug && (level >= m_mLevel)) || 
//				(level >= IMPORTANT) || 
//				(moduleId == D_TEMP))
//		if(level >= IMPORTANT || moduleId == D_TEMP)
			p(moduelToString(moduleId), msg);
	}
	
	// verbose messages
	public static void pv(int moduleId, String msg)
	{
		_p(moduleId, msg, VERBOSE);
	}

	public static void pvt(int moduleId, String msg)
	{
		D.p(moduelToString(D.D_SUM), msg);
	}

	// debug messages
	public static void pd(int moduleId, String msg)
	{
		_p(moduleId, msg, DEBUG);
	}
	
	// important messages
	public static void pi(int moduleId, String msg)
	{
		_p(moduleId, msg, IMPORTANT);
	}

	// summary messages
	public static void ps(int moduleId, String msg)
	{
		_p(moduleId, msg, SUMMARY);
	}
	
	// fatal / todo messages
	public static void pf(int moduleId, String msg)
	{
		_p(moduleId, "TODO:......... " + msg, FATAL);
	}
	
	public static String moduelToString(int level)
	{
		switch(level)
		{
		case D_SSA: 
			return "SSA__: ";
		case D_DT:
			return "DT___: ";
		case D_SQL: 
			return "SQL__: ";
		case D_DG:
			return "DG___: ";
		case D_PATH:
			return "PATH__: ";
			
		case D_PS:
			return "PS___: ";

		case D_TR: 
			return "Transform_____: ";
			
		case D_TEMP:
			return "TEMP_____: ";
			
		case D_SUM:
			return "Summary : ";
		default:
			return "Default__: ";
		}
	}

	public static void presults(String str) {
		D.pv(D.D_TEMP, str);
		logto.flush();		
		System.out.println(" \n " + str);
		System.out.flush();
	}

	public static void pr(String msg) {
		D.p(moduelToString(D.D_SUM), msg);
	}
	
	public static void pr(int i, String msg) {
		D.p(moduelToString(D.D_SUM), msg);
	}

}
