package edu.uic.rites.sisl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
//import java.util.concurrent.CopyOnWriteArrayList;

import at.ac.tuwien.infosys.www.pixy.MyOptions;
import at.ac.tuwien.infosys.www.pixy.analysis.dep.DepGraph;
import at.ac.tuwien.infosys.www.pixy.conversion.Cfg;
import at.ac.tuwien.infosys.www.pixy.conversion.TacFunction;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNode;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeCallBuiltin;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeEcho;

public class TransformationResults {

	public PreciseReport pr;
	private ArrayList<LoopHandled> handledLoops;
	private static TransformationResults results;
	public static TransformationResults get()
	{
		if(results == null)
			results = new TransformationResults();
		return results;
	}
	
	private TransformationResults()
	{
		this.sqlParsingFailed = new ArrayList<SQLSinkFailure>();
		this.conflictTransfs = new ArrayList<ConflictTrans>();
		this.totalSymbQueries = 0;
		this.totalTransformedQueries = 0;
		this.totalLinesChanged = 0;
		this.exceptionReports = new ArrayList<ExceptionReport>();
		this.handledLoops = new ArrayList<LoopHandled>();
		this.pr = new PreciseReport();
	}
	
	
	public void addSQLParserFailure(CfgNode sink, ControlFlow cf,
			String symbQuery) 
	{
		// increment the analyzed flows by 1 
//		this.incrementSinkCount(1);
		
		SQLSinkFailure ssf = new SQLSinkFailure(sink, symbQuery, cf);
		sqlParsingFailed.add(ssf);
		pr.addSQLParserFailure(sink, symbQuery, cf);
	}

	public void addHandledLoop(CfgNode sink, ArrayList<CfgNode> loopBody, ArrayList<CfgNode> dependsNodes) 
	{
		LoopHandled lh = new LoopHandled(sink, loopBody, dependsNodes);
		handledLoops.add(lh);
	}

	
	private int totalSymbQueries;
	
	
//	/**
//	 * @return the totalSymbQueries
//	 */
//	public int getTotalSymbQueries() {
//		return totalSymbQueries;
//	}
//
//	/**
//	 * @param totalSymbQueries the totalSymbQueries to set
//	 */
//	public void setTotalSymbQueries(int totalSymbQueries) {
//		this.totalSymbQueries = totalSymbQueries;
//	}

//	/**
//	 * @return the totalTransformedQueries
//	 */
//	public int getTotalTransformedQueries() {
//		return totalTransformedQueries;
//	}

//	/**
//	 * @param totalTransformedQueries the totalTransformedQueries to set
//	 */
//	public void setTotalTransformedQueries(int totalTransformedQueries) {
//		this.totalTransformedQueries = totalTransformedQueries;
//	}

//	/**
//	 * @return the totalLinesChanged
//	 */
//	public int getTotalLinesChanged() {
//		return totalLinesChanged;
//	}

	/**
	 * @param totalLinesChanged the totalLinesChanged to set
	 */
	public void setTotalLinesChanged(int totalLinesChanged) {
		this.totalLinesChanged = totalLinesChanged;
	}

//	/**
//	 * @return the sqlParsingFailed
//	 */
//	public ArrayList<SQLSinkFailure> getSqlParsingFailed() {
//		return sqlParsingFailed;
//	}

//	/**
//	 * @param sqlParsingFailed the sqlParsingFailed to set
//	 */
//	public void setSqlParsingFailed(ArrayList<SQLSinkFailure> sqlParsingFailed) {
//		this.sqlParsingFailed = sqlParsingFailed;
//	}
	
	private int totalTransformedQueries; 
	private int totalLinesChanged;
	private ArrayList<SQLSinkFailure> sqlParsingFailed;
	private ArrayList<ConflictTrans> conflictTransfs;
	private ArrayList<ExceptionReport> exceptionReports;
	
	private int getUnforceableExceptions()
	{
		int iException = 0;
		for(ExceptionReport er : exceptionReports)
		{
			if(er.iType == SISLException.NONREPEATABLE_PARTIAL_QUERY_FROM_LOOP ||
					er.iType == SISLException.LOOPSTMT_VIOLATE_CONSTRAINT ||
					er.iType == SISLException.FOUND_CALLBACK_VIOLATIONS_IN_TRANSFORM)
				iException++;
		}
		
		return iException;
	}
	
	public void reportResults() {
		System.out.println(" Reporting results now...");
		D.pr(" Reporting results now...");

		int iFailed = this.sqlParsingFailed.size();
		int iUnforceable = getUnforceableExceptions();
		int iExceptions = 0;
		if(!MyOptions.option_F)
		{
			iExceptions = exceptionReports.size();
		}
		else
			iExceptions = iUnforceable; 
		
		int totalFailure = iFailed  + getConflictingPaths() + iExceptions;
		
		int iTransformed = this.totalSymbQueries - totalFailure;
		
		if(iTransformed < 0)
			iTransformed = 0;
		
		System.out.println(" ==================== PS Transformation Results ================ \n");
		D.pr(" ==================== PS Transformation Results ================ \n");
		System.out.println(" Analyzed Symbolic Queries - " + totalSymbQueries + "\n"); 
		D.pr(" Analyzed Symbolic Queries - " + totalSymbQueries + "\n"); 

		System.out.println(" Transformed to PreparedStatements - " + iTransformed + "\n");
		D.pr(" Transformed to PreparedStatements - " + iTransformed + "\n");
		
		System.out.println(" Program statements changed - " + this.totalLinesChanged + " \n ");
		D.pr(" Program statements changed - " + this.totalLinesChanged + " \n ");

		String maxCfReport = toStr_MaxCfInterProc(iTransformed);
		System.out.println(maxCfReport);
		D.pr(maxCfReport);
		
		String maxArgsReport = toStr_MaxArgsExtracted(iTransformed);
		System.out.println(maxArgsReport);
		D.pr(maxArgsReport);
		
		System.out.println(" Transformation did not change " + totalFailure + " flows. Combined failures ---> See the log file");
		D.pr(" Transformation did not change " + totalFailure + " flows. Combined failures ---> See the log file");
		
		if(this.sqlParsingFailed.size() != 0)
		{
			int iF = 1;
//			System.out.println(" SQL Parsing failed for " + iFailed + " sinks  : (check for bugs / infeasible paths) -- \n");
			D.pr(" SQL Parsing failed for " + iFailed + " sinks  : (check for bugs / infeasible paths) -- \n");

			for(SQLSinkFailure ssf : this.sqlParsingFailed)
			{
//				System.out.println("\t Failed# " + (iF++) + " \t " + ssf.toString() + " \n");
				D.pr("\t Failed# " + (iF++) + " \t " + ssf.toString() + " \n");
			}
		}
		
		if(this.conflictTransfs.size() != 0)
		{
			int iC = 1; 
//			System.out.println(" Conflicting transformations were detected - ");
			D.pr(" Conflicting transformations were detected - ");

			for(ConflictTrans ct : this.conflictTransfs)
			{
//				System.out.println("\t Failed# " + (iC++) + " \t " + ct.toString() + " \n");
				D.pr("\t Failed# " + (iC++) + " \t " + ct.toString() + " \n");
			}
		}

		if(this.exceptionReports.size() != 0)
		{
			int iE = 1;
//			System.out.println(" Failed to transform following statements - ");
			D.pr(" Failed to transform following statements - ");

			for(ExceptionReport er : this.exceptionReports)
			{
//				System.out.println("\t Failed# " + (iE++) + " \t " + er.toString() + " \n");
				D.pr("\t Failed# " + (iE++) + " \t " + er.toString() + " \n");
			}
		}

		pr.dump();
		

		
		System.out.println("............... Reporting handled loops ------ ");
		D.pr("............... Reporting handled loops .............. \n");
		reportHandledLoops();
		D.pr("............... End ..............\n");
		
		System.out.println("End of LogFile.txt contains details of each warning, with line numbers and transformation proposed changes .");
		System.out.println(" Use -F flag to ignore the warnings - non-white list ops, callbacks");

		//System.out.println(" \n (Derivation trees in DOT format (dotty view) are in graphs folder)");
		//D.presults(str);

		
//		int iFailed = this.sqlParsingFailed.size();
//		int iTransformed = this.totalSymbQueries - iFailed - this.conflictTransfs.size();
//		
//		String str = "";
//		str += " ==================== PS Transformation Results ================ \n";
//		str += " Analyzed Symbolic Queries - " + totalSymbQueries + "\n"; 
//		str += " Transformed to PreparedStatements - " + iTransformed + "\n";
//		str += " Program statements changed - " + this.totalLinesChanged + " \n ";
//		
//		if(this.sqlParsingFailed.size() != 0)
//		{
//			int iF = 1;
//			str += " SQL Parsing failed for " + iFailed + " sinks  : (check for bugs / infeasible paths) -- \n";
//			for(SQLSinkFailure ssf : this.sqlParsingFailed)
//				str += "\t Failed# " + (iF++) + " \t " + ssf.toString() + " \n";
//		}
//		
//		if(this.conflictTransfs.size() != 0)
//		{
//			int iC = 1; 
//			str += " Conflicting transformations were detected - ";
//			for(ConflictTrans ct : this.conflictTransfs)
//			{
//				str += "\t Failed# " + (iC++) + " \t " + ct.toString() + " \n";
//			}
//		}
//
//		if(this.exceptionReports.size() != 0)
//		{
//			int iE = 1;
//			str += " Failed to transform following statements - ";
//			for(ExceptionReport er : this.exceptionReports)
//			{
//				str += "\t Failed# " + (iE++) + " \t " + er.toString() + " \n";
//			}
//		}
//		str += " \n (Derivation trees in DOT format (dotty view) are in graphs folder)";
//		D.presults(str);
	}


	private void reportHandledLoops() {
		
		int total = this.handledLoops.size();
		String msg = " Successfully handled loops - " + total + " \n";
		msg += " unique loops - ";
				
		int iArr = 0;
		if(this.handledLoops != null && this.handledLoops.size() != 0)
			iArr = this.handledLoops.size();

		ArrayList<String> loops = new ArrayList<String>();
		
		int[] iCount = new int [iArr];
		for(LoopHandled lh : this.handledLoops)
		{
			String l = " \t " + lh.toString() + "\n";
			
			int index = loops.indexOf(l);
			if(index < 0)
			{
				loops.add(l);
				index = loops.indexOf(l);
				iCount[index] = 1;
			}
			else
			{
				iCount[index]++;
			}
		}
		
		int i = 0;
		for(String s : loops)
		{
			msg += " \t [" + iCount[i] + "] Loop#" + (i+1) + " " + s;
			i++;
		}
		
		D.pr(msg);
		System.out.println(msg);
	}

	private int getConflictingPaths() {
		int iSize = 0;
		if(this.conflictTransfs.size() != 0)
		{
			for(ConflictTrans ct : this.conflictTransfs)
			{
				ArrayList<Conflict> conflicts = ct.getConflicts();
				if(conflicts != null)
					iSize += conflicts.size();
			}
		}

		return iSize;
	}

	public void incrementSinkCount(int i) {
		D.pv(D.D_TEMP, " increment sink count called - current count - " + this.totalSymbQueries);
		this.totalSymbQueries += i;
	}

	public void incrementTotalChanges(int size) {
		this.totalLinesChanged += size;
	}

	public void addConflict(ArrayList<FlowChanges> confl) 
	{
		ConflictTrans cts = new ConflictTrans();
		if(confl.size() == 0)
			throw new Error(" an empty add conflict requested");
		
		CfgNode sink = null;
		for(FlowChanges fc : confl)
		{
//			private String symbQuery; 		
			Conflict con = new Conflict();
			con.setCf(fc.interProcFlow);
			con.setProposedTrans(fc.proposedChanges);
			cts.addConflictTrans(con);
			sink = fc.sink;
		}
		if(sink == null)
			throw new Error(" conflict location cannot be null");
		cts.setConflictLocation(sink);
		this.conflictTransfs.add(cts);
		
		pr.addConflict(sink);
	}

	public void incrementTransSink(int size) {
		this.totalTransformedQueries += size;
	}

	public void addFailureReport(SISLException se, DepGraph dg, SinkPath sp) {
		
		ArrayList<CfgNode> nodes = new ArrayList<CfgNode>(DepGraph.getAllCfgNodes(dg.getNodes(), true));
		String strNodes = CfgNode.getProgLinesSorted(nodes);
		ExceptionReport er = new ExceptionReport(se, dg.getRoot().getCfgNode(),
					strNodes);	
		
		// increment number of flows analyzed
//		this.incrementSinkCount(1);
		D.pr(" Adding a failure report for this exception - " + se.getSISLMessage());
		if(!this.exceptionReports.contains(er))
			this.exceptionReports.add(er);
	}

	public void addFailureReportX(SISLException se, ControlFlow cf, CfgNode sink) {
		
//		ArrayList<CfgNode> nodes = new ArrayList<CfgNode>(DepGraph.getAllCfgNodes(dg.getNodes()));
		String strNodes = CfgNode.getProgLinesSorted(cf.getAll());
		ExceptionReport er = new ExceptionReport(se, sink,
					strNodes);	
		
		// increment number of flows analyzed
//		this.incrementSinkCount(1);
		
		if(!this.exceptionReports.contains(er))
			this.exceptionReports.add(er);
	}

	private ControlFlow cfMaxInterFlow;
	private int cfMaxInterNumber;
	private int cfTotalInterFlows; 
	public void addMaxCfInterProc(ControlFlow cf) 
	{
		if(this.cfMaxInterFlow == null)
		{
			this.cfMaxInterFlow = cf;
			this.cfMaxInterNumber = cf.iNumberOfFnsTraversed;
			this.cfTotalInterFlows = cf.iNumberOfFnsTraversed;
			return;
		}
		
		if(this.cfMaxInterNumber < cf.iNumberOfFnsTraversed)
		{
			this.cfMaxInterFlow = cf;
			this.cfMaxInterNumber = cf.iNumberOfFnsTraversed;
		}

		this.cfTotalInterFlows += cf.iNumberOfFnsTraversed;
	}

	public String toStr_MaxCfInterProc(int iSymbQueries) 
	{
		float fAvgFns = 0;
		// avoid exception
		if(iSymbQueries != 0)
			fAvgFns = (float)this.cfTotalInterFlows / (float)iSymbQueries;
		
		String str = " Average number of functions traversed for symbolic query computation- " + fAvgFns + "\n";
		str += "\t max number of " + this.cfMaxInterNumber + " functions are traversed in this flow - ";
		
		if(this.cfMaxInterFlow != null)
		{
			for(String s : CfgNode.getProgLines(this.cfMaxInterFlow.getAll()))
			{
				str += " \n \t \t" + s;
			}
		}
		return str;
	}

	private int maxArgs;
	private ControlFlow maxArgsCf;
	private ArrayList<ProgramChange> maxArgsPC; 
	private int totalArgsExtracted;
	public void addMaxArgsExtracted(DerivationTree dt, ControlFlow cf,
			ArrayList<ProgramChange> progChanges) 
	{
		int iArgs = dt.getQuestionMarks();
		if(this.maxArgsPC == null)
		{
			this.maxArgsPC = new ArrayList<ProgramChange>(progChanges); 
			this.maxArgs = iArgs;
			this.totalArgsExtracted = iArgs;
			this.maxArgsCf = cf;
			return;
		}
		
		if(this.maxArgs < iArgs)
		{
			this.maxArgsPC = new ArrayList<ProgramChange>(progChanges);
			this.maxArgs = iArgs;
			this.maxArgsCf = cf;
		}
		
		this.totalArgsExtracted += iArgs;
	}	
	
	public String toStr_MaxArgsExtracted(int iSymbQueries) 
	{
		float fAvgArgs = 0;
		// avoid exception
		if(iSymbQueries != 0)
			fAvgArgs = (float)this.totalArgsExtracted / (float)iSymbQueries;
		
		String str = " Average number of arguments extracted from symbolic queries - " + fAvgArgs + "\n";
		str += "\t max number of " + this.maxArgs + " were extracted from following control flow - ";		
		if(this.maxArgsCf != null)
		{
			for(String s : CfgNode.getProgLines(this.maxArgsCf.getAll()))
			{
				str += " \n \t \t" + s;
			}
		}

		if(this.maxArgsPC != null)
		{
			str += " \n transformation made following program changes ----- ";
			for(ProgramChange pc : this.maxArgsPC)
			{
				str += "\n\t\t " + pc;
			}
		}
		return str;
	}
}

class ConflictTrans
{
	public ConflictTrans()
	{
		conflicts = new ArrayList<Conflict>();
		conflictTrans = new ArrayList<ProgramChange>();
	}
	
	/**
	 * @return the conflicts
	 */
	public ArrayList<Conflict> getConflicts() {
		return conflicts;
	}

	/**
	 * @param conflicts the conflicts to set
	 */
	public void setConflicts(ArrayList<Conflict> conflicts) {
		this.conflicts = conflicts;
	}

	/**
	 * @return the conflictLocation
	 */
	public CfgNode getConflictLocation() {
		return conflictLocation;
	}

	/**
	 * @param conflictLocation the conflictLocation to set
	 */
	public void setConflictLocation(CfgNode conflictLocation) {
		this.conflictLocation = conflictLocation;
	}

	/**
	 * @return the conflictTrans
	 */
	public ArrayList<ProgramChange> getConflictTrans() {
		return conflictTrans;
	}

	/**
	 * @param conflictTrans the conflictTrans to set
	 */
	public void setConflictTrans(ArrayList<ProgramChange> conflictTrans) {
		this.conflictTrans = conflictTrans;
	}

	public void addConflictTrans(Conflict cf) {
		this.conflicts.add(cf);
	}

	private ArrayList<Conflict> conflicts; 
	private CfgNode conflictLocation;
	private ArrayList<ProgramChange> conflictTrans; 
	
	public String toString()
	{
		
		String fileLoc = conflictLocation.getFileName() + ": " + 
			conflictLocation.getOrigLineno();
		
		String str = "";
		str += " Conflict transforming - " + fileLoc + " \n ";
		str += " \t resulted from : \n ";
		
		int i = 0;
		for(ProgramChange co : this.conflictTrans)
			str += "\t\t " + co.toString() + " from path " + (i++)+ "\n";
		
		i = 0;
		for(Conflict c : this.conflicts)
		{
			str += " \t Path " + (i++) + "\n" ;
			str += " \t\t " + c.toString();
		}
	
		D.pr(" reporting a conflict - " + str);
		
		return str;
	}
}

class Conflict
{
	private String symbQuery; 
	private ControlFlow cf; 
	private ArrayList<ProgramChange> proposedTrans; 
	
	/**
	 * @return the symbQuery
	 */
	public String getSymbQuery() {
		return symbQuery;
	}

	/**
	 * @param symbQuery the symbQuery to set
	 */
	public void setSymbQuery(String symbQuery) {
		this.symbQuery = symbQuery;
	}

	/**
	 * @return the cf
	 */
	public ControlFlow getCf() {
		return cf;
	}

	/**
	 * @param cf the cf to set
	 */
	public void setCf(ControlFlow cf) {
		this.cf = cf;
	}

	/**
	 * @return the proposedTrans
	 */
	public ArrayList<ProgramChange> getProposedTrans() {
		return proposedTrans;
	}

	/**
	 * @param proposedTrans the proposedTrans to set
	 */
	public void setProposedTrans(ArrayList<ProgramChange> proposedTrans) {
		this.proposedTrans = proposedTrans;
	}

	public String toString()
	{
		String str = "";
		//str += " Symbolic Query - " + this.symbQuery + " \n";

		str += " \t query computed in path - \n" + cf.getProgStatement();
		str += " \t\t proposedChanges - \n" ;
		for(ProgramChange pc : this.proposedTrans)
			str += " \t\t\t " + pc.toString() + "\n";
		
		D.pv(D.D_TEMP, " reporting a conflict contributor - " + str);
		return str; 
	}
}

class LoopHandled
{
	public LoopHandled(CfgNode sink, ArrayList<CfgNode> loopBody, ArrayList<CfgNode> dependsNodes) {
		this.sink = sink;
		this.loopBody = new ArrayList<CfgNode>();
		this.loopBody.addAll(loopBody);
		this.dependsNodes = new ArrayList<CfgNode>();
		this.dependsNodes.addAll(dependsNodes);
	}
	
	public String toString()
	{
		String str = "";
		str += " \t ======= LOOP allowed by the tool  \n"; 
		str += " \t\t generated for query sink - " + this.sink  + " computed in the following stmts - \n"; 
		for(String s : CfgNode.getProgLines(dependsNodes))
			str += "\t\t\t " + s + "\n"; 
		str += " \t\t Loop body - \n ";
		for(String s: CfgNode.getProgLines(loopBody))
			str += "\t\t\t " + s + "\n";

		return str; 
	}
	
	CfgNode sink;
	String symbQuery; 
	ArrayList<CfgNode> loopBody;	
	ArrayList<CfgNode> dependsNodes;
}



class SQLSinkFailure
{
	public SQLSinkFailure(CfgNode sink, String symbQuery, ControlFlow cf) {
		this.sink = sink;
		this.symbQuery = symbQuery;
		this.cf = cf;	
	}
	
	public String toString()
	{
		String str = "";
		str += " Symbolic Query - " + symbQuery + " \n\t"; 
		str += " generated in statements - \n " + cf.getProgStatement();
		
		D.pv(D.D_TEMP, " reporting a symbolic query failure - \n\t\t" + str);
		return str; 
	}
	
	CfgNode sink;
	String symbQuery; 
	ControlFlow cf;	
}

class ExceptionReport
{
	public ExceptionReport(SISLException se, CfgNode exitPoint, String strNodes) {
		this.iType = se.getType();
		this.reason = se.getSISLMessage();
		this.sqlSink = exitPoint;
		this.stmtsInvolved = strNodes; 
	}
	String stmtsInvolved;
	CfgNode sqlSink;	
	String reason; 
	public int iType;
	
	public String toString()
	{
		String str = " For SQL sink in File " + sqlSink.getFileName() + " Line " + 
			sqlSink.getOrigLineno() + " following failure occurred - \n"; 
		return str + reason;
	}
}

/* 
 * details are captured by individual classes above...this class summarizes the
 * statistics
 */
class PreciseReport
{
	int totalSQLParserFailures; 
	ArrayList<ToolBug> sqlParserFailures;
	
	int totalNonWhiteListedOperations;
	ArrayList<ToolBug> uniqNonWhiteListOps; 
	
	int totalConflicts; 
	ArrayList<CfgNode> uniqConflictSinks; 
	
	int totalCallbackViolations; 
	ArrayList<CfgNode> uniqCallbackViolators;
	
	int totalToolBugs;
	private ArrayList<ToolBug> allNonWhiteListOps; 
	
	public PreciseReport()
	{
		this.totalCallbackViolations = this.totalConflicts = 
			this.totalNonWhiteListedOperations = this.totalSQLParserFailures = 
				this.totalToolBugs = 0;
		
		this.sqlParserFailures = new ArrayList<ToolBug>();
		this.uniqNonWhiteListOps = new ArrayList<ToolBug>();
		this.uniqConflictSinks = new ArrayList<CfgNode>();
		this.uniqCallbackViolators = new ArrayList<CfgNode>();
		this.allNonWhiteListOps = new ArrayList<ToolBug>();
	}
	
	public void dump() {

		String str = "";
		boolean bIgnore = MyOptions.option_F;
		if(bIgnore) 
			str = "[  Ignored  ]  ";
		
		System.out.println(" \t " + this.totalSQLParserFailures +  " : Total SQL Parser Failures -  (each sink reported below once)");
		D.pr(" \t " + this.totalSQLParserFailures +  " Total SQL Parser Failures - (each sink reported below once)");
//		for(ToolBug tb : this.sqlParserFailures)
//		{
//			System.out.println("\t\t " + CfgNode.getProgLineCode(tb.spf_cn));
//			D.pr(D.D_TR, "\t\t " + CfgNode.getProgLineCode(tb.spf_cn));
//		}
		printUniqSymbQueries();
		
		
//		System.out.println(" \t " + str + this.totalNonWhiteListedOperations + " : Total Non white " +
//			"listed operations applied on modified variables (each instance reported below once)"); 
//		D.pr(D.D_TR, " \t " + this.totalNonWhiteListedOperations + " : Total Non white " +
//			"listed operations applied on modified variables (each instance reported below once)"); 

		printUniqNonWhiteListOp();
//		ArrayList<String> lines = CfgNode.getProgLines(this.uniqNonWhiteListOps);
//		for(String s : lines)
//		{
//			System.out.println("\t " + s);
//			D.pr(D.D_TR, "\t " + s);
//		}
		
		System.out.println(" \t " + this.totalConflicts + " : Total conflicts detected (each sink reported once below)");
		D.pr(" \t " + this.totalConflicts + " : Total conflicts detected (each sink reported once below)");
		for(CfgNode cn : this.uniqConflictSinks)
		{
			System.out.println("\t\t " + CfgNode.getProgLineCode(cn));
			D.pr("\t\t " + CfgNode.getProgLineCode(cn));
		}

		System.out.println(" \t " + str + this.totalCallbackViolations + " : Total potential violations with callbacks (each callback location reported once)");
		D.pr(" \t " + this.totalCallbackViolations + " : Total potential violations with callbacks (each callback location reported once)");

		for(CfgNode cn : this.uniqCallbackViolators)
		{
			System.out.println("\t\t " + CfgNode.getProgLineCode(cn));
			D.pr("\t\t " + CfgNode.getProgLineCode(cn));
		}
		
		ToolBug.dumpReportsInternal();
		System.out.println(" \t " + this.totalToolBugs + " : Total tool failures (incomplete handling)");
		D.pr(" \t " + this.totalToolBugs + " : Total tool failures (incomplete handling)");
	//	D.pr(D.D_TEMP, " \t " + this.totalToolBugs + " : Total tool failures (incomplete handling)");
	}

	private void printUniqNonWhiteListOp() {

		ArrayList<CfgNode> violation = new ArrayList<CfgNode>();
	
		int iArr = 0;
		if(this.uniqNonWhiteListOps != null && this.uniqNonWhiteListOps.size() != 0)
			iArr = this.uniqNonWhiteListOps.size();
		
		int[] iCount = new int [iArr];
		D.pr(D.D_TEMP, " sizeof arr = " + iCount.length);
		for(ToolBug tb : this.uniqNonWhiteListOps)
		{
			int index = violation.indexOf(tb.nwlo_cfgN);
			if(index < 0)
			{
				violation.add(tb.nwlo_cfgN);
				index = violation.indexOf(tb.nwlo_cfgN);
				iCount[index] = 1;
			}
			else
			{
				iCount[index]++;
			}
		}
		
		
		String str = "";
		boolean bIgnore = MyOptions.option_F;
		if(bIgnore) 
			str = "[  Ignored  ]  ";

		System.out.println(" \t " + str + this.totalNonWhiteListedOperations + " : Total Non white " +
		"listed operations applied on modified variables (each instance reported below once)"); 
		
		D.pr(" \t " + str + this.totalNonWhiteListedOperations + " : Total Non white " +
		"listed operations applied on modified variables (each instance reported below once)"); 

		
		int index = 0;
		for(CfgNode cn : violation)
		{
			System.out.println("\t [ " + iCount[index] + " ] - " + CfgNode.getProgLineCode(cn));
			D.pr("\t [ " + iCount[index] + " ] - " + CfgNode.getProgLineCode(cn));
			index++;
		}
	}

	private void printUniqSymbQueries() {
		// sort them by 
		ArrayList<String> uniqSymbQ = new ArrayList<String>();
		ArrayList<ControlFlow> sampleFlows = new ArrayList<ControlFlow>();
		ArrayList<CfgNode> sinks = new ArrayList<CfgNode>();
		ArrayList<Integer> count = new ArrayList<Integer>();
	
		int iArr = 0;
		if(this.sqlParserFailures != null && this.sqlParserFailures.size() != 0)
			iArr = this.sqlParserFailures.size();
		
		int[] iCount = new int [iArr];
		D.pv(D.D_TEMP, " sizeof arr = " + iCount.length);
		for(ToolBug tb : this.sqlParserFailures)
		{
			int index = uniqSymbQ.indexOf(tb.spf_symbQuery);
			if(index < 0 || !sinks.contains(tb.spf_sink))
			{
				uniqSymbQ.add(tb.spf_symbQuery);
				sampleFlows.add(tb.spf_cf);
				sinks.add(tb.spf_sink);
				//count.add(new Integer(1));
				
				index = uniqSymbQ.indexOf(tb.spf_symbQuery);
				iCount[index] = 1;
			}
			else
			{
				iCount[index]++;
			}
		}
		
		int index = 0;
		for(String sq : uniqSymbQ)
		{
			System.out.println("\t [ " + iCount[index] + " ] - " + sq);
			D.pr( "\t [ " + iCount[index] + " ] - " + sq);
			D.pr( "\t\t Sink - " + CfgNode.getProgLine(sinks.get(index)));
			D.pr( "\t\t Sample flow that generated it\n \t\t " + 
					CfgNode.getProgLines(sampleFlows.get(index).getAll()));
			index++;
		}
	}

	public void addSQLParserFailure(CfgNode cn, String symbQuery, ControlFlow cf)
	{
		this.totalSQLParserFailures ++;
		
		ToolBug tb = new ToolBug();
		tb.spf_symbQuery = symbQuery; 
		tb.spf_sink = cn;
		tb.spf_cf = cf;
		tb.iReason = ToolBug.SQL_PARSER_FAILURE; 
		this.sqlParserFailures.add(tb);
	}

	public void addNonWhiteListOp(CfgNode cn)
	{
		ToolBug tb = new ToolBug();
		tb.nwlo_cfgN = cn; 
		tb.iReason = ToolBug.NWLOP_STATIC_VIOLATION;

		
		// all non-white list is for debugging, 
		// tool captures the conflicts originating 
		// at a prog function that calls inbuilts. 
		// this all non white list ops would contain 
		// records for prog functions as well.
		// uniqNonWhiteList only captures violating
		// inbuilt calls that are to be reported in stats
		this.allNonWhiteListOps.add(tb);
		
		
		if(cn instanceof CfgNodeCallBuiltin || 
				cn instanceof CfgNodeEcho)
		{
			this.totalNonWhiteListedOperations ++;
			this.uniqNonWhiteListOps.add(tb);
		}
	}

	public void addConflict(CfgNode cn)
	{
		this.totalConflicts++;
		if(!this.uniqConflictSinks.contains(cn))
			this.uniqConflictSinks.add(cn);
	}

	public void addCallbackViolation(CfgNode cn)
	{
		this.totalCallbackViolations++;
		if(!this.uniqCallbackViolators.contains(cn))
			this.uniqCallbackViolators.add(cn);
	}

	public void addToolBugs()
	{
		this.totalToolBugs++;
	}

	
	
/*	public void addToolFailure(int reason,
			TacFunction fn,
			LinkedHashMap<Cfg, CopyOnWriteArrayList<CfgNode>> allFnDepen) {
		
		ToolBug tb = new ToolBug();
		switch(reason)
		{
		case SISLException.INTERPROC_FAILURE_ZERO_PROC_FLOW:
			tb.set_INTERPROC_FAILURE_ZERO_PROC_FLOW(allFnDepen, fn);
			break;
			
		default:
			break;
		}
		
		ToolBug.addReport(tb);
	}
*/
	
}

class ToolBug
{
	public static final int UNCLASSIFIED_YET = 1000;
	public static final int INTERPROC_FAILURE_ZERO_PROC_FLOW = 100;		// path intra 0 flows
	public static final int INTERPROC_INDIRECT_RECURSION = 101;			// indirect recursion
	public static final int INTRAPROC_DIRECT_RECURSION = 102;
	public static final int SAV_REACHABILITY_FAILURE = 103;
	public static final int UNRESOLVED_ENTRY_POINT = 104;
	private static final int COULD_NOT_LOCATE_ROOT_DT = 105;
	private static final int COULD_NOT_LOCATE_DATA_SUBTREE = 106;

	
	public static final int INTERNAL_FAILURES = 1001;
	public static final int SQL_PARSER_FAILURE = 1002;					// maybe tool bugs or approach limits 
	public static final int NWLOP_STATIC_VIOLATION = 1003;		// a non-white list opn detected

	
	

	int iReason;

	public ToolBug()
	{
	}
	
	// these are probably problems in impls
	public static ArrayList<ToolBug> toolBugs;
	
	// these may not be the problem with impl, but limits of static analysis
	// SQL Parser failure
	// Conflicts
	// 
	public static ArrayList<ToolBug> toolReports;

	static
	{
		if(toolBugs == null)
			toolBugs = new ArrayList<ToolBug>();
		
		if(toolReports == null)
			toolReports = new ArrayList<ToolBug>();
	}
			
	public String toString()
	{
		String str = "";
		switch(this.iReason)
		{
		/* ------------ Tool bugs ------------------ */
		case INTERPROC_FAILURE_ZERO_PROC_FLOW:
			str += toStr_INTERPROC_FAILURE_ZERO_PROC_FLOW();
			break;
			
		case ToolBug.INTERPROC_INDIRECT_RECURSION: 
			str += toStr_INTERPROC_INDIRECT_RECURSION();
			break;
			
		case ToolBug.INTRAPROC_DIRECT_RECURSION: 
			str += toStr_INTRAPROC_DIRECT_RECURSION();
			break;
					
		case ToolBug.SAV_REACHABILITY_FAILURE:
			str += toStr_SAV_REACHABILITY_FAILURE();
			break;
			
		case ToolBug.UNCLASSIFIED_YET:
			str += toStr_UNCLASSIFIED_YET();
			break;
			
		case ToolBug.UNRESOLVED_ENTRY_POINT:
			str += toStr_UNRESOLVED_ENTRY_POINT();
			break;

		case ToolBug.COULD_NOT_LOCATE_ROOT_DT:
			str += toStr_COULD_NOT_LOCATE_ROOT_DT();
			break;
		
		case ToolBug.COULD_NOT_LOCATE_DATA_SUBTREE:
			str += toStr_COULD_NOT_LOCATE_DATA_SUBTREE();
			break;
		/* ------------------ reports for static limits or approach limitations ------- */
			
		default :
			str += " Unspecified reason " + this.iReason;
			break;
		}
		
		return str;
	}

	public static void dumpReportsInternal()
	{
		System.out.println(" Total tool failures - " + toolBugs.size());
		D.pr( " Total tool failures - " + toolBugs.size());
		for(ToolBug tb : toolBugs)
		{
			if(tb.iReason > INTERNAL_FAILURES)
				continue;
			D.pr( " ToolBug : " + tb.toString());
			System.out.println("\t ToolBug  : " + tb.toString());
		}
	}
	
	// fields for INTERPROC_FAILURE_ZERO_PROC_FLOW
	LinkedHashMap<Cfg, ArrayList<CfgNode>> allFnDepen;
	TacFunction inFn ;	
	public static void add_INTERPROC_FAILURE_ZERO_PROC_FLOW(
			LinkedHashMap<Cfg, ArrayList<CfgNode>> allFnDepen,
			TacFunction inFn)
	{
		ToolBug tb = new ToolBug();
		tb.iReason = INTERPROC_FAILURE_ZERO_PROC_FLOW;
		tb.allFnDepen = allFnDepen;
		tb.inFn = inFn;
		toolBugs.add(tb);
	}

	public String toStr_INTERPROC_FAILURE_ZERO_PROC_FLOW()
	{
		String str = "INTERPROC_FAILURE_ZERO_PROC_FLOW : ";
		str += " \n \t no nodes in intraPaths of " + this.inFn.getName();
		str += " \n all dependencies - ";
		for(Map.Entry<Cfg, ArrayList<CfgNode>> en : this.allFnDepen.entrySet())
		{
			String fname = " \n In Function - " + 
				Cfg.getFunction(en.getKey().getHead()).getName();
			ArrayList<String> lines = CfgNode.getProgLines(en.getValue());
			for(String s : lines)
				fname += "\n\t line - " + s;
			str += fname + "\n";
		}

		return str;
	}

	// fields for PHi LHS un handled
	private String phiLhsUnhandled;
	public static void add_TOOL_BUG_PHILHS_UNHANDLED(String msg) {
		ToolBug tb = new ToolBug();
		tb.phiLhsUnhandled = msg; 
		toolBugs.add(tb);	
	}
	
	public String toStr_TOOL_BUG_PHILHS_UNHANDLED() {
		return "TOOL_BUG_PHILHS_UNHANDLED: " + this.phiLhsUnhandled;
	}
	
	private SISLException se; 
	public static void add_UNCLASSIFIED_YET(SISLException seNew) {
		ToolBug tb = new ToolBug();
		tb.iReason = UNCLASSIFIED_YET;
		tb.se = seNew;
		toolBugs.add(tb);
	}

	public String toStr_UNCLASSIFIED_YET() {
		String str = "";
		for(StackTraceElement ste : this.se.getStackTrace())
			str += ste.toString() + " \n";
		return "UNCLASSIFIED_YET : " + this.se.getSISLMessage() + str;
	}

	// members for INTERPROC_INDIRECT_RECURSION;
	TacFunction fnIndirectRecDetectedAt;
	LinkedHashMap<Cfg, ArrayList<CfgNode>> inFndepend;
	ArrayList<ControlFlow> interPathsBeforeRec;
	public static void add_INTERPROC_INDIRECT_RECURSION(TacFunction fn,
			LinkedHashMap<Cfg, ArrayList<CfgNode>> allFnDepen2,
			ArrayList<ControlFlow> interPaths) {

		ToolBug tb = new ToolBug();
		tb.iReason = INTERPROC_INDIRECT_RECURSION;
		tb.fnIndirectRecDetectedAt = fn;
		tb.inFndepend = allFnDepen2;
		tb.interPathsBeforeRec = interPaths;
		toolBugs.add(tb);
	}

	public String toStr_INTERPROC_INDIRECT_RECURSION() {
		String str = "INTERPROC_INDIRECT_RECURSION: ";
		str += " \n recursion detected at - " + this.fnIndirectRecDetectedAt.getName() ;
		str += " \n All function dependencies - ";
		for(Map.Entry<Cfg, ArrayList<CfgNode>> en : this.inFndepend.entrySet())
		{
			String fname = " \n In Function - " + 
				Cfg.getFunction(en.getKey().getHead()).getName();
			ArrayList<String> lines = CfgNode.getProgLines(en.getValue());
			for(String s : lines)
				fname += "\n\t line - " + s;
			str += fname + "\n";
		}
		
		str += " all paths enumerated so far - " + 
			(this.interPathsBeforeRec == null ? " none " : this.interPathsBeforeRec.size());
		if(this.interPathsBeforeRec != null)
		for(ControlFlow cf : this.interPathsBeforeRec)
			str += " \n \t Path - " + CfgNode.getProgLines(cf.getAll());

		return str;
	}

	
	private LinkedHashMap<ArrayList<TacFunction>, TacFunction> invokeOrder;
	private ControlFlow path;
	private ArrayList<CfgNode> dependsNodes;
	private String intraMsg;
	public static void add_INTRAPROC_DIRECT_RECURSION(
			LinkedHashMap<ArrayList<TacFunction>, TacFunction> invokeOrder,
			ControlFlow path, ArrayList<CfgNode> dependsNodes, String msg) {
		
		ToolBug tb = new ToolBug();
		tb.iReason = INTRAPROC_DIRECT_RECURSION;
		tb.invokeOrder = invokeOrder;
		tb.path = path;
		tb.dependsNodes = dependsNodes;
		tb.intraMsg = msg;
		toolBugs.add(tb);
	}

	public String toStr_INTRAPROC_DIRECT_RECURSION() {
		String str = " INTRAPROC_DIRECT_RECURSION : " + this.intraMsg;
		return str;
	}

	
	private String savMsg;
	public static void addSAV_REACHABILITY_FAILURE(String msg) {
		ToolBug tb = new ToolBug();
		tb.iReason = SAV_REACHABILITY_FAILURE;
		tb.savMsg = msg; 
		toolBugs.add(tb);
	}

	public String toStr_SAV_REACHABILITY_FAILURE() {
		String str = "SAV_REACHABILITY_FAILURE : " + this.savMsg;
		return str;
	}

	private ArrayList<TacFunction> entryPoints;
	private TacFunction exitPoint;
	private LinkedHashMap<TacFunction, ArrayList<TacFunction>> inOrder;
	String msgunresl;
	public static void addUNRESOLVED_ENTRY_POINT(
			LinkedHashMap<TacFunction, ArrayList<TacFunction>> inOrder,
			ArrayList<TacFunction> entryPoints, TacFunction exitPoint, String msg) {
		ToolBug tb = new ToolBug();
		tb.iReason = UNRESOLVED_ENTRY_POINT;
		tb.entryPoints = entryPoints; 
		tb.exitPoint = exitPoint; 
		tb.inOrder = inOrder;
		tb.msgunresl = msg;
		
		toolBugs.add(tb);
	}

	private String toStr_UNRESOLVED_ENTRY_POINT() 
	{
		String str = "\t\tUNRESOLVED_ENTRY_POINT: \n";
		str += "\n\t msg - " + this.msgunresl;
		str += " \n\t entry points - " + this.entryPoints; 
		str += " \n\t exitPoint - " + this.exitPoint; 
		str += " \n\t inOrder - " + inOrder;
		return str;
	}

	// fields for SQL_PARSER_FAILURE;
	public String spf_symbQuery;
	public CfgNode spf_sink;
	public ControlFlow spf_cf;

	// fields for NON White list opn;
	public CfgNode nwlo_cfgN;

	
	public ArrayList<DerivationNode> cnlrdt_dnList;
	public CfgNode cnlrdt_sink;
	public ControlFlow cnlrdt_cf;
	public static void add_COULD_NOT_LOCATE_ROOT_DT(
			ArrayList<DerivationNode> dnList, CfgNode sink, ControlFlow cf) 
	{
		ToolBug tb = new ToolBug();
		tb.cnlrdt_cf = cf; 
		tb.cnlrdt_dnList = dnList;
		tb.cnlrdt_sink = sink;
		tb.iReason = COULD_NOT_LOCATE_ROOT_DT;
		toolBugs.add(tb);
	}
	
	public String toStr_COULD_NOT_LOCATE_ROOT_DT() 
	{
		String str = "COULD_NOT_LOCATE_ROOT_DT : ";
		str += " for sink - " + CfgNode.getProgLine(this.cnlrdt_sink);
		str += " for Control flow - " + CfgNode.getProgLines(this.cnlrdt_cf.getAll()); 
//		str += " for Derivation Nodes - " + CfghNode.getProgLines(this.cnlrdt_dnList = dnList;
		str += " for sink - " + CfgNode.getProgLine(this.cnlrdt_sink);
		return str;
	}

	ArrayList<DerivationNode> cnlds_dataNodes;
	CfgNode cnlds_sink;
	String cnlds_symbQuery;	
	String cnlds_msg; 
	public static void add_COULD_NOT_LOCATE_DATA_SUBTREE(
			ArrayList<DerivationNode> dataNodes, CfgNode sink,
			String symbQuery, String msg) 
	{	
		ToolBug tb = new ToolBug();
		tb.cnlds_dataNodes = new ArrayList<DerivationNode>(dataNodes);
		tb.cnlds_sink = sink;
		tb.cnlds_symbQuery = symbQuery;
		tb.iReason = COULD_NOT_LOCATE_DATA_SUBTREE;
		tb.cnlds_msg = msg;
		toolBugs.add(tb);		
	}
	
	public String toStr_COULD_NOT_LOCATE_DATA_SUBTREE() 
	{	
		String str = "COULD_NOT_LOCATE_DATA_SUBTREE";
		str += this.cnlds_msg;
		return str;
	}

}