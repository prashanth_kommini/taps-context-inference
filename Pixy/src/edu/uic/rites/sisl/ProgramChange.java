/**
 * 
 */
package edu.uic.rites.sisl;

import java.util.ArrayList;
import java.util.Comparator;

/**
 * @author prithvi
 *
 */
public class ProgramChange
implements Comparable<ProgramChange>
{
	public static final int ADDBEFORE = 1;
	public static final int REPLACE = 2;
	public static final int ADDAFTER = 3;
	
	
	private String inFile; 
	private Integer lineNumber;
	private Integer changeOperation;
	private String stmt;
	private int nodeNumber; 
	
	
	public String getInFile(){ return this.inFile; }
	public void setInFile(String file) { this.inFile = file; }
	
	
	
	public ProgramChange()
	{
		
	}
	/**
	 * @return the changeOperation
	 */
	public Integer getChangeOperation()
	{
		return changeOperation;
	}
	/**
	 * @param changeOperation the changeOperation to set
	 */
	public void setChangeOperation(Integer changeOperation)
	{
		this.changeOperation = changeOperation;
	}
	/**
	 * @return the lineNumber
	 */
	public Integer getLineNumber()
	{
		return lineNumber;
	}
	/**
	 * @param lineNumber the lineNumber to set
	 */
	public void setLineNumber(Integer lineNumber)
	{
		this.lineNumber = lineNumber;
	}
	/**
	 * @param string
	 */
	public void setStmt(String string)
	{
		this.stmt = string;
	}

	public String toString()
	{
		String fileName = null;
		StringBuffer sb = new StringBuffer(this.inFile);
    	int end = sb.lastIndexOf("/");
    	if(end >= 0)
    		fileName = sb.substring(end + 1);
    	else
    		fileName = sb.toString();
		return opToStr() + " ## " + fileName + " : " + this.lineNumber + 
			//" DT number " + this.nodeNumber + " --> " + 
			"\t" + this.stmt;
	}
	
	public boolean equals(ProgramChange pc)
	{
		boolean bRet = false;
		if(this.changeOperation == pc.changeOperation &&
				this.inFile.equals(pc.inFile) &&
				this.lineNumber.equals(pc.lineNumber) &&
				this.stmt.equals(pc.stmt))
			bRet = true; 
		
		D.pv(D.D_TEMP, " Comparing this " + this.toString() + 
				" with " + pc.toString() + " result - " + bRet);
		return bRet;
	}
	
	public String opToStr()
	{
		switch(this.changeOperation)
		{
		case ADDAFTER:
			return "ADDAFTER";
		case ADDBEFORE:
			return "ADDBEFORE";
		case REPLACE:
			return "REPLACE";
		default: 
			return "DONTKNOW";
		}
	}
	
	public int compare(Object o1, Object o2) 
	{
		ProgramChange pc1 = (ProgramChange) o1; 
		ProgramChange pc2 = (ProgramChange) o2; 
		
		if(!pc1.getInFile().equals(pc2.getInFile()))
			throw new Error("trying to compare to lines from different files");
		
		int iLine1 = pc1.getLineNumber();
		int iLine2 = pc2.getLineNumber();
		int iOp1 = pc1.getChangeOperation();
		int iOp2 = this.changeOperation;
		
		if(iLine1 < iLine2)
			return -1 ;
		else
		if(iLine1 == iLine2)
		{
			// iOp1 = REPLACE, and iOp2 = ADDAFTER | ADDBEFORE
			// iOp2 = REPLACE, and iOp2 = ADDAFTER | ADDBEFORE
			if(iOp1 < iOp2)
				return -1; 
			else
			if(iOp2 < iOp1)
				return 1; 
			else
				return 0;
		}
		else
			return 1;
	}
	
	
	public int compareTo(ProgramChange o) 
	{
		ProgramChange pc1 = (ProgramChange) o; 
		
		if(!pc1.getInFile().equals(this.inFile))
			throw new Error("trying to compare to lines from different files");
		
		int iLine1 = pc1.getLineNumber();
		int iLine2 = this.lineNumber;
		
		if(iLine1 < iLine2)
			return 1 ;
		else
		if(iLine1 == iLine2)
		{
			int iNumber1 = pc1.getDTNumber();
			if(iNumber1 > this.nodeNumber)
				return 1; 
			else
			if(iNumber1 < this.nodeNumber)
				return -1;
			else
				return 0;
		}
		else
			return -1;
	}
	
	public String getStmt() 
	{
		return this.stmt;
	}
	
	public int getDTNumber() 
	{
		return this.nodeNumber;
	}
	
	public void setDTNumber(int nodeNumber) 
	{
		this.nodeNumber = nodeNumber;
	}
	
	public String getLocation() {
		return this.inFile + ":" + this.lineNumber;
	}
	public static ArrayList<ProgramChange> getStmtsForOp(
			ArrayList<ProgramChange> pc1,
			int operation) {
		ArrayList<ProgramChange> forOps = new ArrayList<ProgramChange>();
		
		for(ProgramChange pc : pc1)
		{
			if(pc.changeOperation == operation)
				forOps.add(pc);
		}
		return forOps;
	}
	
	private boolean conflict;
	public void setConflictCause() {
		conflict = true;
	}
	public boolean getConflictCause() {
		return conflict;
	}
	public String getLocationWithLhs() {

		String loc = this.getLocation();
		String lhs = "";
		int iStart = stmt.indexOf('=');
		if(iStart != -1)
			lhs += stmt.substring(0, iStart);
		
		return loc + lhs;
	}
}
