/**
 * 
 */
package edu.uic.rites.sisl;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.io.*;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.*;
import sql92.SimpleNode;
import edu.uic.rites.sisl.sql.*;
import at.ac.tuwien.infosys.www.pixy.Dumper;
import at.ac.tuwien.infosys.www.pixy.MyOptions;
import at.ac.tuwien.infosys.www.pixy.analysis.dep.DepGraph;
import at.ac.tuwien.infosys.www.pixy.analysis.dep.DepGraphNode;
import at.ac.tuwien.infosys.www.pixy.analysis.dep.DepGraphNormalNode;
import at.ac.tuwien.infosys.www.pixy.analysis.dep.DepGraphOpNode;
import at.ac.tuwien.infosys.www.pixy.analysis.inter.Context;
import at.ac.tuwien.infosys.www.pixy.analysis.inter.InterAnalysisInfo;
import at.ac.tuwien.infosys.www.pixy.analysis.inter.InterAnalysisNode;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.*;
import at.ac.tuwien.infosys.www.pixy.conversion.*;

/**
 * @author prithvi
 *
 */
public class DerivationTree
{
	// variable that holds query
	private DerivationNode root; 
	// easy reference to first leaf node;
	// leaves are connected just like B-trees. 
	private DerivationNode firstLeaf;
	// a collection of nodes that this derivation tree is built on
	private ArrayList<CfgNode> associatedCfgNodes; 
	private Set<String> queryVars;
	private Set<DerivationNode> queryVarsDNs;
	private HashMap<String, String> varContexts;
	private ArrayList<ArrayList<Object>> varContextsDNs;
	public HashMap<String, ArrayList<DerivationNode>> varsMap;

	public void setQueryVarsDNs(Set<DerivationNode> queryVarsDNs){ this.queryVarsDNs = queryVarsDNs; }
	public void setQueryVars(Set<String> queryVars){ this.queryVars = queryVars; }
	public void setRoot(DerivationNode dn) { root = dn; }
	public void setFirstLeaf(DerivationNode leaf) { firstLeaf = leaf; }
	public void setAssocCfgNodes(ArrayList<CfgNode> assCfgs){ associatedCfgNodes = assCfgs; }

	public HashMap<String, ArrayList<DerivationNode>> getVarsMap(){ return varsMap; }
	public HashMap<String, String> getVarContextsMap(){ return this.varContexts; }
	public ArrayList<ArrayList<Object>> getVarContextsMapDNs(){ return this.varContextsDNs; }
	public DerivationNode getRoot() { return root; }
	public DerivationNode getFirstLeaf() { return firstLeaf; }
	public ArrayList<CfgNode> getAssocCfgNodes(){ return associatedCfgNodes; }

	private String symbQuery;
	private String newQuery;
	private ControlFlow thisFlow;
	private DepGraph depGraph;
	private CfgNode sink;
	public static ArrayList<DerivationNode> visitedIds;
	public int dtID;

	public ArrayList<DerivationNode> getQueryVarsDNs(){
		ArrayList<DerivationNode> queryVarsListDNs = new ArrayList<DerivationNode>(this.queryVarsDNs);
		return queryVarsListDNs;
	}

	public ArrayList<String> getQueryVars(){
		ArrayList<String> queryVarsList = new ArrayList<String>(this.queryVars);
		return queryVarsList;
	}

	public void setVarContextsDNs(){
		System.out.println("setVarContextsDNs called");
		ArrayList<ArrayList<Object>> localVarContextsDNs = new ArrayList<ArrayList<Object>>();
		for(DerivationNode node: queryVarsDNs){		
			for(String varName: this.getVarContextsMap().keySet()){
				String reversedVarName = varNameConversionReverse(varName);
				if(node.getId().toString().equals(reversedVarName)){
					System.out.println("DNtoVarName match found - DnName: " + node.getId().toString() + " varName: " + reversedVarName);
					ArrayList<Object> tuple = new ArrayList<Object>();
					tuple.add(node);
					tuple.add(this.getVarContextsMap().get(varName));
					localVarContextsDNs.add(tuple);
				}
				else{
					System.out.println("DNtoVarName match NOT found - DnName: " + node.getId().toString() + " varName: " + reversedVarName);
				}
			}
		}
		System.out.println("VarContextsDNs was set to: ");
		for(ArrayList<Object> varToContext: localVarContextsDNs){
			DerivationNode var = (DerivationNode) varToContext.get(0);
			ArrayList<String> sanits = this.getSanitFunctions(var);
			varToContext.add(sanits);
			String cfgNodeType = var.getCfgNode()!= null ? "" + var.getCfgNode().getClass(): " null ";
			System.out.println("***Var is: " + var.getId().toString() + " and Context is: " + varToContext.get(1) + " Sanitis are: " + varToContext.get(2).toString() + " CfgNodeType: " + cfgNodeType + "***");
		}
		this.varContextsDNs = localVarContextsDNs;
	}

	//mali
	private ArrayList<String> getSanitFunctions(DerivationNode node){//for each leaf, list the sanitization functions applied on the value
		DerivationNode currentNode = node;
		ArrayList<String> funcList = new ArrayList<String>();
		ArrayList<DerivationNode> nodesInPathToRoot = new ArrayList<DerivationNode>();
		System.out.println("Nodes in path from "+ node.getId().toString() + " to Root");
		while( currentNode != null && ( currentNode.getCfgNode() != null || currentNode.getId().getVariable().isTemp() ) ){
			CfgNode cfgn = currentNode.getCfgNode();
			if(cfgn != null){
				System.out.println("currentNode is: " + currentNode.getId().toString() + " CFGNodeType is: " + cfgn.getClass()); 
			}
			else{
				System.out.println("currentNode is: " + currentNode.getId().toString() + " CFGNodeType is: null but isTemp"); 
			}
			if(cfgn != null){
				if(cfgn instanceof CfgNodeCall){
					System.out.println("");
					CfgNodeCall cfgnc = (CfgNodeCall)cfgn;
					if(SanitizationFunctions.wpSanitFunctions.contains(cfgnc.getFunctionNamePlace().toString())){
						funcList.add(cfgnc.getFunctionNamePlace().toString());
					}
				}
				else if(cfgn instanceof CfgNodeCallBuiltin){
					CfgNodeCallBuiltin cfgnbf = (CfgNodeCallBuiltin)cfgn;
					if(SanitizationFunctions.builtinSanitFunctions.contains(cfgnbf.getFunctionName())){
						funcList.add(cfgnbf.getFunctionName());
					}
				}
				else if(cfgn instanceof CfgNodeCallRet){
					CfgNodeCall callNode = ((CfgNodeCallRet)cfgn).getCallNode();
					if(SanitizationFunctions.wpSanitFunctions.contains(callNode.getFunctionNamePlace().toString())){
						funcList.add(callNode.getFunctionNamePlace().toString());
					}
				}
			}
			currentNode = currentNode.getParent();
		}

		if(currentNode == null){
			System.out.println("Exited while due to reaching root");
		}
		else if( currentNode.getCfgNode() == null ){
			System.out.println("Exited while due to " + currentNode.getId().toString() + "'s CFGNode being null");
		}
		else{
			System.out.println("Exited due to unknown reason");
		}
		return funcList;
	}

	public void setVarContexts(){
		HashMap<String, String> localVarContexts = new HashMap<String, String>();
		for(String var: this.getQueryVars()){
			localVarContexts.put(var, "");
		}
		System.out.println("Looping through varContexts");
		// this.varContexts = localVarContexts;
		// for(String var: this.varContexts.keySet()){
		//  System.out.println("Var is: " + var + " and Context is: " + varContexts.get(var));
		// }

		HTMLParser parser = new HTMLParser();
		parser.setVars(this.getQueryVars());
		File file = this.getCorrespQueryFile();
		if(file != null ){
			String htmlToParse = readFileInToString(file);
			try{
				parser.parse(htmlToParse);
			}
			catch(Exception e){
				System.out.println("ERROR: "+ e + e.getStackTrace());
			}
		}
		else{
			System.out.println("QueryFile not found");
		}
		this.varContexts = parser.getVarContextsMap();
	}

	public File getCorrespQueryFile(){
		String str = "query_" + this.dtID;
		System.out.println("getCorrespQueryFile was called and the dtID is: " + this.dtID + " str is: " + str);
		File dir = new File(MyOptions.pixy_home + "/queries/");
		File[] listOfFiles = dir.listFiles();
		if (listOfFiles != null) {
			System.out.println("listofFiles was not null");
			for(File eachFile : listOfFiles){
				System.out.println("Filename is : " + eachFile.getName());
				if(eachFile.getName().startsWith(str)){
					System.out.println("Found query file");
					return eachFile;
				}
				else{
					System.out.println("File Name doesnt start with: " + str + "Filename is : " + eachFile.getName());
				}
			}
		}
		else {
			System.out.println("listofFiles was null");
			return null;
		}
		return null;
	}
	
	public static String readFileInToString(File file) {
		//create file object
		BufferedInputStream bis = null;
		String strFileContents = "";
		try{
			//create FileInputStream object
			FileInputStream fis = new FileInputStream(file);
			//create object of BufferedInputStream
			bis = new BufferedInputStream(fis);
			//create a byte array
			byte[] contents = new byte[1024];
			int bytesRead = 0;
			while( (bytesRead = bis.read(contents)) != -1){
				strFileContents += new String(contents, 0, bytesRead);
			}
//          System.out.print(strFileContents);
		}
		catch(FileNotFoundException e){
			System.out.println("File not found" + e);
		}
		catch(IOException ioe)
		{
			System.out.println("Exception while reading the file " + ioe);
		}
		finally
		{
			//close the BufferedInputStream using close method
			try{
				if(bis != null)
				bis.close();
			}
			catch(IOException ioe){
				System.out.println("Error while closing the stream :"+ ioe);
			}
		}
		return strFileContents;
	}

	public static DerivationTree makeDerivationTree(ControlFlow cf, ArrayList<CfgNode> cfgs, 
			CfgNode sink, DepGraph depGraph) throws SISLException
	{    
		System.out.println("makeDerivationTree() was called");
		if(cfgs == null) 
		{
			D.pf(D.D_DT, " Empty list of Cfg nodes passed to makeDerivationTree");
			return null;
		}
		
		D.pr(D.D_TEMP, "Transition_Look: makeDerivationTree all cfg nodes - " + CfgNode.getProgLinesSorted(cfgs));
		cfgs.remove(sink);
		DerivationTree dt = new DerivationTree();
		dt.setAssocCfgNodes(cfgs);
		dt.depGraph = depGraph;
		dt.sink = sink;
		
		DerivationTree.visitedIds = new ArrayList<DerivationNode>();
		ArrayList<DerivationNode> dn = new ArrayList<DerivationNode>();
		boolean bRoot = true; 
		// first element is the root; 
		for(CfgNode cn : cfgs){
			dt.toDN(cn, dn, cf);
		}
		for(DerivationNode dnTmp : dn){
			D.pr(D.D_TEMP, "################ Before branch Derivation node - " + dnTmp.dotName());
		}
		DerivationNode rootDN;
		rootDN = locateRootOfTree(dn, sink, cf);
		dt.setRoot(rootDN);
		rootDN.setBranches(dn, cfgs);
		for(DerivationNode dnTmp : rootDN.getBfOrder()){
			D.pr(D.D_TEMP, "################ Derivation node - " + dnTmp.dotName());
		}
		rootDN.initPlaceholderArgs();
		DerivationTree.dumpDerivationTree(dt,  "__beforeSymEval__");
		dt.symEvalArith();
		rootDN.adjustLeafStatus();
		ArrayList<DerivationNode> leaves = rootDN.getLeavesInOrder();
		DerivationNode leaf = null;     
		int iLeafs = leaves.size();
		for(int i = 0; i < iLeafs; i++){
			leaf = leaves.get(i);
			if(i > 0)
				leaf.setImmPreds(leaves.get(i-1));
			if(i < iLeafs - 1)
				leaf.setImmSuccs(leaves.get(i+1));
			
			D.pd(D.D_DT, "Transition_Look: LEAF In order - " + leaf.toString());
		}
		dt.setFirstLeaf(leaves.get(0));
		DerivationTree.dumpDerivationTree(dt,  "__afterSymEval__");
		dt.setVarsMap(rootDN.getBfOrder());
		System.out.println("VarsMap loop START");
		for(String var: dt.getVarsMap().keySet()){
			System.out.println("Var is: " + var);
		}
		System.out.println("VarsMap loop END");
		return dt; 
	}

	private void setVarsMap(ArrayList<DerivationNode> dnsList){
		// 1. Find nodes which are variable + non-terminals + non-temps
		ArrayList<DerivationNode> allVars = findAllVars(dnsList);
		// 2. Get their the leaves for allVars
		this.varsMap = getVarsLeaves(allVars);
	}

	private static ArrayList<DerivationNode> findAllVars(ArrayList<DerivationNode> dnsList){
		ArrayList<DerivationNode> allVars = new ArrayList<DerivationNode>();
		for(DerivationNode dn: dnsList){
			if( dn.getId().isVariable() && dn.isNT()){
				allVars.add(dn);
			}           
		}
		return allVars;
	}

	private static HashMap<String, ArrayList<DerivationNode>> getVarsLeaves(ArrayList<DerivationNode> allVars){
		HashMap<String, ArrayList<DerivationNode>> varsMap = new HashMap<String, ArrayList<DerivationNode>>();
		for(DerivationNode var: allVars){
			ArrayList<DerivationNode> leaves = var.getLeavesInOrder();
			try{
				varsMap.put(dnToQueryString(var), leaves);
			}
			catch(SISLException se){
					System.out.println(se.getMessage());
			}
		}
		return varsMap;
	}

	// Prasanth added method START
	private static String dnToQueryString(DerivationNode dn) throws SISLException {
		TacPlace id = dn.getId();
		String printString = "In toXSSAmiable ";
		HashMap<DerivationNode, ArrayList<CfgNode>> funcSeq = new HashMap<DerivationNode, ArrayList<CfgNode>>();
		if(id.isLiteral()){
			printString += "Case: IsLiteral ";
			System.out.println(printString);
			return id.toString();
		}
		else if( id.isVariable() && dn.getCfgNode() != null){
			printString += "Case: isVariable ";
			String varString = id.toString();
			String retVal = varNameConversion(varString);
			return retVal;
		}
		else if( id.isVariable() ){
			Variable v = (Variable)id;
			printString += "Case: isVariable ";
			System.out.println(printString);
			return v.getName();
		}
		else if(id.isConstant()){
			printString += "Case: isConstant";
			System.out.println(printString);
			return id.toString();
		}
		else{
			printString += "Case: Neither constant/variable/literal";
			System.out.println(printString);
			throw new Error(" toXSSAmiable ---- unknown for " + id.toString());
		}
	}
	// Prasanth added method END

	private static DerivationNode locateRootOfTree(ArrayList<DerivationNode> dnList,
			CfgNode sink, ControlFlow cf) throws SISLException 
	{
		// find which arg holds query value and assignment to it forms the root of derivation tree
		TacPlace tp= null;
		if (sink instanceof CfgNodeCallBuiltin){
			 CfgNodeCallBuiltin cnc = (CfgNodeCallBuiltin) sink;
			 TacActualParam tap = cnc.getParamList().get(0);
			 tp = tap.getPlace();
		}
		else{
			CfgNodeEcho cnc = (CfgNodeEcho) sink;
			//TacActualParam tap = cnc.getParamList().get(0);
			tp = cnc.getPlace();
		}
		Variable vQueryValue = null;
		
		if(tp.isVariable()){
			vQueryValue = tp.getVariable();
			//D.pr2("phi LHS : "+vQueryValue.isPhiLhs());
		}
		else{
			//throw new Error(" query value is not in a variable at node " + sink);

			D.pv(D.D_TEMP, "Query value is not in a variable at node sink: " + sink);
			SISLException sisl = new SISLException(

				SISLException.BAIL_OUT,
				"Query value is not in a variable at node sink: "
				+ sink
				+" Prog: " +
				CfgNode.getProgLineCode(sink)
			);
			ToolBug.add_UNCLASSIFIED_YET(sisl);
			throw sisl;
		}
		
		D.pv(D.D_TEMP, " query value in variable - " + vQueryValue);
		
		DerivationNode dnRoot = DerivationNode.getDfn(vQueryValue, dnList);
		if(dnRoot != null)
			return dnRoot; 
		//else if(dnRoot == null){
			
		//}
		TacPlace phiSupport = DerivationTree.getPhiLhsMap(sink, cf, vQueryValue);
		D.pv(D.D_TEMP, " phi support for Cfg Node " + sink + " is - " + phiSupport);
		ToolBug.add_COULD_NOT_LOCATE_ROOT_DT(dnList, sink, cf);
		throw new SISLException(SISLException.BAIL_OUT, "BAIL_OUT");
	}

	public DerivationNode toDN(CfgNode cn, ArrayList<DerivationNode> dnList, ControlFlow cf) throws SISLException
	{
		DerivationNode dn = null;
		DerivationNode child = null;
		ArrayList<DerivationNode> children = new ArrayList<DerivationNode>(); 
		TacPlace tpLeft = null;
		String op = null;
		
		if(cn instanceof CfgNodeAssignArray)
		{
			D.pv(D.D_TEMP, "Processing CfgNodeAssignArray " + cn.toString());

			tpLeft = cn.getLeft();
			TacPlace tpRight = null;
			ArrayList<Variable> all = cn.getRightToRename();
			if(all != null)
			{
				tpRight = all.get(0);
				D.pv(D.D_TEMP, "...........getting the right - " + tpRight.toString());
				children.add(createDerivationNode(tpRight));
			}
		}
		else
		if(cn instanceof CfgNodeAssignBinary)
		{
			D.pv(D.D_TEMP, "Processing CfgNodeAssignBinary " + cn.toString());
			
			CfgNodeAssignBinary cnab = (CfgNodeAssignBinary) cn;
			TacPlace tpLeftOperand = cnab.getLeftOperand();
			TacPlace tpRightOperand = cnab.getRightOperand();
			
			// check if we need phi mapping
			TacPlace tpTmp = null;
			if(tpLeftOperand.isVariable() && tpLeftOperand.getVariable().isPhiLhs())
			{
				tpTmp = DerivationTree.getPhiLhsMap(cn, cf, tpLeftOperand);
				if(tpTmp != null)
				{
					tpLeftOperand = tpTmp;
				}
			}

			DerivationNode leftC = createDerivationNode(tpLeftOperand);
			if(tpTmp != null)
			{
				// phi mappings were used in this cocat operation.
				// store this information so that we could use 
				// merge args for replaced var.
				leftC.setPhiMapFor(tpTmp.getVariable());
			}
			
			tpTmp = null;
			if(tpRightOperand.isVariable() && tpRightOperand.getVariable().isPhiLhs())
			{
				tpTmp = DerivationTree.getPhiLhsMap(cn, cf, tpRightOperand);
				if(tpTmp != null)
					tpRightOperand = tpTmp;
			}
			DerivationNode rightC = createDerivationNode(tpRightOperand);

			if(tpTmp != null)
			{
				// phi mappings were used in this cocat operation.
				// store this information so that we could use 
				// merge args for replaced var.
				rightC.setPhiMapFor(tpTmp.getVariable());
			}

			
			int iOp = cnab.getOperator();
			if(iOp != TacOperators.CONCAT)
			{
				D.pv(D.D_DT, "Operator for " + cnab.toString() + " is not concat");
				if(iOp == TacOperators.MINUS || 
						iOp == TacOperators.PLUS || 
						iOp == TacOperators.MULT)
					D.pvt(D.D_TEMP, "Gotcha...........dont expand " +  cn);
				tpLeft = cn.getLeft();
			}
			else
			{
				op = TacOperators.opToName(iOp);
	
				// check if left or right operands are array instances
				// statically, we can only cover a limited number of such 
				// cases - only those where indices are constant
				addArraySupport(tpLeftOperand, leftC, dnList, cn);
				addArraySupport(tpRightOperand, rightC, dnList, cn); 
				
	
				children.add(leftC);
				children.add(rightC);
				D.pv(D.D_DT, "...........getting the left operand - " + tpLeftOperand.toString());
				D.pv(D.D_DT, "...........getting the right operand - " + tpRightOperand.toString());
				
				tpLeft = cn.getLeft();
			}
		}
		else
		if(cn instanceof CfgNodeAssignRef)
		{
			D.pv(D.D_TEMP, "Processing CfgNodeAssignRef " + cn.toString());

			tpLeft = cn.getLeft();
		}
		else
		if(cn instanceof CfgNodeAssignSimple)
		{
			D.pv(D.D_TEMP, "Processing CfgNodeAssignSimple " + cn.toString());

			tpLeft = cn.getLeft();
			CfgNodeAssignSimple cnas = (CfgNodeAssignSimple) cn;
			
			TacPlace tpTmp = null;
			TacPlace tpRight = cnas.getRight();;
			// check if we need phi mapping
			if(tpRight.isVariable() && tpRight.getVariable().isPhiLhs())
			{           
				tpTmp = DerivationTree.getPhiLhsMap(cn, cf, tpRight);
				if(tpTmp != null)
					tpRight = tpTmp;
			}

			DerivationNode node = createDerivationNode(tpRight);
			if(tpTmp != null)
				node.setPhiMapFor(tpTmp.getVariable());
	
			D.pv(D.D_DT, "...........getting the right - " + tpRight.toString());
			children.add(node);
		}
		else
		if(cn instanceof CfgNodeAssignUnary)
		{
			D.pv(D.D_TEMP, "Processing CfgNodeAssignUnary " + cn.toString());
			CfgNodeAssignUnary cnau = (CfgNodeAssignUnary) cn; 
			
			TacPlace tpTmp = null;
			tpLeft = cnau.getLeft();
			TacPlace tp = cnau.getRight();
			// check if we need phi mapping
			if(tp.isVariable() && tp.getVariable().isPhiLhs())
			{           
				tpTmp = DerivationTree.getPhiLhsMap(cn, cf, tp);
				if(tpTmp != null)
				{
					tp = tpTmp;
				}
			}
			DerivationNode node =createDerivationNode(tp); 
			if(tpTmp != null)
				node.setPhiMapFor(tpTmp.getVariable());
			
			children.add(node);
		}
		else
		if(cn instanceof CfgNodeCallBuiltin)
		{
			D.pv(D.D_DT, "Processing CfgNodeCallBuiltin " + cn.toString());

			CfgNodeCallBuiltin cncb = (CfgNodeCallBuiltin) cn;
			// temp variable holds the return value
			tpLeft = cncb.getTempVar();
			
			if(isSummarized(cncb))
			{
				D.pv(D.D_TEMP, " Summarized function - " + cncb.getFunctionName());
				SISLException sisl = new SISLException(SISLException.BAIL_OUT, 
						" Unhandled summarized function - " + 
						cncb.getFunctionName() + " in " + CfgNode.getProgLineCode(cncb));
				ToolBug.add_UNCLASSIFIED_YET(sisl);
				throw sisl;
				//System.exit(1);
			}
		}
		else
		if(cn instanceof CfgNodeCall)
		{
			D.pv(D.D_DT, "Processing CfgNodeCall " + cn.toString());
			CfgNodeCall cnc = (CfgNodeCall) cn;
			// temp variable holds the return value
			// tpLeft = cnc.getTempVar();           
			tpLeft = null;
		}
		else
		if(cn instanceof CfgNodeCallPrep)
		{           
			// create dummy nodes to assign formal arguments to actual arguments
			CfgNodeCallPrep cncp = (CfgNodeCallPrep) cn;
			List<TacActualParam> actuals = cncp.getParamList();
			ArrayList<TacPlace> actualsPlaces = new ArrayList<TacPlace>();
			
			boolean bInDepGraph = isActualDependency(cn);
			if(!bInDepGraph)
			{
				D.pr(D.D_TEMP, " detected that this call - " + cn + " is not in the dep graph...only return...not expanding formal to actual");
			}
			else
			{
			
			
			
			List<TacFormalParam> formals = cncp.getCallee().getParams();
			
			TacPlace left = null;
			TacPlace right = null;
			
			int index = 0;
			// actuals may not be same as formals 
			// get the default node if thats the case 
			D.pv(D.D_TEMP, " processing a call prep node " + cn);
			D.pv(D.D_TEMP, " actuals - ");
			for(TacActualParam tap : actuals)
				D.pv(D.D_TEMP, " \t actual - " + tap.toString());
			D.pv(D.D_TEMP, "formals - ");
			for(TacFormalParam tfp : formals)
			{
				D.pv(D.D_TEMP, "\t formal - " + tfp.toString());
				if(tfp.hasDefault())
					D.pv(D.D_TEMP, "\t\t has default - " + tfp.getDefaultCfg());
			}

			int iFormals = formals.size();
			int iActuals = actuals.size();

			for(TacActualParam tap : actuals)
				actualsPlaces.add(tap.getPlace());

			if(iFormals != iActuals)
			{   
				fillupDefaultValues(formals, actuals, actualsPlaces);
			}
			
			for(TacFormalParam formal : formals)
			{
				children = new ArrayList<DerivationNode>(); 
				
				TacPlace rightTmp = null;
				Variable vLeft = formal.getVariable();
				vLeft.setFormal(true);
				
				right = actualsPlaces.get(index++); 
				if(right.isVariable() && right.getVariable().isPhiLhs())
				{
					rightTmp = getPhiLhsMap(cn, cf, right);
					if(rightTmp != null)
						right = rightTmp;
				}
				
				dn = createDerivationNode(vLeft);
				DerivationNode c = createDerivationNode(right);
				if(rightTmp != null)
					c.setPhiMapFor(rightTmp.getVariable());
				
				c.setParent(dn);
				dn.setChild(c);
				dn.setCfgNode(cncp);
				
				dn.setActual2Formal(true);
				
				D.pi(D.D_DT, " set actual2formal adding derivation node = " + dn.toString() + " cfg node - " + cncp);
				dnList.add(dn);
			}
			}
			tpLeft = null;
		}
		else
		if(cn instanceof CfgNodeCallRet)
		{
			D.pr(D.D_TEMP, "Processing CfgNodeCallRet " + cn.toString());
			CfgNodeCallRet cncr = (CfgNodeCallRet) cn; 
			D.pr(D.D_TEMP, "Return variable for method - " + cncr.getFileName() + ":" + 
					cncr.getOrigLineno() + " " + 
					cncr.getCallNode().getCallee().getRetVar().getName());
			cncr.setRetVar(cncr.getCallNode().getCallee().getRetVar());
			D.pr(D.D_TEMP, " predec = " + cncr.getSuccessor(0));
			
			// introduce a dummy variable that assigns the return value of the 
			// called function to temporary variable in the current scope           
			tpLeft = cncr.getTempVar();
			dn = createDerivationNode(tpLeft);
			dn.setCfgNode(cncr);
			dn.setReturnAssign();

			TacPlace tp = cncr.getCallNode().getCallee().getRetVar();
			DerivationNode c = createDerivationNode(tp);
			c.setParent(dn);
			dn.setChild(c);
			
			D.pr(D.D_TEMP, " adding return assignment - " + tpLeft + " = " + tp );
			dnList.add(dn);
			
			tpLeft = null;
		}
		else
		if(cn instanceof CfgNodeCallUnknown)
		{
			D.pr(D.D_TEMP, "Unhalded CfgNodeCallUnknown" + cn.toString());
			
		}
		else
		{
			D.pr(D.D_TEMP, " cfgToDN not handled - " + cn.toString());
		}
		
		if(tpLeft != null)      
		{
			dn = createDerivationNode(tpLeft, op);

			dn.setCfgNode(cn);
			if(children != null)
			{
				for(DerivationNode c : children)
				{
					c.setParent(dn);
					dn.setChild(c);
				}
			}
			
			dnList.add(dn);
		}
		return dn; 
	}

	private boolean isActualDependency(CfgNode cn) 
	{
		boolean bFound = false;
		// ensure that there was a call to invoked in the dep graph. 
		// because of extending the cfg nodes, we have spurious calls
		// in longer run need to correct the algo.
		// int iExitL = exitT.getOrigLineno();
		List<CfgNode> allRealDeps = this.depGraph.getAllCfgNodes(this.depGraph.getNodes(), true);
		D.pr(D.D_PATH, " all nodes in depGraph - " + allRealDeps);
		D.pr(D.D_TEMP, "Checking if this exit was present in the dep nodes - " + cn);
		// unfortunately following doesnt work/ equality in CfgNode? 
		//if(allRealDeps.contains(cn)) 
		for(CfgNode cnd : allRealDeps)
		{
			if(cnd.equalsX(cn))
			{
				bFound = true;
				break;
			}
		}
		
		return bFound;
	}

	private boolean isSummarized(CfgNodeCallBuiltin cncb) {
		String[] summarized = {"vsprintf", "implode", "join", "eval"};
		String calledF = cncb.getFunctionName();
		for(String s : summarized)
		{
			if(s.equals(calledF))
				return true;
		}
		
		return false;
	}

	private List<TacActualParam> fillupDefaultValues(
			List<TacFormalParam> formals, List<TacActualParam> actuals, 
			ArrayList<TacPlace> actualsPlaces) {

		int iFormals = formals.size();
		for(int i = actuals.size(); i < iFormals; i++)
		{
			TacFormalParam tfp = formals.get(i); 
			// TODO: one case of failure would be get_func_args()
			if(!tfp.hasDefault())
				throw new Error(" doesnt have enough actual arguments, and formals doesnt support default");
			
			Cfg def = tfp.getDefaultCfg();
			List<CfgNode> defs = def.getAll();
			D.pv(D.D_TEMP, " for formal param - " + tfp.toString());
			for(CfgNode cn : defs)
			{
				if(cn instanceof CfgNodeAssignSimple)
				{
					actualsPlaces.add(i, ((CfgNodeAssignSimple) cn).getRight());
					D.pv(D.D_TEMP, " cfg node in default param " + cn);
				}
				else
				{
					throw new Error(" unhandled default param statement - " + cn);
				}
			}
		}
		return null;
	}

	private boolean handledArrayCase(TacPlace var)
	{
		boolean bRet = false;
		// we are only interested in array variables.
		if(!var.isVariable())
			return bRet; 
		
		Variable v = var.getVariable();
		
		// and only in array elements being referenced
		// check if this array is a global, if so, again, we cannot statically handle
		// check if all indices are static, we cannot handle any dynamic index
		// statically
		if(!v.isArrayElement() || v.isGlobal() || v.hasNonLiteralIndices())
			return bRet;
		
		return true;
	}
	
	private void addArraySupport(TacPlace var, 
			DerivationNode varNode, ArrayList<DerivationNode> allDNs, CfgNode cn) 
	{
		if(!handledArrayCase(var))
			return;
		
		D.pv(D.D_TEMP, " need to process - " + var + " in node - " + cn);       
		
		Variable v = var.getVariable();
		Variable vOrig = v.getOrig();
		if(vOrig == null)
			vOrig = v; 
		
		D.pv(D.D_TEMP, " original for " + v + " is " + vOrig);
		DepGraphNormalNode dgnn = null;
		for(DepGraphNode dgn : this.depGraph.bfIterator())
		{
			if(dgn instanceof DepGraphNormalNode)
			{
				dgnn = (DepGraphNormalNode) dgn;
				TacPlace tp = dgnn.getPlace();
				if(tp.equals(vOrig))
				{
					D.pv(D.D_TEMP, " there is a match in tacplace and original");
					break;
				}
				D.pv(D.D_TEMP, " dgnn - " + dgnn);
			}
		}
		// locate the children literal definition, if any
		TacPlace tpdgnnT = null;
		if(dgnn != null)
		{
			DepGraphNode dgnT = dgnn;
			while(dgnT != null)
			{
				List<DepGraphNode> list = depGraph.getSuccessors(dgnT);
				if(list != null && list.size() > 0)
				{
					dgnT = list.get(0);
					if(dgnT instanceof DepGraphNormalNode)
					{
						DepGraphNormalNode dgnnT = (DepGraphNormalNode) dgnT;
						tpdgnnT = dgnnT.getPlace();
						D.pv(D.D_TEMP, " place for child" + tpdgnnT);
						if(tpdgnnT.isLiteral())
						{
							D.pv(D.D_TEMP, " found the required definition");
							break;
						}
						else
						{
							if(!handledArrayCase(tpdgnnT))
								return;
						}
					}
					else
						return;
				}
				else
				{
					dgnT = null;
				}
				D.pv(D.D_TEMP, " printing the children - " + dgnT);
			}
		}
		
		
		if(tpdgnnT == null) return;

		D.pv(D.D_TEMP, " introduce a new DN statement that maps - " + var + " to " + tpdgnnT);
		
		DerivationNode dn = createDerivationNode(tpdgnnT);
		dn.setParent(varNode);
		varNode.setChild(dn);
		allDNs.add(dn);
	}

	private static TacPlace getPhiLhsMap(CfgNode cn, ControlFlow cf, TacPlace tp) 
	{
		D.pv(D.D_TEMP, " getting philhs map for stmt - " + cn + " and tac place  " + tp);
		TacPlace tpReturn = null;
		if(tp.isVariable())
		{
			Variable v = tp.getVariable();
			
			// D.pr2("gggggggggggggg "+ cf.getPhiLhsToRhsMap(cn, v)+" variable "+v);
			// TODO: think through where else we need this
			if(v.isPhiLhs())
			{	
				// D.pr2("YESSSSS"); 
				tpReturn = cf.getPhiLhsToRhsMap(cn, v);
				D.pv(D.D_TEMP, " #### replacing phi rhs arg " + tp + 
						" with - " + tpReturn);
			}
		}
		
		D.pv(D.D_TEMP, " return value - " + tpReturn);
		return tpReturn;
	}

	public static DerivationNode createDerivationNode(TacPlace tp, String operator)
	{
		DerivationNode dn = createDerivationNode(tp);
		dn.setOperator(operator);
		return dn;
	}
	
	public static DerivationNode createDerivationNode(TacPlace tp)
	{
		DerivationNode dn = null;
		if(tp == null) return null;
		
		if(tp.isVariable())
		{
			dn = new DerivationNodeNonTerminal(tp);
		}
		else
		if(tp.isConstant() || tp.isLiteral())
		{
			boolean bNum = false; 
			if(tp.isLiteral())
			{
				Literal lt = (Literal) tp; 
				bNum = lt.isCompletelyNumeric();
			}
			dn = new DerivationNodeTerminal(tp, bNum);
			D.pv(D.D_DT, "########## created a Derivation node - " + dn.toString() + " id = " + 
					dn.getId() + " is constant - " + tp.isConstant() + " is literal - " + 
					tp.isLiteral());
		}
		else
		{
			throw new Error("createDerivationTree called with - " + tp.toString());
		}
		
		return dn;
	}

	public ArrayList<DerivationNode> bfOrder()
	{
		ArrayList<DerivationNode> nodes = this.root.getBfOrder();
		nodes.add(0, root);
		return nodes;
	}
	
	public ArrayList<DerivationNode> dfOrder()
	{
		ArrayList<DerivationNode> df = new ArrayList<DerivationNode>();
		return df;
	}

	public void writeDotUnique(String graphName, Set fillUs, boolean shortName, Writer outWriter) throws IOException {
			
		outWriter.write("digraph cfg {\n  label=\"");
		outWriter.write(Dumper.escapeDot(graphName, 0));
		outWriter.write("\";\n");
		outWriter.write("  labelloc=t;\n");
		
		// print nodes
		int idCounter = 0;
		HashMap<DerivationNode, Integer> node2Int = new HashMap<DerivationNode,Integer>();

		ArrayList<DerivationNode> bf = this.bfOrder();
		for (DerivationNode dn : bf) 
		{
			node2Int.put(dn, ++idCounter);
			
			String styleString = "";
			if (fillUs.contains(dn)) {
				styleString = ",style=filled";
			}

			// if (!dn.isNT()) {
			// 	styleString = ",style=filled,color=lightblue";
			// }

			if(dn.isData())
			{
				styleString = ",style=filled,color=lightgreen";
			}
			
			if(dn.getDataSubtree())
			{
				styleString = ",style=filled,color=orange";
			}
			
			if(dn.isBorrowee())
			{
				styleString = ",style=filled,color=turquoise";              
			}
		
			if(dn.getPhiMapFor() != null)
			{
				styleString = ",style=filled,color=lightblue";
			}
			
			if(dn.isQuestionMark())
			{
				styleString  = ",style=filled,color=yellow";
			}
			
			String shapeString = "shape=ellipse";
			if (dn == this.root) {
				shapeString = "shape=box";
			}
			else
			if(dn.isQuestionMark()){
				shapeString = "shape=circle";
			}
			
			String name;
			if (shortName) {
				name = dn.dotNameShort();
			} else {
				name = dn.dotName();
			}
			outWriter.write("  n" + idCounter + " [" + shapeString + ", label=\"" + 
					name + "\"" + styleString + "];\n");
			
		}
		
		// print edges
		List<String> lines = new LinkedList<String>();
		for (DerivationNode from : bf) 
		{
			DerivationNode parent = from.getParent();
			if(parent != null)
			{
//              D.p(D.D_TEMP, "Node - " + from.dotName() + " adding parent " + parent.dotName());
//              lines.add(" n" + node2Int.get(from) + " -> n" + node2Int.get(parent));
			}

			List<DerivationNode> toList = from.getChildren();
			if(toList == null)
				continue; 
			
			int i = 1;
			for (DerivationNode to : toList) {
					lines.add("  n" + node2Int.get(from) + " -> n" + node2Int.get(to) + ";");
			}            
		}
		
		for (DerivationNode dnLeaf : this.root.getLeavesInOrder())
		{
			DerivationNode dnPred = dnLeaf.getImmPred();
			DerivationNode dnSucc = dnLeaf.getImmSucc();
			
			if(dnPred != null)
			{
				lines.add(" n" + node2Int.get(dnPred) + " -> n" + node2Int.get(dnLeaf) + ";");
				lines.add(" n" + node2Int.get(dnLeaf) + " -> n" + node2Int.get(dnPred) + ";");
			}

		// if(dnSucc != null)
		// {
		// 	lines.add(" n" + node2Int.get(dnSucc) + " -> n" + node2Int.get(dnLeaf) + ";");
		// 	lines.add(" n" + node2Int.get(dnLeaf) + " -> n" + node2Int.get(dnSucc) + ";");
		// }

			Collections.sort(lines);
			for (String line : lines) {
				outWriter.write(line);
				outWriter.write("\n");
			}
			outWriter.write("}\n");
		}
	}
	
	public static int dtCounter;
	public static void dumpDerivationTree(DerivationTree dt, String filePrefix)
	{
		dtCounter++;
		dt.dtID = dt.dtCounter;
		filePrefix += "_" + dtCounter + "_";
		DerivationNode root = dt.getRoot();
		CfgNode cfnRoot = root.getCfgNode();
		String str = cfnRoot.getFileName();
		int ind = str.lastIndexOf("/");
		if(ind > 0)
			str = str.substring( ind + 1);

		str += filePrefix;
		String path = MyOptions.pixy_home + "/graphs/";
		String name = str ; //+ cfnRoot.getEnclosingFunction().getName();
		String pathFull = path + name + "_DerivationT.dot";
		
		try {
			Writer outWriter = new FileWriter(pathFull);
			dt.writeDotUnique(name, new HashSet<DerivationNode>(), false, outWriter);
			outWriter.close();
		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		// D.p(D.D_TEMP, " printing the derivation tree - ");
		// for(DerivationNode dnn : dn)
		// {
		// 	D.p(D.D_TEMP, dnn.toString());
		// }
		// D.p(D.D_TEMP, "\n\n" + root.toString("$$$$$$$$"));
	}
	
	public void makePS() throws SQLException, SISLException
	{
		System.out.println("makePS() was called");
		// Cfg nodse that are in loop, are marked specially
		// check whether any parts of this query come from such CFG nodes. 
		this.identifyLoopContributions();
		String query = null;
		if (this.sink instanceof CfgNodeCallBuiltin){ //sql query 
			query = this.getSQLQuery();
			System.out.println("the symbolic query is ..... : "+ query);
			this.setSymbQuery(query);
			
			//Abeer
			String newQuery = this.getSQLQuery();
			System.out.println("the new symbolic query is ...... : "+ newQuery);
			//Abeer
			this.setNewQuery(newQuery);
		}
		else { //xss sink 

			query = this.getEchoQuery() ;
			System.out.println("Symbolic Query is: "+ query);
			this.setSymbQuery(query);
		}
		D.pr(D.D_PS, "Transition_Session2_Look: ....\t derivation tree contains query - " + query);
		if(query == null || query.equals("")){
			return;
		}
		
		// DerivationNode dnT =  getFirstLeaf();
		// while(dnT != null)
		// {
		// 	// tmp = next.getId().toString();
		// 	int iBegin = dnT.getBeginOffset();
		// 	int iEnd = dnT.getEndOffset(); 
		// 	D.pd(D.D_PS, "Transition_Session2_Look: Derivation tree leaf node contributed: " + query.substring(iBegin , iEnd + 1));
			
		// 	dnT = dnT.getImmSucc();
		// }		
		
		// SQLParser sp = null;
		// D.pd(D.D_PS, "Transition_Session2_Look: Parsing the SQL query: " + query);
		// sp = new SQLParser(query);
		
		// sp.tokenize();
		
		// // make sure that loop contributed parts can be repeated in the SQL grammar
		// boolean bIsRepeatable = isRepeatable(query, sp.getRepeatablePartialQueries());

		
		// while(sp.hasMoreTokens())
		// {
		// 	int iBegin = sp.getBeginOffset();
		// 	int iEnd = sp.getEndOffset();
			
		// 	String lexeme = sp.getLexeme();
			
		// 	D.pr(D.D_TEMP, "Transition_Session2_Look: SQL token lexeme - " + lexeme + 
		// 			" begin offset = " + iBegin + " end offset - " + iEnd + 
		// 			" partial query part - " + query.substring(iBegin, iEnd+1));
		// 	/*if(lexeme != null && lexeme.length() > 2 && lexeme.charAt(0) == '\'' &&
		// 			lexeme.charAt(lexeme.length() - 1) == '\'')
		// 	{
		// 		iBegin++;
		// 		iEnd--;
		// 	}*/
		// 	DerivationNode dnBegin = locateNode(iBegin);
		// 	DerivationNode dnEnd = locateNodePlus(iEnd);
			
		// 	ArrayList<DerivationNode> tokenNodes = 
		// 		new ArrayList<DerivationNode>();
			
		// 	D.pi(D.D_PS, "Token is constructed in leaves - ");
		// 	DerivationNode dnTemp = dnBegin; 
		// 	if(dnTemp == dnEnd)
		// 	{
		// 		D.pi(D.D_PS, " a single node - " + dnTemp.dotName());
		// 		tokenNodes.add(dnTemp);
		// 	}

		// 	while(dnTemp != dnEnd)
		// 	{
		// 		D.pi(D.D_PS, " +++++++++++++ leaf = " + dnTemp.dotName());
				
		// 		tokenNodes.add(dnTemp);
		// 		dnTemp = dnTemp.getImmSucc();
		// 	}
						
		// 	// if the identified token is totally contained in a single leaf
		// 	// it means query contains static data, and we dont need to pull
		// 	// it out in PreparedStmt parameters. Only, if there is a conflict
		// 	// we will resort to pulling out static data in params. 
		// 	boolean bStaticData = isStaticData(iBegin, iEnd, tokenNodes);
			
		// 	if(bStaticData)
		// 		continue;

		// 	// for each query, a single NT root hosts 
		// 	// most of the token parts. at most two NT 
		// 	// nodes may also host part of data token. 
		// 	// single NT that hosts most of data (and data only)
		// 	// is the spot where we need to introduce data
		// 	// subtree and pull the partial token parts from 
		// 	// the adjacent NT (with partial code) nodes.
			
		// 	introduceQuestionMark(iBegin, iEnd, tokenNodes);
						
		// }
		
		// // now assign which statements must store and propogate (? - arg) mappings
		// setComputationArgs();
		
		// setBFSNodeCounter(this);
		// DerivationTree.dumpDerivationTree(this, "__transformedDT");
	}

	private boolean isRepeatable(String query, ArrayList<Integer> repeatablePartialQueries)
	throws SISLException
	{		
		DerivationNode dn = this.getFirstLeaf();
		DerivationNode tmp = dn;
		// check if all parts of the query came from the loop
		boolean bAllInLoop = true; 
		while(tmp != null)
		{
			if(!tmp.isFromLoop()){
				bAllInLoop = false;
				break;
			}
			tmp = tmp.getImmSucc();
		}
		
		if(bAllInLoop)
		{
			D.pr(D.D_TEMP, " All parts of query came from loop - allow it to execute without " +
					"requiring repeatable range check ");
			return true;
		}
		
		while(dn != null)
		{
			if(!dn.isFromLoop())
			{
				dn = dn.getImmSucc();
				continue;
			}
			
			D.pr(D.D_TEMP, " Processing node - " + dn.getId());
			
			int iStart = dn.getBeginOffset();
			int iEnd = -1;
			while(dn != null)
			{
				if(dn.isLastLeaf())
					break;
				else
					dn = dn.getImmSucc();
			}
			
			if(dn != null)
			{
				iEnd = dn.getEndOffset();
				D.pr(D.D_TEMP, " Loop contributed part - " + iStart + " ends at - " + iEnd);
				boolean bFound = false;
				for(int i = 0; i < repeatablePartialQueries.size(); i = i + 2)
				{
					Integer iRStart = repeatablePartialQueries.get(i);
					Integer iREnd = repeatablePartialQueries.get(i+1);
					D.pr(D.D_TEMP, " Repeatable query starts - " +  iRStart + " ends - " + iREnd);
					if(iStart >= iRStart.intValue() && iEnd <= iREnd.intValue())
					{
						bFound = true; 
						break;
					}
				}
				if(bFound != true)
				{
					// if we have not extracted any token from this loop contribution, then, dont 
					// check if it can be repeated. 
					boolean bFoundToken = isDataTokenInRange(iStart, iEnd);
					if(bFoundToken)
					{
						String msg = " Partial query - " + query.substring(iStart, iEnd);
						msg += " cannot be repeated in SQL grammar and was contributed by loop in the following path \n ";
						
						String nameDt = "__UNREPEATABLE_PARTIALQUERY_FROM_LOOP___" + "\"" + this.symbQuery + "\"";
						DerivationTree.dumpDerivationTree(this,  nameDt);

						msg += CfgNode.getProgLines(this.associatedCfgNodes) + "\n";
						msg += " \n in this symbolic query - " + this.symbQuery;
						msg += " \n check this derivation tree in graphs -  " + nameDt;
						msg += " \n for this sink - " + CfgNode.getProgLine(sink);
							
						D.pr(D.D_TEMP, msg);
						// dump the derivation tree for debugging
						//ToolBug.add_UNREPEATABLE_QUERY_FROM_LOOP(sink, this.symbQuery, msg);
						throw new SISLException(SISLException.NONREPEATABLE_PARTIAL_QUERY_FROM_LOOP, msg);
					}
					else
						D.pr(D.D_TEMP, " Did not find a loop contribution to be repeatable, neither it gives any token ignoring");
				}
			}
			else
			{
				String msg = " Could not find end of the repeatable query \n ";
				
				String nameDt = "__UNREPEATABLE_PARTIALQUERY_FROM_LOOP___" + "\"" + this.symbQuery + "\"";
				DerivationTree.dumpDerivationTree(this,  nameDt);

				msg += CfgNode.getProgLines(this.associatedCfgNodes) + "\n";
				msg += " \n in this symbolic query - " + this.symbQuery;
				msg += " \n check this derivation tree in graphs -  " + nameDt;
				msg += " \n for this sink - " + CfgNode.getProgLine(sink);
				D.pr(D.D_TEMP, msg);
				// dump the derivation tree for debugging
				//ToolBug.add_UNREPEATABLE_QUERY_FROM_LOOP(sink, this.symbQuery, msg);
				throw new SISLException(SISLException.NONREPEATABLE_PARTIAL_QUERY_FROM_LOOP, msg);
			}
			dn = dn.getImmSucc();
		}
		return true;
	}

	private void throwSISLException(String query, int start, int end, int nonrepeatablePartialQueryFromLoop) {
		// TODO Auto-generated method stub
	}

	private boolean isDataTokenInRange(int start, int end){
		ArrayList<SimpleNode> tokens = SimpleNode.getTokens();
		for(SimpleNode sn : tokens)
		{
			int iTokenStart = sn.getBeginOffset() ;
			int iTokenEnd = sn.getEndOffset();
			
			if(iTokenStart >= start && iTokenEnd <= end)
				return true;
		}
		return false;
	}

	private void setBFSNodeCounter(DerivationTree tree) 
	{
		DerivationNode root = tree.getRoot();
		DerivationNode.nodeCounter = 0;
		
		ArrayList<DerivationNode> dn = new ArrayList<DerivationNode>();
		dn.add(root);
		setBFSCounter(dn);
	}
	
	private void setBFSCounter(ArrayList<DerivationNode> nodeList) 
	{
		ArrayList<DerivationNode> children = new ArrayList<DerivationNode>(); 
		for(DerivationNode dn : nodeList)
		{
			dn.setNodeCounter(dn.nodeCounter);
			ArrayList<DerivationNode> kids = dn.getChildren();
			if(kids != null)
				children.addAll(kids);
		}
		
		if(children.size() > 0)
		{
			DerivationNode.nodeCounter++;

			setBFSCounter(children);
		}
	}
	
	public boolean isStaticData(int tokenBegin, int tokenEnd, ArrayList<DerivationNode> tokenNodes)
	{
		boolean bRes = false; 
		if(tokenNodes == null) return true;
		
		if(tokenNodes.size() == 1 && 
				tokenNodes.get(0).embedsTokenWithCode(tokenBegin, tokenEnd))
			bRes = true; 
		
		return bRes;
	}
	
	public void introduceQuestionMark(int tokenBegin, int tokenEnd, ArrayList<DerivationNode> tokenLeafs) throws SISLException
	{
		boolean bNeedNewNode = false;
		DerivationNode dataSubtree = null;
		ArrayList<DerivationNode> dataNodes = new ArrayList<DerivationNode>();
		ArrayList<DerivationNode> borrowNodes = null;
		for(DerivationNode leaf : tokenLeafs)
		{
			leaf.markDataContributions(tokenBegin, tokenEnd);
			if(leaf.isData())
				dataNodes.add(leaf);
		}
		// if all data is not contained in data nodes, we need to 
		// introduce new node that will root the data subtree
		if(!dataNodes.containsAll(tokenLeafs) && tokenLeafs.containsAll(dataNodes))
		{
			bNeedNewNode = true;
			// and we may have to borrow part of token from the code+data nodes
			borrowNodes = new ArrayList<DerivationNode>();
			borrowNodes.addAll(tokenLeafs);
			borrowNodes.removeAll(dataNodes);
		}
		boolean change = true;
		//while(change)
		{
			//change = false;
			D.pf(D.D_PS, " ###################################### before iteration");
			for(DerivationNode t : dataNodes)
			{
				D.pf(D.D_PS, " ############# data node - " + t.dotName());
			}
		
			ArrayList<DerivationNode> dts = new ArrayList<DerivationNode>(dataNodes);
			while(change)
			{
				change = false;
				for(DerivationNode data : dts)
				{
					DerivationNode parent = data.getParent();
					if(dataNodes.containsAll(parent.getChildren()))
					{
						parent.setData();
						dataNodes.add(parent);
						dataNodes.removeAll(parent.getChildren());
						dts = dataNodes; 
						change = true;
						break;
					}
				}
				
			}
			dataNodes = dts;
			D.pf(D.D_PS, " ###################################### iteration");
			for(DerivationNode t : dataNodes)
			{
				D.pf(D.D_PS, " ############# data node - " + t.dotName());
			}
		}
		if(dataNodes.size() > 1)
		{
			String msg = " ";
			msg += " failed to close on a single data subtree location";
			D.pr(D.D_PS, " failed to close on a single data subtree location");
			ArrayList<CfgNode> tokenCfgNodes = new ArrayList<CfgNode>();
			ArrayList<CfgNode> dataLeafs = new ArrayList<CfgNode>();

			for(DerivationNode dn : tokenLeafs)
			{
				CfgNode t = dn.getCfgNode();
				if(t != null)
					dataLeafs.add(t);
			}
			
			for(DerivationNode dn : dataNodes)
			{
				CfgNode t = dn.getCfgNode();
				if(t != null)
					tokenCfgNodes.add(t);
				D.pr(D.D_PS, "found data nodes - " + dn.dotName());
			}

			String nameDt = "__COULDNOTLOCATEDATASUBTREE___" + "\"" + this.symbQuery + "\"";
			DerivationTree.dumpDerivationTree(this,  nameDt);

			msg += "All data leafs for data subtree - \n" +
				CfgNode.getProgLines(dataLeafs) + "\n";
			msg += " \n Data nodes remaining to be resolved for subtree root - " + 
				CfgNode.getProgLines(tokenCfgNodes) + "\n";
			msg += " \n in this symbolic query - " + this.symbQuery;
			msg += " \n check this derivation tree in graphs -  " + nameDt;
			msg += " \n for this sink - " + CfgNode.getProgLine(sink);
				
			D.pr(D.D_TEMP, msg);
			// dump the derivation tree for debugging
			ToolBug.add_COULD_NOT_LOCATE_DATA_SUBTREE(dataNodes, sink, this.symbQuery, msg);
			throw new SISLException(SISLException.BAIL_OUT, "BAIL_OUT");
			//throw new Error("^^^^^^^^^^^^^ Could not locate datasubtree location!!!!");
		}
		//data subtree should start as a parent of this node.
		dataSubtree = dataNodes.get(0);
		TacPlace tp = dataSubtree.getId();
		if(tp.isVariable())
		{
			Variable v = tp.getVariable();
			if(v.isTemp())
			{
				// data subtree root cannot be temp. 
				// this happens in case of use of globals
				// go down until we find a node with non-temp var
				ArrayList<DerivationNode> dsChildren = dataSubtree.getChildren();
				if(dsChildren == null || dsChildren.size() == 0)
				{
					String nameDt = "__COULDNOTLOCATEDATASUBTREE___" + "\"" + this.symbQuery + "\"";
					DerivationTree.dumpDerivationTree(this,  nameDt);
					String msg = "";
					msg += " Located root of data subtree - \n" +
						CfgNode.getProgLine(dataSubtree.getCfgNode()) + "\n";
					msg += " \n - All nodes in derivation tree " + 
						CfgNode.getProgLines(DepGraph.getAllCfgNodes(this.depGraph.getNodes(), true)) + "\n";
					msg += " \n in this symbolic query - " + this.symbQuery;
					msg += " \n check this derivation tree in graphs -  " + nameDt;
					msg += " \n for this sink - " + CfgNode.getProgLine(sink);
					D.pr(D.D_TEMP, msg);
					// dump the derivation tree for debugging
					ToolBug.add_COULD_NOT_LOCATE_DATA_SUBTREE(dataNodes, sink, this.symbQuery, msg);
					throw new SISLException(SISLException.BAIL_OUT, "BAIL_OUT");
				}
				DerivationNode child = dataSubtree.getChildren().get(0);
				boolean process = true;
				while(child != null)
				{
					TacPlace childId = child.getId(); 
					if(childId.isVariable() && !childId.getVariable().isTemp())
					{
						dataSubtree = child; 
						break;
					}
					child = child.getChildren() == null ? null : child.getChildren().get(0);
				}
			}
		}
		dataSubtree.setDataSubtree();
		D.pr(D.D_TEMP, " Transition_Session2_Look: found subtree location as parent of - " + dataSubtree.dotName());
		DerivationNode dnnt = null;
		if(bNeedNewNode)
		{
			dnnt = dataSubtree.dup();
			if(IsolationAnalysis.m_bFixNameProblem)
			{
				Variable v = dataSubtree.getId().getVariable();
				if(borrowNodes.size() > 1)
				{
					Variable vNew = v.dup(v.getName() + "_PIPER_tmp");
					vNew.setOrig(null);
					dnnt.setId(vNew);   
				}
			}
			dnnt.setActual2Formal(false);
			dnnt.setCfgNode(dataSubtree.getParent().getCfgNode());
			ArrayList<DerivationNode> children = new ArrayList<DerivationNode>();
			DerivationNode oldParent = dataSubtree.getParent();         
			ArrayList<DerivationNode> oldChildren = oldParent.getChildren();
			int index = oldChildren.indexOf(dataSubtree);
			oldChildren.remove(index);
			oldChildren.add(index, dnnt);
			dnnt.setParent(oldParent);
			dataSubtree.setParent(dnnt);
			dnnt.setChildren(null);
			dnnt.setNewNode();
			// since this is a new node hosting data token
			// there are no other arguments...except for the 
			// current token that is assembled. 
			// __args should point to the temporary value
			// that holds the complete token
			// FIX : dnnt.setArgsVar(dataSubtree.getArgsVar());
			//dnnt.setArgsVar(dataSubtree.getArgsVar());
			if(IsolationAnalysis.m_bFixNameProblem)
			{
//              vArgDup.setOrig(null);
				dnnt.setArgsVar(dnnt.getId().getVariable());
				//vArgDup.setOrig(null);
				D.pr(D.D_TEMP, " Fixing the incorrect token dnnt - " + dnnt.getId() + " is assigned args - " + dnnt.getId().getVariable());
			}
			else
			{
				dnnt.setArgsVar(dataSubtree.getArgsVar());
			}
			if(borrowNodes.size() > 2){
				throw new Error(" Borrow nodes size is greater than 2");
			}
			Literal leftDonated = null;
			Literal rightDonated = null;
			for(DerivationNode dn : borrowNodes)
			{
				dn.setBorrowee();
				if(dn.isLeftDoner(tokenBegin, tokenEnd))
				{
					leftDonated = dn.getDonated();
					D.pv(D.D_TEMP, " left donated = " + leftDonated);
				}
				else
				{
					rightDonated = dn.getDonated();
					D.pv(D.D_TEMP, " right donated = " + rightDonated);
				}
			}
			DerivationNode child = null;
			if(leftDonated != null)
			{
				child = new DerivationNodeTerminal(leftDonated);
				child.setParent(dnnt);
				dnnt.setChild(child);               
			}
			dnnt.setChild(dataSubtree);
			if(rightDonated != null)
			{
				child = new DerivationNodeTerminal(rightDonated);
				child.setParent(dnnt);
				dnnt.setChild(child);
			}
		}
		// if new node was created PS node is father of that node
		// otherwise it is the father of data subtree
		DerivationNode questionChild = null;
		if(bNeedNewNode)
			questionChild = dnnt; 
		else
			questionChild = dataSubtree; 
		DerivationNode questionMark = questionChild.dup();
		DerivationNode oldParent = questionChild.getParent();           
		ArrayList<DerivationNode> oldChildren = oldParent.getChildren();
		int index = oldChildren.indexOf(questionChild);
		oldChildren.remove(index);
		oldChildren.add(index, questionMark);
		questionMark.setParent(oldParent);
		questionChild.setParent(questionMark);
		questionMark.setChildren(null);
		questionMark.setChild(questionChild);
		questionMark.setQuestionMark();
		questionMark.setCfgNode(oldParent.getCfgNode());
		questionMark.setAssembledToken(bNeedNewNode);
		D.pr(D.D_TEMP, "Transition_Session2_Look: Setting the argument of question mark node - " + questionChild.getArgsVar());
		questionMark.setArgsVar(questionChild.getArgsVar());
		return;
	}
	
	public DerivationNode locateNode(int queryOffset)
	{
		DerivationNode next = getFirstLeaf();
		
		while(next != null)
		{
			if(next.containsQueryOffset(queryOffset))
				return next; 
			
			next = next.getImmSucc();
		}

		return next; 
	}

	public DerivationNode locateNodePlus(int queryOffset)
	{
		DerivationNode last = locateNode(queryOffset);
		if(last != null)
			last = last.getImmSucc();
		
		return last; 
	}

	public boolean isLeafFromLoopStmt(DerivationNode dn)
	{
		boolean isFromLoop = false; 
		DerivationNode tmp = dn.getParent();
		// since derivation tree has nodes for temporary
		// variables, walk up to find the parent who is 
		// an actual variable
		while(tmp != null)
		{
			TacPlace tmpId = tmp.getId();
			if(!tmpId.isVariable() || tmpId.getVariable().isTemp())
			{
				tmp = tmp.getParent();
				continue; 
			}
			
			break;
		}
		if(tmp != null && tmp.getCfgNode() != null)
			isFromLoop = tmp.getCfgNode().isInLoop();
		D.pr(D.D_TEMP, " Parent node for leaf " + dn.getId() + 
				" came from loop - " + isFromLoop);
		return isFromLoop;
	}
	
	public void identifyLoopContributions()
	{
		DerivationNode dn = getFirstLeaf();
		
		DerivationNode next = dn;
		DerivationNode last = null;
		while(next != null)
		{
			boolean bCameFromLoop = isLeafFromLoopStmt(next);
			if(bCameFromLoop)
			{
				next.setFromLoop();
				D.pr(D.D_TEMP, " next = " + next.getId() + 
						" last - " + (last == null?" null":last.getId()));
				if(last != null && !last.isFromLoop())
				{
					D.pr(D.D_TEMP, " \t \t setting first leaf - " + next.getId());
					next.setFirstLeaf();
				}
			}
			
			last = next;
			next = next.getImmSucc();
		}
		
		next = dn;
		DerivationNode tmp = null;
		while(next != null)
		{
			if(next.isFirstLeaf())
			{
				D.pr(D.D_TEMP, " first leaf - " + next.getId());
				tmp = next.getImmSucc();
				while(tmp != null && tmp.isFromLoop())
				{
					next = tmp;
					tmp = tmp.getImmSucc();
				}
				if(next != null)
				{
					D.pr(D.D_TEMP, " last leaf - " + next.getId());
					next.setLastLeaf();
				}
			}
			next = next.getImmSucc();
		}
		return; 
	}

	public String getSQLQuerySummarized()
	{
		int iOffset = -1;
		String query = "";
		DerivationNode dn = getFirstLeaf();
//		String query = toSQLAmiable(dn.getId());
		String tmp = null;
//		iOffset = query.length() - 1;
//		dn.setQueryOffsets(0, iOffset);
		while(dn != null)
		{
//			tmp = toSQLAmiableSummarized(dn, dn.getId());
			tmp = toSQLAmiable(dn.getId());
			
			dn.setQueryOffsets(iOffset + 1, iOffset + tmp.length());
			
			iOffset += tmp.length();
			query += tmp;
			
			dn = dn.getImmSucc();
		}
		dn = getFirstLeaf();
		D.pr(D.D_TEMP, " Query - " + query);
		while(dn != null)
		{
//			tmp = toSQLAmiableSummarized(dn, dn.getId());
			tmp = toSQLAmiable(dn.getId());
			int iBegin = dn.getBeginOffset();
			int iEnd = dn.getEndOffset();
			D.pr(D.D_TEMP, " Leaf - " + tmp + " query offset - " + iBegin + " - " + iEnd + " partial query - " + query.substring(iBegin, iEnd + 1));
			//dn.setQueryOffsets(iOffset + 1, iOffset + tmp.length());			
			dn = dn.getImmSucc();
		}
		D.ps(D.D_PS, "@@@@@@@@@@@@@@@@@ getSQLQuery - " + query);
		return query; 
	}

	//Modified by Abeer
	//recognize create statements to extract table constraints
	public String getSQLQuery() throws SQLException{
		int iOffset = 0;
		DerivationNode dn = getFirstLeaf(); //the 1st term in the Q
		//Abeer
		if (dn.getCfgNode() != null)
			this.setQueryLine(dn.getCfgNode().getLocNew());
		else if(dn.getParent().isNT() && dn.getParent().getCfgNode() != null) 
			this.setQueryLine(dn.getParent().getCfgNode().getLocNew());
		//end
		

		String query = toSQLAmiable(dn.getId());
		//Abeer to store Q parts 
		ArrayList<String> QList=new ArrayList<String>();
		QList.add(query);
		String tmp = null;
		iOffset = query.length() - 1;
		dn.setQueryOffsets(0, iOffset);
		DerivationNode next = dn.getImmSucc();
		while(next != null)
		{
			tmp = toSQLAmiable(next.getId());
			next.setQueryOffsets(iOffset + 1, iOffset + tmp.length());
			iOffset += tmp.length();
			QList.add(tmp);
			query += tmp;
			
			next = next.getImmSucc();
		}
		D.ps(D.D_PS, "@@@@@@@@@@@@@@@@@ getSQLQuery - " + query);
		return query; 
	}

	// for each terminal that contains only integer and + operator is used in 
	// its parent nodes...go back until we have a node that fathers all nodes
	// and uses a dot operator
	public void symEvalArith()
	{
		DerivationNode root = this.getRoot();
		boolean change = true;
		while(change)
		{
			change = false;
			for(DerivationNode leaf : root.getBfOrder())
			{
				if(leaf.isNumberLiteral())
				{
					D.pi(D.D_PS, " Leaf with numeric value - " + leaf.dotName());
					change = symEvalArith(leaf);
					if(change)
						break;
				}
			}
		}
	}

	public boolean symEvalArith(DerivationNode leaf)
	{
		boolean bFound = false;
		DerivationNode tmp = leaf.getParent();
		while(tmp != null)
		{
			boolean bArith = TacOperators.isArithmetic(tmp.getOperator());
			D.pv(D.D_TEMP, " checking for symb eval of numbers - " + tmp + " - " + bArith);
			if(bArith)
			{
				bFound = true;
				break;
			}
			tmp = tmp.getParent();
		}
		// equivalent to being evaluated for query purposes
		if(bFound)
		{
			D.ps(D.D_PS, "~~~~~~~~~~~~~~~~ found sym eval node - " + tmp.dotName());
			tmp.setChildren(null);
		}
		return bFound;
	}

	public String toSQLAmiableSummarized(DerivationNode dn, TacPlace id)
	{
		String prefix = "";
		String suffix = "";
		if(dn.isFirstLeaf()){
			prefix = " PIPER_LOOP_BEGIN ";
		}
		if(dn.isLastLeaf()){
			suffix = " PIPER_LOOP_END ";
		}
		return prefix + toSQLAmiable(id) + suffix;
	}

	public String toSQLAmiable(TacPlace id)
	{
		if(id.isLiteral())
		{
			return id.toString();
		}
		else
		if(id.isVariable())
		{
			Variable v = (Variable)id;
			v.setSQLName();
			D.pi(D.D_PS, " Sql amiable formatting for - " + v.getName() + " is - " + v.getSQLName());
			return v.getSQLName();
		}
		else
		if(id.isConstant())
		{
			D.pi(D.D_PS, " Returning Sql amiable formatting for - " + id.toString());
			return id.toString();
		}
		else
		{
			throw new Error(" toSQLAmiable ---- unknown for " + id.toString());
		}
	}

	private static String varNameConversion(String varString){
		System.out.println("varNameConversion called");
		// $ - DOLLARSYMB, [ - SQROPENSYMB, ] - SQRCLOSESYMB, ( - PARNTHSOPENSYMB, ) - PARNTHCLOSESYMB
		// . DOTSYMB
		String varStringMod = varString.replace(".", "DOTSYMB");
		varStringMod = varStringMod.replace("$", "DOLLARSYMB");
		varStringMod = varStringMod.replace("[", "SQROPENSYMB");
		varStringMod = varStringMod.replace("]", "SQRCLOSESYMB");
		varStringMod = varStringMod.replace("(", "PARNTHSOPENSYMB");
		varStringMod = varStringMod.replace(")", "PARNTHSOPENSYMB");
		varStringMod = varStringMod.replace("_", "UNDERSCORESYMB");
		return varStringMod;
		// return varString;
	}

	private static String varNameConversionReverse(String varString){
		System.out.println("varNameConversion called");
		// $ - DOLLARSYMB, [ - SQROPENSYMB, ] - SQRCLOSESYMB, ( - PARNTHSOPENSYMB, ) - PARNTHCLOSESYMB
		// . DOTSYMB
		String varStringMod = varString.replace("DOTSYMB", ".");
		varStringMod = varStringMod.replace("DOLLARSYMB", "$");
		varStringMod = varStringMod.replace("SQROPENSYMB", "[");
		varStringMod = varStringMod.replace("SQRCLOSESYMB", "]");
		varStringMod = varStringMod.replace("PARNTHSOPENSYMB", "(");
		varStringMod = varStringMod.replace("PARNTHSOPENSYMB", ")");
		varStringMod = varStringMod.replace("UNDERSCORESYMB", "_");
		return varStringMod;
	}

	// Prasanth added method START
	public String toXSSAmiable(DerivationNode dn) throws SISLException {
		TacPlace id = dn.getId();
		String printString = "In toXSSAmiable ";
		HashMap<DerivationNode, ArrayList<CfgNode>> funcSeq = new HashMap<DerivationNode, ArrayList<CfgNode>>();
		if(id.isLiteral()){
			printString += "Case: IsLiteral ";
			System.out.println(printString);
			return id.toString();
		}
		else if( id.isVariable() && dn.getCfgNode() != null){
			printString += "Case: isVariable ";
			// ArrayList<CfgNode> cfgNodes = new ArrayList<CfgNode>();
			// CfgNodeCallBuiltin callNode = (CfgNodeCallBuiltin) dn.getCfgNode();
			// DerivationNode parent = dn.getParent();
			// System.out.println("CFG Node present for var Id: " + dn.getId().toString() + " isTemp: " + parent.getId().getVariable().isTemp());
			// cfgNodes.add(dn.getCfgNode());
			// while( parent != null && parent.getId().isVariable() && parent.getCfgNode() != null){// && !parent.getId().toString().endsWith("SSA0")){ 
			//  System.out.println("CFG Node present for var Id: " + parent.getId().toString() + " isTemp: " + parent.getId().getVariable().isTemp());
			//  // cfgNodes.add(parent.getCfgNode());
			//  parent = parent.getParent();
			// }
			// if(parent != null){
			//  System.out.println("Exited While for dn: " + parent.getId() + " Number: " + parent.nodeNumber + " isVariable: " + parent.getId().isVariable() + " isTemp: " + parent.getId().getVariable().isTemp() );
			// }
			// if(cfgNodes.size() > 0){
			//  System.out.println("Node List loop START");
			//  for(CfgNode node: cfgNodes){
			//      System.out.println("Node is of type" + node.getClass() + " \nToString: " + node.toString());
			//  }
			//  System.out.println("Node List loop END");
			// }
			String varString = id.toString();
			String retVal = varNameConversion(varString);
			return retVal;
		}
		else if( id.isVariable() ){
			printString += "Case: isVariable ";
			System.out.println(printString);
			String varString = id.toString();
			String retVal = varNameConversion(varString);
			return retVal;
		}
		else if(id.isConstant()){
			printString += "Case: isConstant";
			System.out.println(printString);
			return id.toString();
		}
		else{
			printString += "Case: Neither constant/variable/literal";
			System.out.println(printString);
			throw new Error(" toXSSAmiable ---- unknown for " + id.toString());
		}
	}
	// Prasanth added method END

	public void setComputationArgs()
	{
		// bottom up processing, reverse of breadth first order 
		ArrayList<DerivationNode> bottomUp = this.bfOrder();
		Collections.reverse(bottomUp);
		ArrayList<DerivationNode> children = null;
		for(DerivationNode dn : bottomUp)
		{
			// terminals dont have any ? - args mapping
			//if(!dn.isNT())
			//  continue;		
			children = dn.getChildren();
			int iSize = 0;
			if(children != null){
				iSize = children.size();
			}
			D.pv(D.D_PS, "-------------------- Bottom up - number "  + dn.nodeNumber + " node -" + dn.getCfgNode());	
			switch(iSize)
			{
			// nodes with one child, directly take the arguments from children 
			// these are unary assignment statements 

			// nodes with two children are binary operations 
			
			// nodes with 3 children are introduced by us, and are concatenation operations 
			// for constructing data subtrees.
			case 0: 
				break;
			case 1: 
			case 2:
			case 3:
				// detect if this is a return statement                 
				if(dn.isReturnAssign())
				{
					// id() variable receiving value in caller
					// only child is the value returned by callee
					//	ArrayList<TacPlace> childQMap = dn.getChildren().get(0).getQuestionMaps();
					//	if(childQMap != null && childQMap.size() > 1)
					//	if(returnValue != null)
					//	D.p(D.D_TEMP, " !!!!! receiver = " + receiver + " returnvalue = " + returnValue);
					DerivationNode returnN = dn.getChildren().get(0);
					ArrayList<TacPlace> retQMap = returnN.getQuestionMaps(); 
					if( retQMap != null && retQMap.size() > 0)
					{
						CfgNode cncr = dn.getCfgNode();
						//TacPlace receiver = dn.getArgsVar();
						TacPlace receiver = dn.getParent().getArgsVar();
						dn.getParent().resetQuestionMap();
						TacPlace returnValue = returnN.getArgsVar();
						cncr.addReturnValue(receiver, returnValue);
					}
					// questionMap = receiver.getVariable(); 
				}
				// detect if this is a call stmt...formal - actual mapping
				if (dn.isActual2Formal() && (dn.getCfgNode() instanceof CfgNodeCallPrep) )
				{
					// id() is the formal parameter in callee
					// and child is the actual param being passed
					D.ps(D.D_PS, " actual to formal map " + dn.getCfgNode());
					CfgNodeCallPrep cncp = (CfgNodeCallPrep) dn.getCfgNode();
					// if actual doesnt have a ? - arg map, 
					// dont flag for passing ? args during method invocation
					DerivationNode child = dn.getChildren().get(0);
					ArrayList<TacPlace> chQs = child.getQuestionMaps(); 
					if(chQs != null && chQs.size() > 0)
					{
						TacPlace actual = child.getArgsVar();
						TacPlace formal = dn.getArgsVar();
						cncp.addActual2FormalArgs(actual, formal);
					}
				}

				if(dn.isQuestionMark())
				{
					// only one child. 
					ArrayList<DerivationNode> childrenM = dn.getChildren(); 
					if(children.size() > 1)
						throw new Error("? nodes cannot have more than one child " + dn);
					
					// to enable un-optimized transf...enable this
					// and comment next 
					// dn.addQuestionMaps(childrenM.get(0).getId());
			//		if(childrenM.get(0).isNewNode())
			//		{
			//			Variable v = childrenM.get(0).getId().getVariable();
			//			Variable vNew = v.dup(Variable.makeArgsName(v, true));
//						vNew.setOrig(null);
//						dn.addQuestionMapsUnique(vNew); 
//						D.pf(D.D_TEMP, "  for new node added a temp question mark node variable v- " + v);
//					}
//					else
					{
						Variable v = dn.getId().getVariable().getOrig();
						if(v == null)
							v = dn.getId().getVariable();
						dn.addQuestionMapsUnique(v);
						D.pf(D.D_TEMP, "  for question mark node variable v- " + v);
					}
				}
				
/*              Variable vPhiMap = dn.getPhiMapFor(); 
				if( vPhiMap != null)
				{
					// check if any children of this node has a vPhiMap argument
					//boolean bChildrenCompute = subtreeContains(vPhiMap, dn);
					//if(bChildrenCompute)
					{
						D.pv(D.D_TEMP, " adding vPhiMap for node - " + dn);
						vPhiMap = vPhiMap.getOrig();
						String s = Variable.makeArgsName(vPhiMap, true);
						Variable v = vPhiMap.dup(s);
						dn.addQuestionMapsUnique(v);
					}
				}
*/
				// if children don't have any ? - maps dont add any here. 
				ArrayList<DerivationNode> childrenM = dn.getChildren(); 
				if(childrenM != null)
				{
					for(DerivationNode child : childrenM)
					{
						ArrayList<TacPlace> chQ = child.getQuestionMaps();
						Variable vPhiMap = child.getPhiMapFor(); 
						if(vPhiMap == null && (chQ == null || chQ.size() == 0))
							continue; 
						else
						{
							if(child.isQuestionMark())
							{
								dn.addQuestionMapsUnique(child.getQuestionMaps().get(0));
								// to enable un-optimized transf...enable this
								// dn.addQuestionMaps(child.getArgsVar());
							}
							else
							{
								if(vPhiMap != null)
								{
									D.pv(D.D_TEMP, " adding vPhiMap for node - " + dn);
									vPhiMap = vPhiMap.getOrig();
									String s = Variable.makeArgsName(vPhiMap, true);
									Variable v = vPhiMap.dup(s);
									dn.addQuestionMapsUnique(v);
								}
								if(child.getId().getVariable().isTemp()){
									dn.addAllQuestionMapsUnique(child.getQuestionMaps());
								}
								else
								{
									dn.addQuestionMapsUnique(child.getArgsVar());
								}
							}
						}
					}
				}
				break;
				
			default:
				break;
			}
		}
		// now check which variables used in phi functions are used as LHS
		// for all such cases, update the parent with LHS type argument.
		/*for(DerivationNode dn : bottomUp)
		{
			
			Variable vPhiMap = dn.getPhiMapFor(); 
			if( vPhiMap == null)
				continue; 
			
			D.pv(D.D_TEMP, " adding vPhiMap for node - " + dn);
			vPhiMap = vPhiMap.getOrig();
			String s = Variable.makeArgsName(vPhiMap, true);
			Variable v = vPhiMap.dup(s);
			dn.addQuestionMapsUnique(v);
		}*/
//		DerivationTree.dumpDerivationTree(this,  "__afterTransf_step1__");
	}
	
	private boolean subtreeContains(Variable phiMap, DerivationNode dn) {
		boolean bRet = false;
		if(phiMap.getOrig() != null){
			phiMap = phiMap.getOrig();
		}
		ArrayList<DerivationNode> nodes = dn.getBfOrder();
		for(DerivationNode d : nodes){
			TacPlace tp = d.getId();
			if(!tp.isVariable()){
				continue; 
			}
			Variable v = tp.getVariable();
			if(v.getOrig() != null){
				v = v.getOrig();
			}
			Variable vArg = d.getArgsVar();
			if(vArg.getOrig() != null){
				vArg = vArg.getOrig();
			}
			if(v.equals(phiMap) || (vArg != null && vArg.equals(phiMap))){
				bRet = true; 
				break;
			}
		}
		return bRet;
	}

	public ArrayList<DerivationNode> getPlaceHolders(){
		ArrayList<DerivationNode> placeholders = new ArrayList<DerivationNode>();
		
		for( DerivationNode dn : this.getRoot().getInOrder())
		{
			if(dn.isQuestionMark())
				placeholders.add(dn);
			
			D.pd(D.D_PS, "In order traversal - " + dn.dotNameShort());
		}
		
		return placeholders; 
	}
		public void setSymbQuery(String symbQuery) {
		this.symbQuery = symbQuery;
	}
	public String getSymbQuery() {
		return symbQuery;
	}
	public void setNewQuery(String newQuery) {
		this.newQuery = newQuery;       
	}
	
	public String getNewQuery() {
		return newQuery ;       
	}
	
	public CfgNode getSink() {
		return sink;
	}

	public ArrayList<ChangedStmt> getChangedNodes() {
		ArrayList<DerivationNode> bf = this.bfOrder();
		ArrayList<ChangedStmt> changes = new ArrayList<ChangedStmt>();
		ArrayList<DerivationNode> dnChanged = new ArrayList<DerivationNode>();
		ArrayList<CfgNode> cfgChanged = new ArrayList<CfgNode>();
		for (DerivationNode dn : bf) 
		{
			if(dn.isBorrowee())
			{
				if(!dnChanged.contains(dn))
					dnChanged.add(dn);
			}
			if(dn.isQuestionMark())
			{
				DerivationNode dnParent = dn.getParent();
				if(dnParent != null && !dnChanged.contains(dnParent))
					dnChanged.add(dnParent);
			}
			boolean added = false;
			if(dn.isNewNode())
			{
				ArrayList<DerivationNode> children = dn.getChildren();
				for(DerivationNode child : children)
				{
					TacPlace tp = child.getId();
					//if(tp.isLiteral())
					{
						
					}
				}
			}
		}
		// find all the parents of these nodes
		ArrayList<DerivationNode> iter = new ArrayList<DerivationNode>(dnChanged);
		while(true)
		{
			ArrayList<DerivationNode> dnt = new ArrayList<DerivationNode>();
			for(DerivationNode dn : iter)
			{
				DerivationNode dnParent = dn.getParent();
				while(dnParent != null)
				{
					if(!dnt.contains(dnParent) && !dnChanged.contains(dnt))
						dnt.add(dnParent);
					dnParent = dnParent.getParent();
				}
			}
			if(!dnt.isEmpty())
			{
				dnChanged.addAll(dnt);
				iter = dnt;
			}
			else{
				break;
			}
		}
		for(DerivationNode dn : dnChanged)
		{
			if(dn.isQuestionMark()){
				continue;
			}
			TacPlace tp = dn.getId();
			if(tp != null && !tp.isVariable()){
				continue;
			}
			Variable v = tp.getVariable();
			if(v.isTemp()){
				continue;
			}
			CfgNode c = dn.getCfgNode(); 
			//if(c.isAlwaysProcess())
				//continue; 
//			if( c != null && !cfgChanged.contains(c) && !c.isAlwaysProcess())
			if( c != null && !cfgChanged.contains(c))
			{
				cfgChanged.add(c);
				ChangedStmt.addIfAbsent(changes, dn);
			}
		}
		return changes;
	}
	
	public void getData(ArrayList<DataStmt> dataVars, ArrayList<CodeStmt> unmodifiedQueries, ArrayList<CodeStmt> modifiedQueries) 
	{
		ArrayList<DerivationNode> bf = this.bfOrder();  
		ArrayList<DerivationNode> dnChanged = new ArrayList<DerivationNode>();
		ArrayList<DerivationNode> rootsDataSubtrees = new ArrayList<DerivationNode>();
		ArrayList<DerivationNode> mQueries = new ArrayList<DerivationNode>();
		for (DerivationNode dn : bf) 
		{
			if(dn.isBorrowee())
			{
				if(!mQueries.contains(dn))
					mQueries.add(dn);
			}
			
			if(dn.isQuestionMark())
			{
				if(!rootsDataSubtrees.contains(dn))
					rootsDataSubtrees.add(dn);
			}
		}
		// find all the parents of these nodes
		ArrayList<DerivationNode> changed = 
			new ArrayList<DerivationNode>(mQueries);
		for(DerivationNode dnt : rootsDataSubtrees){
			if(!changed.contains(dnt)){
				changed.add(dnt);
			}
		}
		for(DerivationNode dn : changed)
		{
			DerivationNode dnParent = dn.getParent();
			while(dnParent != null)
			{
				if(!dnChanged.contains(dnParent))
					dnChanged.add(dnParent);
				dnParent = dnParent.getParent();
			}
		}
		for(DerivationNode dn : dnChanged)
		{
			if(dn.isQuestionMark())
				continue;
			
			TacPlace tp = dn.getId();
			if(tp != null && !tp.isVariable()){
				continue;
			}
			
			Variable v = tp.getVariable();
			if(v.isTemp()){
				continue;
			}
			// record this cfg and dn - partial query has been modified
			CfgNode c = dn.getCfgNode(); 
			if( c != null )
				CodeStmt.addIfAbsent(modifiedQueries, c, v, true);
		}
		for(DerivationNode dn : rootsDataSubtrees)
		{
			D.pr(" root of data subtree ------ " + dn.getId());
			ArrayList<DerivationNode> subtree = dn.getBfOrder();
			for(DerivationNode dnD : subtree)
			{
				TacPlace tp = dnD.getId();          
				if(tp != null && !tp.isVariable())
					continue;
				
				Variable v = tp.getVariable();
				if(v.isTemp())
					continue;

				// record this cfg and dn - partial query has been modified
				CfgNode c = dnD.getCfgNode(); 
				if( c != null )
					DataStmt.addIfAbsent(dataVars, c, v);

			}
		}
	}

	public int getQuestionMarks() {
		int iQuestionMarks = 0;

		for (DerivationNode dn : this.bfOrder()) 
			if(dn.isQuestionMark())
				iQuestionMarks++;

		return iQuestionMarks;
	}

	String queryLine;
	public String getQueryLine() {
		return queryLine;
	}
	public void setQueryLine(String string){
		this.queryLine = string;
	}
	public void printAllLeaves(DerivationNode dn){
		System.out.println("PRINTING ALL LEAVES OF DERIVATION TREE START");
		DerivationNode next = dn.getImmSucc();
		while(next != null){
			System.out.println(next.toString());
			next = next.getImmSucc();
		}
		System.out.println("PRINTING ALL LEAVES OF DERIVATION TREE END");
	}

	private static void addVarsToList(DerivationNode dn, String dnName, Set<String> localQueryVars, Set<DerivationNode> localQueryVarsDNs){
		if(dn.getId().isVariable()){
			localQueryVarsDNs.add(dn);
			localQueryVars.add(dnName);
		}
	}

	// Prasanth varNameConversion
	public String getEchoQuery() throws SISLException{

		int iOffset = 0;
		DerivationNode dn = getFirstLeaf(); //the 1st term in the echo
		Set<String> localQueryVars = new HashSet<String>();
		Set<DerivationNode> localQueryVarsDNs = new HashSet<DerivationNode>();
		System.out.println("getEchoQuery was called");
		if (dn.getCfgNode() != null)
			this.setQueryLine(dn.getCfgNode().getLocNew());
		else if(dn.getParent().isNT() && dn.getParent().getCfgNode() != null) 
			this.setQueryLine(dn.getParent().getCfgNode().getLocNew());

		if(dn.getCfgNode() != null ){
			System.out.println("Var is: " + dn.getId().toString() + " and CFGNode Type is: " + dn.getCfgNode().getClass() );
		}
		else{
			System.out.println("Var is: " + dn.getId().toString() + " and CFGNode Type is: null");
		}

		String query = this.toXSSAmiable(dn);//this.toAmiable(dn.getId()); //this.toSQLAmiableA(dn);//this.toXSSAmiable(dn); //
		System.out.println("Query Appending started with values: \n****START****\n" + query + "\n****END****\n");
		addVarsToList(dn, this.toXSSAmiable(dn), localQueryVars, localQueryVarsDNs);
		iOffset = query.length() - 1;
		String tmp = "";
		dn.setQueryOffsets(0, iOffset);
		DerivationNode next = dn.getImmSucc();
		Boolean matchFound = false;
		while(next != null){
			if(dn.getCfgNode() != null ){
				System.out.println("Var is: " + next.getId().toString() + " and CFGNode Type is: " + next.getCfgNode().getClass() );
			}
			else{
				System.out.println("Var is: " + next.getId().toString() + " and CFGNode Type is: null");
			}
			matchFound = false;
			for(String eachVarId: this.getVarsMap().keySet()){
				if(eachVarId.equals(this.toXSSAmiable(next))){
					System.out.println("MATCH FOUND for varID " + eachVarId + " and next Id: " + this.toXSSAmiable(next));
					String toAdd = "";
					for(DerivationNode eachLeaf: this.getVarsMap().get(eachVarId) ){
						toAdd += this.toXSSAmiable(eachLeaf);
					}
					addVarsToList(next, toAdd, localQueryVars, localQueryVarsDNs);
					matchFound = true;
					tmp = toAdd;
					break;
				}
				else{
					System.out.println("MISMATCH for varID " + eachVarId + " and next Id: " + this.toXSSAmiable(next));         
				}
			}

			if(matchFound == false){
				tmp = this.toXSSAmiable(next);//this.toSQLAmiable(next.getId());// this.toSQLAmiableA(next);  //this.toXSSAmiable(next)
				addVarsToList(next, this.toXSSAmiable(next), localQueryVars, localQueryVarsDNs);
				next.setQueryOffsets(iOffset + 1, iOffset + tmp.length());
				iOffset += tmp.length();    
				System.out.println("Value to be added to query: \n****START****\n" + tmp + "\n****END****\n" );
			}
			query += tmp;
			next = next.getImmSucc();
		}
		DerivationNode root = this.getRoot();
		CfgNode cfnRoot = root.getCfgNode();
		String fileName = cfnRoot.getFileName();
		String str =  "query_" + this.dtCounter + "_";
		String queriesPath = MyOptions.pixy_home + "/queries/";
		String name = str + fileName + ".html";
		String absoluteFile = queriesPath + name ;
		try {
			PrintWriter outWriter = new PrintWriter(absoluteFile);
			outWriter.println(unescapePHPString(query));
			outWriter.close();
		} 
		catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}

		this.setQueryVarsDNs(localQueryVarsDNs);
		this.setQueryVars(localQueryVars);
		System.out.println("Looping through query vars list");
		for(String dnName: this.getQueryVars()){
			System.out.println("VarName is: " + dnName);
		}
		this.setVarContexts();
		this.setVarContextsDNs();

		D.ps(D.D_PS, "@@@@@@@@@@@@@@@@@ getXSSQuery - " + query);
		return query; 
	}

	/*
	 *
	 * unescape_perl_string()
	 *
	 *      Tom Christiansen <tchrist@perl.com>
	 *      Sun Nov 28 12:55:24 MST 2010
	 *
	 * It's completely ridiculous that there's no standard
	 * unescape_java_string function.  Since I have to do the
	 * damn thing myself, I might as well make it halfway useful
	 * by supporting things Java was too stupid to consider in
	 * strings:
	 * 
	 *   => "?" items  are additions to Java string escapes
	 *                 but normal in Java regexes
	 *
	 *   => "!" items  are also additions to Java regex escapes
	 *   
	 * Standard singletons: ?\a ?\e \f \n \r \t
	 * 
	 *      NB: \b is unsupported as backspace so it can pass-through
	 *          to the regex translator untouched; I refuse to make anyone
	 *          doublebackslash it as doublebackslashing is a Java idiocy
	 *          I desperately wish would die out.  There are plenty of
	 *          other ways to write it:
	 *
	 *              \cH, \12, \012, \x08 \x{8}, \u0008, \U00000008
	 *
	 * Octal escapes: \0 \0N \0NN \N \NN \NNN
	 *    Can range up to !\777 not \377
	 *    
	 *      TODO: add !\o{NNNNN}
	 *          last Unicode is 4177777
	 *          maxint is 37777777777
	 *
	 * Control chars: ?\cX
	 *      Means: ord(X) ^ ord('@')
	 *
	 * Old hex escapes: \xXX
	 *      unbraced must be 2 xdigits
	 *
	 * Perl hex escapes: !\x{XXX} braced may be 1-8 xdigits
	 *       NB: proper Unicode never needs more than 6, as highest
	 *           valid codepoint is 0x10FFFF, not maxint 0xFFFFFFFF
	 *
	 * Lame Java escape: \[IDIOT JAVA PREPROCESSOR]uXXXX must be
	 *                   exactly 4 xdigits;
	 *
	 *       I can't write XXXX in this comment where it belongs
	 *       because the damned Java Preprocessor can't mind its
	 *       own business.  Idiots!
	 *
	 * Lame Python escape: !\UXXXXXXXX must be exactly 8 xdigits
	 * 
	 * TODO: Perl translation escapes: \Q \U \L \E \[IDIOT JAVA PREPROCESSOR]u \l
	 *       These are not so important to cover if you're passing the
	 *       result to Pattern.compile(), since it handles them for you
	 *       further downstream.  Hm, what about \[IDIOT JAVA PREPROCESSOR]u?
	 *
	 */

	public final static String unescapeEscapedQuotes(String oldStr) {
		String modStr = oldStr.replace("\\\"", "\"");
		return modStr;
	}

	public final static String unescapePHPString(String oldstr) {
		System.out.println("unescapePHPString() was called");
		/*
		 * In contrast to fixing Java's broken regex charclasses,
		 * this one need be no bigger, as unescaping shrinks the string
		 * here, where in the other one, it grows it.
		 */ 
		StringBuffer newstr = new StringBuffer(oldstr.length());    
		boolean saw_backslash = false;
		for (int i = 0; i < oldstr.length(); i++) {
			int cp = oldstr.codePointAt(i);
			if (oldstr.codePointAt(i) > Character.MAX_VALUE) {
				i++; /****WE HATES UTF-16! WE HATES IT FOREVERSES!!!****/
			}
			if (!saw_backslash) {
				if (cp == '\\') {
					saw_backslash = true;
				} else {
					newstr.append(Character.toChars(cp));
				}
				continue; /* switch */
			}
			if (cp == '\\') {
				saw_backslash = false;
				newstr.append('\\');
				newstr.append('\\');
				continue; /* switch */
			}
			switch (cp) {
				case '\"':
					if(saw_backslash){ System.out.println("saw_backslash == true"); }
					System.out.println("Case of doublequote");
					newstr.append('\"');
					break;
				case '\'':
					if(saw_backslash){ System.out.println("saw_backslash == true"); }
					System.out.println("Case of singlequote");
					newstr.append('\"');
					break;				
				case 'r':   newstr.append('\r');
							break; /* switch */ 
				case 'n':   newstr.append('\n');
							break; /* switch */
				case 'f':   newstr.append('\f');
							break; /* switch */ 
				/* PASS a \b THROUGH!! */
				case 'b':   newstr.append("\\b");
							break; /* switch */ 
				case 't':   newstr.append('\t');
							break; /* switch */ 
				case 'a':	newstr.append('\007');
							break; /* switch */ 
				case 'e':   newstr.append('\033');
							break; /* switch */ 
				/*
				 * A "control" character is what you get when you xor its
				 * codepoint with '@'==64.  This only makes sense for ASCII,
				 * and may not yield a "control" character after all.
				 *
				 * Strange but true: "\c{" is ";", "\c}" is "=", etc.
				 */
				case 'c':
					if (++i == oldstr.length())
					{
						die("trailing \\c");
					}
					cp = oldstr.codePointAt(i);
					/*
					 * don't need to grok surrogates, as next line blows them up
					 */
					if (cp > 0x7f) { die("expected ASCII after \\c"); }
					newstr.append(Character.toChars(cp ^ 64));
					break; /* switch */
				case '8':
				case '9':
					die("illegal octal digit");
							/* NOTREACHED */
				/*
				 * may be 0 to 2 octal digits following this one
				 * so back up one for fallthrough to next case;
				 * unread this digit and fall through to next case.
				 */
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
					--i;
						/* FALLTHROUGH */ 
				/*
				 * Can have 0, 1, or 2 octal digits following a 0
				 * this permits larger values than octal 377, up to
				 * octal 777.
				 */
				case '0': {
					if (i+1 == oldstr.length()) {
						/* found \0 at end of string */
						newstr.append(Character.toChars(0));
						break; /* switch */
					}
					i++;
					int digits = 0;
					int j;
					for (j = 0; j <= 2; j++) {
						if (i+j == oldstr.length()) {
							break; /* for */
						}
						/* safe because will unread surrogate */
						int ch = oldstr.charAt(i+j);
						if (ch < '0' || ch > '7') {
							break; /* for */
						}
						digits++;
					}
					if (digits == 0) {
						--i;
						newstr.append('\0');
						break; /* switch */
					}
					int value = 0;
					try {
						value = Integer.parseInt(
									oldstr.substring(i, i+digits), 8);
					} catch (NumberFormatException nfe) {
						die("invalid octal value for \\0 escape");
					}
					newstr.append(Character.toChars(value));
					i += digits-1;
					break; /* switch */
				} /* end case '0' */
				case 'x':  {
					if (i+2 > oldstr.length()) {
						die("string too short for \\x escape");
					}
					i++;
					boolean saw_brace = false;
					if (oldstr.charAt(i) == '{') {
							/* ^^^^^^ ok to ignore surrogates here */
						i++;
						saw_brace = true;
					}
					int j;
					for (j = 0; j < 8; j++) {
						if (!saw_brace && j == 2) {
							break;  /* for */
						}
						/*
						 * ASCII test also catches surrogates
						 */
						int ch = oldstr.charAt(i+j);
						if (ch > 127) {
							die("illegal non-ASCII hex digit in \\x escape");
						}
						if (saw_brace && ch == '}') { break; /* for */ }
						if (! ( (ch >= '0' && ch <= '9')
									||
								(ch >= 'a' && ch <= 'f')
									||
								(ch >= 'A' && ch <= 'F')
								)
							 )
						{
							die(String.format(
								"illegal hex digit #%d '%c' in \\x", ch, ch));
						}
					}
					if (j == 0) { die("empty braces in \\x{} escape"); }
					int value = 0;
					try {
						value = Integer.parseInt(oldstr.substring(i, i+j), 16);
					} catch (NumberFormatException nfe) {
						die("invalid hex value for \\x escape");
					}
					newstr.append(Character.toChars(value));
					if (saw_brace) { j++; }
					i += j-1;
					break; /* switch */
				}
				case 'u': {
					if (i+4 > oldstr.length()) {
						die("string too short for \\u escape");
					}
					i++;
					int j;
					for (j = 0; j < 4; j++) {
						/* this also handles the surrogate issue */
						if (oldstr.charAt(i+j) > 127) {
							die("illegal non-ASCII hex digit in \\u escape");
						}
					}
					int value = 0;
					try {
						value = Integer.parseInt( oldstr.substring(i, i+j), 16);
					} catch (NumberFormatException nfe) {
						die("invalid hex value for \\u escape");
					}
					newstr.append(Character.toChars(value));
					i += j-1;
					break; /* switch */
				}
				case 'U': {
					if (i+8 > oldstr.length()) {
						die("string too short for \\U escape");
					}
					i++;
					int j;
					for (j = 0; j < 8; j++) {
						/* this also handles the surrogate issue */
						if (oldstr.charAt(i+j) > 127) {
							die("illegal non-ASCII hex digit in \\U escape");
						}
					}
					int value = 0;
					try {
						value = Integer.parseInt(oldstr.substring(i, i+j), 16);
					} catch (NumberFormatException nfe) {
						die("invalid hex value for \\U escape");
					}
					newstr.append(Character.toChars(value));
					i += j-1;
					break; /* switch */
				}
				default:
					newstr.append('\\');
					newstr.append(Character.toChars(cp));
				 /*
				* say(String.format(
				*		"DEFAULT unrecognized escape %c passed through", cp));
				*/
					break; /* switch */
			}
			saw_backslash = false;
		}
		/* weird to leave one at the end */
		if (saw_backslash) {
			newstr.append('\\');
		}
		return newstr.toString();
	}

	/*
	 * Return a string "U+XX.XXX.XXXX" etc, where each XX set is the
	 * xdigits of the logical Unicode code point. No bloody brain-damaged
	 * UTF-16 surrogate crap, just true logical characters.
	 */
	public final static String uniplus(String s) {
		if (s.length() == 0) {
		 return "";
		}
		/* This is just the minimum; sb will grow as needed. */
		StringBuffer sb = new StringBuffer(2 + 3 * s.length());
		sb.append("U+");
		for (int i = 0; i < s.length(); i++) {
			sb.append(String.format("%X", s.codePointAt(i)));
			if (s.codePointAt(i) > Character.MAX_VALUE) {
				i++; /****WE HATES UTF-16! WE HATES IT FOREVERSES!!!****/
			}
			if(i+1 < s.length()) {
				sb.append(".");
			}
		}
		return sb.toString();
	}

	private static final void die(String foa) {
		throw new IllegalArgumentException(foa);
	}

	private static final void say(String what) {
		System.out.println(what);
	}
}
