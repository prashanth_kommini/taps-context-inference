/**
 * 
 */
package edu.uic.rites.sisl;

import java.util.ArrayList;

import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNode;
import at.ac.tuwien.infosys.www.pixy.conversion.*;
/**
 * @author prithvi
 *
 */
public class DerivationNodeTerminal extends DerivationNode
{
	public DerivationNodeTerminal(TacPlace id)
	{
		this.id = id;
		this.type = TERMINAL;

		nodeCounter++;
		nodeNumber = nodeCounter;
		if(id == null)
			throw new Error(" terminal id cannot be null -- ");
	}	

	public DerivationNodeTerminal(TacPlace id, boolean bConstant)
	{
		this(id);
		this.isNumber = bConstant;
	}
	
	public DerivationNodeTerminal(TacPlace id, String op)
	{
		this(id);
		this.operator = op;
	}	

	public DerivationNode dup()
	{
		DerivationNodeTerminal dnt = 
			new DerivationNodeTerminal(this.getId(), this.operator);
		dnt.type = this.type;
		dnt.progNode = this.progNode;
		dnt.isNumber = this.isNumber;
		dnt.phiValue = this.phiValue;

		return dnt;
	}
}
