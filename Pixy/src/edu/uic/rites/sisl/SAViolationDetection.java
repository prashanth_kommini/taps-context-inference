package edu.uic.rites.sisl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
//import java.util.concurrent.CopyOnWriteArrayList;

import at.ac.tuwien.infosys.www.pixy.MyOptions;
import at.ac.tuwien.infosys.www.pixy.analysis.dep.DepGraph;
import at.ac.tuwien.infosys.www.pixy.conversion.Cfg;
import at.ac.tuwien.infosys.www.pixy.conversion.TacActualParam;
import at.ac.tuwien.infosys.www.pixy.conversion.TacFormalParam;
import at.ac.tuwien.infosys.www.pixy.conversion.TacFunction;
import at.ac.tuwien.infosys.www.pixy.conversion.TacOperators;
import at.ac.tuwien.infosys.www.pixy.conversion.TacPlace;
import at.ac.tuwien.infosys.www.pixy.conversion.Variable;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNode;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeAssignArray;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeAssignBinary;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeAssignRef;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeAssignSimple;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeAssignUnary;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeBasicBlock;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeCall;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeCallBuiltin;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeCallPrep;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeCallRet;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeCallUnknown;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeEcho;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeIf;



/**
 * This class ensures that the program statements being checked wont have 
 * undsired affect on other parts of the codebase. 
 * 
 * High Level Algorithm:
	 1. get all CFG nodes that are proposed to be changed specifically: not that 
	 	will now have placeholders, nodes that donate some part to make up a data
	 	subtree, and all ancestors of such nodes.
	 2. for each such node, get its CFG and get all operations that use it...
	 if they are not in the whitelist, declare failure if forbid is set, issue 
	 warning otherwise. 
 * 
 * @author sisl
 * 
 */
public class SAViolationDetection {
	
	/**
	 * in case a violation is detected, this contains the proposed
	 * changes for the violating transformation
	 */
	ArrayList<ProgramChange> progChanges;
	
	/**
	 * violating control flow 
	 */
	ControlFlow cf; 
	
	/**
	 * violating sink
	 */
	CfgNode sink; 
	
	/**
	 * violating derivation tree
	 */
	DerivationTree dt;
	
	/**
	 * unused
	 */
	private boolean forbidOnWarning; 
	
	/**
	 * singleton object for SAViolationDetection
	 */
	private static SAViolationDetection salimit;
	
	/**
	 * private constructor, initialize violation rules
	 */
	private SAViolationDetection()
	{
		initViolRules();
	}
	
	/**
	 * This method initializes a whitelist of functions that are allowed
	 * to work on modified CFG Nodes
	 * "append", "join", "merge", "implode", "mysql_query"
	 * Essentially the above exceptions are needed for construction and execution of 
	 * SQL queries.
	 */
	private void initViolRules() {
		// TODO : make this configurable through a file 
		String[] str = 
		{
				"append", 
				"join",
				"merge",
				"implode",
				"mysql_query"
		};
		
		SAViolationDetection.inBuiltAllowed = new ArrayList<String>();
		for(String s : str)
			SAViolationDetection.inBuiltAllowed.add(s);		
	}

	/**
	 * Only one instance can be created
	 * @return singleton SAViolationDetection analysis instance
	 */
	public static SAViolationDetection get()
	{
		if(salimit == null)
			salimit = new SAViolationDetection();
		
		return salimit;
	}
	
	/**
	 * @param cf	: control flow to be analyzed for use of non-whitelisted inbuilt functions 
	 * @param sink	: sink under consideration
	 * @param dt	: derviation tree for SQL query being computed at sink
	 * @param forbid	: if true, transformation is aborted when a violation is detected. 
	 * @param pcRequired : proposed program changes in the control flow cf
	 * @return			: null when no violations are found.
	 * @throws SISLException: if a violation is detected, a SISLException with violation details is
	 * thrown.
	 */
	public ArrayList<ViolationReport> analyze(ControlFlow cf, 
			CfgNode sink, DerivationTree dt, boolean forbid, 
			ArrayList<ProgramChange> pcRequired) 
	throws SISLException 
	{
		this.cf = cf;
		this.sink = sink;
		this.dt = dt; 
		this.forbidOnWarning = forbid; 
		this.progChanges = pcRequired;
		
		D.pr("Static violation detection started...");
		// checks : 
		// get all impacted nodes - 
			// placeholders... 
			// borrowers 
			// and all nodes above them
		// for each such node, get its CFG and get all operations that use it...
		// if they are not in the whitelist, declare failure if forbid is set, issue 
		// warning otherwise. 
		
		// get all the nodes of derivation tree, that contain some proposed 
		// change 
		ArrayList<ChangedStmt> changedStmts = dt.getChangedNodes();
		if(changedStmts.size() == 0)
		{
			D.pr(D.D_TEMP, " SA Violation: there are no changed nodes in the " +
					"derivation tree - no violation");
			return null;
		}
		
		SISLException secb = null; 
		SISLException seiu = null;
/*		try
		{
			// detect callbacks in the control flow 
			//getCallbackViolations(changedStmts, dt);
			getCallBacksInbuilt(f)
		}
		catch(SISLException se2)
		{
			secb = se2;
		}
		*/
//		try
		{
			// detect use of modified vars 
			D.pr(D.D_TEMP, " changed statements - " + changedStmts);//ChangedStmt.getProgLinesSorted(changedStmts));
			getViolations(changedStmts);		
		}
//		catch(SISLException se3)
		{
//			seiu = se3;
		}
/*
		String msg = null;
		if(secb != null)
		{
			msg = " \n " + secb.getSISLMessage() + " \n";
			if(seiu != null)
			{
				msg += " \n " + seiu.getSISLMessage() + "\n";
			}
		}
		else
		if(seiu != null)
		{
			msg = " \n " + seiu.getSISLMessage() + " \n";
		}

		if(!MyOptions.option_F)
		{
			if(msg != null)
				throw new SISLException(SISLException.FOUND_STATIC_WARNINGS, msg);
		}*/
		return null;
	}

	/**
	 * @param cf: Find call back violations in control flow cf. Call back violations 
	 * are caused by language features that allow variables to hold function names 
	 * and be invoked through special functions e.g., read 
	 * http://php.net/manual/en/function.call-user-func.php.
	 * 
	 * @throws SISLException: TAPS does not handle language callbacks and throws an exception if the control flow 
	 * under consideration contains such a function.
	 */
	public static void getCBViolations(ControlFlow cf) throws SISLException 
	{
		D.pr("....Starting callback based violations detection...");
		
		// helper method that returns a list of violating inbuilt calls in control flow cf
		ArrayList<CfgNode> calls = getCallBacksInbuilt(cf);
		
		if((calls == null || calls.size() == 0))
		{
			D.pr(" no callback capable functions invoked....");
			return;
		}
		
		/*
		 * If a violation is detected, directly add it to the TransformationResults for 
		 * reporting purposes. Additionally throw FOUND_CALLBACK_VIOLATIONS_IN_TRANSFORM
		 * type of exception if -f (force transformation) option is not specified. 
		 */
		String str = "";
		TransformationResults trs = TransformationResults.get();			
		for(CfgNode cn : calls)
		{
			trs.pr.addCallbackViolation(cn);
			str += " \t [ \n " + cn + " \n\t ] \n";
		}

		if(!MyOptions.option_F)
		{
			throw new SISLException(SISLException.FOUND_CALLBACK_VIOLATIONS_IN_TRANSFORM, str);
		}
	}
	

	/**
	 * @param cf2
	 * @return: return a list of CFG nodes that contain violating language 
	 * callbacks in the control flow cf2; empty list otherwise
	 */
	private static ArrayList<CfgNode> getCallBacksInbuilt(ControlFlow cf2)
	{
		// the following two are the known callbacks used for PHP. 
		// for more refer to http://php.net
    	ArrayList<String> sisl_langCallBacks = new ArrayList<String>();
    	sisl_langCallBacks.add("call_user_func_array");
    	sisl_langCallBacks.add("call_user_func");

    	ArrayList<CfgNode> inbuilts = new ArrayList<CfgNode>();
    	
		ArrayList<CfgNode> inFlow = cf2.getAll();			
		for(CfgNode cn : inFlow)
		{
			String fname = null;
			if(cn instanceof CfgNodeCallBuiltin)
			{
				// check to see if any inbuilt function call matches
				// with names in sisl_langCallBacks
				fname = ((CfgNodeCallBuiltin) cn).getFunctionName();
				if(sisl_langCallBacks.contains(fname))
					inbuilts.add(cn);
			}
		}

		return inbuilts;
	}

	/** Unused method 
	 * @param changedStmts
	 * @param dt
	 * @throws SISLException 
	 */
	private void getCallbackViolations(ArrayList<ChangedStmt> changedStmts,
			DerivationTree dt) throws SISLException 
	{
//		ArrayList<CfgNode> flowNodes = this.cf.getNodesBetween();
		D.pr("....Starting callback based violations detection...");
//		D.pr(" nodes between flow - " + flowNodes);
		ArrayList<CfgNodeCallBuiltin> builtins = new ArrayList<CfgNodeCallBuiltin>();
		ArrayList<CfgNodeCall> calls = new ArrayList<CfgNodeCall>(); 

		getCallBacksInFlow(cf, builtins, calls);
		if((builtins == null || builtins.size() == 0) && 
				(calls == null || calls.size() == 0))		{
			D.pr(" no callback capable functions invoked....");
			return;
		}
		
		if(builtins != null)
			for(CfgNodeCallBuiltin cncb : builtins)
				D.pr(" ========== Found a builtin callback in current flow - " + cncb.getFunctionName());

		if(calls != null)
			for(CfgNodeCall cnc : calls)
				D.pr(" ========== Found a callback capable call in current flow - " + cnc.getCallee().getName());

		
		ArrayList<DataStmt> dataVars = new ArrayList<DataStmt>();
		ArrayList<CodeStmt> unmodifiedQueries = new ArrayList<CodeStmt>();
		ArrayList<CodeStmt> modifiedQueries = new ArrayList<CodeStmt>();
		dt.getData(dataVars, unmodifiedQueries, modifiedQueries);
		
		D.pr("..............printing derivation tree analysis data......");
		if(dataVars != null)
		{
			D.pr("Data nodes - ");
			for(DataStmt ds : dataVars)
				D.pr("\t data - " + ds);
		}
		
		if(unmodifiedQueries != null)
		{
			D.pr("Un modified query nodes - ");
			for(CodeStmt cs : modifiedQueries)
				D.pr(" \t unmodified query - " + cs);			
		}
		
		if(modifiedQueries != null)
		{
			D.pr("Modified query nodes - ");
			for(CodeStmt cs : modifiedQueries)
				D.pr(" \t modified query - " + cs);
		}
		
		ArrayList<CallBackViolationReport> reports = 
			new ArrayList<CallBackViolationReport>();

		detectDataViolation(dataVars, builtins, calls, reports);
		//	detectPartialQueriesViolation(allQueryVars);

		String str = "";
		if(reports.size() != 0)
		{
			TransformationResults trs = TransformationResults.get();			
			for(CallBackViolationReport cbvr : reports)
			{
				ArrayList<CBFailure> cbfs = cbvr.probableFailures;
				for(CBFailure f : cbfs)
				{
					trs.pr.addCallbackViolation(f.root);
				}

				str += " \t [ \n " + cbvr + " \n\t ] \n";
			}

			if(!MyOptions.option_F)
			{
				throw new SISLException(SISLException.FOUND_CALLBACK_VIOLATIONS_IN_TRANSFORM, str);
			}
		}
	}

	// algorithm : UNUSED Method
	// if a callback uses 
	//			a. unreferenced data : issue warning for missed transformation
	//			b. referenced data : issue warning for potential conflicts
	//			c. does not contain any data used in queries - issue ? warning 
	//				for missed transformation
	/**
	 * @param dataVars
	 * @param builtins
	 * @param calls
	 * @param reports
	 */
	private void detectDataViolation(ArrayList<DataStmt> dataVars, 
			ArrayList<CfgNodeCallBuiltin> builtins, 
			ArrayList<CfgNodeCall> calls, ArrayList<CallBackViolationReport> reports) 
	{
		boolean usesSameData = false; 
		ArrayList<Variable> varsInQuery = new ArrayList<Variable>();
		for(DataStmt ds : dataVars)
		{
			if(!varsInQuery.contains(ds.v))
				varsInQuery.add(ds.v);
		}
		
		ArrayList<CfgNode> allCalls = new ArrayList<CfgNode>(builtins);
		allCalls.addAll(calls);
		
		for(CfgNode cn : calls)
		{
			List<Variable> actuals = null;			
			List<TacActualParam> actualTacs = null;
			if(cn instanceof CfgNodeCallBuiltin )
			{
				actuals = cn.getVariables();				
				actualTacs = ((CfgNodeCallBuiltin) cn).getParamList();
			}
			else
			if(cn instanceof CfgNodeCall)
			{
				actuals = cn.getVariables();
				actualTacs = ((CfgNodeCall) cn).getParamList();
			}
			
			// now, then, it may be a violation if it contains
			for(Variable dataInQuery : varsInQuery)
			{
				int actualChanged = inUse(dataInQuery, actuals); 
				if(actualChanged != -1)
				{
					boolean isRef = actualTacs.get(actualChanged).isReference();
					System.out.println("Found a callback using same data as query....." + 
							cn + " variable - " + dataInQuery + " ? reference - " + isRef);
//					System.exit(1);
					int iType = CallBackViolationReport.SAME_DATA_NO_REF_IN_CALLBACK;
					if(isRef)
						iType = CallBackViolationReport.SAME_DATA_WITH_REF_IN_CALLBACK;
					
					//CallBackViolationReport cbvr = new CallBackViolationReport
					//	(dataInQuery, cn, progChanges, iType);
					addReportsCBV(iType, dataInQuery, cn, reports, this.progChanges);
//					reports.add(cbvr);
//					throw new SISLException(SISLException.FOUND_CALLBACK_VIOLATIONS_IN_TRANSFORM, 
	//							cbvr.toString());
//					}
						
				}
			}
			
			
		}
		
		
	}

	/**
	 * @param cf2
	 * @param builtins
	 * @param calls
	 */
	private void getCallBacksInFlow
		(ControlFlow cf2, ArrayList<CfgNodeCallBuiltin> builtins, 
				ArrayList<CfgNodeCall> calls) 
	{
		ArrayList<CfgNode> inFlow = cf2.getAllInterProcInBetween();
			
		for(CfgNode cn : inFlow)
		{
			String fname = null;
			if(cn instanceof CfgNodeCallBuiltin)
			{
				fname = ((CfgNodeCallBuiltin) cn).getFunctionName();
				if(DepGraph.sisl_langCallBacks.contains(fname))
					builtins.add((CfgNodeCallBuiltin)cn);
			}
			else
			if(cn instanceof CfgNodeCall)
			{
				TacFunction invoked = ((CfgNodeCall) cn).getCallee();
				fname = invoked.getName();
				if(DepGraph.sisl_allCallers.contains(invoked))
					calls.add((CfgNodeCall)cn);
			}
			else
			{
				continue;
			}			
		}

		return;
	}

	
	//  ============== changed queries reuse detection


	/**
	 * 
	 */
	private static ArrayList<String> inBuiltAllowed;	
	/**
	 * @param changedStmts
	 * @return
	 * @throws SISLException
	 */
	private ArrayList<ViolationReport> getViolations(
			ArrayList<ChangedStmt> changedStmts) throws SISLException {
		
		//ArrayList<Failure> viols = new ArrayList<Failure>();
		ArrayList<Failure> vioT = null;
		ArrayList<ViolationReport> reports = new ArrayList<ViolationReport>();
		
		ArrayList<ChangedStmtGroup> csGroupsToProcess = ChangedStmt.groupByCfg(changedStmts);
		
		for(ChangedStmtGroup csg : csGroupsToProcess)
		{
			CfgNode cnForSSA = null;
			ArrayList<Variable> changedVarsT =  new ArrayList<Variable>();
			ArrayList<CfgNode> forReach = new ArrayList<CfgNode>();
			
			for(ChangedStmt cs : csg.group ) 
			{
				for(Variable vt : cs.getChangedVars())
					if(!changedVarsT.contains(vt))
						changedVarsT.add(vt);
					
				CfgNode cfgN = cs.cn; 
				if(cfgN.getFileName().equals("<file name unknown>") || 
						cfgN.getOrigLineno() < 0)
					continue; 
				else
					cnForSSA = cfgN;
				
				if(!forReach.contains(cfgN))
					forReach.add(cfgN);
			}

			CfgAnalysis.resetMe();
			ArrayList<Variable> changedVars =  new ArrayList<Variable>(changedVarsT);

			// check that the CFG is already in SSA format, transform otherwise
			// reuse the logic for reachability computation from SSA analysis
			TacFunction tf = cnForSSA.getEnclosingFunction();
			Cfg cfg = tf.getCfg();
			SSATransformer.transformSSACfg(cfg);
			D.pr(D.D_TEMP, " In flow - " + this.cf);
			D.pr(D.D_TEMP, " ############### computing violations for - " + csg);

			
			ArrayList<CfgNode> reachableAfterChange = new ArrayList<CfgNode>();
			for(CfgNode cfgN : forReach)
				SAViolationDetection.reachable(cfgN, reachableAfterChange);
			
			extendBasicBlocks(reachableAfterChange);
			
			D.pr(D.D_TEMP, " nodes reachable from \n \t" + csg + " -- \n\t\t" + 
					CfgNode.getProgLinesSorted(reachableAfterChange));
			
			changedVars = extendSetOfChangedVariables(changedVars, reachableAfterChange);
			
			ArrayList<CfgNodeCall> calls = new ArrayList<CfgNodeCall>();
			ArrayList<CfgNodeCallBuiltin> inbuilts = new ArrayList<CfgNodeCallBuiltin>();
			ArrayList<CfgNodeEcho> echoes = new ArrayList<CfgNodeEcho>();
			
			findCalls(reachableAfterChange, calls, inbuilts, echoes);
			
			D.pr(".........................printing data independence violation analysis results..........");
			D.pr(" in program calls - " + calls.size() + " ----- " + calls);
			D.pr(" builtin calls - " + inbuilts.size() + " ----- " + inbuilts);
			D.pr(" echoes calls - " + echoes.size() + " ----" + echoes);
			D.pr(" extended set of vars to track - " + changedVars);
			
			for(Variable changed : changedVars)	
			{
				D.pr(" Invoking calls program from in cfg with - " + calls + " with changed var - " + changed);
				
				D.pr(D.D_TEMP, " 111 before call prog changed set - ");
				vioT = findViolatingCallsProgram(calls, changed, csg);
				addReports(vioT, changed, reports, csg);
				//D.pr(" \t\t\t violations - " + vioT);
				
				D.pr(D.D_TEMP, " 111 before call in built changed set - ");
				vioT = findViolatingCallsInbuilt(inbuilts, changed, csg);
				addReports(vioT, changed, reports, csg );

				D.pr(D.D_TEMP, " 111 before call echoes changed set - ");
				vioT = findViolatingEchoes(echoes, changed, csg);
				addReports(vioT, changed, reports, csg);
			}
		}
		
		TransformationResults trs = TransformationResults.get();
		if(reports.size() != 0)
		{
			// found violation for current symbolic query 
			//dt.getSymbQuery()
			String str = "{ \n"; //\n found violations - \n\t{\n ";
			for(ViolationReport vr : reports)
			{
				str += " \t [ \n " + vr + " \n\t ] \n";
				ArrayList<Failure> fails = 
					new ArrayList<Failure>(vr.probableFailures); 
				
				D.pr(" ================= violations report =======================");
				D.pr(" Recommended changes  = " + vr.recommendedChange );
				ArrayList<Failure> nestedNodes = new ArrayList<Failure>();
				while(true)
				{
					for(Failure f : fails)
					{
						D.pr("check this for correctness - adding a violation " + f.root);
						trs.pr.addNonWhiteListOp(f.root);
						if(f.nested != null)
						{
							D.pr("...............a nested non-white list violation" + f.nested);
							nestedNodes.addAll(f.nested);
						}
					}
					
					if(nestedNodes.size() != 0)
					{
						fails.clear();
						fails.addAll(nestedNodes);
						nestedNodes.clear();
					}
					else
						break;
				}
				D.pr(" ================= violations report end =======================");

//				String varName = changed.getName();
//				if(changed.getOrig() != null)
//					varName = changed.getOrig().getName();
//				
//				D.pr(D.D_TEMP, "Inbuilt functions --- found violations for variable - " + 
//						varName + " at CfgNode - " + vi);
//				str += "\t [ variable - " + varName + " at line - " + 
//				vi.getFileName() + " : " + vi.getOrigLineno() + " ] \n";
			}
			str += "\t }";
//			if(this.forbidOnWarning || )
			if(!MyOptions.option_F)
			{
				throw new SISLException(SISLException.FOUND_STATIC_VIOLATIONS_IN_TRANSFORM, str);
			}
		}
		
		return reports;
	}

    /**
     * @param reports
     */
    private void removeDups(ArrayList<ViolationReport> reports) 
    {
    	
	}

	/**
	 * @param echoes
	 * @param changed
	 * @param csg
	 * @return
	 */
	private ArrayList<Failure> findViolatingEchoes(
			ArrayList<CfgNodeEcho> echoes, Variable changed, ChangedStmtGroup csg) 
	{
    	ArrayList<Failure> viol = null;

    	for(CfgNodeEcho echo : echoes )
    	{
	    	List<Variable> actuals = echo.getVariables();
			D.pr(D.D_TEMP, " actual for inbuilt - " + echo + actuals);
			// now, then, it may be a violation if it contains
			// the changed variable, this is not correct...
			// for each change we must look at nodes reachable from 
			// that...not in the whole CFG..
	
			if(inUse(changed, actuals) != -1)
			{
				if(viol == null)
					viol = new ArrayList<Failure>();
				viol.add(new Failure(echo));
			}
    	}
    	
		return viol;
	}

	/**
	 * @param reachableAfterChange
	 */
	private void extendBasicBlocks(ArrayList<CfgNode> reachableAfterChange) {
    	ArrayList<CfgNode> extendedList = 
    		new ArrayList<CfgNode>();
    	
    	for(CfgNode cn : reachableAfterChange)
    	{
    		if(cn instanceof CfgNodeBasicBlock)
    		{
    			for(CfgNode cnt : ((CfgNodeBasicBlock) cn).getContainedNodes())
    				if(!extendedList.contains(cnt))
    					extendedList.add(cnt);
    		}
    		else
    		{
				if(!extendedList.contains(cn))
					extendedList.add(cn);
    		}
    	}
    	
    	reachableAfterChange = new ArrayList<CfgNode>(extendedList);
	}

	/**
	 * @param changedVars
	 * @param reachableAfterChange
	 * @return
	 */
	private ArrayList<Variable> extendSetOfChangedVariables(ArrayList<Variable> changedVars,
			List<CfgNode> reachableAfterChange) 
    {
		boolean bChange = true;
		boolean bChange1 = false;
		D.pr(D.D_TEMP, " in extend - " + reachableAfterChange);
		
//		while(bChange)
		{
			bChange = false; 
	    	for(CfgNode cn : reachableAfterChange)
			{
	        	D.pv(D.D_TEMP, " !!!!!!!!!!!!!!! analyzing - " + cn );    	
	    		LinkedHashMap<Variable, ArrayList<Variable>> phis = cn.getPhiNodes();
	    		if(phis != null && phis.size() != 0)
	    		{
	    			for(Map.Entry<Variable, ArrayList<Variable>> e : phis.entrySet())
	    			{
	    				Variable phiLhs = e.getKey();
	    				List<Variable> phiRhs = e.getValue();
	    				D.pv(D.D_TEMP, " \t #### phi value for " + cn +  " -> Lhs = " + 
	    						phiLhs + " rhs = " + phiRhs + " changed - " + changedVars);
	    				
	    				ArrayList<Variable> t = new ArrayList<Variable>(changedVars);
	    				for(Variable changed : changedVars)
	    				{
	    					if(phiRhs.contains(changed) && !t.contains(phiLhs))
	    					{
	    						bChange = true;
	    						t.add(phiLhs);
	    					}
	    				}
	    				changedVars = t;
	    			}
	    		}
	    		
	    		if(cn instanceof CfgNodeAssignArray ||
	        		cn instanceof CfgNodeAssignBinary || 
	        		cn instanceof CfgNodeAssignSimple || 
	        		cn instanceof CfgNodeAssignUnary)
	    		{
	    			ArrayList<Variable> all = cn.getRightToRename();

	    			D.pr(D.D_TEMP, " processing an assign stmt now - " + cn);
	    			D.pr(D.D_TEMP, " \t Right to rename - " + all );
	    			D.pr(D.D_TEMP, " \t changed vars - " + changedVars);
	    			D.pr(D.D_TEMP, "\t left - " + cn.getLeft());
					bChange1 = extend_helper(all, changedVars, cn.getLeft());
					if(!bChange)
						bChange = bChange1; 
	    		}
	    		else
	    		if( cn instanceof CfgNodeAssignRef	)
	    		{
	    			CfgNodeAssignRef cnar = (CfgNodeAssignRef)cn;
	    			Variable vTmp = cnar.getRight().getVariable();
	
	    			ArrayList<Variable> all = new ArrayList<Variable>();
	    			all.add(vTmp);
	    			bChange1 = extend_helper(all, changedVars, cnar.getLeft());
	    			if(!bChange)
	    				bChange = bChange1;
	    		}
	    		else
	    		if( cn instanceof CfgNodeCall)
	    		{
	    			CfgNodeCall cnc = (CfgNodeCall) cn;
	    			
	    			// TODO: this should not add callers return value 
	    			// but the return assignment....
	    			bChange1 = analyzeCfgForReturn(changedVars, cnc);
	    			if(!bChange)
	    				bChange = bChange1;
	    			
	    			D.pv(D.D_TEMP, "analyzed cfg returned with - " + bChange1 + " chagned vars - " + changedVars);
	    			//bChange1 = extend_helper(cnc.getVariables(), changedVars, cnc.getTempVar());
	    			//if(!bChange)
	    			//	bChange = bChange1;
	    			//bChange = extend_helper_cfg(cnc.getCallee().getCfg(), changedVars, cnc.getTempVar());
	    		}
	    		else
	    		if( cn instanceof CfgNodeCallBuiltin )
	    		{
	    			CfgNodeCallBuiltin cncb = (CfgNodeCallBuiltin) cn;
	    			if(cncb.getFunctionName().equals("mysql_query"))
	    				continue;
	    			
	    			Variable vReturn = cncb.getTempVar();
	    			bChange1 = extend_helper(cncb.getVariables(), changedVars, cncb.getTempVar(), false);
	    			if(bChange1 && cn.getSuccessors() != null)
	    			{
	    				
	    				// inbuilt calls have the format 
	    				//		inbuilt(arguments)<temp_return>
	    				//		var = temp_return;
	    				// so if the inbuilt takes in a changed variable
	    				// conservatively, mark the var as changed
    					CfgNode suc = cn.getSuccessor(0);
    					D.pv(D.D_TEMP, "check ...........successor of inbuilt call......" + cncb + " is - " + suc);
    					List<Variable> rhs = suc.getRightToRename();
    					D.pv(D.D_TEMP, "\t\t right to rename contains - " + rhs + " and vReturn " + 
    							vReturn + " and succ left - " + suc.getLeft());
    					if(rhs.contains(vReturn))
    					{
    						changedVars.add(suc.getLeft());
    					}
	    			}
	    			
	    			if(!bChange)
	    				bChange = bChange1;
	    		}
	    		else
	    		{
	    			D.pv(D.D_TEMP, " \t\t extension not handled - " + cn.toString());
	    		}
	    		
	        	D.pv(D.D_TEMP, " \t\t chagned vars - - " + changedVars);
			}		    	
		}
    	
		ArrayList<Variable> newExtended = new ArrayList<Variable>();
    	for(Variable v : changedVars)
    	{
    	//	if(v.isTemp())
    	//		continue;
    		newExtended.add(v);
    	}
    	
    	changedVars = newExtended; 
    	D.pr(D.D_TEMP, "Extended set of vars - " + changedVars);
    	return changedVars;
	}

//	private boolean extend_helper_cfg(CfgNodeCall call, Cfg cfg, ArrayList<Variable> changedVars,
//			Variable tempVar) {
//
//		List<Variable> actuals = call.getVariables();
//		boolean bRetTainted = false;
//		// now, then, it may be a violation if it contains
//		// the changed variable, this is not correct...
//		// for each change we must look at nodes reachable from 
//		// that...not in the whole CFG..
//		for(Variable changed : changedVars)
//		{
//			int actualChanged = inUse(changed, actuals); 
//			if( actualChanged != -1)
//			{
//				// now analyze Cfg of the called method to find violations
//				List<TacFormalParam> formals = call.getCallee().getParams();
//				Variable vFormal = formals.get(actualChanged).getVariable();
//				ArrayList<Failure> nested = new ArrayList<Failure>();
//				D.pr("Finding violation for formal " + vFormal + " in - " + call.getCallee().getName());
//				boolean v = findViolationsInCfg(call.getCallee().getCfg(), vFormal, nested, cs);
//			}
//		}
//		
//		return false;
//	}

	/**
	 * @param changedVars
	 * @param cnc
	 * @return
	 */
	private boolean analyzeCfgForReturn(ArrayList<Variable> changedVars,
			CfgNodeCall cnc) 
	{
		boolean bRet = false;
		TacFunction called = cnc.getCallee();
		Cfg cfg = called.getCfg();	
//		DepGraph.transformSSACfg(cfg);

		D.pv(D.D_TEMP, " Starting return analysis for function ----- " + 
				called.getName() + " changed vars - " + changedVars);

		ArrayList<Variable> actuals = new ArrayList<Variable>(cnc.getVariables());
		List<TacFormalParam> formals = called.getParams();
		D.pv(D.D_TEMP, " actual parameters - " + actuals);
		
		ArrayList<Variable> formalsToAnalyze = new ArrayList<Variable>();
		
		for(Variable v : changedVars)
		{
			int actualChanged = inUse(v, actuals); 			
			if(actualChanged == -1)
				continue;
			
			Variable vFormal = formals.get(actualChanged).getVariable();
			if(!formalsToAnalyze.contains(vFormal))
				formalsToAnalyze.add(vFormal);
		}

		

		D.pv(D.D_TEMP, " all formals to track ----- " + formalsToAnalyze);
		// detect if we have seen this already
		CfgAnalysis ca = CfgAnalysis.isAlreadyAnalyzed(cfg);
		if(ca == null)
		{
			ca = new CfgAnalysis(cfg, formalsToAnalyze, changedVars);
			CfgAnalysis.addNewAnalysis(ca);
		}
		else
		{
			// if we come here ---- recursion
			if(ca.iProcessed == -1)
			{
				// processing started but we re-visited...there is recursion
				// err on the side of false warnings...mark this method as returning 
				// changed vars. 
				ca.setProcessed(true);
				// and return false to terminate the change affect from this method.
				return true;
			}
			else
			{
				// we have already processed what required to be processed.
				// terminate anybody outside sensing changes. 
				return false;
			}
		}
		
		
		extendSetOfChangedVariables(formalsToAnalyze, cfg.getAll());
		
		Variable cfgReturn = called.getRetVar();
		D.pv(D.D_TEMP, " extended set of formals ----- " + formalsToAnalyze);
		D.pv(D.D_TEMP, " return value of this function - " + cfgReturn);
		if(formalsToAnalyze.contains(cfgReturn))
		{
			// now the return value is directly tainted...
			// add the caller's return value as to be tracked. 
			Variable vReturn = cnc.getTempVar();
			D.pv(D.D_TEMP, " callers receiving value - " + vReturn);
			if(changedVars.contains(vReturn))
			{
				bRet = false;
				ca.setProcessed(true);
			}
			else
			{
				D.pv(D.D_TEMP, " extending the set of return variables from - " + changedVars);
				changedVars.add(vReturn);
				bRet = true;
				ca.setProcessed(true);
			}
		}
		else
		{
			ca.setProcessed(false);
		}
		
		return bRet;
	}

	/**
	 * @param list
	 * @param changedVars
	 * @param vt
	 * @return
	 */
	private boolean extend_helper(List<Variable> list,
			ArrayList<Variable> changedVars, Variable vt)
	{
		return extend_helper(list, changedVars, vt, true);
	}
	
	/**
	 * @param list
	 * @param changedVars
	 * @param vt
	 * @param add
	 * @return
	 */
	private boolean extend_helper(List<Variable> list,
			ArrayList<Variable> changedVars, Variable vt, boolean add) 
	{
		boolean bChange = false; 
		
		if(list == null || list.size() == 0)
			return bChange;
		
		D.pv(D.D_TEMP, " trying to extend - changed - " + changedVars + " rhs = " + list + " lhs = " + vt);
		for(Variable v : list)
		{
			if(v == null) continue; 
			if(changedVars.contains(v))
			{
				if(!changedVars.contains(vt))
				{
					if(vt.isMember())
						continue;
					
					bChange = true;
					if(add)
						changedVars.add(vt);
				}
				
				Variable arr = null;
				if(vt.isArrayElement())
				{
					arr = vt.getEnclosingArray();
					if(arr.isMember())
						continue;

					if(!changedVars.contains(arr))
					{
						bChange = true;
						if(add)
							changedVars.add(arr);
					}
				}				
			}
		}		
		D.pd(D.D_TEMP, " \t after extending -!!!! added new? " + bChange + " changed - " + changedVars);
		
		return bChange;
	}

	/**
	 * @param reachableAfterChange
	 * @param calls
	 * @param inbuilts
	 * @param echoes
	 */
	private void findCalls(List<CfgNode> reachableAfterChange,
			ArrayList<CfgNodeCall> calls, ArrayList<CfgNodeCallBuiltin> inbuilts, ArrayList<CfgNodeEcho> echoes) 
    {
		for(CfgNode cn : reachableAfterChange)
		{
			D.pv(D.D_TEMP, " analyzing - node - " + cn);
			if(cn instanceof CfgNodeCall && !calls.contains(cn))
			{
				D.pv(D.D_TEMP, " \t found a call node - " + cn);
				calls.add((CfgNodeCall)cn);
			}
			
			if(cn instanceof CfgNodeCallBuiltin && !inbuilts.contains(cn))
			{
				D.pv(D.D_TEMP, " \t found an inbuilt call - " + cn);
				inbuilts.add((CfgNodeCallBuiltin)cn);
			}
			
			if(cn instanceof CfgNodeEcho && !echoes.contains(cn))
			{
				D.pv(D.D_TEMP, "\t found an echo node - " + cn);
				echoes.add((CfgNodeEcho)cn);
			}
		}
    }

	/**
	 * @param from
	 * @param visited
	 * @return
	 * @throws SISLException
	 */
	public static ArrayList<CfgNode> reachable(CfgNode from, ArrayList<CfgNode> visited) throws SISLException 
    {	
    	TacFunction tf = from.getEnclosingFunction();
    	Cfg cfg = tf.getCfg();
    	CfgNode exit = cfg.getTail();
    	
    	boolean bVisited = visited.contains(from);
        if (!bVisited) 
        {
        	if(from.equals(exit))
        	{
//       		D.pv(D.D_TEMP, prefix + " found the exit node " + exit);
        		visited.add(exit); 
        		return visited;
        	}	

            visited.add(from);

        	if(from instanceof CfgNodeIf)
        	{
        		CfgNodeIf cni = (CfgNodeIf)from;
            	CfgNode thenB = cni.getThen();
            	if(thenB instanceof CfgNodeBasicBlock)
            		thenB = ((CfgNodeBasicBlock)thenB).getContainedNodes().get(0);

            	CfgNode elseB = cni.getElse();
            	if(elseB instanceof CfgNodeBasicBlock)
            		elseB = ((CfgNodeBasicBlock)elseB).getContainedNodes().get(0);

        		reachable(thenB, visited);            	
        		reachable(elseB, visited);
        	}
        	else
        	{
        		List<CfgNode> succ = from.getSuccessorsNoBlocks();
        		D.pv(D.D_TEMP, "\t\t out edges - " + succ.size() + " and -- " + succ);
            	if(succ.size() == 1)
            	{
                	D.pv(D.D_TEMP, "\t\t\t start - " + succ.get(0));
            		reachable(succ.get(0), visited);
            	}
            	else
            	{
            		if(succ.size() != 0)
            		{
    					String msg = " TODO: Successor size is " + succ.size() + " succ " + succ;
            			ToolBug.addSAV_REACHABILITY_FAILURE(msg);
            			SISLException sisl = new SISLException( 
            					SISLException.BAIL_OUT, 
            					"BAIL_OUT");
            			sisl.fillInStackTrace();
            			throw sisl;
            		}
            	}
            }
        }
        return visited;
    }

	/**
	 * @param iType
	 * @param violated
	 * @param cn
	 * @param reports
	 * @param pc
	 */
	private void addReportsCBV(int iType, Variable violated, CfgNode cn, 
			ArrayList<CallBackViolationReport> reports, ArrayList<ProgramChange> pc) 
	{		
		CallBackViolationReport vr = findReportCBV(violated, cn, reports, pc);
		if(vr == null)
		{
			vr = new CallBackViolationReport(violated, 
					new CBFailure(cn, iType, violated), 
					this.progChanges);
			reports.add(vr);
		}
		else
		{
			vr.addFailure(new CBFailure(cn, iType, violated));
		}
	}

	/**
	 * @param violated
	 * @param cn
	 * @param reports
	 * @param pc
	 * @return
	 */
	private CallBackViolationReport findReportCBV(Variable violated,
			CfgNode cn, ArrayList<CallBackViolationReport> reports, 
			ArrayList<ProgramChange> pc) 
	{
		for(CallBackViolationReport vr : reports)
		{
			if(vr.recommendedChange.equals(pc))
				return vr;
		}
	
		return null;
	}

	
	
	/**
	 * @param vioT
	 * @param changed
	 * @param reports
	 * @param csg
	 */
	private void addReports(ArrayList<Failure> vioT, Variable changed,
			ArrayList<ViolationReport> reports, ChangedStmtGroup csg) 
	{
		if(vioT == null || vioT.size() == 0)
			return; 
		
		for(Failure violated : vioT)
		{
			ViolationReport vr = findReport(changed, vioT, reports, csg);
			if(vr == null)
			{
				vr = new ViolationReport(violated, this.progChanges, csg);
				reports.add(vr);
			}
			
			vr.addFailureStmt(violated);
		}	
	}

	/**
	 * @param changed
	 * @param vioT
	 * @param reports
	 * @param csg
	 * @return
	 */
	private ViolationReport findReport(Variable changed,
			ArrayList<Failure> vioT, ArrayList<ViolationReport> reports, ChangedStmtGroup csg) 
	{
		for(ViolationReport vr : reports)
		{
			if(vr.csg.equals(csg))
				return vr;
		}
	
		return null;
	}

	/**
	 * @param calls
	 * @param changed
	 * @param csg
	 * @return
	 */
	private ArrayList<Failure> findViolatingCallsProgram(ArrayList<CfgNodeCall> calls,
			Variable changed, ChangedStmtGroup csg) 
	{
		ArrayList<CfgNode> inFlow = this.cf.getAll();
		ArrayList<Failure> viol = null;
		
		D.pr(" In find program violation - " + calls);
		for(CfgNodeCall call : calls)
		{
			// do not analyze calls in the flow...
			// violations may occur from other calls...
			// SSA will help to reduce the false positives
			if(inFlow.contains(call))
			{
				D.pr(D.D_TEMP, " Ignored a call node in flow ----------------- " + call);
				continue; 
			}
			else
			{
				D.pr(D.D_TEMP, " Processing a call node ............ " + call);
			}
			D.pr(" \t changed variable - " + changed);
			
			List<Variable> actuals = call.getVariables();
			
			// now, then, it may be a violation if it contains
			// the changed variable, this is not correct...
			// for each change we must look at nodes reachable from 
			// that...not in the whole CFG..
			D.pr("\t actuals - " + actuals);
			int actualChanged = inUse(changed, actuals); 
			if( actualChanged != -1)
			{
				// now analyze Cfg of the called method to find violations
				List<TacFormalParam> formals = call.getCallee().getParams();
				Variable vFormal = formals.get(actualChanged).getVariable();
				ArrayList<Failure> nested = new ArrayList<Failure>();
				D.pr("\t \t Finding violation for formal " + vFormal + " in - " + call.getCallee().getName());
				boolean v = findViolationsInCfg(call.getCallee().getCfg(), vFormal, nested, csg);
				if(v)
				{
//					D.pr("\t\t\t\t ########## VIOLATION " + call + " nested - " + nested);
					
					Failure f = new Failure(call);
					f.addToNested(nested);
					if(viol == null)
						viol = new ArrayList<Failure>();
					viol.add(f);
					D.pr(D.D_TEMP, " a nested probl - " + f);
				}
			}
		}

		if(viol != null)
			for(Failure vi : viol)
				D.pr(D.D_TEMP, "\t\t\t VIOLATION PROG CALL --- for variable - " + 
						changed.getName() + " for Change Stmt - " + csg + " violation - " + viol);
		return viol;		
	}

	/**
	 * @param cfg
	 * @param changed
	 * @param nested
	 * @param csg
	 * @return
	 */
	private boolean findViolationsInCfg(Cfg cfg, Variable changed, ArrayList<Failure> nested, ChangedStmtGroup csg) 
	{
		boolean bViol = false; 
		ArrayList<Failure> vioT = null;

		ArrayList<CfgNodeCall> calls = new ArrayList<CfgNodeCall>();
		ArrayList<CfgNodeCallBuiltin> inbuilts = new ArrayList<CfgNodeCallBuiltin>();
		ArrayList<CfgNodeEcho> echoes = new ArrayList<CfgNodeEcho>();
		
		ArrayList<Variable> changedVars = new ArrayList<Variable>();
		changedVars.add(changed);

		extendSetOfChangedVariables(changedVars, cfg.getAll());
		
		ArrayList<CfgNode> stmts = new ArrayList<CfgNode>(cfg.getAll());
		extendBasicBlocks(stmts);
		
		findCalls(stmts, calls, inbuilts, echoes);			
		D.pr("\t\t ------------ found stmts in cfg ------------" + 
				Cfg.getFunction(cfg.getHead()).getName());
		D.pr("\t\t\t in cfg \t\t\t calls - " + calls );
		D.pr("\t\t\t in cfg \t\t\t inbuilts - " + inbuilts );
		D.pr("\t\t\t in cfg \t\t\t calls - " + echoes );
		D.pr("\t\t\t changed vars = " + changedVars);
		for(Variable v : changedVars)
		{
			D.pr(" \t\t\t\t Analyzing calls now - ");
			vioT = findViolatingCallsProgram(calls, v, csg);
			if(vioT != null)
				nested.addAll(vioT);
			
			D.pr(" \t\t\t\t Analyzing inbuilts now - ");
			vioT = findViolatingCallsInbuilt(inbuilts, v, csg);
			if(vioT != null)
			{
				nested.addAll(vioT);
				D.pr(D.D_TEMP, "............ size of vioT - " + vioT.size() + " vioT - " + vioT);
			}
			
			D.pr("\t\t\t\t analyzing echoes now");
			vioT = findViolatingEchoes(echoes, v, csg);
			if(vioT != null)
				nested.addAll(vioT);
		}
		
		if(vioT != null)
			for(Failure vi : vioT)
				D.pr(D.D_TEMP, "\t\t\t VIOLATION IN CFG --- for variable - " + 
						changed.getName() + " violation - " + vi);

		boolean bRet = false; 
		if(nested.size() != 0)
			bRet = true;
		
		D.pr(".....................completed CFG analysis...........: changed - " + bRet);
		return bRet;
	}

	/**
	 * @param inbuilts
	 * @param changed
	 * @param csg
	 * @return
	 */
	private ArrayList<Failure> findViolatingCallsInbuilt(
			ArrayList<CfgNodeCallBuiltin> inbuilts, Variable changed, ChangedStmtGroup csg) 
	{
		ArrayList<Failure> viol = null;
		ArrayList<CfgNode> inFlow = this.cf.getAll();
		
		for(CfgNodeCallBuiltin inb : inbuilts)
		{
		
			String name = inb.getFunctionName();
			if(SAViolationDetection.inBuiltAllowed.contains(name))
				continue; 

			// do not analyze calls in the flow...
			// violations may occur from other calls...
			// SSA will help to reduce the false positives
			if(inFlow.contains(inb))
				continue; 


			List<Variable> actuals = inb.getVariables();
			D.pr(D.D_TEMP, " actual for inbuilt - " + inb + " are - " + actuals);
			// now, then, it may be a violation if it contains
			// the changed variable, this is not correct...
			// for each change we must look at nodes reachable from 
			// that...not in the whole CFG..

			if(inUse(changed, actuals) != -1)
			{
				D.pr(D.D_TEMP, " !!!!!!!!!! found a violation for inbuilt call " + inb);
				if(viol == null)
					viol = new ArrayList<Failure>();
				viol.add(new Failure(inb));
			}
		}

		if(viol != null)
			for(Failure vi : viol)
				D.pr(D.D_TEMP, "\t\t\t VIOLATION IN BUILT --- for variable - " + 
						changed.getName() + " for Change group - " + csg + " violation - " + vi);
		return viol;
	}

	
	/**
	 * @param changed
	 * @param actuals
	 * @return
	 */
	private int inUse(Variable changed, List<Variable> actuals) {
		D.pr(D.D_TEMP, "!!!!!!!!! detecting in use - changed = " + changed + " actuals - " + actuals);
		// compare with original not SSA; 
//		Variable toCompare = changed.getOrig(); 
	//	if(toCompare == null)
		//	toCompare = changed; 
		
		if(actuals == null || actuals.size() == 0)
			return -1; 
		
		int i = -1;
		for(Variable v : actuals)
		{
			i++;
			if(v == null) continue; 
		
//			Variable vTmp = v.getOrig();
	//		if(vTmp == null)
		//		vTmp = v; 
			
	//		if(vTmp.equals(toCompare))
		//		return i;
			if(v.equals(changed))
				return i;

		}
		
		return -1; 
	}
}

/**
 * @author prithvi
 *
 */
class ViolationReport
{
	/**
	 * 
	 */
	public ChangedStmtGroup csg;
//	Variable v; 
	//ArrayList<CfgNode> probableFailures; 
	/**
	 * 
	 */
	ArrayList<Failure> probableFailures; 
	/**
	 * 
	 */
	ArrayList<ProgramChange> recommendedChange; 

	//
//	public ViolationReport(Variable v, CfgNode failure, CopyOnWriteArrayList<ProgramChange> copyOnWriteArrayList, ChangedStmt cs)
	/**
	 * @param failure
	 * @param pcs
	 * @param csg
	 */
	public ViolationReport(Failure failure, 
			ArrayList<ProgramChange> pcs, ChangedStmtGroup csg)
	{
//		this.v = v; 
//		this.probableFailures = new ArrayList<CfgNode>();
		this.probableFailures = new ArrayList<Failure>();
		this.probableFailures.add(failure);
		
		this.recommendedChange = pcs;
		this.csg = csg;
	}
	
	/**
	 * @param violated
	 */
	public void addFailureStmt(Failure violated) {
		if(this.probableFailures == null)
			this.probableFailures = new ArrayList<Failure>();
		
		if(!this.probableFailures.contains(violated))
			this.probableFailures.add(violated);
	}

	/**
	 * @param vr
	 * @return
	 */
	public boolean equals(ViolationReport vr)
	{
		//		if(this.v != null && vr.v != null)
//			bEq = this.v.equals(vr.v);
//	
//		if( bEq )
//			bEq = ControlFlow.compareForSameNodes(this.probableFailures, vr.probableFailures);
//		
//		if(bEq && !(this.recommendedChange.containsAll(vr.recommendedChange) && 
//				vr.recommendedChange.containsAll(this.recommendedChange)))
//			bEq = false; 
		if(this.csg.equals(vr.csg))
			return true;
		
		return false; 
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
		String vars = " Variable(s) - [ ";
		for(ChangedStmt cs : this.csg.group)
		{
			ArrayList<String> varT = new ArrayList<String>();
			for (Variable v : cs.changedVars)
			{
				String vName = v.getName();
				
				if(v.getOrig() != null)
					vName = v.getOrig().getName();
				
				if(!varT.contains(vName))
					varT.add(vName);
			}	
			for(String s : varT)
				vars += s + " , ";
			vars += " ] ";
		}
		
		String str = " \t Static Analysis Warning : " + vars + " require(s) "; 	
		str += " modification to transform as - \n"; 
		for(ProgramChange pc : this.recommendedChange)
			str += "\t\t " + pc + "\n";
		
		str += " \t and may cause change of semantics in these stmts - ";
		for(Failure f : this.probableFailures)
		{
//			String absFile = cn.getAbsFileName();
//			String fileName = cn.getFileName();
//			int iLine = cn.getOrigLineno();
//			
//			String fa = ControlFlow.getLineNumber(absFile, iLine);
//			str += " \n\t\t " + fileName + ":" + iLine + "< " + fa + " >";
			str += " \n \t\t" + f.toString();
		}
		return str; 
	}
}

class Failure
{
	CfgNode root; 
	ArrayList<Failure> nested; 
	int iType ;
	
	public Failure(CfgNode root){ this.root = root;}
	
	public Failure(CfgNode cn, int type) {
		this(cn);
		this.iType = type;
	}

	public void addToNested(Failure f){
		if(this.nested == null)
			this.nested = new ArrayList<Failure>();
		
		this.nested.add(f);
	}
	
	public void addToNested(ArrayList<Failure> nested2)
	{
		if(this.nested == null)
			this.nested = new ArrayList<Failure>();
		this.nested.addAll(nested2);
		D.pv(D.D_TEMP, "Adding ................nesetd - " + this.nested + " argument - " + nested2);
	}
	
	public String toString()
	{
		String str = "";
		String absFile = this.root.getAbsFileName();
		String fileName = this.root.getFileName();
		int iLine = this.root.getOrigLineno();
		
		String fa = ControlFlow.getLineNumber(absFile, iLine);
		str += fileName + ":" + iLine + "< " + fa + " >";
		
		if(this.nested != null && this.nested.size() != 0)
		{
			str += "\n\t\t\t\t Above method call has following violations - ";
			for(Failure f : this.nested)
			{
				str += "\n\t\t\t\t\t - " + f.toString();
			}
		}
		
		return str;
	}
	
//	public boolean equals(Failure f)
//	{
//		if(this.)
//	}
}

class CBFailure
{
	CfgNode root; 
	int iType ;
	Variable v;
		
	public CBFailure(CfgNode cn, int type, Variable v) {
		this.root = cn;
		this.iType = type;
		this.v = v;
	}

	public String toString()
	{
		String vName = this.v.getName();
		if(this.v.getOrig() != null)
			vName = this.v.getOrig().getName();
		
		String str = "";
		String absFile = this.root.getAbsFileName();
		String fileName = this.root.getFileName();
		int iLine = this.root.getOrigLineno();
		
		String fa = ControlFlow.getLineNumber(absFile, iLine);
		str += " \t Callback " + fileName + ":" + iLine + "< " + fa + " >";		
		
		switch(this.iType)
		{
		case CallBackViolationReport.MODIFIED_PARTIAL_QUERY_IN_CALLBACK:
			str += " \n \t\t\t Possible semantics violation: uses a partial query " + 
//				vName + " that will be modified in transformation";
				"that will be modified by transformation (partial query in " + vName + ")";

			break;
		case CallBackViolationReport.SAME_DATA_NO_REF_IN_CALLBACK:
			str += " \n \t\t\t Possible missed transformation: - uses data used in query computation (data in " + 
				vName + " )";
			break;
		case CallBackViolationReport.SAME_DATA_WITH_REF_IN_CALLBACK:
			str += " \n \t\t\t Possible semantics violation: uses data used in query computation by reference - (data in " + 
				vName + ")";
			break;
		case CallBackViolationReport.UNMODIFIED_PARTIAL_QUERY_IN_CALLBACK:
			str += " \n \t\t\t Possible missed transformation: uses a partial query (partial query stored in - " + 
				vName + " - unmodified in transformation)";

			break;

		default:
			str += " internal error: callback violation not populated";
				break;
		}

		return str;
	}
}


class DataStmt
{
	Variable v; 
	CfgNode cn ;
	public DataStmt(Variable v, CfgNode cn)
	{
		this.v = v; 
		this.cn = cn;
	}
	
	public String toString()
	{
		String loc = cn.getFileName() + " : " + cn.getOrigLineno();
		return " Data variable - " + this.v.getName() + " in prog : " + loc + " ";
	}

	public static void addIfAbsent(ArrayList<DataStmt> dataVars, CfgNode c,
			Variable v2) {

		for(DataStmt dsd : dataVars)
		{
			if(dsd.v.equals(v2))
				return;
		}
		DataStmt ds = new DataStmt(v2, c);
		dataVars.add(ds);
	}
}

class CodeStmt
{
	Variable v; 
	CfgNode cn ;
	boolean modified;
	public CodeStmt(Variable v, CfgNode cn, boolean mod)
	{
		this.v = v; 
		this.cn = cn;
		this.modified = mod;
	}
	
	public static void addIfAbsent(ArrayList<CodeStmt> modifiedQueries,
			CfgNode c, Variable v2, boolean mod) 
	{
		CodeStmt cs = new CodeStmt(v2, c, mod);
		if(!modifiedQueries.contains(cs))
			modifiedQueries.add(cs);
	}
	
	public String toString()
	{
		String loc = cn.getFileName() + " : " + cn.getOrigLineno();
		return " Partial query stmt - " + loc + " " + 
			(modified ? "modified" : "unmodified") ;
	}

}

class CallBackViolationReport
{
	public static final int SAME_DATA_NO_REF_IN_CALLBACK = 1;
	public static final int SAME_DATA_WITH_REF_IN_CALLBACK = 2; 
	public static final int MODIFIED_PARTIAL_QUERY_IN_CALLBACK = 3;
	public static final int UNMODIFIED_PARTIAL_QUERY_IN_CALLBACK = 4;
	
	Variable v;
	boolean isReference; 
	int type;
	
	ArrayList<CBFailure> probableFailures; 
	ArrayList<ProgramChange> recommendedChange; 

	public CallBackViolationReport(Variable v, CBFailure f,
			ArrayList<ProgramChange> pcs)
	{
		this.v = v; 
		this.probableFailures = new ArrayList<CBFailure>();
		this.probableFailures.add(f);		
		this.recommendedChange = pcs;
		this.type = type; 
//		this.cs = cs;
	}
	
	public void addFailure(CBFailure failure) {
		this.probableFailures.add(failure);
	}

	public boolean equals(CallBackViolationReport vr)
	{
		if(this.recommendedChange.equals(vr.recommendedChange) 
				&& this.type == vr.type)
			return true;
		
		return false; 
	}
	
	public String toString()
	{
		String str = " \t Following change will be made by transformation: \n";
		for(ProgramChange pc : this.recommendedChange)
			str += "\t\t " + pc + "\n";
		
		str += " \t and may cause following violations - ";
		for(CBFailure f : this.probableFailures)
			str += " \n \t " + f.toString();

		return str; 
	}	
}

class CfgAnalysis
{
	int iProcessed;
	Cfg cfg; 
	ArrayList<Variable> formalsAnalyzed; 
	ArrayList<Variable> forChangedVars; 
	boolean bResult;	
	
	private static ArrayList<CfgAnalysis> analyzed; 
	
	
	public CfgAnalysis(Cfg cfg, ArrayList<Variable> formalsAnalyzed, 
			ArrayList<Variable> forChangedVars)
	{
		this.cfg = cfg; 
		this.forChangedVars = forChangedVars; 
		this.formalsAnalyzed = formalsAnalyzed; 
		this.iProcessed = -1;
		
		if(CfgAnalysis.analyzed == null)
		{
			CfgAnalysis.analyzed = new ArrayList<CfgAnalysis>();
		}
	}
	
	public static void resetMe() {
		CfgAnalysis.analyzed = new ArrayList<CfgAnalysis>();
	}

	public void setProcessed(boolean bResult) {
		this.iProcessed = 1;
		this.bResult = bResult;
	}

	public static void addNewAnalysis(CfgAnalysis ca) {
		CfgAnalysis.analyzed.add(ca);
	}

	public static CfgAnalysis isAlreadyAnalyzed(Cfg cfg)
	{
		if(CfgAnalysis.analyzed == null)
			return null;
	
		for(CfgAnalysis ca : CfgAnalysis.analyzed)
		{
			if(ca.cfg.equals(cfg))
				return ca; 
		}
		
		return null;
	}
}