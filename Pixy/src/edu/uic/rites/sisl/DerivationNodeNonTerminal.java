/**
 * 
 */
package edu.uic.rites.sisl;

import at.ac.tuwien.infosys.www.pixy.Dumper;
import at.ac.tuwien.infosys.www.pixy.conversion.TacPlace;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNode;

import java.util.*;
/**
 * @author prithvi
 *
 */
public class DerivationNodeNonTerminal extends DerivationNode
{
	public DerivationNodeNonTerminal(TacPlace id, String op)
	{			
		this(id);
		this.operator = op;
	}		

	public DerivationNodeNonTerminal(TacPlace id)
	{
		this.id = id;
		this.type = NONTERMINAL;
		if(id == null)
			throw new Error(" id cannot be null -- ");
		nodeCounter++;
		nodeNumber = nodeCounter;
	}		
	
	public DerivationNode dup()
	{
		DerivationNodeNonTerminal dnnt = 
			new DerivationNodeNonTerminal(this.getId(), this.operator);
		dnnt.type = this.type;
		dnnt.progNode = this.progNode;
		dnnt.children = new ArrayList<DerivationNode>();
		if(children != null)
			for(DerivationNode dn : children)
				dnnt.children.add(dn.dup());
		
		dnnt.isNumber = this.isNumber;
		
		D.pv(D.D_DT, " dupping actual2formal: " + this.actual2formal + " for = " + dnnt + " cfg node - " + this.progNode );
		dnnt.actual2formal = this.actual2formal;
		dnnt.returnAssign = this.returnAssign;
		
		dnnt.phiValue = this.phiValue;
		return dnnt;
	}

}
