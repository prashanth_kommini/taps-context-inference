package edu.uic.rites.sisl;

import java.util.ArrayList;

import at.ac.tuwien.infosys.www.pixy.conversion.Cfg;
import at.ac.tuwien.infosys.www.pixy.conversion.TacPlace;
import at.ac.tuwien.infosys.www.pixy.conversion.Variable;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNode;

public class ChangedStmt {

	CfgNode cn; 
	ArrayList<Variable> changedVars; 
	
	/**
	 * @return the changedVars
	 */
	public ArrayList<Variable> getChangedVars() {
		return changedVars;
	}

	public ChangedStmt(CfgNode cn, ArrayList<Variable> changedVars)
	{
		this.cn = cn ; 
		this.changedVars = changedVars; 
	}
	
	public ChangedStmt(CfgNode cn2) {
		this.cn = cn2; 
		this.changedVars = new ArrayList<Variable>();
	}

	public boolean equals(ChangedStmt cs)
	{
		D.pr(D.D_TEMP, " Changed stmt - equality test : " + cn + " and " + cs);
		return this.cn.equals(cs.cn);
	}
	
	public String toString()
	{
		String str = "";
		str += cn.getFileName() + " : " + cn.getOrigLineno();
		if(changedVars != null)
		{
			str += " [changed values of - [ ";
			for(Variable v : changedVars)
				str += v.getName() + ", ";
			
			str += " ]";
		}
		
		return str;
		
	}

	public static void addIfAbsent(ArrayList<ChangedStmt> changes,
			DerivationNode dn) 
	{
		CfgNode cn = dn.getCfgNode();
		if(cn == null)
			return; 
		
		ChangedStmt csFound = null;
		for(ChangedStmt cs : changes)
		{
			if(cs.cn.equals(cn))
			{
				csFound = cs; 
				break;
			}
		}
		
		if(csFound == null)
		{
			csFound = new ChangedStmt(cn);
			changes.add(csFound);
		}
		
    	TacPlace tp = dn.getId();
		if(tp.isVariable())
		{
			Variable v = tp.getVariable();
//			if(v.getOrig() != null)
//				v = v.getOrig();
			D.pr(D.D_TEMP, "........Adding variable = " + v);
			
			if(!csFound.changedVars.contains(v))
				csFound.changedVars.add(v);	
		}
	}

	public static String getProgLinesSorted(ArrayList<ChangedStmt> changedStmts) {
		ArrayList<CfgNode> cns = new ArrayList<CfgNode> ();
		
		for(ChangedStmt cs : changedStmts)
			cns.add(cs.cn);
			
		return CfgNode.getProgLinesSorted(cns);
	}
	
	public static ArrayList<ChangedStmtGroup> groupByCfg(ArrayList<ChangedStmt> changes)
	{
		ArrayList<ChangedStmtGroup> groups = new ArrayList<ChangedStmtGroup>();
		
		ArrayList<ChangedStmt> visited = new ArrayList<ChangedStmt>();
		
		for(ChangedStmt cs : changes)
		{
			if(!visited.contains(cs))
				visited.add(cs);
			else
				continue; 
			
			Cfg cscfg = Cfg.getFunction(cs.cn).getCfg();
			ChangedStmtGroup csg = new ChangedStmtGroup();			
			csg.addChange(cs);
			groups.add(csg);
			
			for(ChangedStmt csRest : changes)
			{
				if(csRest.equals(cs) || visited.contains(csRest))
					continue; 
				
				// now we are seeing a new change
				Cfg csRestCfg= Cfg.getFunction(cs.cn).getCfg();
				if(cscfg.equals(csRestCfg))
				{
					visited.add(csRest);
					csg.addChange(csRest);
				}
			}
		}
		
		return groups;
	}
}

class ChangedStmtGroup
{
	ArrayList<ChangedStmt> group; 
	public ChangedStmtGroup()
	{
		this.group = new ArrayList<ChangedStmt>();
	}
	
	public void addChange(ChangedStmt cs)
	{
		if(!this.group.contains(cs))
			this.group.add(cs);
	}

	public String toString()
	{
		String str = "\t {{ ";
		for(ChangedStmt cs : group)
			str += " \t\t\t " + cs + " \n";
		
		str += "\t }} ";
		return str;
	}
}
