/**
 * 
 */
package edu.uic.rites.sisl;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
//import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;

import at.ac.tuwien.infosys.www.pixy.analysis.dep.DepGraph;
import at.ac.tuwien.infosys.www.pixy.analysis.dep.DepGraphNode;
import at.ac.tuwien.infosys.www.pixy.analysis.dep.DepGraphNormalNode;
import at.ac.tuwien.infosys.www.pixy.analysis.dep.DepGraphOpNode;
import at.ac.tuwien.infosys.www.pixy.analysis.dep.DepGraphUninitNode;
import at.ac.tuwien.infosys.www.pixy.analysis.dep.Sink;
import at.ac.tuwien.infosys.www.pixy.conversion.Cfg;
import at.ac.tuwien.infosys.www.pixy.conversion.Literal;
import at.ac.tuwien.infosys.www.pixy.conversion.TacActualParam;
import at.ac.tuwien.infosys.www.pixy.conversion.TacFunction;
import at.ac.tuwien.infosys.www.pixy.conversion.TacPlace;
import at.ac.tuwien.infosys.www.pixy.conversion.Variable;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNode;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeAssignSimple;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeAssignUnary;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeCall;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeCallBuiltin;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeCallPrep;

/**
 * @author sisl
 *
 */
/**
 * @author prithvi
 *
 */
public class IsolationAnalysis
{
	// singleton for IsolationAnalysis
	private static IsolationAnalysis ia;
	
	// all control flows to be considered for transformation
	private ArrayList<ControlFlow> allPaths; 
	
	// all proposed program changes 
	private ArrayList<ProgramChange> progChanges;
	
	// kludge for fixing name problem with certain variables 
	public static boolean m_bFixNameProblem;
	
	//private static TransformationResults results;
	
	public ArrayList<ControlFlow> getPaths(){ return allPaths; }
	
	/* 
	 * SISL: Initialize allPaths (collection of control flows that create and execute SQL queries)
	 * and progChanges (collection of program changes to convert a given application to make use 
	 * of PREPARE statements 
	 */
	private IsolationAnalysis()
	{
		this.m_bFixNameProblem = false;
		this.allPaths = new ArrayList<ControlFlow>();
		this.progChanges = new ArrayList<ProgramChange>();
	}
	
	/* 
	 * create an instance of IsolationAnalysis, if not present 
	 */
	public static void init()
	{
		if(ia == null)
			ia = new IsolationAnalysis();						
	}

	/**
	 * 1. First major step is given a DepGraph dg, find control flows that it encloses. 
	 * 2. For each control flow, create a derivation (execution) tree. 
	 * 3. Find SQL query that the execution tree computes and parse it using a SQL parser. 
	 * 4. Find data subtrees in execution tree with the help of SQL parser. 
	 * 5. Introduce Placeholder nodes as the root of all data subtrees (statements that 
	 * compute data arguments to queries). 
	 * 6. Capture program changes represented by the new derivation tree. 
	 * @param depGraph: dependency graph for sink path sp 
	 * @param sp : sink path to be transformed
	 * @return : unused
	 * @throws SISLException: if there are conflicts in transforming this SinkPath 
	 * or other errors (including tool bugs) occur
	 */
	private ArrayList<ControlFlow> enumeratePaths(DepGraph depGraph, SinkPath sp) 
	throws SISLException
	{
		System.out.println("enumeratePaths() called");
		DerivationTree dt = null;

		CfgNode sink = depGraph.getRoot().getCfgNode();
		ArrayList<ControlFlow> interPaths = null;
		
		try
		{
			// perform both intra and inter procedural path enumeration 
			interPaths = InterProcPathEnumerator.enumeratePaths(depGraph, sp);
		}
		catch(Exception e)
		{
			TransformationResults.get().incrementSinkCount(1);
			if(e instanceof SISLException)
				throw (SISLException ) e;
			
			e.printStackTrace();
			throw new Error(" Unexpected exception - " + e);
		}
		
		D.pr("returned total paths- " + (interPaths == null? 0 : interPaths.size()));
		System.out.println("returned total paths- " + (interPaths == null? 0 : interPaths.size()));
		
		for(ControlFlow cf : interPaths)
		{

			D.pr("trying paths - \n");
			if(cf.isProcessed())
				continue; 
			else
				cf.setProcessed(true);

			try
			{
				TransformationResults.get().incrementSinkCount(1);
				D.pr("Transition_Look ....checking for language callbacks in this flow...");
				
				// SISL: Language callback functions  such as call_user_func_array, 
				// cannot be reliably analyzed in a static fashion. To make sure 
				// that we are sound, look for the presence of such functions in 
				// the control control flow, and if found declare a SAViolation (Static Analysis
				// violation).
				TimeProfiler.start();
				SAViolationDetection.getCBViolations(cf);
				TimeProfiler.end(D.D_CBV);

				TimeProfiler.start();
				
				
				// SISL: for each interprocedural control flow, create the corresponding 
				// derivation (execution) tree i.e., capture execution steps the control 
				// flow performs to compute the query that is being executed at the sink. 
				
				D.pr(" Transition_Look: processing interflow - " + cf.toString());
				for(CfgNode cn : cf.getAll())
					D.pr(" \t cfg Node " + cn);
			
				ArrayList<CfgNode> toDerivationTree = new ArrayList<CfgNode> ();
				for(CfgNode cn : cf.getAll())
					toDerivationTree.add(cn);
								
				D.pr("............found none......generating derivation tree now...");
				
				dt = DerivationTree.makeDerivationTree(cf, toDerivationTree, sink, depGraph);		
				
				D.pr("...done......transforming the derivation tree...");


				D.pr(D.D_TEMP, "Transition_Session2_Look Starting the derivation tree transformation- ");

				// SISL: convert the derivation (execution) tree to instead generate a 
				// prepare statement. 
				dt.makePS();

				// SISL: update stats about max functions traversed by control flows, 
				// max data arguments extracted from queries
				TransformationResults.get().addMaxCfInterProc(cf);
				TransformationResults.get().addMaxArgsExtracted(dt, cf, ia.progChanges);
				
				// SISL: Optimize proposed changes to this control flow 
				ia.dumpProgChanges(cf, sink, dt);
				TimeProfiler.end(D.D_DT);
				
		        TimeProfiler.start();
				D.pr("checking for violation related to use of values that will be modified after transformation...");
				SAViolationDetection.get().analyze(cf, sink, dt, true, ia.progChanges);
				TimeProfiler.end(D.D_SAV);
				
				TimeProfiler.start();
				D.pr("Transition_Look...generated symbolic query\n (SISL_QUERY:) " + dt.getSymbQuery() + 
						CfgNode.getProgLinesSorted(cf.getAll()) + 
						"\n\t\t Proposed changes - " + 
						"\n\t\t " + ia.progChanges.toString());
				
				Transformation.addProposedChanges(cf, sink, ia.progChanges);
				ia.progChanges.clear();
				TimeProfiler.end(D.D_TR);
			}
			catch(Exception se)
			{
				D.pr(D.D_TEMP, "caught xception " + se.getMessage());
				se.printStackTrace();
				if(se instanceof SISLException)
				{
					if(((SISLException) se).getType() != SISLException.BAIL_OUT)
						TransformationResults.get().addFailureReportX((SISLException)se, cf, sink);	
					else
					{
						// dont report bail out messages they have already been added
						//ToolBug.add_UNCLASSIFIED_YET((SISLException)se);
					}
				}
				else
				if(se instanceof SQLException)
				{
					String symbQuery = dt.getSymbQuery();
					D.pr(" ....... symbolic query - " + 
							symbQuery + " failed - " + se.getMessage());
					TransformationResults.get().addSQLParserFailure(sink, cf, symbQuery);
				}
				else
				{
					D.pv(D.D_TEMP, " .... failed exception - " + se.getMessage() + " ");
					se.printStackTrace();
					D.pr(" ...failed...Exception --- " + se.getStackTrace());
					
					SISLException seNew = new SISLException(SISLException.INTERNAL_TOOL_BUG, 
							se.getMessage());
					seNew.setStackTrace(se.getStackTrace());
					ToolBug.add_UNCLASSIFIED_YET(seNew);
				}
			}
		}		
	
		return null;
	}

	/*
	 * The following changes may occur in a derviation tree, 
	 * 	a. New node introduced (corresponds to a new statement being inserted before the child node)
	 *  b. A node modified (corresponds to a statement being modified) 
	 *  c. query argument propagation (corresponds to additional meta data being passed around along with 
	 *  PS structure, corresponds to assigning to and reading from superglobal _GLOBALS;
	 *  Look for changes of types a, b and c and record them for conflict detection and program transformation
	 */
	public void dumpProgChanges(ControlFlow cf, CfgNode sink, DerivationTree dt) throws SISLException
	{	
		// Conflicts such as the following can only be checked once we have changes for all flows. 
		// optimzed program output may have more conflicts e.g., 
		/* if()
		 * 		$q = "select * from x where uid = '$u';
		 * else
		 * 		$q = "select * from x where uid = '$u' or u = 'admin'";
		 * 
		 * will result in a conflict immediately after if-else
		 */
		boolean bDumpOptimized = true;
		
		for(DerivationNode dn : dt.bfOrder())
		{
			String inFile = dn.getLocation();
			D.ps(D.D_TR, " processing dn for location - " + inFile + " -- " + dn.dotName());

			dumpParamPassing(dn);
			dumpPlaceholderComputation(dn);

			if(!bDumpOptimized)
				dumpChanges2Orig(dn);
			else
				dumpChanges2OrigOpt(dn);				

			dumpReturnRecv(dn);			
		}		
		
		// now add the change for sink. 
		// replace it with 
		dumpSink(dt);
		
		
		// now store this information for conflict detection
		// and transformation later
		saveMap(cf, sink);
	}
	
	private void saveMap(ControlFlow cf, CfgNode sink) {
		// TODO Auto-generated method stub
		
	}

	private void dumpSink(DerivationTree dt) 
	{
		CfgNode sink = dt.getSink();
		D.pv(D.D_TEMP, " Dumping changes to sink " + sink);

		if(sink instanceof CfgNodeCallBuiltin)
		{
			// stmts created for $result = mysql_query($query);
//			Dumping changes to sink examples.php : 23\nmysql_query(test1_1.$query_SSA0) <test1_1._t0_0>
//			params of the inbuilt calls - 
//				test1_1.$query_SSA0
//			tmp value - test1_1._t0_0
//			successor - examples.php : 23\ntest1_1.$result = test1_1._t0_0_SSA2
			
			TacPlace queryValue = null;
			Variable queryVariable = null;
			CfgNodeCallBuiltin cncb = (CfgNodeCallBuiltin) sink;
			List<TacActualParam> params = cncb.getParamList();
			D.pv(D.D_TEMP, " params of the inbuilt calls - " );
			for(TacActualParam tap : params)
			{
				D.pv(D.D_TEMP, " \t\t  " + tap.getPlace());
				
			}
			// first argument is the query
			queryValue = params.get(0).getPlace();
			if(queryValue.isVariable())
			{
				queryVariable = queryValue.getVariable();
				if(queryVariable.getOrig() != null)
					queryVariable = queryVariable.getOrig();
				D.pv(D.D_TEMP, " query value is held in variable - " + queryVariable); 
			}
			else
			{
				throw new Error(" query value is a literal : check with constant queries");
			}
			
			Variable vRet = cncb.getTempVar();
			if(vRet.getOrig() != null)
				vRet = vRet.getOrig();
			
			D.pv(D.D_TEMP, " tmp value - " + cncb.getTempVar());

			CfgNode succ  = cncb.getSuccessorsNoBlocks().get(0);
			D.pv(D.D_TEMP, " successor - " + succ);
			if(!(succ instanceof CfgNodeAssignSimple) || 
					!(succ.getOrigLineno() == cncb.getOrigLineno()))
			{
				vRet = null;
				D.pv(D.D_TEMP, " there is no return value from here");
			}
			else
			{
				Variable vRhs = succ.getRightToRename().get(0);
				if(vRhs.getOrig()!= null)
					vRhs = vRhs.getOrig();
				
				if(!vRhs.equals(vRet))
					D.pv(D.D_TEMP, " this sql sink does not return any value");
				else
				{
					vRet = succ.getLeft();
					if(vRet.getOrig() != null)
						vRet = vRet.getOrig();
					D.pv(D.D_TEMP, " this mysql call returns " + vRet);
				}
			}
			
			String lhs = "";
			if(vRet != null)
			{
				lhs += Variable.makeArgsName(vRet, false) + " = ";
			}
			
			String qV = Variable.makeArgsName(queryVariable, false);
			String qVArgs = Variable.makeArgsName(queryVariable, true);
			String rhs = " executePS( " + qV + " , " + qVArgs + " ); ";
			
			DerivationNodeNonTerminal dnnt = new DerivationNodeNonTerminal(queryVariable, "");
			dnnt.setCfgNode(sink);
			ProgramChange pc = createProgramChange(ProgramChange.REPLACE, dnnt, lhs + rhs);
			addIfAbsent(pc);

		}
		else
			throw new Error(" sink is unhandled - " + sink.getClass());		
	}

//	public static void transformFiles()
//	{
//		CopyOnWriteArrayList<ProgramChange> ap = new CopyOnWriteArrayList<ProgramChange>();
//		ap.addAll(ia.progChanges);
//		
//		ArrayList<ProgramChange> apTmp = null;		
//		for(ProgramChange pc : ap)
//		{
////			D.pv(D.D_TEMP, )
//			String file = pc.getInFile();
//			apTmp = ia.getAllChangesInFile(file, ap);
//			ia.makeChangesInFile(file, apTmp);
//			if(ap.size() == 0)
//				break;
//		}
//		
//	}


//    File file = new File("C:\\MyFile.txt");
//    FileInputStream fis = null;
//    BufferedInputStream bis = null;
//    DataInputStream dis = null;
//
//    try {
//      fis = new FileInputStream(file);
//
//      // Here BufferedInputStream is added for fast reading.
//      bis = new BufferedInputStream(fis);
//      dis = new DataInputStream(bis);
//
//      // dis.available() returns 0 if the file does not have more lines.
//      while (dis.available() != 0) {
//
//      // this statement reads the line from the file and print it to
//	// the console.
//        System.out.println(dis.readLine());
//      }
//
//      // dispose all the resources after using them.
//      fis.close();
//      bis.close();
//      dis.close();
//
//    } catch (FileNotFoundException e) {
//      e.printStackTrace();
//    } catch (IOException e) {
//      e.printStackTrace();
//    }
//  }
	
	/**
	 * @param dn
	 */
	private void dumpReturnRecv(DerivationNode dn)
	{
		boolean isReturn = dn.isReturnAssign() || (dn.getChildren() != null && 
				dn.getChildren().size() > 0 && 
				dn.getChildren().get(0).isReturnAssign());
		if(!isReturn)
			return;

		CfgNode progNode = dn.getCfgNode();

		LinkedHashMap<TacPlace, TacPlace> lhm = null;
		
		if(progNode != null)
			lhm = progNode.getReturnValue();

    	if(lhm != null)
    	{
    		for(Map.Entry<TacPlace, TacPlace> returnV : lhm.entrySet())
    		{
    			TacPlace returnTo = returnV.getKey(); 
    			TacPlace returnFrom = returnV.getValue();
    			if(!returnTo.isVariable())
    				throw new Error(" TacPlace for return value isn't a variable - " + returnTo);
				
    			Variable retToV = returnTo.getVariable();
    			Variable retFromV = returnFrom.getVariable();
    			
//    			String lhs = Variable.makeArgsName(retToV, false);
  //  			String rhs = "$GLOBALS['" + retFromV.getName() + "'] ;";
    			String lhs = retToV.getName();
    			String rhs = "$GLOBALS['" + retFromV.getName() + "'] ;";
    			
    			String stmt = lhs + " = " + rhs + ";";
				ProgramChange pc = createProgramChange(ProgramChange.ADDAFTER, dn, stmt);
				addIfAbsent(pc);
				D.ps(D.D_TR, " Receiving return value in caller adding stmt - " + stmt);
    		}
    	}
	}

	/**
	 * record new statements needed for passing query data args list.
	 */
	private void dumpParamPassing(DerivationNode dn)
	{
		if(!dn.isActual2Formal())
			return;
		
		CfgNode progNode = dn.getCfgNode();

		LinkedHashMap<TacPlace, TacPlace> args = progNode.getActual2FormalArgs();
		if(args != null)
		{
			CfgNode cn = dn.getCfgNode();
			if(!(cn instanceof CfgNodeCallPrep))
				throw new Error(" actual2formal but cfg node is not call prep!!" + dn.toString());
			
			CfgNodeCallPrep cncp = (CfgNodeCallPrep) cn;			
			CfgNode headCallee = cncp.getCallee().getCfg().getHead().getSuccessor(0);
				
			for (Map.Entry<TacPlace, TacPlace> a2f : args.entrySet())
			{
				TacPlace actual  = a2f.getKey();
				TacPlace formal = a2f.getValue();
				
				String actualName = "";
				if(actual.isVariable())
				{
					Variable vActual = actual.getVariable();
					// actual is a variable
					// actualName = vActual.getName();
					actualName = Variable.makeArgsName(vActual, false);
				}
				else
				{
					// actual is a literal
					Literal l = (Literal)actual;
					actualName = "\"" + l.getStringValue() + "\"";
				}

				Variable formalV = formal.getVariable();
				String formalName = getFormalName(formalV);
				
				String stmt = formalName + " = " + actualName + "; ";
				ProgramChange pc = createProgramChange(ProgramChange.ADDBEFORE, dn, stmt);
				addIfAbsent(pc);
				
				D.ps(D.D_TR, " Transition_Session2_Look: Parameter passing " + a2f.getKey().toString() + " -> " + a2f.getValue() + " ");
				D.ps(D.D_TR, " Transition_Session2_Look: adding stmt - " + stmt);
				
				stmt = Variable.makeArgsName(formalV.getOrig(), true) + " = " + formalName + " ;";
				pc = createProgramChange(ProgramChange.ADDBEFORE, dn, stmt);
				pc.setInFile(headCallee.getAbsFileName());
				pc.setLineNumber(headCallee.getOrigLineno());
				addIfAbsent(pc);
				D.ps(D.D_TR, " Transition_Session2_Look: Receiving value in callee adding stmt - " + stmt);
			}
		}
	}

	
	
	/**
	 * @param formalV
	 * @return
	 */
	private String getFormalName(Variable formalV)
	{
		String fName = formalV.getSymbolTable().getName();
		//String fName = formalV.getName();
		fName += formalV.getName();
		
		fName = "$GLOBALS['" + fName + "']";
		return fName;
	}

	/**
	 * @param dn
	 */
	private void dumpPlaceholderComputation(DerivationNode dn)
	{
		if(dn.isActual2Formal() || dn.isQuestionMark())
			return; 
		if(dn.isReturnAssign() || (dn.getChildren() != null && 
				dn.getChildren().size() > 0 && 
				dn.getChildren().get(0).isReturnAssign()))
			return;
		
		TacPlace tp = dn.getId();
		if(!tp.isVariable() || tp.getVariable().isTemp())
		{
			D.pd(D.D_TR, " skipping placeholder computation of - " + dn.toString());
			return; 
		}
	
		Variable vLeftArg = dn.getArgsVar();
		ArrayList<TacPlace> qMap = dn.getQuestionMaps();
		if(qMap != null)
		{
			ArrayList<String> qArgs = new ArrayList<String>();
			if(!(qMap.size() == 1 && qMap.get(0).equals(vLeftArg)))
			{
				for(TacPlace q : qMap)
				{
					D.ps(D.D_TR,  
							" Add Q mapping = (" + q + " , " + 
							((q.getVariable() == null)? "null" : q.getVariable().getName()) + ")");

					if(q.isVariable())
					{
						Variable qVar = q.getVariable();
						Variable qTmp = qVar;
						//String name = qVar.getName();
						
						String name = qVar.getName();//''Variable.makeArgsName(qVar, false);
						// TODO:
						if(name.lastIndexOf("__args") == -1)
						{
							if(qVar.getOrig() != null)
								qTmp = qVar.getOrig();
							
							// name = qTmp.getName();
							name = Variable.makeArgsName(qTmp, false);
						}
						
						if(qVar.isReturnVariable())
							name = "$GLOBALS['" + name + "']";
						
						qArgs.add(name);
					}
					else
					{
						throw new Error(" if ? - arg mapping is to a non-variable" + q);
					}
				}				
			}
			
			String rhs = "";				
//			String lhs = vLeftArg.getName();
			String lhs = Variable.makeArgsName(vLeftArg, false);
			
			if(vLeftArg.isReturnVariable())
				lhs = "$GLOBALS['" + lhs + "']";
			
			// TODO: 
			// detect if in a loop
			boolean loop = false;
			int iSize = qArgs.size();
			if(iSize > 0)
			{
				D.ps(D.D_TEMP, " lhs = " + lhs + " get 0 = " + qArgs.get(0) + " equals = " + 
						lhs.equals(qArgs.get(0)));

				if(!loop)
				{
					if(iSize == 1 && lhs.equals(qArgs.get(0)))
						return; 
					
					if(iSize > 1)
						rhs += " = merge_helper ( ";
					else
						rhs += " = ";
					
					int iTmp = 0;
					for(String str : qArgs)
					{
						rhs += str;
						if(iTmp < (iSize - 1))
							rhs += " , ";
						
						iTmp ++; 
					}
					
					if(iSize > 1)
						rhs += " ); ";
					else
						rhs += " ; ";
				}
				else
				{
					// add the left hand side args...
					// if its a loop
					qArgs.add(0, lhs);
					iSize = qArgs.size();
					
					if(iSize == 1 && lhs.equals(qArgs.get(0)))
						return; 
					
					if(iSize > 1)
						rhs += " = array_merge ( ";
					else
						rhs += " = ";
					
					int iTmp = 0;
					for(String str : qArgs)
					{
						rhs += str;
						if(iTmp < (iSize - 1))
							rhs += " , ";
						
						iTmp ++; 
					}
					
					if(iSize > 1)
						rhs += " ); ";
					else
						rhs += " ; ";					
				}

				int op = ProgramChange.ADDBEFORE;
				if(vLeftArg.isReturnVariable())
					op = ProgramChange.ADDBEFORE;

//				Variable vPhim = dn.getPhiMapFor();
	//			D.ps(D.D_TEMP, " Creating a program change - check this - op : " + op + " " 
		//				+ " stmt  = " + lhs + " " + rhs + " where dn phi map - " + vPhim + 
			//			" corresponding actual = " + ((vPhim == null) ? null : vPhim.getOrig()));
				ProgramChange pc = createProgramChange(op, dn, lhs + rhs);
				addIfAbsent(pc);
				D.pr(D.D_PS,"Transition_Session2_Look: created a program change for placeholder node: " + pc);
			}
		}
	}

	/**
	 * @param pc
	 */
	private void addIfAbsent(ProgramChange pc)
	{
		if(pc == null)
			return;
		boolean bFound = false;
		for(ProgramChange p : this.progChanges)
		{
			if(p.equals(pc))
			{
				bFound = true;
				break;
			}
		}
		if(!bFound)
		{
			if(!this.progChanges.contains(pc))
			{
				this.progChanges.add(pc);
				D.ps(D.D_TR, "+++++++++ added  new stmt - " + pc.toString());
			}
		}		
	}

	private boolean isReplacePresent(String loc)
	{
		boolean bFound = false;
		for(ProgramChange p : this.progChanges)
		{
			if(p.getLocation().equals(loc) && 
					p.getChangeOperation() == ProgramChange.REPLACE)
			{
				bFound = true;
				break;
			}
		}
		
		return bFound;
	}

	
	/**
	 * @param add
	 * @param dn
	 * @param string
	 * @return
	 */
	private ProgramChange createProgramChange(int add, DerivationNode dn, String string)
	{
		ProgramChange pc = new ProgramChange();
		pc.setChangeOperation(add);
		pc.setInFile(dn.getCfgNode().getAbsFileName());
		pc.setLineNumber(dn.getCfgNode().getOrigLineno());
		pc.setStmt(string);
		pc.setDTNumber(dn.nodeNumber);
		return pc;
	}

	public String getGlobal(Variable v, String suffix)
	{
//		String name = v.getName();
//		if(name.charAt(0) == '$')
	//	{
		//	StringBuffer sb = new StringBuffer(name);
		//	name = sb.deleteCharAt(0).toString();
	//	}
		//String name = v.getSymbolTable().getName() + 
		String name = v.getName() + suffix;
//		String name = getName(v, suffix);
		return "$GLOBALS['" + name + "']";
	}
		
	/**
	 * @param dn
	 * @throws SISLException 
	 */
	private void dumpChanges2Orig(DerivationNode dn) throws SISLException
	{
		// 1. dump nodes that gave away data tokens 
		dumpBorrowees(dn);

		// 3. dump the parents who take ? instead of data now 
		dumpQuestionIntroducers(dn);

		// 2. dump newly created statemnets that root the 
		// data subtree.
		dumpNewNodes(dn);
		
	}

	private void dumpChanges2OrigOpt(DerivationNode dn) throws SISLException
	{
		// 1. dump nodes that gave away data tokens 
		 dumpBorrowees(dn);

		// 3. dump the parents who take ? instead of data now 
		dumpQuestionIntroducersOpt(dn);

		// 2. dump newly created statements that root the 
		// data subtree.
		dumpNewNodes(dn);
		
	}
	
	
	/**
	 * @param dn
	 */
	private void dumpQuestionIntroducers(DerivationNode dn)
	{
		boolean bOptimized = false;
		if(!dn.isQuestionMark())
			return; 

		DerivationNode tmp = dn.getParent();
		
		// since derivation tree has nodes for temporary
		// variables, walk up to find the parent who is 
		// an actual variable
		while(tmp != null)
		{
			TacPlace tmpId = tmp.getId();
			if(!tmpId.isVariable() || tmpId.getVariable().isTemp())
			{
				tmp = tmp.getParent();
				continue; 
			}
			
			break;			
		}
		
		if(tmp == null) throw new Error(" couldnt find parent of ? node " + dn);
		 
		DerivationNode impactedNode = tmp;
		String rhs = getCode(impactedNode, true, bOptimized);


		
		Variable vLhs = impactedNode.getId().getVariable();
		if(vLhs.getOrig() != null)
			vLhs = vLhs.getOrig();
		
//		String lhs = vLhs.getName();
		String lhs = Variable.makeArgsName(vLhs, false);

//		Variable phi = dn.getPhiMapFor();
	//	if(phi != null)
		//	rhs = " array_merge ( " + lhs + " , " + rhs + " )"; 

		String stmt = "";
		if(!lhs.equals(rhs))
			stmt = lhs + " = " + rhs + " ; ";

//		String stmt = lhs + " = " + rhs + " ; ";

		ProgramChange pc = createProgramChange(ProgramChange.REPLACE, 
				impactedNode, stmt);
		addIfAbsent(pc);
		 

		lhs = null;
		stmt = null;

		vLhs = dn.getId().getVariable();
		if(vLhs.getOrig() != null)
			vLhs = vLhs.getOrig();
		
		pc = null;

		lhs = dn.getArgsVar().getName();
		// lhs = Variable.makeArgsName(dn.getArgsVar(), false);
		//stmt = lhs + " = SISL_Concat ( " + lhs + " , " + vLhs.getName() + " ) ;";
		stmt = lhs + " = array ( " + Variable.makeArgsName(vLhs, false) + " ) ;";
		pc = createProgramChange(ProgramChange.ADDBEFORE, dn, stmt);
		addIfAbsent(pc);

		
		lhs = Variable.makeArgsName(vLhs, false);
		stmt = lhs + " = \"?\" ;";

		pc = createProgramChange(ProgramChange.ADDBEFORE, dn, stmt);
		addIfAbsent(pc);
		

	}

	/**
	 * Dump the program code that this cfg corresponds to 
	 * @param impactedNode
	 * @return
	 */
	private String getCode(DerivationNode startNode, boolean bStart, boolean bOptimized)
	{
		int iChildren = 0;
		// if it is a formal to actual map, stop here
		if(startNode.isActual2Formal() && 
				startNode.getCfgNode() instanceof CfgNodeCallPrep)
		{
			return id2Code(startNode.getId(), startNode);
		}
		
		// if its a question mark, just return ? 
		if(startNode.isQuestionMark())
		{
			D.pv(D.D_TEMP, "question mark node start node - " + 
					startNode + " optimization - " + bOptimized);
			// add __args -> value; 
			// and return its id. 
			if(!bOptimized)
				return id2Code(startNode.getId(), startNode);
			else
				return "\"?\"";
		}
		
		TacPlace tp = startNode.getId();
		if(!bStart && tp.isVariable())
		{
			Variable v = tp.getVariable();
			if(!v.isTemp())
			{
				if(v.getOrig() != null)
					v = v.getOrig();
				
				return id2Code(v, startNode);
			}
		}
		
		ArrayList<DerivationNode> children = startNode.getChildren();
		if(children == null)
			iChildren = 0; 
		else
			iChildren = children.size();
		
		switch(iChildren)
		{
		case 0:
			return id2Code(startNode.getId(), startNode);
		case 1: 
			return getCode(children.get(0), false, bOptimized);
		case 2: 
		{
			String codeLeft = getCode(children.get(0), false, bOptimized);
			String codeRight = getCode(children.get(1), false, bOptimized);
			if(!startNode.getOperator().equals("."))
				throw new Error(" currently cannot handle ops other than CONCAT .");
			
			if(codeLeft == null && codeRight == null)
				throw new Error(" cannot have all children return null code!!!");
			
			if(codeLeft == null)
				return codeRight; 
			
			if(codeRight == null)
				return codeLeft; 
			
			if(codeLeft.startsWith("\"") && codeLeft.endsWith("\"") && 
					codeRight.startsWith("\"") && codeRight.endsWith("\""))
			{
				StringBuffer sb = new StringBuffer(codeLeft);
				sb.deleteCharAt(sb.length() - 1);
				sb.deleteCharAt(0);
			
				StringBuffer sbr = new StringBuffer(codeRight);
				sbr.deleteCharAt(sbr.length() - 1);
				sbr.deleteCharAt(0);
				
				return ("\"" + sb.append(sbr).toString() + "\"");
			}
			else
				return codeLeft + "." + codeRight;
		}			
		default: 
			throw new Error(" DN cannot have more than 2 children!!!!");
		}		
	}

	/**
	 * @param dn
	 */
	private void dumpNewNodes(DerivationNode dn)
	{
		if(!dn.isNewNode())
			return; 
		
		TacPlace tpLhs = dn.getId();
		Variable vLhs = tpLhs.getVariable();
		
		ArrayList<DerivationNode> children = dn.getChildren();
		ArrayList<String> args = new ArrayList<String>();
		for(DerivationNode child : children)
		{
			TacPlace tChild = child.getId();
			// go down until we find a real variable; 
			if(tChild.isVariable() && tChild.getVariable().isTemp())
			{
				// TODO: this is a heck..
				boolean change = true;
				DerivationNode tmp = child.getChildren().get(0);
				while(change)
				{
					change = false;
					TacPlace tid = tmp.getId();
					if(tid.isVariable() && tid.getVariable().isTemp())
					{
						tmp = tmp.getChildren().get(0);
						change = true;
					}
					else
					{
						String str = id2Code(tid, null);
						if(str != null) 
						{
							args.add(str);
						}
					}
				}
			}
			else
			{
				String str = id2Code(tChild, null);
				if(str != null)
					args.add(str);
			}
		}

		int iSize = args.size();

		String rhs = "";
		int iTmp = 0;
		
		ArrayList<String> optArgs = evalStaticStringConcat(args);
		for(String str : optArgs)
		{
			rhs += str ;
			if(iTmp < (iSize - 1))
				rhs += ".";
			iTmp ++;
		}

		if(!m_bFixNameProblem)
		{
			if(vLhs.getOrig() != null)
				vLhs = vLhs.getOrig();
		}
		
		String lhs = Variable.makeArgsName(vLhs, false);
//		lhs = lhs.replace('$', 'X');
//		lhs = "$__temp_PIPER_" + lhs;

		//if(vLhs.isTemp())
			//lhs = "$" + lhs;
		
		if(!lhs.equals(rhs))
		{
			String stmt = lhs + " = " + rhs + ";" ; 
			ProgramChange pc = createProgramChange(ProgramChange.ADDBEFORE, dn, stmt);
			addIfAbsent(pc);
			D.pr(D.D_PS, "Transition_Session2_Look: Computed a program change - " + pc);
		}
	}

	private ArrayList<String> evalStaticStringConcat(ArrayList<String> args)
	{
		ArrayList<String> optArgs = new ArrayList<String>();
		optArgs.addAll(args);
		boolean change = true;
		while(change)
		{
			change = false;
			for(int i = 0; i < optArgs.size() - 1; i++)
			{
				String iP = optArgs.get(i);
				String iP1 = optArgs.get(i+1);
				if(iP.startsWith("\"") && iP.endsWith("\"") && 
						iP1.startsWith("\"") && iP1.endsWith("\""))
				{
					StringBuffer iPNew = new StringBuffer(iP);
					StringBuffer iP1New = new StringBuffer(iP1);
					
					iPNew.deleteCharAt(iPNew.length() - 1);
					iPNew.deleteCharAt(0);
					
					iP1New.deleteCharAt(iP1New.length() - 1);
					iP1New.deleteCharAt(0);
					
					String concatedArg = "\"" + iPNew.append(iP1New).toString() + "\"";
					optArgs.add(i, concatedArg);
					optArgs.remove(i+1);
					optArgs.remove(i+2);
					change = true;
					break;
				}
			}
		}

		return optArgs;
	}
	
	private String id2Code(TacPlace id, DerivationNode dn)
	{
		if(id.isVariable())
		{
			Variable v = id.getVariable();
			if(v.getOrig() != null)
				v = v.getOrig();

			return Variable.makeArgsName(v, false);
		}
		else
		{
			String str = "";
			if(id.isConstant())
				str = ""+id;
			else
			{
				Literal l = (Literal) id;
				str = l.toString();
			}
	//		boolean bNum = l.isCompletelyNumeric();
//			if(dn != null)
//			{
//			CfgNode cn = dn.getCfgNode();
//			if(cn != null)
//			{
//				ParseNode pn = cn.getParseNode();
//				if(pn != null)
//				{
//					
//				}
//			}
//			}
			//String strL = l.getStringValueLiteral().toString();
			
			if(!str.equals(""))
				return ("\"" + str + "\"");
		}

		return null;
	}
	
	/**
	 * @param dn
	 * @throws SISLException 
	 */
	private void dumpBorrowees(DerivationNode dn) throws SISLException
	{
		if(!dn.isBorrowee())
			return; 

		D.pv(D.D_TEMP, " Dumping borrowee " + dn );
		
		DerivationNode tmp = dn.getParent();
		// since derivation tree has nodes for temporary
		// variables, walk up to find the parent who is 
		// an actual variable
		while(tmp != null)
		{
			TacPlace tmpId = tmp.getId();
			if(!tmpId.isVariable() || tmpId.getVariable().isTemp())
			{
				tmp = tmp.getParent();
				continue; 
			}
			
			break;			
		}
		
		D.pv(D.D_TEMP, " Found parent non-tmp node - " + tmp);
		if(tmp == null) 
		{
			D.pv(D.D_TEMP, " couldnt find parent of borrowee node " + dn);
			SISLException sisl = new SISLException(SISLException.BAIL_OUT, 
					" couldnt find parent of borrowee node " + 
						CfgNode.getProgLineCode(dn.getCfgNode()));
			ToolBug.add_UNCLASSIFIED_YET(sisl);
			throw sisl;

//			throw new Error(" couldnt find parent of borrowee node " + dn);
		}
		 
		// check to see if this node is already present in the list of changes
		// as a REPLACE operation
		
		String loc = tmp.getCfgNode().getAbsFileName() + ":" + 
			tmp.getCfgNode().getOrigLineno();
		if(isReplacePresent(loc))
		{
			D.pv(D.D_TEMP, loc + " REPLACE already present in " + ia.progChanges);
			return;
		}
		
		boolean bOptimized = true;
		DerivationNode impactedNode = tmp;
		String rhs = getCode(impactedNode, true, bOptimized);
		if(rhs == null || rhs.equals("null"))
			rhs = "\"\"";
		
		Variable vLhs = impactedNode.getId().getVariable();
		if(vLhs.getOrig() != null)
			vLhs = vLhs.getOrig();
		
		String lhs = Variable.makeArgsName(vLhs, false);
		
		String stmt = "";
		if(!lhs.equals(rhs))
			stmt = lhs + " = " + rhs + " ; ";
		ProgramChange pc = createProgramChange(ProgramChange.REPLACE, 
				impactedNode, stmt);
		addIfAbsent(pc);
		D.pr(D.D_PS, "Transition_Session2_Look: Dumping a node that gave away part of data computation to data subtree - " + pc);
	}

/*	public void addPaths(DepGraph depGraph)
	{
        DepGraphNode x = depGraph.getRoot();
        ArrayList<ControlFlow> pathsFromX = pathsFromX(x, depGraph);

        if(pathsFromX == null)
        	return ; 
        
        addCorrectPhiRhs(pathsFromX); 
        
        // add all statements for any line numbers currently in the path
        
        pathsFromX = extendWRelevantStmts(pathsFromX);
        
        addLoopIteration(pathsFromX, depGraph);
        
        this.allPaths.addAll(pathsFromX);
	}

    private void addLoopIteration(ArrayList<ControlFlow> pathsFromX,
			DepGraph depGraph) 
    {	
    	for(ControlFlow cf : pathsFromX)
    	{
    		if(!cf.isLoopBody())
    			continue; 
    	
    		ArrayList<CfgNode> existingNodes = cf.getAll();
			D.pd(D.D_TEMP, " Loop body control flow - " + cf.toString());
    		
			IterationHelper ih = cf.getIterationHelper();
    		D.pd(D.D_TEMP, " rectify the loop body iteartion with " );
			
    		ArrayList<CfgNode> nodesToRectify = ih.getImpactedNodes();
			LinkedHashMap<Variable, ArrayList<Variable>> phiDefToUse = ih.getPhiDef();
			
			Variable vOutRhs = ih.getRenamed();
			Variable vReplaceThis = null;
			Variable vReplaceWith = null;
			Variable vOutLhs = null;

			if(phiDefToUse.size() > 1 )
				throw new Error(" Nested loop handling is not present!!!!");
			
			for(Map.Entry<Variable, ArrayList<Variable>> ph : phiDefToUse.entrySet())
			{		
				vReplaceThis = ph.getKey();
				D.pd(D.D_TEMP, "\t\t\t Def to replace - " + vReplaceThis);
				for(Variable v : ph.getValue())
				{
					if(!v.equals(vOutRhs))
					{
						vReplaceWith = v;
						D.pd(D.D_TEMP, " \t\t\t with this - " + vReplaceWith);
					}
					else
					{
						vOutLhs = ph.getKey();
						D.pd(D.D_TEMP, " Loop outgoing value - " + vOutLhs + " = " + vOutRhs);
					}
				}
			}	
			
			for(CfgNode cn : nodesToRectify)
			{					
				D.pd(D.D_TEMP, " \t\t\t rectify - " + cn);
				if(existingNodes.contains(cn))
				{
					cn.setLoopBody(true);
					cn.rightRename(vReplaceThis, vReplaceWith, false);
				}
				
			}
			CfgNodeAssignUnary cnau = new CfgNodeAssignUnary(vOutLhs, vOutRhs, 1, null);
			cnau.setLoopBody(true);
			existingNodes.add(cnau);
    	}
	}

	public ArrayList<ControlFlow> extendWRelevantStmts(ArrayList<ControlFlow> pathsFromX)
    {
    	ArrayList<ControlFlow> complPathsX = new ArrayList<ControlFlow>();
    	
        for(ControlFlow cf : pathsFromX)
        {
        	ControlFlow cnew = new ControlFlow();
        
        	String proc = null, procLast = null;
        	for(CfgNode cn : cf.getAll())
        	{
        		proc = cn.getAbsFileName() + cn.getOrigLineno();
        		if(procLast != null && procLast.equals(proc))
        			continue; 
        		else
        			procLast = proc; 
        		
        		ArrayList<CfgNode> proximitySen = getCfgNodesForSameLine(cn);
        		cnew.addAll(proximitySen);
        	}
        	if(cf.isLoopBody())
        		cnew.setIterationHelper(cf.getIterationHelper());
        	
        	complPathsX.add(cnew);
        }
        
        return complPathsX;
    }
*/
    public static ArrayList<CfgNode> getCfgNodesForSameLine(CfgNode cn)
    {
		String inFile = cn.getAbsFileName() + cn.getOrigLineno();

		// locate the beginning and end of sentences for Origin line number
		// that cn corresponds to 
		CfgNode cStart = null;
		CfgNode cEnd = null;
		
		CfgNode cTmp = cn;
		
		boolean bSame = true; 
		while(bSame)
		{
			String tmpFile = cTmp.getAbsFileName() + cTmp.getOrigLineno();
			if(tmpFile.equals(inFile))
				cStart = cTmp; 
			else
				break;
			
			List<CfgNode> predTmp = cTmp.getPredecessors();
			if(predTmp == null || predTmp.size() > 1 || predTmp.size() == 0)
				break;
			
			cTmp = predTmp.get(0);
		}
		D.pv(D.D_PATH, "Start node - " + cStart.toString() + " node before - " + cTmp.toString());

		cTmp = cn;
		bSame = true; 
		while(bSame)
		{
			String tmpFile = cTmp.getAbsFileName() + cTmp.getOrigLineno();
			if(tmpFile.equals(inFile))
				cEnd = cTmp; 
			else
				break;
			
			List<CfgNode> succTmp = cTmp.getSuccessors();
			if(succTmp == null || succTmp.size() > 1 || succTmp.size() == 0)
				break;
			
			cTmp = succTmp.get(0);
		}
		D.pv(D.D_PATH, "End node - " + cEnd.toString() + " node after - " + cTmp.toString());

		// order of the statements for same line is important...so now start from the cStart 
		// and collect all statements 
		ArrayList<CfgNode> nodes = new ArrayList<CfgNode>();
		cTmp = cStart; 
		// Fixes after the NDSS2010 numbers -
		// while (true) : caused null pointer exception on equality test
		// for CVE-2009-3226
		while(cTmp != null)
		{	
			nodes.add(cTmp);
			if(cTmp.equals(cEnd))
				break;
			
			cTmp = cTmp.getSuccessor(0);
		}
		
		
		D.pd(D.D_PATH, " proxim nodes for - " + cn.toString());
		for(CfgNode c : nodes)
			D.pd(D.D_PATH, "---------- " + c.toString());
		
		Collections.reverse(nodes);
		return nodes; 
    }
    
    
	public void addCorrectPhiRhs(ArrayList<ControlFlow> pathsFromX)
	{
        ArrayList<CfgNode> phiAssigns = null;
        // now add phi function definitions relevant for each path
        for(ControlFlow cf : pathsFromX)
        {
        	phiAssigns = new ArrayList<CfgNode>();
        	// for each node
        	for(CfgNode cn : cf.getAll())
        	{
        		LinkedHashMap<Variable, ArrayList<Variable>> phis = cn.getPhiNodes();
        	
        		// check if there are phi functions
        		if(phis != null && phis.size() > 0)
        		{
        			// and if so, for each phi function
        			for(Map.Entry<Variable, ArrayList<Variable>> ent : phis.entrySet())
        			{
        				// determine which RHS value is applicable for current path.
        				Variable vLhs = ent.getKey();
        				ArrayList<Variable> vRhs = ent.getValue();
        				
        				for(CfgNode t : cf.getAll())
        				{
        					Variable vt = t.getLeft();
        					if(vt != null && vRhs.contains(vt))
        					{
        						CfgNodeAssignUnary cnau = new CfgNodeAssignUnary(vLhs, vt, 1, null);
        						phiAssigns.add(cnau);
        						break;
        					}
        				}
        			}
        		}
        	}
        	for(CfgNode cn : phiAssigns)
        		cf.addNode(cn);
        }        
	}
	
	// SISL: 
	// Main driver method. Steps
	// 1.  allDeps is a Map which contains All dep graphs for a given sink.
	// 2. 
	public static void start(LinkedHashMap<Collection<DepGraph>, Sink> allDeps) 
	{

		init();

		TimeProfiler.start();
		
		int i = 0;
		// for each sink and the corresponding collection of dependency graphs
		for(Map.Entry<Collection<DepGraph>, Sink> m : allDeps.entrySet())
		{
			Collection<DepGraph> em = m.getKey();
			Sink sn = m.getValue();
			
			D.pi(D.D_TEMP, " Set " + (i++) + " of Dep graphs for " + 
					sn.getFileName() + " : " + sn.getLineNo());
			
			int j = 0;
			for(DepGraph depGraph : em)
			{
            	D.pv(D.D_TEMP, " \t\t " + (j++) + " Dep graph Nodes- \n \t\t\t [ " + depGraph.getProgLinesForNodes() + " ] ");
				//DepGraph.performSSAAnalysis(depGraph);
//                TimeProfiler.start();
            	// SISL: perform SSA transformation of the functions in the depGraph. 
            	SSATransformer.transform(depGraph);
//		        TimeProfiler.end(D.D_SSA);
            	//D.pr(".....................................SSA transform time - " + (et - st) + " ms");
			}			
		}	
	
		TimeProfiler.end(D.D_SSA);
		
		for(Map.Entry<Collection<DepGraph>, Sink> m : allDeps.entrySet())
		{
			 D.pr(D.D_TEMP, ".......in deps");
			 // SISL: for each sink -- enumerate paths, create execution tree, and collect
			 // changes for using PREPARE statements.
			 processSinkPath(m);
		}		

		TimeProfiler.start();
		D.pr(D.D_TEMP, "Transition_Session2_Look: Processing proposed program changes now...");
		Transformation.processProposedChanges();		
    	TimeProfiler.e();
    	TimeProfiler.end(D.D_TR);
		TransformationResults.get().reportResults();
	}

	/*
	 * SISL: 
	 * For each sink and each dependency graph for it, perform enumerate path analysis.
	 */
	private static void processSinkPath(Entry<Collection<DepGraph>, Sink> m) 
	{
		SinkPath sp = new SinkPath(m.getValue());
		D.pr("enumerating paths for ----- " + m.getKey().size() + " graphs");

		for(DepGraph dg : m.getKey())
		{	
			try
			{
				ia.enumeratePaths(dg, sp);
				//ArrayList<ControlFlow> paths = InterProcPathEnumerator.getPaths(dg, sp);
			}
//			catch(SISLException se)
//			{
//				TransformationResults.get().addFailureReport(se, dg, sp);	
//			}
			catch(SISLException se)
			{					
				//SISLException sisl = new SISLException(SISLException.INTERNAL_TOOL_BUG, se.getMessage());
				//sisl.setStackTrace(se.getStackTrace());
				if(se.getType() != SISLException.BAIL_OUT)
				{
					TransformationResults.get().addFailureReport(se, dg, sp);
				}
			}
			catch(Exception e)
			{
				SISLException sisl = new SISLException(SISLException.INTERNAL_TOOL_BUG, e.getMessage());
				sisl.setStackTrace(e.getStackTrace());
				ToolBug.add_UNCLASSIFIED_YET(sisl);
				
				e.printStackTrace();				
			}
		}
	}		

	private void dumpQuestionIntroducersOpt(DerivationNode dn)
	{
		if(!dn.isQuestionMark())
			return; 

		boolean bOptimized = true;
		DerivationNode tmp = dn.getParent();
		
		// since derivation tree has nodes for temporary
		// variables, walk up to find the parent who is 
		// an actual variable
		while(tmp != null)
		{
			TacPlace tmpId = tmp.getId();
			if(!tmpId.isVariable() || tmpId.getVariable().isTemp())
			{
				tmp = tmp.getParent();
				continue; 
			}
			
			break;			
		}
		
		if(tmp == null) throw new Error(" couldnt find parent of ? node " + dn);
		 
		DerivationNode impactedNode = tmp;
		D.pv(D.D_TEMP, " processing ? node ---- found parent - " + tmp);
		String rhs = getCode(impactedNode, true, bOptimized);
		D.pv(D.D_TEMP, " code returned for impacted node - " + rhs);
		
		ProgramChange pc = null;
		
		Variable vLhs = impactedNode.getId().getVariable();
		if(vLhs.getOrig() != null)
			vLhs = vLhs.getOrig();
		
//		String lhs = vLhs.getName();
		String lhs = Variable.makeArgsName(vLhs, false);

//		String stmt = lhs + " = " + rhs + " ; ";
		String stmt = "";
		if(!lhs.equals(rhs))
			stmt = lhs + " = " + rhs + " ; ";

		pc = createProgramChange(ProgramChange.REPLACE, 
				impactedNode, stmt);
		addIfAbsent(pc);
		D.pr(D.D_PS, "Transition_Session2_Look: Created program changes for parent of ? node - " + pc);

//		lhs = null;
//		stmt = null;
/*
		vLhs = dn.getId().getVariable();
		if(vLhs.getOrig() != null)
			vLhs = vLhs.getOrig();
		
		pc = null;

		lhs = dn.getArgsVar().getName();
		// lhs = Variable.makeArgsName(dn.getArgsVar(), false);
		//stmt = lhs + " = SISL_Concat ( " + lhs + " , " + vLhs.getName() + " ) ;";
		stmt = lhs + " = array ( " + Variable.makeArgsName(vLhs, false) + " ) ;";
		pc = createProgramChange(ProgramChange.ADDBEFORE, dn, stmt);
		addIfAbsent(pc);
*/
		/*
		lhs = Variable.makeArgsName(vLhs, false);
		stmt = lhs + " = \"?\" ;";

		pc = createProgramChange(ProgramChange.ADDBEFORE, dn, stmt);
		addIfAbsent(pc);
		*/

	}

	private void dumpPlaceholderComputationOpt(DerivationNode dn)
	{
		if(dn.isActual2Formal() || dn.isQuestionMark())
			return; 
		if(dn.isReturnAssign() || (dn.getChildren() != null && 
				dn.getChildren().size() > 0 && 
				dn.getChildren().get(0).isReturnAssign()))
			return;

		// for optimized case it is handled while dumping the modified code
		if(dn.isQuestionMark())
			return; 
		
		TacPlace tp = dn.getId();
		if(!tp.isVariable() || tp.getVariable().isTemp())
		{
			D.pd(D.D_TR, " skipping placeholder computation of - " + dn.toString());
			return; 
		}
	
		Variable vLeftArg = dn.getArgsVar();
		ArrayList<TacPlace> qMap = dn.getQuestionMaps();
		if(qMap != null)
		{
			ArrayList<String> qArgs = new ArrayList<String>();
			if(!(qMap.size() == 1 && qMap.get(0).equals(vLeftArg)))
			{
				for(TacPlace q : qMap)
				{
					D.ps(D.D_TR,  
							" Add Q mapping = (" + q + " , " + 
							((q.getVariable() == null)? "null" : q.getVariable().getName()) + ")");

					if(q.isVariable())
					{
						Variable qVar = q.getVariable();
						Variable qTmp = qVar;
						//String name = qVar.getName();
						
						String name = qVar.getName();//''Variable.makeArgsName(qVar, false);
						// TODO:
						if(name.lastIndexOf("__args") == -1)
						{
							if(qVar.getOrig() != null)
								qTmp = qVar.getOrig();
							
							// name = qTmp.getName();
							name = Variable.makeArgsName(qTmp, false);
						}
						
						if(qVar.isReturnVariable())
							name = "$GLOBALS['" + name + "']";
						
						qArgs.add(name);
					}
					else
					{
						throw new Error(" if ? - arg mapping is to a non-variable" + q);
					}
				}				
			}
			
			String rhs = "";				
//			String lhs = vLeftArg.getName();
			String lhs = Variable.makeArgsName(vLeftArg, false);
			
			if(vLeftArg.isReturnVariable())
				lhs = "$GLOBALS['" + lhs + "']";
			
			// TODO: 
			// detect if in a loop
			boolean loop = false;
//			Variable phi = dn.getPhiMapFor();
			//if(phi != null)
			//	loop = true;

			
			int iSize = qArgs.size();
			if(iSize > 0)
			{
				D.ps(D.D_TEMP, " lhs = " + lhs + " get 0 = " + qArgs.get(0) + " equals = " + 
						lhs.equals(qArgs.get(0)));

				if(!loop)
				{
					if(iSize == 1 && lhs.equals(qArgs.get(0)))
						return; 
					
					if(iSize > 1)
						rhs += " = merge_helper ( ";
					else
						rhs += " = ";
					
					int iTmp = 0;
					for(String str : qArgs)
					{
						rhs += str;
						if(iTmp < (iSize - 1))
							rhs += " , ";
						
						iTmp ++; 
					}
					
					if(iSize > 1)
						rhs += " ); ";
					else
						rhs += " ; ";
				}
				else
				{
					// add the left hand side args...
					// if its a loop
					qArgs.add(0, lhs);
					iSize = qArgs.size();
					
					if(iSize == 1 && lhs.equals(qArgs.get(0)))
						return; 
					
					if(iSize > 1)
						rhs += " = array_merge ( ";
					else
						rhs += " = ";
					
					int iTmp = 0;
					for(String str : qArgs)
					{
						rhs += str;
						if(iTmp < (iSize - 1))
							rhs += " , ";
						
						iTmp ++; 
					}
					
					if(iSize > 1)
						rhs += " ); ";
					else
						rhs += " ; ";					
				}

				int op = ProgramChange.ADDBEFORE;
				if(vLeftArg.isReturnVariable())
					op = ProgramChange.ADDAFTER;

				Variable vPhim = dn.getPhiMapFor();
				D.ps(D.D_TEMP, " Creating a program change - check this - op : " + op + " " 
						+ " stmt  = " + lhs + " " + rhs + " where dn phi map - " + vPhim + 
						" corresponding actual = " + ((vPhim == null) ? null : vPhim.getOrig()));
				ProgramChange pc = createProgramChange(op, dn, lhs + rhs);
				D.pv(D.D_TEMP, "Created change " + pc);
				addIfAbsent(pc);
			}
		}
	}

	/*	
	public ArrayList<ControlFlow> pathsFromX(DepGraphNode x, DepGraph depGraph)
    {
		ArrayList<ControlFlow> pathsX = new ArrayList<ControlFlow>();
        Map<DepGraphNode,List<DepGraphNode>> edges = depGraph.getEdges();

        if(x.isLoop())
        	D.pd(D.D_TEMP, "!!!!!!!!!!!!!!! found a depnode with loop body set - " + x + " cfg node - " + getCfgNode(x));
        
        CfgNode cfgnX = getCfgNode(x);
        if(cfgnX == null)
        	return pathsX;

        boolean xOp = false; 
        if(x instanceof DepGraphOpNode)
        	xOp = true; 
        
        List<DepGraphNode> toList = edges.get(x);
        D.pv(D.D_TEMP, " to list - " + ((toList == null) ? " null " : "" + toList.size()));
        
        ArrayList<ControlFlow> tmpOp = new ArrayList<ControlFlow>();

        if(toList != null )
        {
//        	if(toList.size() > 2)
//       		throw new Error("at most can handle 2 children -- look for inbuilt fns");
        
	        for(DepGraphNode to : toList) 
	        {
	    		tmpOp = pathsFromX(to, depGraph);    		
	            // if its an op node...
	        	// otherwise, it is beginning of a new path. 
	        	if(xOp)
	        	{
	        		if(pathsX.size() == 0)
	        			pathsX = tmpOp;
	        		else
	        		{
	        			// x = x1 . x2
	        			// x1 and x2 may be built in multiple paths
	        			// take the cross product of number of ways
	        			// in which x1 can be built and x2 can be built
	        			// that determines total paths that can construct
	        			// x
	        			pathsX = crossProduct(pathsX, tmpOp);
	        		}
	        	}
	        	else
	        	{
	        		pathsX.addAll(tmpOp);
	        	}
	        }
        }
        
        if(pathsX.size() == 0)
        {
        	ControlFlow cf = new ControlFlow();
        	cf.addNode(cfgnX);
        	pathsX.add(cf);
        	if(x.isLoop())
        		cf.setIterationHelper(x.getIterationHelper());
        }
        else
        {
	        for(ControlFlow cf : pathsX)
	        {
	        	cf.addNode(0, cfgnX);
	        	if(x.isLoop())
	        	{
	        		D.pd(D.D_TEMP, " !!!!!!!! setting the control flow as loop - " + cf.toString());
	        		cf.setIterationHelper(x.getIterationHelper());
	        	}
	        }
        }
        
        D.pd(D.D_TEMP, pathsX.size() + "  .......number of paths from " + cfgnX);
        return pathsX; 
    }

    private CfgNode getCfgNode(DepGraphNode x)
    {
    	CfgNode cn = null;
    	
    	if(x instanceof DepGraphOpNode)
    	{
    		DepGraphOpNode dgon = (DepGraphOpNode) x;
    		cn = dgon.getCfgNode();
    	}
    	else
    	if(x instanceof DepGraphNormalNode)
    	{
    		DepGraphNormalNode dgnn = (DepGraphNormalNode) x;
    		cn = dgnn.getCfgNode();
    	}
    	else
    	{
    	}
    	
    	return cn;
    }

	public ArrayList<ControlFlow> 
		crossProduct(ArrayList<ControlFlow> prefix, ArrayList<ControlFlow> suffix)
	{
		if(prefix == null || suffix == null)
			throw new Error("cross product args cannot be null");
		D.pf(D.D_TEMP, " cross product called - " + prefix.size() + " suffix - " + suffix.size());
		
		ArrayList<ControlFlow> cfCross = new ArrayList<ControlFlow>();
		for(ControlFlow path : prefix)
		{
			for(ControlFlow suff : suffix)
			{
				D.pf(D.D_TEMP, "\t\t\t cross - " + prefix.toString() + " and " + suffix.toString());
				ControlFlow cf = new ControlFlow();
				cf.addAll(path).addAll(suff);
				cfCross.add(cf);
				if(path.isLoopBody())
					cf.setIterationHelper(path.getIterationHelper());
				if(suff.isLoopBody())
					cf.setIterationHelper(suff.getIterationHelper());
				
			}
		}
		
		return cfCross;
	}

	public static void removeCyclicDep(DepGraph depGraph)
    {        
		//removeCycle(depGraph.getRoot(), depGraph);
        //return; 
    }

	private static void removeCycle(DepGraphNode node, DepGraph depGraph) 
	{		
		ArrayList<DepGraphNode> reachable = new ArrayList<DepGraphNode>();
		
		reachable = reachableFromX(node, depGraph);
		if(findX(reachable, node))
		{
			D.pf(D.D_TEMP, " ++++++++++++ cycle at node - " + node.dotName());
			Set<DepGraphNode> inEdges = depGraph.getPredecessors(node);
			for(DepGraphNode dgn : inEdges )
			{
				D.pf(D.D_TEMP, "\t\t\t\t Inedges = " + dgn.dotName() );
			}
			for(DepGraphNode dgn : reachable)
			{
				D.pf(D.D_TEMP, "\t\t\t reachable - " + dgn.dotName());
			}
	
			CopyOnWriteArraySet<DepGraphNode> cinEdges = new CopyOnWriteArraySet<DepGraphNode>();
			cinEdges.addAll(inEdges);
			
			retainAllX(cinEdges, reachable);
			
			
			for(DepGraphNode dgn : cinEdges)
			{
				D.pf(D.D_TEMP, "\t\t\t\t back edge from = " + dgn.dotName());
			
				locatePhi(dgn);	
				depGraph.removePred(dgn, node);
			}
		}
		
        List<DepGraphNode> toList = depGraph.getEdges().get(node);
        if(toList != null && toList.size() != 0)
        	for(DepGraphNode dn : toList)
        		removeCycle(dn, depGraph);
	}

	private static void locatePhi(DepGraphNode dgn) 
	{
		DepGraphNormalNode dgnn = null;
		DepGraphOpNode dgon = null;
		
		if(dgn instanceof DepGraphNormalNode)
			dgnn = (DepGraphNormalNode) dgn;
		else
			dgon = (DepGraphOpNode) dgn;
				
		Variable v = null;
		TacPlace  tp = null;
		CfgNode cn = null;
		if(dgnn != null)
		{
			tp = dgnn.getPlace();
			if(tp.isVariable())
				v = tp.getVariable();
			else
				throw new Error(" loop doesnt involve a var " + tp);
		
			cn = dgnn.getCfgNode();
			if(cn == null)
				throw new Error(" loop cycle doesnt have a CFG node associated ");
			D.pi(D.D_TR, " cycle node - " + cn);
		}
		else
		{
			throw new Error(" Loop doesnt have an ID associated...!!! " + dgn);
		}

		Variable vRenamed = null;
		CfgNode to = null;
		ArrayList<CfgNode> forSameLine = getCfgNodes(cn, v);
		for(CfgNode ct : forSameLine )
		{
			LinkedHashMap<Variable, CfgNode> phiC = ct.getContributedToPhi(); 
			if(phiC != null && phiC.size() > 0)
			{
				for(Map.Entry<Variable, CfgNode> m : phiC.entrySet())
				{
					Variable vKey = m.getKey();
					Variable vKeyOrig = vKey.getOrig();
					if(vKeyOrig == null)
						continue; 
					
					if(vKeyOrig.equals(v))
					{
						vRenamed = vKey; 
						to = m.getValue();
						break;
					}
				}
			}
			if(to != null)
				break;
		}
		D.pi(D.D_TR, " original variable v = " + v + " is renamed to = " + vRenamed + " cfn = " + to);
		LinkedHashMap<Variable, ArrayList<Variable>> phiDef = to.getPhiNodes();
		if(phiDef == null || phiDef.size() == 0)
			throw new Error(" target phi defintion is empty!!");
		for(Map.Entry<Variable, ArrayList<Variable>> ph : phiDef.entrySet())
		{
			ArrayList<Variable> phiRhs = ph.getValue();
			if(phiRhs.contains(vRenamed))
			{
				D.pd(D.D_TEMP, " variable - " + ph.getKey() + " size of args = " + ph.getValue().size());
				for(Variable vt : phiRhs)
					D.pd(D.D_TEMP, " RHS of phi - " + vt);
			}
		}
		
		dgn.setLoop(true);
		dgn.addIterationHelper(new IterationHelper(dgn, forSameLine, phiDef, vRenamed));
	}

	private static ArrayList<CfgNode> getCfgNodes(CfgNode cn, Variable v) {
		
		ArrayList<CfgNode> sameLine = new ArrayList<CfgNode>();
		ArrayList<CfgNode> tmp = null;
		int iLine = cn.getOrigLineno();

		tmp = new ArrayList<CfgNode>();
		List<CfgNode> l = null;
		l = cn.getPredecessors();
		if ( l != null) tmp.addAll(l);
		while(tmp.size() != 0)
		{
			CfgNode ct = tmp.get(0);
			tmp.remove(ct);
			D.pd(D.D_TEMP, " tmp size = " + tmp.size() + " current node - " + ct);
			if(ct.getOrigLineno() == iLine)
			{			
				sameLine.add(ct);
				l = ct.getPredecessors();
				if(l != null) tmp.addAll(l);				
			}
		}

		tmp = new ArrayList<CfgNode>();
		l = cn.getSuccessorsNoBlocks();
		if( l != null) tmp.addAll(l);
		while(tmp.size() != 0)
		{
			CfgNode ct = tmp.get(0);
			tmp.remove(ct);
			D.pd(D.D_TEMP, " tmp size = " + tmp.size() + " current node - " + ct);
			if(ct.getOrigLineno() == iLine)
			{				
				sameLine.add(ct);
				l = ct.getSuccessorsNoBlocks();
				if(l != null) tmp.addAll(l);				
			}
		}

		sameLine.add(cn);

		for(CfgNode c : sameLine)
			D.pd(D.D_TEMP, "same line ------ \t " + c);
		
		return sameLine;
	}

	private static void retainAllX(CopyOnWriteArraySet<DepGraphNode> inEdges,
			ArrayList<DepGraphNode> reachable) 
	{
		for(DepGraphNode dgn : inEdges)
		{
			if(!findX(reachable, dgn))
				inEdges.remove(dgn);
		}
	}

	private static ArrayList<DepGraphNode> reachableFromX(DepGraphNode node,
			DepGraph depGraph) 
	{
		
        Map<DepGraphNode,List<DepGraphNode>> edges = depGraph.getEdges();
        List<DepGraphNode> tEd = null;
		ArrayList<DepGraphNode> tmp = new ArrayList<DepGraphNode>();
		ArrayList<DepGraphNode> reac = new ArrayList<DepGraphNode>();
		
        List<DepGraphNode> toList = edges.get(node);
        if(toList != null && toList.size() > 0)
        	tmp.addAll(toList);

        boolean bChange = true;
        while(bChange)
        {
        	bChange = false; 
        	
        	for(DepGraphNode dn : tmp)
        	{
        		if(findX(reac, dn))
        			continue; 
        		
        		if(node.equalsX(dn))
        		{
        			reac.add(dn);
        			//D.pf(D.D_TEMP, "avoiding self replication .............. + " + dn);
        		}
        		else
        		{
        			reac.add(dn);
        			tEd = edges.get(dn);
        			if(tEd != null)
        			{
        				tmp.addAll(tEd);
        				bChange = true;
        				break;
        			}
        		}
        		
        	}
        }
        
		return reac;
	}

	private static boolean findX(ArrayList<DepGraphNode> reac, DepGraphNode dn) {
		
		for(DepGraphNode dgn : reac)
			if(dgn.equalsX(dn))
				return true;
		
		return false;
	}

	private void printHelper(CfgNode stmt) {
    	CopyOnWriteArrayList<CfgNode> imm = stmt.getImdDominated();
		D.pd(D.D_TEMP, " \t stmt - " + stmt);
		for(CfgNode cni : imm)
			D.pd(D.D_TEMP, " \t\t\t in block immediately dominates " + cni.toString());

		D.pd(D.D_TEMP, " \t\t\t ======================= ");

		CopyOnWriteArrayList<CfgNode> domF  = stmt.getDominanceFrontier();
		for(CfgNode cni : domF)
			D.pd(D.D_TEMP, " \t\t\t in dominance frontier " + cni.toString());
	}


	*/	
	/*
	public static void process(Collection<DepGraph> depGraphs)
	{
		
		CfgNode queryExec = null;
		D.ps(D.D_PATH, "Beginning path analysis..............");

		for(DepGraph depGraph : depGraphs)
		{
		// add all ctrl paths reaching sql sinks from this depgraph
		// into the list of control flows to be transformed
//			ia.addPaths(depGraph);
			
//			ia.enumeratePaths(depGraph);
		}
		
		
		for(ControlFlow cf : ia.getPaths())
		{
			if(!cf.isProcessed())
				D.ps(D.D_PATH, " path ------- " + cf.toString());
		}
		
		// make derivation tree for each path
		// transform the derivation tree
		for(ControlFlow cf : ia.getPaths())
		{
			if(cf.isProcessed())
				continue; 
			else
				cf.setProcessed(true);
	
			ArrayList<CfgNode> toDerivationTree = new ArrayList<CfgNode> ();
			boolean bLast = true;
			for(CfgNode cn : cf.getAll())
			{
				if(bLast)
				{
					queryExec = cn;
					bLast = false;
					continue;
				}
				toDerivationTree.add(cn);
			}
			
			//Collections.reverse(toDerivationTree);
			DerivationTree dt = DerivationTree.makeDerivationTree(toDerivationTree);		
			dt.makePS();
			ia.dumpProgChanges(dt);
		}		
	}
*/
}
