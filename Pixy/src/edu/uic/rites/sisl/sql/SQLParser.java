/**
 * 
 */
package edu.uic.rites.sisl.sql;
import sql92.*;
import java.io.*;
import java.sql.SQLException;
import java.util.*;
import edu.uic.rites.sisl.*;

/**
 * @author prithvi
 *
 */
public class SQLParser
{
	sql92 parser; 
	String query; 
	ArrayList<SimpleNode> tokens;
	SimpleNode currentToken;
	int iToken; 
	
	public SQLParser(String sql)
	{
		this.query = sql; 
		iToken = -1;
		currentToken = null;
		SimpleNode.resetTokens();
	}
	
	public void tokenize()
	throws SQLException
	{
		parser = new sql92(new StringReader(new String(this.query + ";;")));
		tokens = new ArrayList<SimpleNode> ();
		System.out.println(" Query - " + this.query);
		try
		{
			parser.traceStatement();	
		}
		catch(Exception e)
		{
			System.out.println(" Caught Exception in SQLParser....re-throwing it...");
			SimpleNode.resetTokens();
			e.printStackTrace();
			throw new SQLException(e.getMessage());			
		}

		if(SimpleNode.getException() != null)
		{
			String msg = SimpleNode.getException();
			SimpleNode.resetTokens();
			throw new SQLException(msg);
		}
		
		ArrayList<SimpleNode> toks = SimpleNode.getTokens(); 
		if(toks != null)
		{
			tokens.addAll(toks);
			Collections.reverse(tokens);
			
			for(SimpleNode tok : tokens)
				D.pr(D.D_PS, "################# identified tokens - " + tok.toString());
			
			ArrayList<Integer> repeatable = SimpleNode.getRepeatablePartialQueries();
			int i = -1;
			for(Integer ir : repeatable)
			{
				if(++i % 2 == 0)	
					D.pr(D.D_TEMP, "\t\t\t repeatable range start - " + ir);
				else
					D.pr(D.D_TEMP, "\t\t\t repeatable range end - " + ir);
			}
		}
	}
	
	public int getBeginOffset()
	{
		if(currentToken == null)
			throw new Error("Call hasMoreTokens() before get begin/end offset");
		int iBegin = 0; 
		iBegin = currentToken.getBeginOffset();
		return iBegin;
	}
	
	public int getEndOffset()
	{
		if(currentToken == null)
			throw new Error("Call hasMoreTokens() before get begin/end offset");

		int iEnd = 0; 
		iEnd = currentToken.getEndOffset();
		return iEnd;
	}
	
	public String getLexeme()
	{
		if(currentToken == null)
			throw new Error("Call hasMoreTokens() before get lexeme");
		
		return currentToken.getLexeme();
	}
	
	public boolean hasMoreTokens()
	{
		if(tokens.size() == 0)
		{
			D.pv(D.D_TEMP, " no tokens found!!!!");
			return false; 
		}
		
		if(iToken < tokens.size() - 1)
		{
			iToken ++; 
			currentToken = tokens.get(iToken);
			return true;
		}
		else
			return false; 
	}
	
	public static void main(String[] args)
	{
		String sql = "select * from x where uid = '% john %';;";
		SQLParser sp = new SQLParser(sql);
		try
		{
			String parseTree = sp.parser.traceStatement();	
			sp.tokenize();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		
		while(sp.hasMoreTokens())
		{
			System.out.println("token - " + sp.getLexeme() + " begin - " + 
				sp.getBeginOffset() + " end - " + sp.getEndOffset());
		}
	}

	public ArrayList<Integer> getRepeatablePartialQueries() {
		return SimpleNode.getRepeatablePartialQueries();
	}
}
