package edu.uic.rites.sisl;

public class TimeProfiler 
{
	
	public static final int dg = 1; 
	public static final int ssa = 2; 
	public static final int intra = 3; 
	public static final int inter = 4; 
	public static final int cbv = 5; 
	public static final int div = 6;
	public static final int dt = 7;
	public static final int ca = 8;
	
	
	public static long progStart; 
	public static long progEnd; 
	
	private static long startTime; 
	private static long endTime; 
	public static long elapsed; 
	
	public static long depGraphs;
	public static long ssaTransformation;
	public static long intraPathEnumeration;
	public static long interPathEnumeration; 
	public static long cbViolation;
	public static long dataIndeViolation;
	public static long derivationTree; 
	public static long conflictAnalysis;

	public static long[] dg_sub;
	public static long[] ssa_sub;
	public static long[] intra_sub;
	public static long[] inter_sub; 
	public static long[] cbv_sub;
	public static long[] div_sub;
	public static long[] dt_sub; 
	public static long[] ca_sub;
	
	public static void reset() {
		depGraphs = 0;
		ssaTransformation = 0;
		intraPathEnumeration = 0; 
		interPathEnumeration = 0; 
		cbViolation = 0;
		dataIndeViolation = 0;
		derivationTree = 0;
		conflictAnalysis = 0;
		startTime = endTime = elapsed = 0 ; 
		
		dg_sub = new long[10];
		ssa_sub = new long[10];
		intra_sub = new long[10];
		inter_sub = new long[10];
		cbv_sub = new long[10];
		div_sub = new long[10];
		dt_sub = new long[10];
		ca_sub = new long[10];
	}
	
	public static void start() { startTime = System.currentTimeMillis();}
	public static void end() { endTime  = System.currentTimeMillis(); elapsed = endTime - startTime;}

	public static void s() { progStart = System.currentTimeMillis();}
	public static void e() { progEnd  = System.currentTimeMillis();}

	public static void end(int dg) {
		end();		
		switch(dg)
		{
			case D.D_DG:
				depGraphs += elapsed;
				break;
		
			case D.D_SSA:
				ssaTransformation += elapsed;
				break;
				
			case D.D_INTRA:
				intraPathEnumeration += elapsed; 
				break;
				
			case D.D_INTER:
				interPathEnumeration += elapsed; 
				break;
				
			case D.D_CBV:
				cbViolation += elapsed; 
				break;
				
			case D.D_SAV:
				dataIndeViolation += elapsed; 
				break;
				
			case D.D_DT:
				derivationTree += elapsed; 
				break;
				
			case D.D_TR:
				conflictAnalysis += elapsed; 
				break;
		}
		
		startTime = endTime = elapsed = 0;
	}
	
	private static String pt(long total, long part)
	{
		if(total == 0)
			return "\t\t\t total = 0";
		return " \t\t\t" + part + " ( " + (int)((part * 100)/total) + " %)";
	}
	
	public static void reportTimeProfile()
	{
		long accounted = 
			depGraphs +  ssaTransformation +  intraPathEnumeration + 
			interPathEnumeration + cbViolation  + dataIndeViolation + 
			derivationTree + conflictAnalysis;
		long total = progEnd - progStart; 
		
		D.pr("\n=========\t\t Timing Profile");
		
		D.pr("Total time = \t\t\t"  + total);
		D.pr("\t in dep graphs - " + pt(total, depGraphs));
		D.pr("\t in ssa transf - " + pt(total, ssaTransformation));
		D.pr("\t in intra path - " + pt(total, intraPathEnumeration));
		for(int i = 0; i < intra_sub.length; i++)
		{
			long t = intra_sub[i];
			if(t != 0)
				D.pr("\t\t\t component - " + i + " " + t + "  - " + pt(intraPathEnumeration, t));
		}
		
		D.pr("\t in inter path - " + pt(total, interPathEnumeration));
		for(int i = 0; i < intra_sub.length; i++)
		{
			long t = inter_sub[i];
			if(t != 0)
				D.pr("\t\t\t component - " + i + " " + t + "  - " + pt(interPathEnumeration, t));
		}

		D.pr("\t in dt constru - " + pt(total, derivationTree));
		D.pr("\t in cb violati - " + pt(total, cbViolation));
		D.pr("\t in data indep - " + pt(total, dataIndeViolation));
		D.pr("\t in conf n tra - " + pt(total, conflictAnalysis));
		D.pr("\t in other modl - " + pt(total, total-accounted));
	}

	private static long st;
	private static long en;

	public static void ms() {
		st = System.currentTimeMillis();
	}

	public static void me(int module, int subc) {
		
		en = System.currentTimeMillis();
		long el = en - st; 
		
		System.out.println("TimeProfiler.me() was called");

		switch(module)
		{
		case D.D_INTRA:
			intra_sub[subc] += el;
			break;
			
		case D.D_INTER:
			inter_sub[subc] += el; 
			break;
		}
		
		st = en = 0 ;
	}
}
