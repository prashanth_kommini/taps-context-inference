package edu.uic.rites.sisl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
//import java.util.concurrent.CopyOnWriteArrayList;

import at.ac.tuwien.infosys.www.pixy.analysis.dep.Sink;
import at.ac.tuwien.infosys.www.pixy.conversion.TacFunction;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNode;

public class SinkPath 
{
	private Sink sink;
	private ArrayList<ControlFlow> flows; 
	private ArrayList<TacFunction> fnsTraversed; 
	private LinkedHashMap<ArrayList<CfgNode>, TacFunction> nodesTraversed; 
	private CfgNode entryPoint; 
	private CfgNode exitPoint;
	
	public SinkPath(Sink value){
		this.sink = value; 
		flows = new ArrayList<ControlFlow>();
		fnsTraversed = new ArrayList<TacFunction>();
		nodesTraversed = new LinkedHashMap<ArrayList<CfgNode>, TacFunction>();
	}
	public void setSink(Sink sink) {
		this.sink = sink;
	}
	public Sink getSink() {
		return sink;
	}
	public void setCtrlFlows(ArrayList<ControlFlow> flows) {
		this.flows = flows;
	}
	public ArrayList<ControlFlow> getCtrlFlows() {
		return flows;
	}
	public void setFnsTraversed(ArrayList<TacFunction> fnsTraversed) {
		this.fnsTraversed = fnsTraversed;
	}
	public ArrayList<TacFunction> getFnsTraversed() {
		return fnsTraversed;
	}
	public void setNodesTraversed(LinkedHashMap<ArrayList<CfgNode>, TacFunction> nodesTraversed) {
		this.nodesTraversed = nodesTraversed;
	}
	public LinkedHashMap<ArrayList<CfgNode>, TacFunction> getNodesTraversed() {
		return nodesTraversed;
	}
	public void setEntryPoint(CfgNode entryPoint) {
		this.entryPoint = entryPoint;
	}
	public CfgNode getEntryPoint() {
		return entryPoint;
	}
	public void setExitPoint(CfgNode exitPoint) {
		this.exitPoint = exitPoint;
	}
	public CfgNode getExitPoint() {
		return exitPoint;
	}
	public void setNodesTraversed(
			ArrayList<CfgNode> cfgNodesTraversed, TacFunction tf) 
	{
		ArrayList<CfgNode> al = new ArrayList<CfgNode>();
		al.addAll(cfgNodesTraversed);
		this.nodesTraversed.put(al, tf);		
	} 
}
