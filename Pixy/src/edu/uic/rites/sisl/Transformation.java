package edu.uic.rites.sisl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
//import java.util.concurrent.CopyOnWriteArrayList;

import at.ac.tuwien.infosys.www.pixy.analysis.dep.DepGraph;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNode;

public class Transformation {	
	// make singleton
	public static Transformation get()
	{ 
		if(transform == null)
			transform = new Transformation();
		return transform;
	}

	public static void addProposedChanges(ControlFlow cf, CfgNode sink,
			ArrayList<ProgramChange> progChanges) 
	{
		FlowChanges fc = new FlowChanges(cf, sink, progChanges);
		D.pv(D.D_TEMP, " Adding a flow for this sink - " + sink);
		Transformation.get().addFlowFor(fc, sink);		
	}

	private void addFlowFor(FlowChanges fc, CfgNode sink) {
		
		D.pv(D.D_TEMP, " checking if sink " + sink + " already has an entry" );
		SinkChanges scF = null;
		fc.setSink(sink);

		boolean bFound = false;
		for(SinkChanges sc: this.sinkTransf)
		{
			bFound = sc.forSinkNode(sink); 
			D.pv(D.D_TEMP, "\t checking with " + sc.getSinkCfgNode() + 
					" found - " + bFound + "\n");
			if(bFound)
			{
				scF = sc;
				break;
			}
		}
		if(scF == null)
		{
			D.pv(D.D_TEMP, " creating a new sink change for - " +sink); 
			scF = new SinkChanges(sink);
		}
		
		scF.addNewFlowChanges(fc);
		if(!bFound)
			this.sinkTransf.add(scF);
	}

	
	// manage a mapping between changes in inter-proc paths
	// for a given sink
	private ArrayList<SinkChanges> sinkTransf;
	
	private ArrayList<ProgramChange> toUpdate;

	private static Transformation transform;
	
	private Transformation()
	{
		this.sinkTransf = new ArrayList<SinkChanges>();
	}

	public static void processProposedChanges() 
	{
		System.out.println(" Processing the program changes...");
		Transformation tr = Transformation.get();
		TransformationResults results = TransformationResults.get();

		// first get all sinks that do not have conflicts within their own
		// paths
		for(SinkChanges sc : tr.sinkTransf)
		{
			boolean b = sc.checkIntraConflicts();
			D.pr(D.D_TEMP, " Transition_Session2_Look: conflicts within sink's paths: at node " + sc.getSinkCfgNode() + " has conflicts : " + b);
		}

		// now for each sink determine if it conflicts with proposed changes for 
		// other sinks

		for(SinkChanges sc : tr.sinkTransf)
		{
			// do not consider those sinks with conflicts within its own paths
			//if(sc.interConflict)
			if(sc.intraConflict)
				continue;
			
			// check if sc has intra conflicts with others
			for(SinkChanges sc2 : tr.sinkTransf)
			{
				if(sc.equals(sc2) || sc2.intraConflict)
					continue;
				
				if(SinkChanges.inConflictWith(sc, sc2))
				{
					D.pr(D.D_TEMP, " Transition_Session2_Look: Inter - Sink conflict between sink - " + sc + " \n and \n " + sc2);
					sc.interConflict = true; sc2.interConflict = true; 
					sc.interConflictsWith.add(sc2);
					sc2.interConflictsWith.add(sc);
				}
			}
		}
		
		ArrayList<ProgramChange> changeFiles = 
			new ArrayList<ProgramChange>();
		// now for all sinks that don't have inter or intra conflicts 
		// perform the file changes
		int i = 0;
		for(SinkChanges sc : tr.sinkTransf)
		{
			D.pv(D.D_TEMP, " iteration - " + i++);
			if(sc.interConflict || sc.intraConflict)
			{
				D.pr(D.D_TEMP, "Transition_Session2_Look: increasing size because of conflicts by " + sc.allFlows.size() + 
						sc.interConflict + " or " + sc.intraConflict);
//				results.incrementSinkCount(sc.allFlows.size());
				continue;
			}
			
//			results.incrementSinkCount(sc.allFlows.size());
			results.incrementTransSink(sc.allFlows.size());
			for(FlowChanges fc : sc.allFlows)
			{
				//changeFiles.addAllAbsent(fc.proposedChanges);
				boolean bDup = false;
				for(ProgramChange pc : fc.proposedChanges)
				{
					for(ProgramChange pcExisting : changeFiles)
						if(pc.equals(pcExisting))
						{
							bDup = true;
							break;
						}
					if(!bDup)
						changeFiles.add(pc);
					bDup = false;
				}
			}
		}
		
		D.pv(D.D_TEMP, "Transition_Session2_Look: Change files - " + changeFiles);
		tr.transformFiles(changeFiles);
		
		results.setTotalLinesChanged(changeFiles.size());
		ArrayList<SinkChanges> tmp = 
			new ArrayList<SinkChanges>();
		tmp.addAll(tr.sinkTransf);
		
		for(SinkChanges sc : tmp)
		{
			if(sc.visited)
				continue;
			else
				sc.visited = true;
			
			if(sc.intraConflict)
			{
				D.pv(D.D_TEMP, " Transition_Session2_Look: adding a report of intra sink path conflicts " + sc.getSinkCfgNode());
				results.addConflict(sc.intraConflictsIn);

//				boolean bR = tmp.remove(sc);
	//			D.pv(D.D_TEMP, " removed - " + bR);
			}
			else
			if(sc.interConflict)
			{
				D.pv(D.D_TEMP, " Transition_Session2_Look: reporting an inter-sink conflict");
				ArrayList<FlowChanges> con = new 
					ArrayList<FlowChanges>();
				for(FlowChanges fc : sc.allFlows)
					if(fc.interContributor)
						con.add(fc);
				
				for(SinkChanges scCon : sc.interConflictsWith)
					for(FlowChanges fc : scCon.allFlows)
						if(fc.interContributor)
							con.add(fc);
				results.addConflict(con);
				for(SinkChanges sct : sc.interConflictsWith)
					sct.visited = true; 
				//tmp.removeAll(sc.interConflictsWith);
				
			}
	//		if(tmp.size() == 0)
		//		break;
		}
	}

	public void transformFiles(ArrayList<ProgramChange> proposed)
	{
		ArrayList<ProgramChange> ap = new ArrayList<ProgramChange>();
		ap.addAll(proposed);
		
		ArrayList<ProgramChange> apTmp = null;
		ArrayList<String> processedFiles = new ArrayList<String>();
		
		D.pr("===================================== making changes now ================= ");
		for(ProgramChange pc : ap)
		{
			String file = pc.getInFile();
			if(processedFiles.contains(file))
				continue; 
			else
				processedFiles.add(file);
			
			apTmp = getAllChangesInFile(file, ap);
			D.pr("\t Transition_Session2_Look: Changes in file - " + file + " start ");
			for(ProgramChange pcRecord : apTmp)
			{
				D.pr(" " + pcRecord);
			}
			D.pr("\t Transition_Session2_Look: Changes in file - " + file + " end ");

			makeChangesInFile(file, apTmp);
			if(ap.size() == 0)
				break;
		}		
	}
	

	public void makeChangesInFile(String file, ArrayList<ProgramChange> changes)
	{
		if(changes == null || changes.isEmpty())
			return; 
		
		String transformedFile = file + "__transformed";
		BufferedReader from = getInStream(file);
		
		// overwrite if already exists
		PrintWriter to = getOutStream(transformedFile, true);
		
		ArrayList<String> addAfter = new ArrayList<String>();
		int iAddAfterLine = 0;
		
		D.pd(D.D_TEMP, " transforming file - " + file + " total changes - " + changes.size() + 
				" \n \t changes - " + changes);
		
		int iCurrentLine = 1; 
		for(ProgramChange pc : changes)
		{
//			D.pd(D.D_TR, " considering " + pc.toString() );
			int iLine = pc.getLineNumber();
			D.pv(D.D_TEMP, " current line - " + iCurrentLine + " change - " + pc.toString());
			if(((iCurrentLine > iAddAfterLine) || (iLine != iAddAfterLine)) 
				&& addAfter.size() != 0)
			{
				if(iCurrentLine == iAddAfterLine)
				{
					transferLine(from, to);
					iCurrentLine++;
				}
				
				for(String st : addAfter)
					addLine(to, st);
				
				addAfter.clear();
			}
			
			// write until the current line 
			while(iCurrentLine < iLine)
			{
//				D.pv(D.D_TR, " transfering line " + iCurrentLine + " to " + transformedFile);
				transferLine(from, to);
				iCurrentLine++;
			}
			
			// make changes required now.
			switch(pc.getChangeOperation())
			{
			case ProgramChange.ADDAFTER:
				D.pv(D.D_TEMP, " Adding after line - " + pc.getStmt()); 
				iAddAfterLine = pc.getLineNumber();
				addAfter.add(pc.getStmt());
				break;
				
			case ProgramChange.ADDBEFORE:
				D.pv(D.D_TEMP, " Added line - " + pc.getStmt()); 
				addLine(to, pc.getStmt());
				break;
				
			case ProgramChange.REPLACE:
				iCurrentLine++;
				replaceLine(from, to, pc.getStmt());
				break;
				
			default: 
				transferLine(from, to);
				iCurrentLine++;
				break;
			}
		}

		if(addAfter.size() != 0)
		{
			for(String st : addAfter)
				addLine(to, st);	
			addAfter.clear();
		}

		transferLeftOverLines(from, to);

		try
		{
			D.pv(D.D_TR, "...........closing the file " + transformedFile);
			to.flush();
			from.close(); 
			to.close();
		}
		catch(Exception e)
		{
			D.pf(D.D_TR, " failed to close the original or transformed file - " + e.getMessage());
		}
	}
	
	private void transferLeftOverLines(BufferedReader from, PrintWriter to) {
		String str = null;
		try
		{
			while((str = from.readLine()) != null )
			{
				D.pv(D.D_TEMP, " Writing line - " + str);
				addLine(to, str);
			}
		}
		catch(Exception e)
		{
			throw new Error(" while transferring leftover lines exception = " + e.getMessage());
		}
	}

	private void replaceLine(BufferedReader from, PrintWriter to, String stmt) 
	{
		String str = null;
		try
		{
			str = from.readLine();
			addLine(to, stmt);
		}
		catch(Exception e)
		{
			throw new Error(" failed to replace - " + str + " with " 
					+ stmt  + " error - "+ e.getMessage());
		}		
	}

	private void addLine(PrintWriter to, String stmt) 
	{
		try
		{
			to.println(stmt);
		}
		catch(Exception e)
		{
			throw new Error(" failed to write transformed line - " + 
					stmt + " error - " + e.getMessage());
		}		
	}

	private void transferLine(BufferedReader bis, PrintWriter dos) 
	{
		try
		{
			String read = bis.readLine();
			dos.println(read);
			D.pd(D.D_TEMP, " writing line - " + read);
		}
		catch(Exception e)
		{
			throw new Error(" failed to create final transformed file - " + e.getMessage());
		}
	}

	public static PrintWriter getOutStream(String fileOut, boolean bOverWrite) 
	{
		File file = new File(fileOut);
		try{file.delete();} catch(Exception e){};
		file = new File(fileOut);
		
	    FileOutputStream fos = null;
	    PrintWriter dos = null;
	
	    try {
	      fos = new FileOutputStream(file, bOverWrite);
	      dos = new PrintWriter(fos, false);
	    }
	    catch(Exception e)
	    {
	    	throw new Error("Writing file " + file + " Exception + " + e.getMessage());
	    }

	    return dos;
	}

	public static BufferedReader getInStream(String fileN) 
	{
		File file = new File(fileN);
	    FileInputStream fis = null;
	    BufferedReader br = null;
	    
	    try {
	      fis = new FileInputStream(file);
	      br = new BufferedReader(new InputStreamReader(fis));
	    }
	    catch(Exception e)
	    {
	    	throw new Error("reading file " + file + " Exception + " + e.getMessage());
	    }
		return br;
	}

	public  ArrayList<ProgramChange> getAllChangesInFile(String inFile, 
			ArrayList<ProgramChange> allChanges)
	{
		ArrayList<ProgramChange> pa = new ArrayList<ProgramChange>();
		
		for(ProgramChange pc : allChanges)
		{
			if(inFile.equals(pc.getInFile()))
			{
				pa.add(pc);
			}
		}
	
		Collections.sort(pa);
		D.pv(D.D_TEMP, " All changes in file " + inFile);
		for(ProgramChange pc : pa)
			D.pv(D.D_TEMP, " after sorting - " + pc.toString());

		D.pv(D.D_TEMP, "..........remaining changes.......");
		for(ProgramChange pc : allChanges)
			D.pv(D.D_TEMP, "\t\t\t " + pc.toString());
		
		return pa;
	}
}

class SinkChanges
{
	public boolean visited;
	CfgNode sink;
	ArrayList<FlowChanges> allFlows;
	
	boolean intraConflict; 
	ArrayList<FlowChanges> intraConflictsIn;
	
	boolean interConflict;
	ArrayList<SinkChanges> interConflictsWith;
	
	public SinkChanges(CfgNode sink2) {
		this.sink = sink2; 
		this.allFlows = new ArrayList<FlowChanges>();
		this.intraConflictsIn = new ArrayList<FlowChanges>();
		this.interConflictsWith = new ArrayList<SinkChanges>();
	}
	
	public static boolean inConflictWith(SinkChanges sc1, SinkChanges sc2) 
	{	
		boolean bConflict  = false;
		ArrayList<FlowChanges> fc1 = sc1.allFlows;
		ArrayList<FlowChanges> fc2 = sc2.allFlows; 
	
		for(FlowChanges c1 : fc1)
		{
			for(FlowChanges c2 : fc2)
			{
				boolean bConflicts = FlowChanges.areInConflict(c1, c2);
				if(bConflicts)
				{
					c1.setInterConflict();
					c2.setInterConflict();
					D.pv(D.D_TEMP, " detected an inter-sink conflict in " + 
							sc1.getSinkCfgNode() + " and " + sc2.getSinkCfgNode());
					bConflict = true;
				}
			}
		}
		
		D.pv(D.D_TEMP, bConflict + " conflicts in changes for " + sc1.getSinkCfgNode() + 
				" and " + sc2.getSinkCfgNode());
		return bConflict;
	}

	public CfgNode getSinkCfgNode() {
		return this.sink;
	}

	public boolean checkIntraConflicts() 
	{
		ArrayList<FlowChanges> tm = 
			new ArrayList<FlowChanges>();
		
		D.pv(D.D_TEMP, " Transition_Session2_Look: checking for intra conflicts for sink at line " + 
				this.sink.getFileName() + " : " + this.sink.getOrigLineno());
		
		tm.addAll(this.allFlows);
		
		ArrayList<FlowChanges> processed = new ArrayList<FlowChanges>();
		
		for(FlowChanges fc : tm)
		{	
			if(processed.contains(fc))
				continue; 
			
			for(FlowChanges rest : tm)
			{
				if(fc == rest || processed.contains(rest))
					continue; 
				
				// now detect if fc conflicts with rest 
				boolean bConflicts = FlowChanges.areInConflict(fc, rest);
				if(bConflicts)
				{
					D.pr(D.D_TEMP, " Transition_Session2_Look: \t detected an intra-sink conflict");
					this.intraConflict = true;
					if(!this.intraConflictsIn.contains(fc))
						this.intraConflictsIn.add(fc);
					
					if(!this.intraConflictsIn.contains(rest))
						this.intraConflictsIn.add(rest);
					
					D.pr(D.D_TEMP, "Transition_Session2_Look: \t current in confict " + fc + " \n  and \n \t " + rest);
					processed.add(rest);
				}
			}
			
			// in concurrent lists, non-null placeholders
			// are created for removed entries...avoid 
			// looping in
			if(tm.size() == 0)
				break;
		}
		
		return this.intraConflict;
	}

	public void addNewFlowChanges(FlowChanges fc) {
		this.allFlows.add(fc);
	}

	public boolean forSinkNode(CfgNode sink2) 
	{
		if(this.sink.equals(sink2))
			return true;
		return false;
	}
	
}

class FlowChanges
{
	public FlowChanges(ControlFlow cf, CfgNode sink2,
			ArrayList<ProgramChange> progChanges) 
	{
		this.sink = sink2;
		this.interProcFlow = cf; 
		this.proposedChanges = new ArrayList<ProgramChange>();
		this.proposedChanges.addAll(progChanges);
	}
	
	public void setSink(CfgNode sink2) {
		this.sink = sink2;
	}

	boolean interContributor;
	public void setInterConflict() {
		this.interContributor = true;
	}

	public boolean getInterConflict() {
		return this.interContributor;
	}

	// conflicting transformations
	// if a single line - 
	// 		required to be transformed differently in two paths ( replace )
	//		is to be replaced in only a single path, but is present in another
	public static boolean areInConflict(FlowChanges fc1,
			FlowChanges fc2) 
	{
		
		ArrayList<ProgramChange> pc1 = fc1.proposedChanges;
		ArrayList<ProgramChange> pc2 = fc2.proposedChanges;
		
		boolean bReplaceConflicts = detectReplaceConflicts(pc1, pc2);
		D.pv(D.D_TEMP, " replace conflict : " + bReplaceConflicts + 
				"  + in program changes - " + pc1 + " \n and \n " + pc2);	
		
		boolean bAddBeforeConflicts = detectAddBeforeConflicts(pc1, pc2);
		D.pv(D.D_TEMP, " add before conflict : " + bAddBeforeConflicts + 
				"  + in program changes - " + pc1 + " \n and \n " + pc2);	
		
		if(!bReplaceConflicts && !bAddBeforeConflicts)
			return false;
		else
			return true;
	}

	private static boolean detectAddBeforeConflicts(
			ArrayList<ProgramChange> pc1, ArrayList<ProgramChange> pc2) {
		
		ArrayList<ProgramChange> addBefore1 = ProgramChange.getStmtsForOp(pc1, 
				ProgramChange.ADDBEFORE);		
		ArrayList<ProgramChange> addBefore2 = ProgramChange.getStmtsForOp(pc2,
				ProgramChange.ADDBEFORE);
		
		for(ProgramChange a1 : addBefore1)
		{
			// get the location and proposed change for replace in flow 1
			String loc1 = a1.getLocationWithLhs();
			String stmt1 = a1.getStmt();
			
			// and check against all replace in the flow 2
			for(ProgramChange a2 : addBefore2)
			{
				String loc2 = a2.getLocationWithLhs();
				String stmt2 = a2.getStmt();
				if(loc1.equals(loc2) && !stmt1.equals(stmt2))
				{
					D.pv(D.D_TEMP, " found a conflicting addBefore for same line - " +
							loc1 + " changes = " + stmt1 + " and " + stmt2);
					
					a1.setConflictCause();
					a2.setConflictCause();
					return true;
				}
				else
				{
					if(loc1.equals(loc2))
						D.pv(D.D_TEMP, " no conflicting addBefore for same line - " +
							loc1 + " changes = " + stmt1 + " and " + stmt2);				
					D.pv(D.D_TEMP, " no addbefore confict for - " + stmt1 + " and " + stmt2);
				}
			}
		}
		
		return false;
	}

	private static boolean detectReplaceConflicts(ArrayList<ProgramChange> pc1,
			ArrayList<ProgramChange> pc2) {

		ArrayList<ProgramChange> replacePc1 = ProgramChange.getStmtsForOp(pc1, ProgramChange.REPLACE);
		ArrayList<ProgramChange> replacePc2 = ProgramChange.getStmtsForOp(pc2, ProgramChange.REPLACE);
		
		for(ProgramChange r1 : replacePc1)
		{
			// get the location and proposed change for replace in flow 1
			String loc1 = r1.getLocation();
			String stmt1 = r1.getStmt();
			
			// and check against all replace in the flow 2
			for(ProgramChange r2 : replacePc2)
			{
				String loc2 = r2.getLocation();
				String stmt2 = r2.getStmt();
				if(loc1.equals(loc2) && !stmt1.equals(stmt2))
				{
					D.pv(D.D_TEMP, " found a conflicting replace for same line - " +
							loc1 + " changes = " + stmt1 + " and " + stmt2);
					
					r1.setConflictCause();
					r2.setConflictCause();
					return true;
				}
			}
		}

		return false;
	}

	CfgNode sink; 
	ControlFlow interProcFlow; 
	ArrayList<ProgramChange> proposedChanges;
}