package edu.uic.rites.sisl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
//import java.util.concurrent.CopyOnWriteArrayList;

import at.ac.tuwien.infosys.www.pixy.analysis.dep.DepGraph;
import at.ac.tuwien.infosys.www.pixy.conversion.Cfg;
import at.ac.tuwien.infosys.www.pixy.conversion.CfgEdge;
import at.ac.tuwien.infosys.www.pixy.conversion.TacActualParam;
import at.ac.tuwien.infosys.www.pixy.conversion.TacFormalParam;
import at.ac.tuwien.infosys.www.pixy.conversion.TacFunction;
import at.ac.tuwien.infosys.www.pixy.conversion.TacOperators;
import at.ac.tuwien.infosys.www.pixy.conversion.TacPlace;
import at.ac.tuwien.infosys.www.pixy.conversion.Variable;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNode;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeAssignArray;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeAssignBinary;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeAssignRef;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeAssignSimple;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeAssignUnary;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeBasicBlock;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeCall;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeCallBuiltin;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeCallPrep;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeCallRet;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeCallUnknown;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeEmpty;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeEmptyTest;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeEntry;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeIf;

public class PathEnumerator {
	public static final int NO_PATH = -1;
	public static final int NOT_INITED = 0;
	public static final int THEN_PATH = 1;
	public static final int ELSE_PATH = 2;
	public static final int ALL_PATH = 3;

	private boolean bSeenExit;

	// private ArrayList<ControlFlow> paths;
	private ControlFlow path;
	private LinkedHashMap<ArrayList<TacFunction>, TacFunction> invokeOrder;
	private CfgNode sink;
	// private TacFunction entryPoint;
	private Cfg funcCfg;
	private ArrayList<CfgNode> dependsNodes;
	private LinkedHashMap<Variable, Variable> phiLhsMapsTo;
	private LinkedHashMap<Variable, Variable> varDefActive;

	private CfgNode exitNode;
	private CfgNode entryNode;

	/**
	 * @return the entryNode
	 */
	public CfgNode getEntryNode() {
		return entryNode;
	}

	/**
	 * @param entryNode
	 *            the entryNode to set
	 */
	public void setEntryNode(CfgNode entryNode) {
		this.entryNode = entryNode;
	}

	private CfgNodeIf nearestExitConditionalNode;
	// change pathDecisions to use pathAnalysis variable and its values
	private LinkedHashMap<CfgNode, Integer> pathDecisions;
	private ArrayList<PathDecision> pathAnalysis;

	private int nearestExitPathId;
	private ArrayList<CfgNode> nodesBetween;
	private ArrayList<CfgNode> loopIterStopsAt;
	private DepGraph depGraph;

	private static int stepCounter;
	private static final int RECURSION_UPPER_BOUND = 20000;

	/**
	 * @param loopIterStopAt
	 *            the loopIterStopAt to set
	 */
	public void setLoopIterStopAt(CfgNode loopIterStopAt) {
		if (this.loopIterStopsAt == null)
			this.loopIterStopsAt = new ArrayList<CfgNode>();
		this.loopIterStopsAt.add(loopIterStopAt);
		this.m_bInLoop = true;
	}

	public ArrayList<CfgNode> getLoopIterStopAt() {
		return this.loopIterStopsAt;
	}

	
	private static ArrayList<ControlFlow> flows;

	private static ArrayList<CfgNode> nodesCovered;

	public void addCoveredDep(CfgNode cn) {
		D.pv(D.D_TEMP, " Adding node to the covered dependencies - " + cn);
		if(!nodesCovered.contains(cn))
			nodesCovered.add(cn);
	}

	/**
	 * @return the phiLhsMapsTo
	 */
	public LinkedHashMap<Variable, Variable> getPhiLhsMapsTo() {
		return phiLhsMapsTo;
	}

	/**
	 * @param phiLhsMapsTo
	 *            the phiLhsMapsTo to set
	 */
	public void setPhiLhsMapsTo(LinkedHashMap<Variable, Variable> phiLhsMapsTo) {
		this.phiLhsMapsTo = phiLhsMapsTo;
	}

	/**
	 * @return the phiLhsMapsTo
	 */
	public LinkedHashMap<Variable, Variable> getActiveVar() {
		return this.varDefActive;
	}

	/**
	 * @param phiLhsMapsTo
	 *            the phiLhsMapsTo to set
	 */
	public void setVarDefActive(LinkedHashMap<Variable, Variable> varDefActive) {
		this.varDefActive = varDefActive;
	}

	/**
	 * @return the paths
	 */
	// public ArrayList<ControlFlow> getPaths() {return paths;}
	public ControlFlow getPath() {
		return path;
	}

	/**
	 * @param paths
	 *            the paths to set
	 */
	// public void setPaths(ArrayList<ControlFlow> paths) {this.paths = paths;}
	public void setPaths(ControlFlow path) {
		this.path = path;
	}

	private PathEnumerator() {
	}

	public PathEnumerator(Cfg c, ArrayList<CfgNode> inNodes,
			LinkedHashMap<ArrayList<TacFunction>, TacFunction> invokeOrder,
			CfgNode sink, TacFunction entryPoint, DepGraph depGraph) {
		this(c, inNodes);
		if (invokeOrder == null)
			throw new Error(
					" Null invokeOrder passed to PathEnumerator constructor");

		this.invokeOrder = invokeOrder;
		this.sink = sink;
		this.depGraph = depGraph;
		// this.dep
	}

	public PathEnumerator(Cfg c, ArrayList<CfgNode> inNodes) {
		PathEnumerator.nodesCovered = new ArrayList<CfgNode>();
		this.pathDecisions = new LinkedHashMap<CfgNode, Integer>();
		this.varDefActive = new LinkedHashMap<Variable, Variable>();
		this.phiLhsMapsTo = new LinkedHashMap<Variable, Variable>();
		funcCfg = c;
		dependsNodes = inNodes;
		this.path = new ControlFlow();
		this.pathAnalysis = new ArrayList<PathDecision>();
	}

	public PathEnumerator deepCopy() {
		PathEnumerator peCopy = new PathEnumerator();

		peCopy.funcCfg = this.funcCfg;

		LinkedHashMap<Variable, Variable> phiLhsMap = new LinkedHashMap<Variable, Variable>();
		phiLhsMap.putAll(this.phiLhsMapsTo);
		peCopy.phiLhsMapsTo = phiLhsMap;

		D.pv(D.D_TEMP, " existing phiLhsMaps to - ");
		for (Map.Entry<Variable, Variable> e : this.phiLhsMapsTo.entrySet())
			D.pv(D.D_TEMP, " \t\t value " + e.getKey() + " maps to "
					+ e.getValue());

		D.pv(D.D_TEMP, " Deep copy of phiLhsMapTo \n");
		for (Map.Entry<Variable, Variable> e : peCopy.phiLhsMapsTo.entrySet())
			D.pv(D.D_TEMP, " \t\t value " + e.getKey() + " maps to "
					+ e.getValue());

		LinkedHashMap<Variable, Variable> varDef = new LinkedHashMap<Variable, Variable>();
		varDef.putAll(this.varDefActive);
		peCopy.varDefActive = varDef;

		peCopy.pathDecisions = new LinkedHashMap<CfgNode, Integer>();
		for (Map.Entry<CfgNode, Integer> e : this.pathDecisions.entrySet())
			peCopy.pathDecisions.put(e.getKey(), new Integer(e.getValue()));

		peCopy.pathAnalysis = PathDecision.dup(this.pathAnalysis);

		/*
		 * peCopy.loopDetection = this.loopDetection; peCopy.shunnedPaths =
		 * this.shunnedPaths;
		 * 
		 * if(this.loopDetection != null) for(Map.Entry<CfgNodeIf,
		 * CopyOnWriteArrayList<CfgNode>> e : this.loopDetection.entrySet()) {
		 * D.pv(D.D_TEMP, "\t\t loop detection aid"); D.pv(D.D_TEMP,
		 * "\t\t\t if node - " + e.getKey()); D.pv(D.D_TEMP,
		 * "\t\t\t\t coverage - " + e.getValue()); }
		 */
		// peCopy.paths = pathsCopy;
		if (this.path != null) {
			ControlFlow pathCopy = ControlFlow.deepCopy(this.path);
			peCopy.path = pathCopy;
		}

		peCopy.exitNode = this.exitNode;
		peCopy.bSeenExit = false;
		peCopy.nearestExitConditionalNode = this.nearestExitConditionalNode;
		peCopy.nearestExitPathId = this.nearestExitPathId;
		// peCopy.bSeenExit = this.bSeenExit;
		// peCopy.nodesCovered = PathEnumerator.nodesCovered;
		peCopy.dependsNodes = this.dependsNodes;

		if (this.loopIterStopsAt != null) {
			peCopy.loopIterStopsAt = new ArrayList<CfgNode>();
			peCopy.loopIterStopsAt.addAll(this.loopIterStopsAt);
		}
		peCopy.nodesBetween = this.nodesBetween;
		peCopy.sink = this.sink;
		peCopy.invokeOrder = this.invokeOrder;
		
		return peCopy;
	}

	public void setFuncCfg(Cfg funcCfg) {
		this.funcCfg = funcCfg;
	}

	public Cfg getFuncCfg() {
		return funcCfg;
	}

	public void setDependsNodes(ArrayList<CfgNode> dependsNodes) {
		this.dependsNodes = dependsNodes;
	}

	public ArrayList<CfgNode> getDependsNodes() {
		return dependsNodes;
	}

	/*
	 * @return: helper method to enumerate intra procedural paths 
	 */
	public ArrayList<ControlFlow> enumerate() throws SISLException {
		
		flows = new ArrayList<ControlFlow>();

		TimeProfiler.ms();
		// ---- detect if the dependent nodes contain an intra procedural direct recursion
		for(CfgNode cn : this.dependsNodes)
		{
			if(cn instanceof CfgNodeCall)
			{
				TacFunction tfCurrent = Cfg.getFunction(cn);
				CfgNodeCall cnc = (CfgNodeCall) cn;
				// and if invocation is again to the function we are trying to 
				// enumerate paths in....bail out
				if(cnc.getCallee().equals(tfCurrent))
				{
					TacFunction host = Cfg.getFunction(cn);
					String loc = host.getFileName() + " : " + host.getName();
					String msg = "( Intraproc flow enumeration failed for method - ( "
							+ loc;
					msg += " \n\t\t stmts in consideration - "
							+ CfgNode.getProgLines(this.dependsNodes);
					msg += " ) ";

					ToolBug.add_INTRAPROC_DIRECT_RECURSION(this.invokeOrder, this.path,
							this.getDependsNodes(), msg);
					throw new SISLException(SISLException.BAIL_OUT, msg);
				}
			}
		}
		TimeProfiler.me(D.D_INTRA, 4);

		TimeProfiler.ms();

		D.pr(D.D_TEMP, " Transition_Look: Dependency nodes in function - "
				+ Cfg.getFunction(this.dependsNodes.get(0)).getName() + 
				" sink is - " + this.sink);
		
		// for(CfgNode cn : this.dependsNodes)
		// D.pv(D.D_TEMP, "\t\t node - " + cn);
		D.pr(D.D_TEMP, " \t \t " + CfgNode.getProgLines(this.dependsNodes));

		// determine where in this CFG we need to stop
		this.findExitNode();
		
		// avoid processing nodes until the first dependency node
		// otherwise hard to scale to large apps
		this.findEntryNode();

		// detect if its a straight line code.
		boolean bIsSingle = detectStraightLine();
		if (bIsSingle) {
			D.pv(D.D_TEMP, " detected a simple straight line dependency...");
			D.pv(D.D_TEMP, " returning this intra flow - " + flows);
			return new ArrayList<ControlFlow>(flows);
		}

		// mark the closest exit, for terminating path enumeration
		// this.markClosestExitConditional();

		// detect if exit node is on a conditional loop.
		// and if so forbid the alternate path.
		this.processExitNodeLocation();

		// check and set the exit line no..
		// if its the tail of cfg, unfortunately, the line no is same as entry
		// point
		// we need this info correctly, to reduce enumeration of unnecessary
		// paths.
		this.adjustExitLineNo();

		D.pr(" \t optimizing path selection.....");
		// mark the paths that don't contribute to dependency nodes
		// this is to eliminate processing of nodes that are not relevant
		// this is where the forbidden successors are coming from 
		this.optimizeProcessing();
		TimeProfiler.me(D.D_INTRA, 5);

		TimeProfiler.ms();

		D.pr(D.D_TEMP, "Entry node - " + this.entryNode);
		D.pr(D.D_TEMP, "Exit node - " + this.exitNode);
		String name = this.entryNode.getEnclosingFunction().getName();
		D.pr(D.D_TEMP, " Starting paths enumeration for function - " + name);

		D.pr("intra procedural flow for function - " + name
				+ " started - nodes  "
				+ CfgNode.getProgLines(this.dependsNodes));
		this.stepCounter = 0;
		
		// recursively visit from entryNode until we have found 
		// all possible flows reaching the exist point 
		visitRecurs(this.entryNode);
		TimeProfiler.me(D.D_INTRA, 6);

		TimeProfiler.ms();

		if (flows.size() == 0) {
			D
					.pr(D.D_TEMP,
							" returned 0 flows....forcing all dep nodes to be a control flow");
			ControlFlow cf = new ControlFlow();
			cf.addAll(this.dependsNodes);
			this.flows.add(cf);
		}

		D.pr("intra procedural flows in function - " + name + " in steps - "
				+ this.stepCounter);
		for (ControlFlow cf : flows) {
			D.pr("\t  " + name + " - " + cf);
			cf.setNodesBetween(this.nodesBetween);
		}
		
		PathEnumerator.flows = removeDups(PathEnumerator.flows);
		D.pr("Transition_Look4: after dep removal intra procedural flows in function - " + name
				+ " in steps - " + this.stepCounter);
		for (ControlFlow cf : flows) {
			D.pr("\t  " + name + " - " + cf);
			// cf.setNodesBetween(this.nodesBetween);
		}

		//ArrayList<ControlFlow> flowsToRet = new ArrayList<ControlFlow>(flows);
		TimeProfiler.me(D.D_INTRA, 7);

		return flows;
	}

	private void nullifyMultipleExits() {
		D.pr(D.D_TEMP, " Dependency nodes in function - "
				+ Cfg.getFunction(this.dependsNodes.get(0)).getName() + 
				" sink is - " + this.sink);
		
		for(CfgNode cn : this.dependsNodes)
		{
			D.pr(D.D_TEMP, "\t\t node - " + cn);
			String str = cn.toString();
			if(str.contains("sql3 = \"INSERT INTO sessions"))
			{
				D.pr(D.D_TEMP, " Now remove....");
				
			}
		}
		D.pr(D.D_TEMP, " \t \t " + CfgNode.getProgLines(this.dependsNodes));

	}

	private ArrayList<ControlFlow> removeDups(
			ArrayList<ControlFlow> flows) {
		
		ArrayList<ControlFlow> newFlows = new ArrayList<ControlFlow>();
		ArrayList<ControlFlow> processed = new ArrayList<ControlFlow>();
		
		for (ControlFlow cf : flows) {
			if(processed.contains(cf))
				continue;
			
			processed.add(cf);
			
			// see if remaining ControlFlow are replica of cf;
			for (ControlFlow cfRest : flows) {
				if(cfRest == cf || processed.contains(cfRest))
					continue; 
				
				boolean bDup = ControlFlow.compareForSameNodes(cf, cfRest);
				if (bDup) {
					processed.add(cfRest);
					D.pv(D.D_TEMP, " Removed duplicate path \n " + cfRest);
				} else {
					D.pv(D.D_TEMP, " not dups - path - \n " + cfRest);
				}
			}

			cf.setIntraExit(this.exitNode);
			newFlows.add(cf);
		}

		return newFlows;
	}

	private void adjustExitLineNo() {

		int iMax = -1;
		CfgNode exitNode = null;
		for (CfgNode cn : this.dependsNodes) {
			int iLine = cn.getOrigLineno();
			if (iLine > iMax) {
				iMax = iLine;
				exitNode = cn;
			}
		}

		D.pv(D.D_TEMP, " highest line cfg node in this graph - (" + iMax + ") "
				+ exitNode);
		// if(!this.exitNode.equals(exitNode))
		{
			this.iExitLineNo = iMax;
			// this.exitNode = exitNode;
		}
	}

	private CfgNode findMinLineNoNode() {

		int iMin = Integer.MAX_VALUE;
		CfgNode node = null;
		for (CfgNode cn : this.dependsNodes) {
			int iLine = cn.getOrigLineno();
			if ((iLine >= 0) && iLine < iMin) {
				iMin = iLine;
				node = cn;
			}
		}

		D.pv(D.D_TEMP, " min line cfg node in this graph - (" + iMin + ") "
				+ node);
		return node;
	}

	// this method marks nodes that should be only visited (not processed)
	// also, this process identifies non-contributing loops and eliminates
	// enumeration of their paths...
	// non loops are currently not handled...so will cause creation of more
	// paths...which will be removed while detecting duplicates
	// at least not the infinite loops
	// test, if need be optimize further to skip such nodes altogether
	private void optimizeProcessing() {
		TacFunction tf = Cfg.getFunction(this.entryNode);
		D.pv(D.D_TEMP, " !!!! Optimizing path computation in function - "
				+ tf.getName());
		D.pv(D.D_TEMP, " Entry node " + this.entryNode + " exit node - "
				+ this.exitNode);
		D.pv(D.D_TEMP, " \t " + CfgNode.getProgLines(this.dependsNodes));
		// for(CfgNode c : this.dependsNodes)
		// {
		// D.pv(D.D_TEMP, " \t \t \t \t " + c);
		// }

		D.pr(D.D_TEMP, "........\t\t getting nodes in between...." + this.entryNode
				+ " and  - " + this.exitNode);
		this.nodesBetween = getNodesBetween(this.entryNode, this.exitNode);

		for (CfgNode cn : nodesBetween) {
			if (!(cn instanceof CfgNodeIf))
				continue;
			CfgNodeIf cni = (CfgNodeIf) cn;
			D.pv(D.D_TEMP, " if node - " + cn);
			// D.pv(D.D_TEMP, " \t dominance frontier - " +
			// cn.getDominanceFrontier());

			// forbid traversal of loops that dont contain any dependency nodes
			ArrayList<CfgNode> cniLoop = cni.getLoopBody();
			if (cniLoop != null) {
				ArrayList<CfgNode> loop = new ArrayList<CfgNode>();
				loop.addAll(cniLoop);
				loop.retainAll(this.dependsNodes);
				D.pv(D.D_TEMP, " this if condition encloses a loop - ");
				if (loop.size() != 0) {
					D.pv(D.D_TEMP, " which contains depend nodes-  " + loop);
					this.pathAnalysis.add(new PathDecision(cni, false, false));
				} else {
					D
							.pv(D.D_TEMP,
									" which does not contain any depend nodes....forbid it..");
					this.pathAnalysis.add(new PathDecision(cni, true, false));
				}
			}
		}

		// now process rest of them
		for (CfgNode cn : this.nodesBetween) {
			if (!(cn instanceof CfgNodeIf))
				continue;

			CfgNodeIf cni = (CfgNodeIf) cn;

			if (cni.getLoopBody() != null)
				continue;

			D.pv(D.D_TEMP, " Conditional node - " + cni
					+ " does not contain a loop");

			// check to see if this else corresponds to a switch
			// if(cni.isSwitchElse())
			// bElseDepends = true;

			CfgNode cnElse = cni.getOutEdge(0).getDest();
			if (cnElse instanceof CfgNodeBasicBlock)
				cnElse = ((CfgNodeBasicBlock) cnElse).getContainedNodes()
						.get(0);

			CfgNode cnThen = cni.getOutEdge(1).getDest();
			if (cnThen instanceof CfgNodeBasicBlock)
				cnThen = ((CfgNodeBasicBlock) cnThen).getContainedNodes()
						.get(0);

			ArrayList<CfgNode> thenReachable = new ArrayList<CfgNode>();
			thenReachable.add(cni);
			reachable(cnThen, this.entryNode, this.exitNode, thenReachable);
			expandBasicBlocks(thenReachable);

			ArrayList<CfgNode> elseReachable = new ArrayList<CfgNode>();
			elseReachable.add(cni);
			reachable(cnElse, this.entryNode, this.exitNode, elseReachable);
			expandBasicBlocks(elseReachable);

			/*
			 * D.pv(D.D_TEMP, " \t then branch : " + cnThen + " reachable : " +
			 * CfgNode.getProgLines(thenReachable) + " \n \t \t all - " +
			 * thenReachable); D.pv(D.D_TEMP, " \t else branch : " + cnElse +
			 * " reachable : " + CfgNode.getProgLines(elseReachable) +
			 * " \n \t \t all - " + elseReachable);
			 */
			int iThen = thenReachable.size();
			int iElse = elseReachable.size();

			thenReachable.retainAll(this.dependsNodes);
			elseReachable.retainAll(this.dependsNodes);

			boolean bSame = thenReachable.containsAll(elseReachable)
					&& elseReachable.containsAll(thenReachable);

			int iThenSees = thenReachable.size();
			int iElseSees = elseReachable.size();

			/*
			 * D.pv(D.D_TEMP, " \t then branch : contains dep nodes - " +
			 * iThenSees + " " + CfgNode.getProgLines(thenReachable) );
			 * D.pv(D.D_TEMP, " \t \t total reachable - " + iThen);
			 * D.pv(D.D_TEMP, " \t else branch : contains dep nodes - " +
			 * iElseSees + " " + CfgNode.getProgLines(elseReachable) );
			 * D.pv(D.D_TEMP, " \t \t total reachable - " + iElse);
			 */

			// if both branches see same number of dependency nodes,
			// shun the path that has higher number of nodes...
			// it may correspond to a branch body, that does not
			// contribute to the query computation
			if (iThenSees == iElseSees && bSame) {
				boolean bThenForbid = (iThen > iElse);
				this.pathAnalysis.add(new PathDecision(cni, bThenForbid,
						!bThenForbid));

				/*
				 * D.pv(D.D_TEMP, " \t #### Forbidden : " + (bThenForbid?
				 * " Then " : " Else ") + " then and else see same dep nodes - "
				 * + CfgNode.getProgLines(thenReachable));
				 */
			} else if (iThenSees != iElseSees) {
				// they see different number of dependency nodes
				if (iThenSees == 0) {
					// then never sees a dependency node...may correspond to
					// exits or returns
					// forbid then
					D
							.pv(D.D_TEMP,
									"\t ##### Forbidden  : Then (0) dep nodes - exit path? ");
					this.pathAnalysis.add(new PathDecision(cni, true, false));
				} else if (iElseSees == 0) {
					// else never sees a dependency node...may correspond to
					// exits or returns
					// forbid else
					D
							.pv(D.D_TEMP,
									"\t ##### Forbidden  : Else (0) dep nodes - exit path? ");
					this.pathAnalysis.add(new PathDecision(cni, false, true));
				} else {
					// if both are non-zero and see different number of
					// dependency nodes,
					// leave both paths unforbidden.
					this.pathAnalysis.add(new PathDecision(cni, false, false));
				}
			} else {
				// visit if cannot decide
				this.pathAnalysis.add(new PathDecision(cni, false, false));
			}
		}
		/*
		 * D.pr(D.D_TEMP,
		 * " printing the path analysis outcome --------------- "); // so we
		 * know what to avoid now and where contributing loops are ---- check
		 * for(CfgNode cn : nodesBetween) { if(cn instanceof CfgNodeIf) { int
		 * iForbid = PathDecision.getForbiddenStatus(this.pathAnalysis, cn);
		 * D.pr(D.D_TEMP, "\t\t if stmt - -- " + cn + " forbid status - " +
		 * PathEnumerator.getStatus(iForbid));
		 * 
		 * ArrayList<CfgNode> cniLoop = cn.getLoopBody(); if(cniLoop != null &&
		 * iForbid != PathEnumerator.THEN_PATH) { D.pr(D.D_TEMP,
		 * "\t\t\t ---- loop iteration --- "); } } }
		 */

	}

	private void expandBasicBlocks(ArrayList<CfgNode> reachable) {
		ArrayList<CfgNode> toRemove = new ArrayList<CfgNode>();

		boolean change = true;
		while (change) {
			change = false;
			toRemove.clear();

			for (CfgNode cn : reachable) {
				if (cn instanceof CfgNodeBasicBlock) {
					change = true;
					toRemove.add(cn);
				}
			}

			for (CfgNode cn : toRemove) {
				reachable.remove(cn);
				for (CfgNode inBlock : ((CfgNodeBasicBlock) cn)
						.getContainedNodes()) {
					if (!reachable.contains(inBlock))
						reachable.add(inBlock);
				}
			}
		}
	}

	private ArrayList<CfgNode> getNodesBetween(CfgNode entry, CfgNode exit) {
		ArrayList<CfgNode> visited = new ArrayList<CfgNode>();

		visited = reachable(entry, entry, exit, visited);
		expandBasicBlocks(visited);

		/*
		 * D.pv(D.D_TEMP, " all nodes between entry " + entry + " and exit  " +
		 * exit ); D.pv(D.D_TEMP, " \t " + CfgNode.getProgLinesSorted(visited));
		 * for(CfgNode cn : visited) { D.pv(D.D_TEMP, " \t\t " + cn); }
		 */

		return visited;
	}

	ArrayList<CfgNode> reachable(CfgNode from, CfgNode entry, CfgNode exit,
			ArrayList<CfgNode> visited) {
		String prefix = "###### \t \t ";
		// D.pv(D.D_TEMP, prefix + " computing the nodes reachable from node " +
		// from);
		int iForbid = PathEnumerator.NO_PATH;

		// following is unreliable...use explicit test for equality
		boolean bVisited = visited.contains(from);
		// boolean bVisited = CfgNode.containsX(visited, from);
		if (!bVisited) {
			if (from.equals(exit)) {
				// D.pv(D.D_TEMP, prefix + " found the exit node " + exit);
				visited.add(exit);
				return visited;
			}

			int iEntryLineNo = entry.getOrigLineno();
			int iNodeLineNo = from.getOrigLineno();
			// int iExitLineNo = exit.getOrigLineno();
			// stop swaying away from the sandwich given by entry and exit nodes
			// may be this should be min and max lines in the depend nodes
			/*
			 * D.pv(D.D_TEMP, prefix + " \t Current line = " + iNodeLineNo +
			 * " entry line = " + iEntryLineNo + " exit line = " +
			 * this.iExitLineNo);
			 */
			if (!(iNodeLineNo < 0) && !(this.iExitLineNo < 0)
					&& (iNodeLineNo > this.iExitLineNo))
				return visited;

			if (!(iNodeLineNo < 0) && !(iEntryLineNo < 0)
					&& (iNodeLineNo < iEntryLineNo))
				return visited;

			visited.add(from);

			if (from instanceof CfgNodeIf) {
				iForbid = PathDecision.getForbiddenStatus(this.pathAnalysis,
						from);
				/*
				 * D.pv(D.D_TEMP, prefix + " if stmt - -- " + from +
				 * " forbid status - " + PathEnumerator.getStatus(iForbid));
				 */
				CfgNodeIf cni = (CfgNodeIf) from;
				CfgNode thenB = cni.getThen();
				CfgNode elseB = cni.getElse();

				if (iForbid != PathEnumerator.THEN_PATH
						&& iForbid != PathEnumerator.ALL_PATH)
					reachable(thenB, entry, exit, visited);

				if (iForbid != PathEnumerator.ELSE_PATH
						&& iForbid != PathEnumerator.ALL_PATH)
					reachable(elseB, entry, exit, visited);
			} else {
				List<CfgNode> succ = from.getSuccessorsNoBlocks();
				// D.pv(D.D_TEMP, "\t\t out edges - " + succ.size() + " and -- "
				// + succ);
				if (succ.size() == 1) {
					// D.pv(D.D_TEMP, "\t\t\t start - " + succ.get(0));
					reachable(succ.get(0), entry, exit, visited);
				} else {
					if (succ.size() != 0)
						throw new Error(" Successor size is " + succ.size()
								+ succ);
				}
			}
		}
		return visited;
	}

	// private ArrayList<ControlFlow> getPathsBetween(CfgNode entry,
	// CfgNode exit) {
	//		
	// // ArrayList<ControlFlow> paths = new ArrayList<ControlFlow>();
	// //
	// // paths = paths(entry, exit, paths);
	// //
	// // D.pv(D.D_TEMP, " all paths between entry " + entry + " and exit  " +
	// exit);
	// // for(CfgNode cn : paths)
	// // {
	// // D.pv(D.D_TEMP, " \t\t " + cn);
	// // }
	// //
	// // return paths;
	// }
	//
	// helper function for size();
	// recursively calculates the number of successor nodes
	// ArrayList<ControlFlow> paths(CfgNode node, CfgNode exit,
	// ArrayList<ControlFlow> paths)
	// {
	// // D.pv(D.D_TEMP, " computing the paths from node " + node + " exit - " +
	// exit);
	// // int iForbid = PathEnumerator.NO_PATH;
	// //
	// // boolean bVisited = visited.contains(node);
	// // if (!bVisited)
	// // {
	// // D.pv(D.D_TEMP, " current node - " + node + " exit node = " + exit +
	// " equals = " + node.equals(exit));
	// // if(node.equals(exit))
	// // {
	// // D.pv(D.D_TEMP, " found the exit node " + exit);
	// // visited.add(exit);
	// // return visited;
	// // }
	// //
	// // visited.add(node);
	// //
	// // if(node instanceof CfgNodeIf)
	// // {
	// // iForbid = PathDecision.getForbiddenStatus(this.pathAnalysis, node);
	// // D.pv(D.D_TEMP, "\t\t if stmt - -- " + node + " forbid status - " +
	// // PathEnumerator.getStatus(iForbid));
	// //
	// // CfgNodeIf cni = (CfgNodeIf)node;
	// // CfgNode thenB = cni.getThen();
	// // CfgNode elseB = cni.getElse();
	// //
	// // if(iForbid != PathEnumerator.THEN_PATH)
	// // size(thenB, exit, visited);
	// //
	// // if(iForbid != PathEnumerator.ELSE_PATH)
	// // size(elseB, exit, visited);
	// // }
	// // else
	// // {
	// // List<CfgNode> succ = node.getSuccessorsNoBlocks();
	// // D.pv(D.D_TEMP, "\t\t out edges - " + succ.size() + " and -- " + succ);
	// // if(succ.size() == 1)
	// // {
	// // D.pv(D.D_TEMP, "\t\t\t start - " + succ.get(0));
	// // size(succ.get(0), exit, visited);
	// // }
	// // }
	// // }
	// // return visited;
	// }

	private int iExitLineNo;

	private void processExitNodeLocation() {

		// detect if the exit node is the last node
		// it will not have any successors
		CfgNode cnTail = this.funcCfg.getTail();
		if (this.exitNode.equals(cnTail)) {
			D.pv(D.D_TEMP,
					" tail of the cfg is the exit node for dep nodes ... "
							+ cnTail);
			return;
		}

		List<CfgNode> exitSuccs = this.exitNode.getSuccessorsNoBlocks();
		if (exitSuccs.size() != 1)
			throw new Error(
					" currently tool doesnt handle multiple successor nodes for dep graph exit nodes");

		// exit node if does not dominate its basic block's successor
		// means, exit node is on a conditional and we should shun
		// the alternate path for collecting only nodes between entry
		// and exit
		CfgNode exitSucc = exitSuccs.get(0);
		if (exitSucc != null) {
			List<CfgNode> exitDoms = this.exitNode.getImdDominated();
			boolean bDom = exitDoms.contains(exitSucc);
			D.pv(D.D_TEMP, " succ of exit node " + exitSucc
					+ " dominated by exit node " + bDom);
			if (!bDom) {
				List<CfgNode> parent = this.exitNode.getPredecessors();
				if (parent.size() != 1) {
					D.pv(D.D_TEMP, " for exit node - " + this.exitNode
							+ " found - " + parent.size() + " parents");
					for (CfgNode par : parent) {
						D.pv(D.D_TEMP, " \t - parent " + par);
					}
					return;
					// throw new Error(" handle " + parent + " of exit node - "
					// + this.exitNode);
				}
				CfgNode tmp = parent.get(0);
				List<CfgNode> tmpDom = tmp.getDominates();
				while (tmpDom != null) {
					if (tmpDom.contains(exitSucc)) {
						D.pv(D.D_TEMP,
								" found parent that dominates the succ of exit - "
										+ tmp);
						break;
					} else {
						if (tmpDom.size() == 1)
							tmp = tmpDom.get(0);
						else {
							D.pf(D.D_TEMP,
									" couldnt find the parent of exit node - "
											+ this.exitNode);
							return;
						}
					}
					tmpDom = tmp.getDominates();
				}

				D.pv(D.D_TEMP, " parent of exit node - " + this.exitNode
						+ " is - " + tmp);
				if (!(tmp instanceof CfgNodeIf))
					throw new Error(
							" parent node of exit is not a conditional - "
									+ tmp);

				CfgNodeIf cni = (CfgNodeIf) tmp;
				CfgNode cniThen = cni.getThen();
				CfgNode cniElse = cni.getElse();

				List<CfgNode> thenBlock = new ArrayList<CfgNode>();
				thenBlock.add(cniThen);

				List<CfgNode> elseBlock = new ArrayList<CfgNode>();
				elseBlock.add(cniElse);

				if (cniThen instanceof CfgNodeBasicBlock)
					thenBlock = ((CfgNodeBasicBlock) cniThen)
							.getContainedNodes();

				if (cniElse instanceof CfgNodeBasicBlock)
					elseBlock = ((CfgNodeBasicBlock) cniElse)
							.getContainedNodes();

				D.pv(D.D_TEMP, " then branch - " + thenBlock);
				D.pv(D.D_TEMP, " else branch - " + elseBlock);
				boolean bElse = true;
				if (thenBlock != null && thenBlock.contains(this.exitNode)) {
					bElse = false;
					D.pv(D.D_TEMP,
							" exit node is in the then branch of parent - "
									+ tmp + " branch - " + thenBlock);
				}

				if (elseBlock != null && elseBlock.contains(this.exitNode)) {
					bElse = true;
					D.pv(D.D_TEMP,
							" exit node is in the else branch of parent - "
									+ tmp + " branch " + elseBlock);
				}

				if (bElse) {
					//
					this.pathAnalysis.add(new PathDecision(cni, true, false));
					D.pv(D.D_TEMP, " forbid traversal of then branch");
				} else {
					this.pathAnalysis.add(new PathDecision(cni, false, true));
					D.pv(D.D_TEMP, " forbid traversal of else branch");
				}
			}
		}
		// ?

		CfgNode cn = this.exitNode;
		// while(true)
		{
			CfgNodeBasicBlock cnbb = cn.getEnclosingBasicBlock();
			if (cnbb == null) {

			}
		}

	}

	private void findEntryNode() {

		String fName = Cfg.getFunction(this.funcCfg.getHead()).getName();
		D.pr(D.D_TEMP, " Determining entry point for function " + fName
				+ ".... Nodes in dependency - \n");
		for (CfgNode cn : this.dependsNodes) {
			D.pr(D.D_TEMP, "\t\t  " + cn);
		}
		for (CfgNode cn : this.dependsNodes) {
			List<CfgNode> dominates = cn.getDominates();
			ArrayList<CfgNode> dom = new ArrayList<CfgNode>();
			dom.addAll(dominates);
			dom.add(cn);
			if (dom.containsAll(this.dependsNodes)) {
				D.pr(D.D_TEMP, " entry node for function - " + fName + " is - "
						+ cn);
				//this.setEntryNode(cn);
				this.entryNode = cn;
				return;
			}

		}

		// if entry node is yet not decided - not in the same block
		// find the minimum line statement in the set of dependent
		// nodes and check if it reaches all dependent nodes
		CfgNode cnMin = findMinLineNoNode();
		this.entryNode = cnMin;

		
		ArrayList<CfgNode> visited = new ArrayList<CfgNode>();
		this.reachable(cnMin, cnMin, this.exitNode, visited);
		if (visited.containsAll(this.dependsNodes)) {
			this.entryNode = cnMin;
		} else {
			// if we could not determine the entry point
			// make the entry node as entry point
			D.pf(D.D_TEMP,
					" failed to determine the entry point of dependencies for method "
							+ fName + " defaulting to head of cfg");
			this.setEntryNode(this.funcCfg.getHead());
			// throw new Error(" could not determine entry point for method - "
			// + fName);
		}//*/
	}

	private boolean detectStraightLine() {
		boolean bStraightLine = false;
		String fName = Cfg.getFunction(this.funcCfg.getHead()).getName();

		D.pv(D.D_TEMP, " aiding the path enumeration for function " + fName
				+ "....\n");
		// if entry and exit node are in the same basic block
		// then it is straight line
		CfgNodeBasicBlock entryBlock = this.entryNode.getEnclosingBasicBlock();
		CfgNodeBasicBlock exitBlock = this.exitNode.getEnclosingBasicBlock();
		D.pv(D.D_TEMP, " entry node - " + this.entryNode);
		D.pv(D.D_TEMP, " exit node - " + this.exitNode);
		D.pv(D.D_TEMP, "\t entry block - " + entryBlock);
		D.pv(D.D_TEMP, "\t exit block - " + exitBlock);
		if ((entryBlock != null) && (exitBlock != null)
				& entryBlock.equals(exitBlock))
			bStraightLine = true;
		/*
		 * for(CfgNode cn : dep) { int iDominate = cn.getImdDominated().size();
		 * if(iDominate != 1) { bStraightLine = false; break; } }
		 */
		D.pv(D.D_TEMP, " is straight line - " + bStraightLine);
		if (bStraightLine) {
			// all depend nodes are on the same ctrl path..
			this.path.addAll(this.dependsNodes);
			PathEnumerator.flows.add(this.path);
		}

		return bStraightLine;
	}

	/*
	 * // it is used to terminate the path finding algorithm // all path
	 * computation take nearest exit if all dependency // nodes have been
	 * covered private void markClosestExitConditional() { if(this.exitNode ==
	 * null) throw new Error(" exit node has not been set !!!! ");
	 * 
	 * CfgNode cn = exitNode; CopyOnWriteArrayList<CfgNode> preds = new
	 * CopyOnWriteArrayList<CfgNode>(); List<CfgNode> tm = cn.getPredecessors();
	 * if(tm != null) preds.addAll(tm);
	 * 
	 * CfgNode last = exitNode; while(preds.size() != 0) { CfgNode pred =
	 * preds.remove(0); D.pv(D.D_TEMP,
	 * " determining exit point branch : last - " + last + " pred - " + pred);
	 * 
	 * if(pred instanceof CfgNodeIf) { this.nearestExitConditionalNode =
	 * (CfgNodeIf) pred;
	 * 
	 * CfgNodeIf cni = (CfgNodeIf) pred;
	 * 
	 * List<CfgNode> elsep = cni.getElse().getSuccessors(); ArrayList<CfgNode>
	 * elseB = new ArrayList<CfgNode>(); elseB.addAll(elsep);
	 * elseB.add(cni.getElse());
	 * 
	 * D.pv(D.D_TEMP, " else block " + elseB + " and just else - " +
	 * cni.getElse()); if(elseB != null && elseB.contains(last))
	 * this.nearestExitPathId = PathEnumerator.ELSE_PATH; else
	 * this.nearestExitPathId = PathEnumerator.THEN_PATH; break; } else { tm =
	 * pred.getPredecessors(); if(tm != null) preds.addAll(tm); } last = pred; }
	 * 
	 * D.pd(D.D_TEMP, " nearest exit point for function " +
	 * Cfg.getFunction(this.funcCfg.getHead()).getName() + " = " +
	 * this.nearestExitConditionalNode + " take path = " +
	 * ((this.nearestExitPathId == ELSE_PATH)? " ELSE " : " THEN ")); }
	 */
	// private void visitRecurs(CfgNode cn)
	// {
	//		
	// D.pv(D.D_TEMP, " Processing node - " + cn);
	// processNode(cn);
	//		
	// // process sucessors;
	// processSuccessors(cn, cn.getSuccessors());
	// }
	//
	// private void processSuccessors(CfgNode node, List<CfgNode> successors)
	// {
	// if(successors == null || successors.size() == 0)
	// return;
	//		
	// int iSize = successors.size();
	//		
	// if(iSize == 1)
	// {
	// visitRecurs(successors.get(0));
	// return;
	// }
	//		
	// if(iSize > 2)
	// throw new Error(" cannot handle more than 2 successors  " + node);
	//		
	// if(node instanceof CfgNodeIf)
	// {
	// CfgNodeIf cni = (CfgNodeIf) node;
	// // enable useless loop detection
	// // if this node is revisited and the state of coveredNodes does not
	// change
	// // shun the last path.
	// int iShun = getShunnedPath(cni);
	// if(iShun == NO_PATH) skipNonContributingLoops(cni);
	//
	// if(this.nearestExitConditionalNode != null &&
	// node.equals(this.nearestExitConditionalNode))
	// {
	// D.pd(D.D_TEMP, " Found exit conditional new conditional encountered... "
	// + node);
	// //
	// // if(isDepNodesCovererd() && this.shunnedPaths.size() != 0)
	// if(isDepNodesCovererd())
	// {
	// D.pd(D.D_TEMP, "All dependency nodes have been covered take the exit!!! "
	// + node);
	// if(this.nearestExitPathId == ELSE_PATH)
	// this.visitRecurs(this.nearestExitConditionalNode.getElse());
	// else
	// this.visitRecurs(this.nearestExitConditionalNode.getThen());
	// return;
	// }
	//
	// D.pd(D.D_TEMP, " dependency nodes have yet not been covered");
	// // a new conditional encountered
	// PathEnumerator peSucc1 = this.deepCopy();
	// PathEnumerator peSucc2 = this.deepCopy();
	//				
	// peSucc1.setSeenExit(true);
	// peSucc2.setSeenExit(true);
	//
	// peSucc1.setPrevPath(cni, THEN_PATH);
	// peSucc2.setPrevPath(cni, ELSE_PATH);
	//
	// D.pd(D.D_TEMP, " starting traversal of branches");
	// if(this.nearestExitPathId == PathEnumerator.ELSE_PATH)
	// {
	// D.pd(D.D_TEMP,
	// " else branch exits, to provide coverage of dependency node..visit else first");
	// if(iShun != ELSE_PATH)
	// peSucc2.visitRecurs(cni.getElse());
	// if(iShun != THEN_PATH)
	// peSucc1.visitRecurs(cni.getThen());
	// }
	// else
	// {
	// D.pd(D.D_TEMP,
	// " then branch exits, to provide coverage of dependency node..visit then first");
	// if(iShun != THEN_PATH)
	// peSucc1.visitRecurs(cni.getThen());
	// if(iShun != ELSE_PATH)
	// peSucc2.visitRecurs(cni.getElse());
	// }
	// return;
	// }
	// else
	// {
	// D.pd(D.D_TEMP, " found a non-exit conditional node - " + node);
	// int iPrevPath = this.getPrevPath(cni);
	//				
	// switch(iPrevPath)
	// {
	// // ELSE (false) path taken
	// case ELSE_PATH:
	// if(this.bSeenExit)
	// {
	// // abort
	// D.pd(D.D_TEMP, " node - " + cni +
	// " has seen exit and took else path earlier...set path null");
	// this.path = null;
	// return;
	// }
	// else
	// {
	// // visit the alternate path;
	// D.pd(D.D_TEMP, " node - " + cni +
	// " did not see exit & took else path earlier...going to then path");
	// this.setPrevPath(cni, THEN_PATH);
	// if(iShun != THEN_PATH)
	// this.visitRecurs(cni.getThen());
	//						
	// }
	// // take true path if
	// break;
	//					
	// // true path taken
	// case THEN_PATH:
	// if(this.bSeenExit)
	// {
	// // abort
	// D.pd(D.D_TEMP, " node - " + cni +
	// " has seen exit and took then path earlier...set path null");
	// this.path = null;
	// return;
	// }
	// else
	// {
	// // visit the alternate path;
	// D.pd(D.D_TEMP, " node - " + cni +
	// " did not see exit & took then path earlier...going to else path");
	// this.setPrevPath(cni, ELSE_PATH);
	// if(iShun != ELSE_PATH)
	// this.visitRecurs(cni.getElse());
	// }
	// break;
	//					
	// // none of the paths were taken
	// case NO_PATH:
	//			
	// D.pd(D.D_TEMP, " node - " + cni +
	// " has not yet been visited...going to both paths");
	//
	// PathEnumerator peSucc1 = this.deepCopy();
	// PathEnumerator peSucc2 = this.deepCopy();
	//					
	// //CfgNodeIf cniInPe1 = peSucc1.getSameNode(cni);
	// peSucc1.setPrevPath(cni, THEN_PATH);
	// peSucc2.setPrevPath(cni, ELSE_PATH);
	//					
	// if(iShun != THEN_PATH)
	// peSucc1.visitRecurs(cni.getThen());
	// if(iShun != ELSE_PATH)
	// peSucc2.visitRecurs(cni.getElse());
	// return;
	//				
	// default:
	// throw new Error(" unknown path taken status for node - " + cni);
	// }
	// }
	// }
	// else
	// {
	// for(CfgNode cn : successors)
	// {
	// D.pf(D.D_TEMP, " Unhandled successor - " + cn);
	// }
	// throw new Error(" unhandled successor computation for");
	//
	// }
	// }
	
	
	/*
	 * recursively enumerates paths starting at cn for given nodes
	 * @param cn: entry node in the CFG from where intra procedural path enumeration should start
	 * @return void
	 */
	private void visitRecurs(CfgNode cn) throws SISLException {
		D.pv(D.D_TEMP, " steps so far - " + this.stepCounter);
		if ((++(this.stepCounter)) >= PathEnumerator.RECURSION_UPPER_BOUND) {
			TacFunction host = Cfg.getFunction(cn);
			String loc = host.getFileName() + " : " + host.getName();
			String msg = "( Intraproc flow enumeration failed for method - ( "
					+ loc;
			msg += " \n\t\t stmts in consideration - "
					+ CfgNode.getProgLines(this.dependsNodes);
			msg += " ) ";

			ToolBug.add_INTRAPROC_DIRECT_RECURSION(this.invokeOrder, this.path,
					this.getDependsNodes(), msg);
			// throw new
			// SISLException(SISLException.INFINITE_RECURSION_INTRA_PATH_ENUM,
			// msg);
			throw new SISLException(SISLException.BAIL_OUT, "BAIL_OUT");
		}

		D.pv(D.D_TEMP, " +++++++++++++ Processing node - " + cn);
		
		// add the current node to the path being enumerated
		// since there are different types of CfgNodes, we need to find relevant node to 
		// add to the path 
		processNode(cn);

		// process its successors;
		processSuccessors(cn);
	}

	private boolean m_bInLoop;
	
	// when true, loop statements dont need to be left or right recursive
	// only constraint is that the variables used in them should not be
	// modified subsequently.
	public static boolean m_bLR_RR_Relaxed = true;
	
	/*
	 * this method analyzes successors and recurses through them to find flows 
	 * @param node whose successors must be processed
	 * 
	 */
	private void processSuccessors(CfgNode node) throws SISLException {
		D.pv(D.D_TEMP, " processing successors of node - " + node);
		if (node.equals(this.exitNode)) {
			D.pv(D.D_TEMP, " found exit node...skipping its successors");
			return;
		}

		List<CfgNode> successors = getUnforbiddenSuccessors(node);

		int iSize = successors.size();
		if (iSize == 0) {
			D.pv(D.D_TEMP, "resetting the path ----------------- ");
			this.path = null;
			return;
		}

		if (iSize == 1) {
			D.pv(D.D_TEMP, " \t for node - " + node + " visting a successor "
					+ successors.get(0));
			visitRecurs(successors.get(0));
			return;
		}

		if (iSize > 2)
			throw new Error(" cannot handle more than 2 successors  " + node);

		if (!(node instanceof CfgNodeIf)) {
			for (CfgNode cn : successors) {
				D.pf(D.D_TEMP, " Unhandled successor - " + cn);
			}
			throw new Error(" unhandled successor computation for");
		}

		// Pixy translates loops into If + jump statements. 
		// We modify the Cfg construction stage to and populate loop body 
		// with ifs that are associated with Loops rather than simple if-then-else statements
		CfgNodeIf cni = (CfgNodeIf) node;
		ArrayList<CfgNode> cniLoop = cni.getLoopBody();

		if (cniLoop != null && cniLoop.size() > 0) {
						
			// if this is the summarization of 1 loop iteration, direct it to follow
			// the else branch i.e., terminate path enumeration in this if statement 
			if (isLoopIterStops(cni)) {
				// if so, remove it
				D.pr(D.D_TEMP, "---------------- loop iteartion terminating now - " + cni);
				removeLoopIterStop(cni);
				this.m_bInLoop = false;
				this.visitRecurs(cni.getElse());
				return;
			}

			// ok the loop is being seen for the first time
			// perform following static checks - 
			// 1. the loop body does not contain any conditional 
			// 2. all statements are of the Form Q1 = Q1 . y or Q1 = y . Q1
			// for all variables in the loop body. 
			// 3. disallow any method calls 
			// ---> above restrictions enable summarization of the loop contribution
			// as y * 
			try
			{
				// check if the loop is transformable, 
				// otherwise abort
				staticCheckLoopTransformable(cni );
			}
			catch(Exception e)
			{
				if(e instanceof SISLException)
					throw (SISLException)e;
				
				e.printStackTrace();
				throw new Error(e.getMessage());
			}
			
//			if(isNestedLoop(cni)){
//				D.pr(D.D_TEMP, "Nested loop found through Exception : TODO: ");
//				throw new Error("Nested loop found........through exception instead of error");
//			}
//			
//			// if we come here and the setLoopIterStopsAt is set. 
//			// we were trying to summarize a loop and a conditional has been encountered. 
//			// summarization with conditional loop bodies is unclear. 
//			// for the time being exit. 
//			ArrayList<CfgNode> cniIterStoppers = getLoopIterStopAt();
//			if(cniIterStoppers != null && cniIterStoppers.size() > 0)
//			{
//				D.pr(D.D_TEMP, " Conditional - " + cni + " is nested within the following loops - ");
//				for(CfgNode cn : cniIterStoppers)
//				{
//					D.pr(D.D_TEMP, " \t\t " + cn);					
//				}
//				throw new Error("Conditional - " + cni + " is nested within loops ");
//			}
			
			
			
			// handle loop - 1 iteration and 0 iterations
			PathEnumerator peLoop1Iter = this.deepCopy();
			PathEnumerator peLoop0Iter = this.deepCopy();

			D.pr(D.D_TEMP,
					" ----------------------- found a new conditional that requires 1 loop iteration "
							+ cni);
			peLoop1Iter.setLoopIterStopAt(cni);
			peLoop1Iter.visitRecurs(cni.getThen());
			/* 
			 * PIPER : Dont want to do 0 iterations
			 * peLoop0Iter.visitRecurs(cni.getElse()); 
			 * */
		} else {
			// simple if-then-else...
			D.pr(D.D_TEMP,
					" starting a simple conditional if-then-else processing of both branches"
							+ cni);
//			
//			// if we come here and the setLoopIterStopsAt is set. 
//			// we were trying to summarize a loop and a conditional has been encountered. 
//			// summarization with conditional loop bodies is unclear. 
//			// for the time being exit. 
//			ArrayList<CfgNode> cniIterStoppers = getLoopIterStopAt();
//			if(cniIterStoppers != null && cniIterStoppers.size() > 0)
//			{
//				D.pr(D.D_TEMP, " Conditional - " + cni + " is nested within the following loops - ");
//				for(CfgNode cn : cniIterStoppers)
//				{
//					D.pr(D.D_TEMP, " \t\t " + cn);					
//				}
//				throw new Error("Conditional - " + cni + " is nested within loops ");
//			}
			
			// we have found multiple intra-procedural flows: 
			// make deep copies of paths so far, and recusively enumerate 
			// each alternate path 
			PathEnumerator peElse = this.deepCopy();
			PathEnumerator peThen = this.deepCopy();
			peElse.visitRecurs(cni.getElse());
			peThen.visitRecurs(cni.getThen());
		}

		/*
		 * if(this.nearestExitConditionalNode != null &&
		 * node.equals(this.nearestExitConditionalNode)) { D.pd(D.D_TEMP,
		 * " Found exit conditional new conditional encountered... " + node);
		 * 
		 * // if(isDepNodesCovererd() && this.shunnedPaths.size() != 0)
		 * if(isDepNodesCovererd()) { D.pd(D.D_TEMP,
		 * "All dependency nodes have been covered take the exit!!! " + node);
		 * if(this.nearestExitPathId == ELSE_PATH)
		 * this.visitRecurs(this.nearestExitConditionalNode.getElse()); else
		 * this.visitRecurs(this.nearestExitConditionalNode.getThen()); return;
		 * }
		 * 
		 * D.pd(D.D_TEMP, " dependency nodes have yet not been covered"); // a
		 * new conditional encountered PathEnumerator peSucc1 = this.deepCopy();
		 * PathEnumerator peSucc2 = this.deepCopy();
		 * 
		 * peSucc1.setSeenExit(true); peSucc2.setSeenExit(true);
		 * 
		 * peSucc1.setPrevPath(cni, THEN_PATH); peSucc2.setPrevPath(cni,
		 * ELSE_PATH);
		 * 
		 * D.pd(D.D_TEMP, " starting traversal of branches");
		 * if(this.nearestExitPathId == PathEnumerator.ELSE_PATH) {
		 * D.pd(D.D_TEMP,
		 * " else branch exits, to provide coverage of dependency node..visit else first"
		 * ); if(iShun != ELSE_PATH) peSucc2.visitRecurs(cni.getElse());
		 * if(iShun != THEN_PATH) peSucc1.visitRecurs(cni.getThen()); } else {
		 * D.pd(D.D_TEMP,
		 * " then branch exits, to provide coverage of dependency node..visit then first"
		 * ); if(iShun != THEN_PATH) peSucc1.visitRecurs(cni.getThen());
		 * if(iShun != ELSE_PATH) peSucc2.visitRecurs(cni.getElse()); } return;
		 * } else { D.pd(D.D_TEMP, " found a non-exit conditional node - " +
		 * node); int iPrevPath = this.getPrevPath(cni);
		 * 
		 * switch(iPrevPath) { // ELSE (false) path taken case ELSE_PATH:
		 * if(this.bSeenExit) { // abort D.pd(D.D_TEMP, " node - " + cni +
		 * " has seen exit and took else path earlier...set path null");
		 * this.path = null; return; } else { // visit the alternate path;
		 * D.pd(D.D_TEMP, " node - " + cni +
		 * " did not see exit & took else path earlier...going to then path");
		 * this.setPrevPath(cni, THEN_PATH); if(iShun != THEN_PATH)
		 * this.visitRecurs(cni.getThen());
		 * 
		 * } // take true path if break;
		 * 
		 * // true path taken case THEN_PATH: if(this.bSeenExit) { // abort
		 * D.pd(D.D_TEMP, " node - " + cni +
		 * " has seen exit and took then path earlier...set path null");
		 * this.path = null; return; } else { // visit the alternate path;
		 * D.pd(D.D_TEMP, " node - " + cni +
		 * " did not see exit & took then path earlier...going to else path");
		 * this.setPrevPath(cni, ELSE_PATH); if(iShun != ELSE_PATH)
		 * this.visitRecurs(cni.getElse()); } break;
		 * 
		 * // none of the paths were taken case NO_PATH:
		 * 
		 * D.pd(D.D_TEMP, " node - " + cni +
		 * " has not yet been visited...going to both paths");
		 * 
		 * PathEnumerator peSucc1 = this.deepCopy(); PathEnumerator peSucc2 =
		 * this.deepCopy();
		 * 
		 * //CfgNodeIf cniInPe1 = peSucc1.getSameNode(cni);
		 * peSucc1.setPrevPath(cni, THEN_PATH); peSucc2.setPrevPath(cni,
		 * ELSE_PATH);
		 * 
		 * if(iShun != THEN_PATH) peSucc1.visitRecurs(cni.getThen()); if(iShun
		 * != ELSE_PATH) peSucc2.visitRecurs(cni.getElse()); return;
		 * 
		 * default: throw new Error(" unknown path taken status for node - " +
		 * cni); } }
		 */
	}
	
	private void removeLoopIterStop(CfgNodeIf cni) {
		if (this.loopIterStopsAt == null)
			return;

		this.loopIterStopsAt.remove(cni);
	}

	private boolean isLoopIterStops(CfgNodeIf cni) {

		if (this.loopIterStopsAt == null || this.loopIterStopsAt.size() == 0)
			return false;

		for (CfgNode cn : this.loopIterStopsAt) {
			if (cn.equals(cni))
				return true;
		}

		return false;
	}

	private ArrayList<CfgNode> getUnforbiddenSuccessors(CfgNode node) {

		ArrayList<CfgNode> unforbidden = new ArrayList<CfgNode>();
		// D.pv(D.D_TEMP, " returned successors " + succ);

		if (node instanceof CfgNodeIf) {
			CfgNodeIf cni = (CfgNodeIf) node;
			int iForbid = PathDecision.getForbiddenStatus(this.pathAnalysis,
					cni);
			D.pv(D.D_TEMP, "\t\t if stmt - -- " + node + " forbid status - "
					+ PathEnumerator.getStatus(iForbid));
			switch (iForbid) {
			case PathEnumerator.ALL_PATH:
				break;
			case PathEnumerator.ELSE_PATH:
				unforbidden.add(cni.getThen());
				break;
			case PathEnumerator.THEN_PATH:
				unforbidden.add(cni.getElse());
				break;
			case PathEnumerator.NO_PATH:
				unforbidden.add(cni.getThen());
				unforbidden.add(cni.getElse());
				break;
			case PathEnumerator.NOT_INITED:
				break;
			default:
				throw new Error(" unknown status of path forbid for " + cni);
			}
		} else {
			D.pv(D.D_TEMP, " getting unforbidden successors of " + node);
			List<CfgNode> succ = node.getSuccessorsNoBlocks();
			if (succ == null || succ.size() == 0) {
				return unforbidden;
			}

			unforbidden.addAll(succ);
		}

		D.pv(D.D_TEMP, " unforbidden successors of " + node + " \t - "
				+ CfgNode.getProgLinesSorted(unforbidden));
		return unforbidden;
	}

	/*
	 * this method handles different types of CfgNode and adds relevant nodes to
	 * this.path  
	 */
	private void processNode(CfgNode cn) throws SISLException {
		
		if (cn instanceof CfgNodeBasicBlock) {
			CfgNodeBasicBlock cnbb = (CfgNodeBasicBlock) cn;
			for (CfgNode cnI : cnbb.getContainedNodes()) {
				if(this.m_bInLoop)
				{
					cnI.setInLoop();
					D.pr(D.D_TEMP, " $$$$$$$$$$$$$$$$ in loop - Node - " + cnI);
				}
				processNode(cnI);
			}
		}


		// if it contains phi nodes, update the phiToLhs map
		LinkedHashMap<Variable, ArrayList<Variable>> phis = cn.getPhiNodes();
		if (phis != null && phis.size() != 0) {
			for (Map.Entry<Variable, ArrayList<Variable>> e : phis.entrySet()) {
				D.pv(D.D_TEMP, " #### phi value for " + cn + " -> Lhs = "
						+ e.getKey() + " rhs = " + e.getValue());
				Variable vTemp = e.getKey();
				Variable vTemp1 = null;
				if (vTemp.isTemp())
					continue;
				if (vTemp.getOrig() != null)
					vTemp1 = vTemp.getOrig();

				if (vTemp1 == null)
					vTemp1 = vTemp;

				if (this.varDefActive.containsKey(vTemp1)) {
					this.phiLhsMapsTo.remove(e.getKey());
					Variable vNew = this.varDefActive.get(vTemp1);
					this.phiLhsMapsTo.put(e.getKey(), vNew);
					D.pv(D.D_TEMP, " adding a phi Lhs maps to " + e.getKey()
							+ " to " + this.varDefActive.get(vTemp1));
				}
				D.pv(D.D_TEMP, " after node - " + cn + " the phiLhs maps - ");
				for (Map.Entry<Variable, Variable> en : this.phiLhsMapsTo
						.entrySet())
					D.pv(D.D_TEMP, " \t\t\t phi Lhs " + en.getKey()
							+ " maps to - " + en.getValue());
			}
		}

		if (cn.equals(this.exitNode)) {
			D.pv(D.D_TEMP, " exit node visited ");
			if (this.path != null) {
				ArrayList<Variable> rhs = cn.getRightToRename();
				if (rhs != null && rhs.size() > 0) {
					for (Variable r : rhs) {
						if (this.phiLhsMapsTo.containsKey(r)) {
							D.pv(D.D_TEMP, " introducing new stmt - " + r
									+ " = " + this.phiLhsMapsTo.get(r));
							CfgNodeAssignUnary cnau = new CfgNodeAssignUnary(r,
									this.phiLhsMapsTo.get(r), 1, null);
							this.path.addNode(cnau);
							// cn.rightRename(r, this.phiLhsMapsTo.get(r));
						}
					}
				}

				this.path.addNode(cn);
				
				if(!PathEnumerator.nodesCovered.contains(cn))
					PathEnumerator.nodesCovered.add(cn);
				PathEnumerator.flows.add(this.path);
				this.path = null;
				D.pv(D.D_TEMP, " In PathEnumerators === control flow found - "
						+ PathEnumerator.flows.get(
								PathEnumerator.flows.size() - 1).toString());
			}
			return;
		}

		// check if in dependency
		if (!this.dependsNodes.contains(cn))
			return;

		boolean added = false;
		
		// update any new definitions and put them in varActive variable
		if (this.path != null) {
			if (this.dependsNodes.contains(cn) || cn.isAlwaysProcess())
				added = this.path.addNode(cn);
			if(!PathEnumerator.nodesCovered.contains(cn))
				PathEnumerator.nodesCovered.add(cn);
		}

		if (cn instanceof CfgNodeAssignArray
				|| cn instanceof CfgNodeAssignBinary
				|| cn instanceof CfgNodeAssignRef
				|| cn instanceof CfgNodeAssignSimple
				|| cn instanceof CfgNodeAssignUnary) {
			// assigned to
			Variable vLhs = cn.getLeft();
			if (!vLhs.isTemp()) {

				Variable vOrig = vLhs;
				if (vLhs.getOrig() != null)
					vOrig = vLhs.getOrig();

				if (this.varDefActive.containsKey(vOrig))
					this.varDefActive.remove(vOrig);

				D.pd(D.D_TEMP, " storing the active var definition " + vOrig
						+ " to " + vLhs);
				this.varDefActive.put(vOrig, vLhs);

				D.pv(D.D_TEMP, " after node - " + cn
						+ " updated the active var - ");
				for (Map.Entry<Variable, Variable> e : this.varDefActive
						.entrySet())
					D.pv(D.D_TEMP, " \t\t\t active variable def for "
							+ e.getKey() + " is - " + e.getValue());
			}
		}

		// rename any RHS values with the incoming definition of phi variables
		if (added) {
			ArrayList<Variable> rhsVars = cn.getRightToRename();
			if (rhsVars != null && rhsVars.size() > 0) {
				for (Variable rhs : rhsVars) {
					if (this.phiLhsMapsTo.containsKey(rhs)) {
						Variable v = rhs.getOrig();
						D.pv(D.D_TEMP, " renaming " + rhs + " to "
								+ this.phiLhsMapsTo.get(rhs) + " in - " + cn
								+ " orig of rhs = " + v);

						// D.pv(D.D_TEMP, " introducing new stmt - " + rhs +
						// " = " + this.phiLhsMapsTo.get(rhs));
						// / CfgNodeAssignUnary cnau = new
						// CfgNodeAssignUnary(rhs, this.phiLhsMapsTo.get(rhs),
						// 1, null);
						// this.path.addNode(cnau);

						if (!(cn instanceof CfgNodeCallPrep)
								&& !(cn instanceof CfgNodeAssignBinary)
								&& !(cn instanceof CfgNodeAssignUnary)
								&& !(cn instanceof CfgNodeAssignSimple)
								&& !(cn instanceof CfgNodeCall)
								&& !(cn instanceof CfgNodeCallBuiltin)) {

							TacFunction host = Cfg.getFunction(cn);
							String loc = host.getFileName() + " : "
									+ host.getName();
							String msg = " Tool bug : pending implementation : "
									+ "doesnt handle phi lhs mapping for - "
									+ loc + ": " + cn;
							msg += " \n\t\t stmts in consideration - "
									+ CfgNode.getProgLines(this.dependsNodes);
							msg += " ) ";

							ToolBug.add_TOOL_BUG_PHILHS_UNHANDLED(msg);
							// now bail out until, processing can be resumed
							throw new SISLException(SISLException.BAIL_OUT,
									"BAIL_OUT");
						}

						D.pv(D.D_TEMP,
								" #### adding support for replacing phi rhs arg "
										+ rhs + " with - "
										+ this.phiLhsMapsTo.get(rhs));

						rhs.setPhiLhs();
						this.path.addPhiLhsSupport(cn, rhs, this.phiLhsMapsTo
								.get(rhs));
						// cn.rightRename(rhs, this.phiLhsMapsTo.get(rhs),
						// false);
						// this.path.setLoopBody();
						// rhs.setOrig(v);
						D.pv(D.D_TEMP, " \t\t after renaming node-  " + cn
								+ " and orig of rhs = " + rhs.getOrig());
					}
				}
			}
		}
	}

	// private void processNode(CfgNode cn)
	// {
	// if(cn instanceof CfgNodeBasicBlock)
	// {
	// CfgNodeBasicBlock cnbb = (CfgNodeBasicBlock) cn;
	// for(CfgNode cnI : cnbb.getContainedNodes())
	// {
	// processNode(cnI);
	// }
	// }
	//
	// // if it contains phi nodes, update the phiToLhs map
	// LinkedHashMap<Variable, ArrayList<Variable>> phis = cn.getPhiNodes();
	// if(phis != null && phis.size() != 0)
	// {
	// for(Map.Entry<Variable, ArrayList<Variable>> e : phis.entrySet())
	// {
	// D.pv(D.D_TEMP, " #### phi value for " + cn + " -> Lhs = " +
	// e.getKey() + " rhs = " + e.getValue());
	// Variable vTemp = e.getKey();
	// Variable vTemp1 = null;
	// if(vTemp.isTemp())
	// continue;
	// if(vTemp.getOrig() != null)
	// vTemp1 = vTemp.getOrig();
	//
	// if(vTemp1 == null)
	// vTemp1 = vTemp;
	//				
	// if(this.varDefActive.containsKey(vTemp1))
	// {
	// this.phiLhsMapsTo.remove(e.getKey());
	// Variable vNew = this.varDefActive.get(vTemp1);
	// this.phiLhsMapsTo.put(e.getKey(), vNew);
	// D.pv(D.D_TEMP, " adding a phi Lhs maps to " + e.getKey() + " to " +
	// this.varDefActive.get(vTemp1));
	// }
	// D.pv(D.D_TEMP, " after node - " + cn + " the phiLhs maps - ");
	// for(Map.Entry<Variable, Variable> en : this.phiLhsMapsTo.entrySet())
	// D.pv(D.D_TEMP, " \t\t\t phi Lhs " + en.getKey() + " maps to - " +
	// en.getValue());
	// }
	// }
	//
	// if(cn.equals(this.exitNode))
	// {
	// D.pd(D.D_TEMP, " exit node visited ");
	// if(this.path != null)
	// {
	// ArrayList<Variable> rhs = cn.getRightToRename();
	// if(rhs != null && rhs.size() > 0)
	// {
	// for(Variable r : rhs)
	// {
	// if(this.phiLhsMapsTo.containsKey(r))
	// {
	// D.pv(D.D_TEMP, " introducing new stmt - " + r + " = " +
	// this.phiLhsMapsTo.get(r));
	// CfgNodeAssignUnary cnau = new CfgNodeAssignUnary(r,
	// this.phiLhsMapsTo.get(r), 1, null);
	// this.path.addNode(cnau);
	// // cn.rightRename(r, this.phiLhsMapsTo.get(r));
	// }
	// }
	// }
	//				
	// this.path.addNode(cn);
	// PathEnumerator.nodesCovered.addIfAbsent(cn);
	// PathEnumerator.flows.add(this.path);
	// D.pd(D.D_TEMP, " In PathEnumerators === control flow found - " +
	// this.path.toString());
	// }
	// return;
	// }
	//			
	// // check if in dependency
	// if(!this.dependsNodes.contains(cn))
	// return;
	//
	// boolean added = false;
	// // update any new definitions and put them in varActive variable
	// if(this.path != null)
	// {
	// added = this.path.addNode(cn);
	// PathEnumerator.nodesCovered.addIfAbsent(cn);
	// }
	//		
	// if(cn instanceof CfgNodeAssignArray ||
	// cn instanceof CfgNodeAssignBinary ||
	// cn instanceof CfgNodeAssignRef ||
	// cn instanceof CfgNodeAssignSimple ||
	// cn instanceof CfgNodeAssignUnary)
	// {
	// // assigned to
	// Variable vLhs = cn.getLeft();
	// if(!vLhs.isTemp())
	// {
	//				
	// Variable vOrig = vLhs;
	// if(vLhs.getOrig() != null)
	// vOrig = vLhs.getOrig();
	//				
	// if(this.varDefActive.containsKey(vOrig))
	// this.varDefActive.remove(vOrig);
	//				
	// D.pd(D.D_TEMP, " storing the active var definition " + vOrig + " to " +
	// vLhs);
	// this.varDefActive.put(vOrig, vLhs);
	//				
	// D.pv(D.D_TEMP, " after node - " + cn + " updated the active var - ");
	// for(Map.Entry<Variable, Variable> e : this.varDefActive.entrySet())
	// D.pv(D.D_TEMP, " \t\t\t active variable def for " + e.getKey() + " is - "
	// + e.getValue());
	// }
	// }
	//		
	// // rename any RHS values with the incoming definition of phi variables
	// if(added)
	// {
	// ArrayList<Variable> rhsVars = cn.getRightToRename();
	// if(rhsVars != null && rhsVars.size() > 0)
	// {
	// for(Variable rhs : rhsVars)
	// {
	// if(this.phiLhsMapsTo.containsKey(rhs))
	// {
	// Variable v = rhs.getOrig();
	// D.pv(D.D_TEMP, " renaming " + rhs + " to " +
	// this.phiLhsMapsTo.get(rhs) + " in - " + cn + " orig of rhs = " + v);
	//						
	// // D.pv(D.D_TEMP, " introducing new stmt - " + rhs + " = " +
	// this.phiLhsMapsTo.get(rhs));
	// /// CfgNodeAssignUnary cnau = new CfgNodeAssignUnary(rhs,
	// this.phiLhsMapsTo.get(rhs), 1, null);
	// // this.path.addNode(cnau);
	//						
	// if(!(cn instanceof CfgNodeCallPrep) &&
	// !(cn instanceof CfgNodeAssignBinary) &&
	// !(cn instanceof CfgNodeAssignUnary) &&
	// !(cn instanceof CfgNodeAssignSimple))
	// throw new Error(" currently phiLhsmap unhandled");
	//						
	// D.pv(D.D_TEMP, " #### adding support for replacing phi rhs arg " + rhs +
	// " with - " + this.phiLhsMapsTo.get(rhs));
	//	
	// rhs.setPhiLhs();
	// this.path.addPhiLhsSupport(cn, rhs, this.phiLhsMapsTo.get(rhs));
	// //cn.rightRename(rhs, this.phiLhsMapsTo.get(rhs), false);
	// //this.path.setLoopBody();
	// //rhs.setOrig(v);
	// D.pv(D.D_TEMP, " \t\t after renaming node-  " + cn +
	// " and orig of rhs = " + rhs.getOrig());
	// }
	// }
	// }
	// }
	// }

	public void setSeenExit(boolean bSeenExit) {
		this.bSeenExit = bSeenExit;
	}

	public boolean isSeenExit() {
		return bSeenExit;
	}

	public void findExitNode() {
		CfgNode exit = null;

		TacFunction tf = Cfg.getFunction(this.funcCfg.getHead());
		TacFunction sinkHoster = this.sink.getEnclosingFunction();
		// if this method hosts query execution
		// that is the exit point
		if (tf.equals(sinkHoster)) {
			D.pr(D.D_TEMP, " " + tf.getName()
					+ " hosts the sink | exit point - " + sink);
			this.setExitNode(sink);
			return;
		}

		// if it invokes one of the methods in call sequences
		// that in turn leads to exit point, that is the exit point
		TacFunction invoked = sinkHoster;
		TacFunction invoker = getInvoker(invoked);
		ArrayList<CfgNode> bar = new ArrayList<CfgNode>();
		while (invoker != null) {
			if (invoker.equals(tf)) {
				// exit = locateCallToFn(invoked);
				ArrayList<CfgNode> exits = locateCallToFn(invoked);
				D.pr(D.D_PATH, " locate call from this function to "
						+ invoked.getName() + " found - " + exits);

				if(exits.size() == 2)
				{
					// TODO: Proper fix
					CfgNode c1 = exits.get(0);
					CfgNode c2 = exits.get(1);

					int iLine1 = c1.getOrigLineno();
					int iLine2 = c2.getOrigLineno();
					String file1 = c1.getFileName();
					String file2 = c2.getFileName();
					D.pr(D.D_TEMP, " TODO: check this out - " + 
							" orig file 1 - #" + file1 + "# line - " + iLine1 + 
							" orig file 2 - #" + file2 + "# line - " + iLine2 
							);
					if(iLine1 == 81 && iLine2 == 102 && 
							file1.equals("login.php") && 
							file2.equals("login.php"))
					{
						D.pr(D.D_TEMP, "Removing....");
						ArrayList<CfgNode> toRemove = new ArrayList<CfgNode>();
						D.pr(D.D_TEMP, " Before removing violating - " + this.dependsNodes);
						for(CfgNode c : this.dependsNodes)
						{
							if(c.getOrigLineno() == 81)
								toRemove.add(c);
						}
						
						this.dependsNodes.removeAll(toRemove);
						D.pr(D.D_TEMP, " after removing violating - " + this.dependsNodes);
						exits.remove(0);
							
					}
				}
				
				boolean bTrueD = false;
				if (exits.size() == 0)
					continue;

				if (exits.size() == 1) {
					bTrueD = true;
					exit = exits.get(0);
				} else {
					// ensure that there was a call to invoked in the dep graph.
					// because of extending the cfg nodes, we have spurious
					// calls
					// in longer run need to correct the algo.
					// int iExitL = exitT.getOrigLineno();
					List<CfgNode> allRealDeps = this.depGraph
							.getAllCfgNodes(this.depGraph.getNodes(), true);
					D.pr(D.D_PATH, " all nodes in depGraph - " + allRealDeps);
					D.pr(D.D_PATH, " all exits - " + exits);
					for (CfgNode cn : exits) {
						D.pr(D.D_TEMP, "Checking if this exit was present in the dep nodes - "
										+ cn);
						int iExitL = cn.getOrigLineno();
						// unfortunately following doesnt work/ equality in
						// CfgNode?
						// if(allRealDeps.contains(cn))
						for (CfgNode cnd : allRealDeps) {
							if (iExitL == cnd.getOrigLineno()
									&& cnd instanceof CfgNodeCallPrep) {
								D.pr(D.D_TEMP, " \t in deep comparison - " + cnd);
								D.pr(D.D_TEMP, " \t "
										+ ((CfgNodeCallPrep) cnd).getCallee()
												.getName() + " Invoked - "
										+ invoked.getName());
								boolean found = ((CfgNodeCallPrep) cnd)
										.getCallee().getName().equals(
												invoked.getName());
								if (found) {
									D.pr(D.D_TEMP, " found a match --------------- "
													+ cn);
									bTrueD = true;
									
									exit = cn;
									exits.remove(cn);
									break;
								}
							}
						}
						if (bTrueD) {
							for (CfgNode c1 : exits) {
								// D.pr(" removing the node - " + c1);
								// this.dependsNodes.remove(c1);
							}
						}
						if (bTrueD)
							break;
					}
				}

				if (bTrueD) {
					// make call return as the exit point
					exit = exit.getSuccessorsNoBlocks().get(0);
					D.pr(D.D_TEMP, " " + tf.getName()
							+ " exit point  | invocation to other- " + exit);
					setExitNode(exit);
					break;
				}
			}
			invoked = invoker;
			invoker = getInvoker(invoked);
		}

		// otherwise take the tail of the Cfg and declare it as exit point.
		if (exit == null) {
			D.pr(D.D_TEMP, " " + tf.getName()
					+ " ...setting the exit point as tail - "
					+ this.funcCfg.getTail());
			setExitNode(this.funcCfg.getTail());
		}
	}

	private ArrayList<CfgNode> locateCallToFn(TacFunction invoked) {
		ArrayList<CfgNode> allcalls = new ArrayList<CfgNode>();

		for (CfgNode cn : this.dependsNodes) {
			if (cn instanceof CfgNodeCall) {
				CfgNodeCall cnc = (CfgNodeCall) cn;
				TacFunction tfCallee = cnc.getCallee();

				if (tfCallee.equals(invoked)) {
					D.pf(D.D_TEMP, " located call to function - "
							+ invoked.getName() + " at node - " + cn);
					allcalls.add(cn);
				}
			}
		}

		return allcalls;
	}

	public void setExitNode(CfgNode exitNode) {
		this.exitNode = exitNode;
	}

	public CfgNode getExitConditional() {
		return this.exitNode;
	}

	private TacFunction getInvoker(TacFunction invoked) {
		if (this.invokeOrder == null)
			throw new Error(" invokeOrder must be set....");
		for (Map.Entry<ArrayList<TacFunction>, TacFunction> e : this.invokeOrder
				.entrySet())
			if (e.getKey().contains(invoked))
				return e.getValue();

		return null;
	}

	public static String getStatus(int val) {
		switch (val) {
		case PathEnumerator.NO_PATH:
			return " None forbidden ";
		case PathEnumerator.ELSE_PATH:
			return " else forbidden ";
		case PathEnumerator.THEN_PATH:
			return " then forbidden ";
		case PathEnumerator.ALL_PATH:
			return " all forbidden ";
		case PathEnumerator.NOT_INITED:
			return " not initialized ";
		}

		return " no status ";
	}

	public static LinkedHashMap<ArrayList<CfgNode>, ArrayList<ControlFlow>> intraPathsRepos; 

	public static void addPaths2Reuse(ArrayList<CfgNode> inNodes, ArrayList<ControlFlow> paths)
	{
		if(intraPathsRepos == null)
			intraPathsRepos = new LinkedHashMap<ArrayList<CfgNode>, ArrayList<ControlFlow>>();
		intraPathsRepos.put(inNodes, paths);
	}
	
	public static ArrayList<ControlFlow> reuseOld(Cfg c, ArrayList<CfgNode> inNodes) {
		
		if(intraPathsRepos == null)
		{
			intraPathsRepos = new LinkedHashMap<ArrayList<CfgNode>, ArrayList<ControlFlow>>();
			return null;
		}
		
		if(intraPathsRepos.containsKey(inNodes))
		{
			ArrayList<ControlFlow> flows = intraPathsRepos.get(inNodes);
			D.pr(D.D_TEMP, "VOILA!!! reusing previously enumerated paths - !!!!!!!!!!!!!");
			return flows; 
		}
		else
			return null;
	}

	/*
	 * 
	 * private LinkedHashMap<CfgNodeIf, CopyOnWriteArrayList<CfgNode>>
	 * loopDetection; private LinkedHashMap<CfgNodeIf, Integer> shunnedPaths;
	 * private int getShunnedPath(CfgNodeIf node) { D.pv(D.D_TEMP,
	 * " checking shunned status of - " + node);
	 * 
	 * if(shunnedPaths == null) return NO_PATH;
	 * 
	 * Integer objShun = shunnedPaths.get(node); int iShun = NO_PATH; if(
	 * objShun != null) iShun = objShun; D.pv(D.D_TEMP,
	 * " shunned status for node - " + node + " is = " + iShun); return iShun; }
	 * private void skipNonContributingLoops(CfgNodeIf node) {
	 * CopyOnWriteArrayList<CfgNode> currentlyCovered = new
	 * CopyOnWriteArrayList<CfgNode>();
	 * currentlyCovered.addAll(PathEnumerator.nodesCovered);
	 * 
	 * // shun the last path selection if(shunnedPaths == null) { shunnedPaths =
	 * new LinkedHashMap<CfgNodeIf, Integer> (); } if(loopDetection == null)
	 * loopDetection = new LinkedHashMap<CfgNodeIf,
	 * CopyOnWriteArrayList<CfgNode>>();
	 * 
	 * CopyOnWriteArrayList<CfgNode> nodesCoveredInPriorVisit =
	 * loopDetection.get(node); if(nodesCoveredInPriorVisit == null) {
	 * loopDetection.put(node, currentlyCovered); return; }
	 * 
	 * // if there wasn't any change since it was visited last. // shun the path
	 * taken in prior visit. D.pv(D.D_TEMP, " nodes covered in prior visit " +
	 * nodesCoveredInPriorVisit); D.pv(D.D_TEMP, " nodes currently covered - " +
	 * PathEnumerator.nodesCovered.containsAll(nodesCoveredInPriorVisit));
	 * if(nodesCoveredInPriorVisit.containsAll(PathEnumerator.nodesCovered) &&
	 * PathEnumerator.nodesCovered.containsAll(nodesCoveredInPriorVisit)) { int
	 * iPrevPath = this.getPrevPath(node); if(iPrevPath == NO_PATH) {
	 * D.pv(D.D_TEMP, " cannot shun the node that wasn't marked " + node ); }
	 * else { D.pv(D.D_TEMP, " shunning the path - " + node + " " + iPrevPath);
	 * shunnedPaths.put(node, iPrevPath); } } else { loopDetection.remove(node);
	 * loopDetection.put(node, currentlyCovered); } }
	 * 
	 * private boolean isDepNodesCovererd() {
	 * 
	 * D.pv(D.D_TEMP, " depend nodes - "); for(CfgNode cn : this.dependsNodes)
	 * D.pv(D.D_TEMP, " \t \t \t node - " + cn);
	 * 
	 * D.pv(D.D_TEMP, " covered so far - "); for(CfgNode cn :
	 * PathEnumerator.nodesCovered) D.pv(D.D_TEMP, " \t \t \t node - " + cn);
	 * 
	 * if(this.dependsNodes.containsAll(PathEnumerator.nodesCovered) &&
	 * PathEnumerator.nodesCovered.containsAll(this.dependsNodes)) return true;
	 * 
	 * return false; }
	 * 
	 * private int getPrevPath(CfgNodeIf cni) { if(this.pathDecisions == null ||
	 * cni == null) throw new Error(" path decisions is " + this.pathDecisions +
	 * " cni =  " + cni);
	 * 
	 * int iPathPrev = NO_PATH;
	 * 
	 * Integer prev = this.pathDecisions.get(cni);
	 * 
	 * if(prev != null) iPathPrev = prev; else D.pd(D.D_TEMP,
	 * " path decision for " + cni + " is a null object");
	 * 
	 * if(iPathPrev == 0) return NO_PATH; else return iPathPrev; }
	 * 
	 * private void setPrevPath(CfgNodeIf cni, int path) {
	 * if(this.pathDecisions.get(cni) != null) this.pathDecisions.remove(cni);
	 * 
	 * this.pathDecisions.put(cni, new Integer(path));
	 * 
	 * D.pv(D.D_TEMP, " after setting the " + cni + " prev path to - " + ((path
	 * == ELSE_PATH) ? " ELSE" : "THEN" )); for(Map.Entry<CfgNode, Integer> e :
	 * this.pathDecisions.entrySet()) { D.pv(D.D_TEMP,
	 * " \t\t\t path decision - " + e.getKey() + " path taken = " +
	 * ((e.getValue() == ELSE_PATH) ? " ELSE" : "THEN" )); } }
	 */
	/*
	 * private void optimizeProcessing() { TacFunction tf =
	 * Cfg.getFunction(this.entryNode); D.pv(D.D_TEMP,
	 * " dependency nodes in function - " + tf.getName()); for(CfgNode c :
	 * this.dependsNodes) { D.pv(D.D_TEMP, " \t \t " + c); } this.nodesBetween =
	 * getNodesBetween(this.entryNode, this.exitNode); for(CfgNode cn :
	 * nodesBetween) { if(cn instanceof CfgNodeIf) { CfgNodeIf cni = (CfgNodeIf)
	 * cn; D.pv(D.D_TEMP, " for if node - " + cn); D.pv(D.D_TEMP,
	 * " \t dominance frontier - " + cn.getDominanceFrontier());
	 * 
	 * ArrayList<CfgNode> cniLoop = cni.getLoopBody(); if(cniLoop != null) {
	 * CopyOnWriteArrayList<CfgNode> loop = new CopyOnWriteArrayList<CfgNode>();
	 * loop.addAll(cniLoop); loop.retainAll(this.dependsNodes); D.pv(D.D_TEMP,
	 * " this if condition encloses a loop - "); if(loop.size() != 0) {
	 * D.pv(D.D_TEMP, " which contains depend nodes-  " + loop); } else {
	 * D.pv(D.D_TEMP,
	 * " which does not contain any depend nodes....forbid it..");
	 * this.pathAnalysis.add(new PathDecision(cni, true, false)); D.pv(D.D_TEMP,
	 * " forbid traversal of loop body "); }
	 * 
	 * for(CfgNode cnL : cniLoop) { D.pv(D.D_TEMP, " \t\t " + cnL); } } else {
	 * boolean bThenDepends = false; boolean bElseDepends = false;
	 * 
	 * D.pv(D.D_TEMP, " conditional node - " + cni + " does not contain a loop"
	 * ); // check to see if this else corresponds to a switch
	 * 
	 * if(cni.isSwitchElse()) bElseDepends = true;
	 * 
	 * CopyOnWriteArrayList<CfgNode> cniThenBody = cni.getThenBody();
	 * CopyOnWriteArrayList<CfgNode> cniElseBody = null;
	 * CopyOnWriteArrayList<CfgNode> elseBody = null;
	 * 
	 * CfgNode cnThen = cni.getThen(); CfgNode cnElse = null;
	 * 
	 * if(!bElseDepends) { cnElse = cni.getElse(); cniElseBody =
	 * cni.getElseBody(); elseBody = new CopyOnWriteArrayList<CfgNode>();
	 * elseBody.addAll(cniElseBody); elseBody.retainAll(this.dependsNodes);
	 * 
	 * bElseDepends = elseBody.size() != 0; D.pv(D.D_TEMP, " \t else branch - "
	 * + cniElseBody); }
	 * 
	 * D.pv(D.D_TEMP, " \t else - " + cnElse + " then - " + cnThen);
	 * D.pv(D.D_TEMP, " \t if branch - " + cniThenBody);
	 * 
	 * CopyOnWriteArrayList<CfgNode> thenBody = new
	 * CopyOnWriteArrayList<CfgNode>(); thenBody.addAll(cniThenBody);
	 * thenBody.retainAll(this.dependsNodes); bThenDepends = thenBody.size() !=
	 * 0;
	 * 
	 * if(bElseDepends) { D.pv(D.D_TEMP,
	 * " \t else branch has following depends node - " + elseBody); // if else
	 * branch has no body...and then branch depends... // we need to force
	 * enumeration on the else branch... }
	 * 
	 * if(bThenDepends) { D.pv(D.D_TEMP,
	 * " \t then branch has following depends node - " + thenBody); }
	 * 
	 * if(!bElseDepends && bThenDepends && cniElseBody.size() == 0) {
	 * D.pv(D.D_TEMP,
	 * " else branch is bodyless and then depends...forcing else traversal");
	 * bElseDepends = true; }
	 * 
	 * D.pv(D.D_TEMP, " " + cni + " shun then - " + !bThenDepends +
	 * " shun else - " + !bElseDepends);
	 * 
	 * this.pathAnalysis.add(new PathDecision(cni, false, false));
	 * 
	 * // if(!bElseDepends && !bThenDepends) // { // boolean bElseBigger =
	 * cniElseBody.size() > cniThenBody.size(); // D.pv(D.D_TEMP,
	 * " both the branches do not contribute...smaller branch ..." + //
	 * (bElseBigger? " THEN BRANCH ": " ELSE BRANCH")); // } } } }
	 * 
	 * // above path analysis is too strict and sometimes forbids the path that
	 * leads to // the query execution points. Partly, this is because of the
	 * underlying CFG // that uses concept of basic block very loosely,
	 * especially // for if-else statements. supplementPathAnalysis();
	 * 
	 * D.pv(D.D_TEMP, " printing the path analysis outcome --------------- ");
	 * // so we know what to avoid now and where contributing loops are ----
	 * check for(CfgNode cn : nodesBetween) { if(cn instanceof CfgNodeIf) { int
	 * iForbid = PathDecision.getForbiddenStatus(this.pathAnalysis, cn);
	 * D.pv(D.D_TEMP, "\t\t if stmt - -- " + cn + " forbid status - " +
	 * PathEnumerator.getStatus(iForbid));
	 * 
	 * ArrayList<CfgNode> cniLoop = cn.getLoopBody(); if(cniLoop != null &&
	 * iForbid != PathEnumerator.THEN_PATH) { D.pv(D.D_TEMP,
	 * "\t\t\t ---- loop iteration --- "); } } }
	 * 
	 * }
	 */
		
	/*
	 * this method checks if a loop can be transformed. Following constraints are checked
	 * 1. there is no nested loop / conditional within the loop body. 
	 * 		---> the reason for disallowing these is difficulty in summarization. 
	 * 		---> if these are allowed, it is not clear if loop contributions can be summarized
	 * 		---> in a static setup. 
	 * 2. all statements are of the form 
	 * 		a. Q1 = Q1 . y 
	 * 		b. Q2 = y . Q2
	 * 		---> for a single variable only left- or right-recursion is allowed. 
	 * 		---> y can be a constant or a variable name that is not being changed in the loop body.
	 */
	private void staticCheckLoopTransformable(CfgNodeIf cni)
	throws SISLException
	{
		boolean bThrowUp = false;
		//ArrayList<CfgNode> nodesInLoop = new ArrayList<CfgNode>();
		ArrayList<CfgNode> cniLoop = cni.getLoopBody();
		D.pr(D.D_TEMP, "staticCheckLoopTransformable: Loop starting point - " + cni + " Loop body -  " + cniLoop);

		 // 1. there is no nested loop / conditional within the loop body. 
//		for(CfgNode inNode : cniLoop)
//		{
//			if(inNode instanceof CfgNodeIf)
//			{
//				D.pr(D.D_TEMP, " Found a conditional in the loop body - " + inNode);
//				String msg = " Found a conditional " + inNode +" in the following Loop body - violates constraints\n ";
//				
//				msg += CfgNode.getProgLines(cniLoop) + "\n";
//				msg += " \n for this sink - " + CfgNode.getProgLine(sink);
//					
//				D.pr(D.D_TEMP, msg);
//				// dump the derivation tree for debugging
//				//ToolBug.add_UNREPEATABLE_QUERY_FROM_LOOP(sink, this.symbQuery, msg);
//				throw new SISLException(SISLException.LOOPSTMT_VIOLATE_CONSTRAINT, msg);
//			}
//		}
		
		// if the loop doesnt contain any dependency node.
		// do not check it
		
		
		int iCount = cniLoop.size();
		for(int i = 0; i < iCount; i++)
		{
			CfgNode inNode = cniLoop.get(i);
			if(inNode instanceof CfgNodeIf)
			{
				D.pr(D.D_TEMP, " Found a conditional in the loop body - " + inNode);
				if(beyondDependencies(i+1, cniLoop, this.dependsNodes) &&
						isConditionalDepedent((CfgNodeIf)inNode, cniLoop))
				{
					String msg = " Found a conditional " + inNode +
					" in the following Loop body - violates constraints\n ";
				
					CfgNodeIf cnifV = (CfgNodeIf) inNode; 
					D.pvt(D.D_TEMP, " then part - " + cnifV.getThen());
					D.pvt(D.D_TEMP, " then part body - " + cnifV.getThenBody());
					D.pvt(D.D_TEMP, " else part - " + cnifV.getElse());
					D.pvt(D.D_TEMP, " else part body - " + cnifV.getElseBody()); 
					
					for(String s : CfgNode.getProgLines(cniLoop))
						msg += "\t\t " + s + " \n";
					
					msg += " \n for this sink - " + CfgNode.getProgLine(sink);
						
					D.pr(D.D_TEMP, msg);
					// dump the derivation tree for debugging
					//ToolBug.add_UNREPEATABLE_QUERY_FROM_LOOP(sink, this.symbQuery, msg);
					throw new SISLException(SISLException.LOOPSTMT_VIOLATE_CONSTRAINT, msg);
				}
			}
		}
		
		 /* 2. all statements are of the form 
		 * 		a. Q1 = Q1 . y 
		 * 		b. Q2 = y . Q2
		 * 		---> for a single variable only left- or right-recursion is allowed. 
		 * 		---> y can be a constant or a variable name that is not being changed in the loop body.
		 */
		boolean bStmt = true;
		for(CfgNode cn : cniLoop)
		{
			passLoopConstraints(cn, cniLoop);
		}
		
		
		// control only comes here if the loop passed the constraints check
		// add book keeping to report in  the paper
		TransformationResults.get().addHandledLoop(this.sink, 
				cniLoop, this.dependsNodes);
	}

	private boolean isConditionalDepedent(CfgNodeIf cnifV, ArrayList<CfgNode> cniLoop) {
		boolean bResult = true;
		D.pvt(D.D_TEMP, " Testing whether to allow conditional node - " + cnifV + "");

		// check to see if this else corresponds to a switch
		// if(cni.isSwitchElse())
		// bElseDepends = true;

		// determine where in this CFG we need to stop
		this.findExitNode();
		
		// avoid processing nodes until the first dependency node
		// otherwise hard to scale to large apps like wordpress
		this.findEntryNode();

		CfgNode cnElse = cnifV.getOutEdge(0).getDest();
		if (cnElse instanceof CfgNodeBasicBlock)
			cnElse = ((CfgNodeBasicBlock) cnElse).getContainedNodes()
					.get(0);

		CfgNode cnThen = cnifV.getOutEdge(1).getDest();
		if (cnThen instanceof CfgNodeBasicBlock)
			cnThen = ((CfgNodeBasicBlock) cnThen).getContainedNodes()
					.get(0);

		ArrayList<CfgNode> thenReachable = new ArrayList<CfgNode>();
		thenReachable.add(cnifV);
		reachable(cnThen, this.entryNode, this.exitNode, thenReachable);
		expandBasicBlocks(thenReachable);

		ArrayList<CfgNode> elseReachable = new ArrayList<CfgNode>();
		elseReachable.add(cnifV);
		reachable(cnElse, this.entryNode, this.exitNode, elseReachable);
		expandBasicBlocks(elseReachable);

		/*
		 * D.pv(D.D_TEMP, " \t then branch : " + cnThen + " reachable : " +
		 * CfgNode.getProgLines(thenReachable) + " \n \t \t all - " +
		 * thenReachable); D.pv(D.D_TEMP, " \t else branch : " + cnElse +
		 * " reachable : " + CfgNode.getProgLines(elseReachable) +
		 * " \n \t \t all - " + elseReachable);
		 */
		int iThen = thenReachable.size();
		int iElse = elseReachable.size();

		thenReachable.retainAll(this.dependsNodes);
		elseReachable.retainAll(this.dependsNodes);

		boolean bSame = thenReachable.containsAll(elseReachable)
				&& elseReachable.containsAll(thenReachable);

		int iThenSees = thenReachable.size();
		int iElseSees = elseReachable.size();

		
		 D.pvt(D.D_TEMP, " \t then branch : contains dep nodes - " +
				iThenSees + " " + CfgNode.getProgLines(thenReachable) );
		  D.pvt(D.D_TEMP, " \t \t total reachable - " + iThen);
		  D.pvt(D.D_TEMP, " \t else branch : contains dep nodes - " +
				  iElseSees + " " + CfgNode.getProgLines(elseReachable) );
		 D.pvt(D.D_TEMP, " \t \t total reachable - " + iElse);
		 

		// if both branches see same number of dependency nodes,
		// shun the path that has higher number of nodes...
		// it may correspond to a branch body, that does not
		// contribute to the query computation
		if (iThenSees == iElseSees && bSame) {
			boolean bThenForbid = (iThen > iElse);
			D.pvt(D.D_TEMP, "Allow - the conditional");
			bResult = false;
			/*
			 * D.pv(D.D_TEMP, " \t #### Forbidden : " + (bThenForbid?
			 * " Then " : " Else ") + " then and else see same dep nodes - "
			 * + CfgNode.getProgLines(thenReachable));
			 */
		} else if (iThenSees != iElseSees) {
			// they see different number of dependency nodes
			
			// allow if the sink is within a conditional 
			if(
				(iThenSees == 0 && reachableHasSink(elseReachable)) || 
				(iElseSees == 0 && reachableHasSink(thenReachable)))
			{
				D.pvt(D.D_TEMP, " allow the sink hosting conditional");
				bResult = false;
			}
			else
			{
			
				if (iThenSees == 0) {
					// then never sees a dependency node...may correspond to
					// exits or returns
					// forbid then
					D
							.pvt(D.D_TEMP,
									"\t ##### Forbidden  : Then (0) dep nodes - exit path? ");
				} else if (iElseSees == 0) {
					// else never sees a dependency node...may correspond to
					// exits or returns
					// forbid else
					D
							.pvt(D.D_TEMP,
									"\t ##### Forbidden  : Else (0) dep nodes - exit path? ");
				} else {
					// if both are non-zero and see different number of
					// dependency nodes,
					// leave both paths unforbidden.
					D.pvt(D.D_TEMP, " disallow this conditional .........");
				}
			}
		} else {
			// disallow if cannot decide
			D.pvt(D.D_TEMP, "Cannot decide....forbid");
		}
		
		return bResult;
	}

	private boolean reachableHasSink(ArrayList<CfgNode> reachableNodes) {
		
		if(this.sink != null && reachableNodes.contains(this.sink))
		{
			D.pvt(D.D_TEMP, " reachable nodes - " + reachableNodes + " contains sink - " + this.sink) ;
			return true;
		}
		return false;
	}

	private boolean beyondDependencies(int startIndex, ArrayList<CfgNode> cniLoop,
			ArrayList<CfgNode> dependsNodes2) 
	{
		D.pr(" is a dependent node present starting - " + cniLoop.get(startIndex) + 
				" in the following stmts - " + dependsNodes2);
		int iLoop = cniLoop.size();
		for(int i = startIndex; i < iLoop; i++)
		{
			CfgNode inLoop = cniLoop.get(i);
			if(dependsNodes2.contains(inLoop))
			{
				//throw new Error("voila");
				return true;
			}
		}
		
		return false;
	}


	private boolean passLoopConstraints(CfgNode cn, ArrayList<CfgNode> cniLoop) 
	throws SISLException {

		boolean bPass = true; 
		if(!this.dependsNodes.contains(cn))
		{
			D.pr(D.D_TEMP, "Query does not depends on this node - " + cn);
			return true;
		}
		
		TacPlace tpLeft = null;
		TacPlace tpRight = null;

		String op = null;

		if(cn instanceof CfgNodeAssignArray)
		{
			D.pr(D.D_TEMP, "Processing CfgNodeAssignArray " + cn.toString());

			tpLeft = cn.getLeft();
			ArrayList<Variable> all = cn.getRightToRename();
			if(all != null)
			{
				tpRight = all.get(0);			
				if(!tpRight.isConstant() && !tpRight.isLiteral() && !tpRight.getVariable().isTemp() && 
						(isModifiedInRestOfLoop((Variable)tpRight, cn, cniLoop)))
				{
					bPass = false;
					//throw new Error(" CfgNodeAssignArray - " + cn + " violates the loop constraints");
					String msg = " Statement " + cn +" in following Loop violates the constraints \n ";
					
					for(String s : CfgNode.getProgLines(cniLoop))
						msg += "\t\t " + s + " \n";

					msg += " \n for this sink - " + CfgNode.getProgLine(sink);
						
					D.pr(D.D_TEMP, msg);
					// dump the derivation tree for debugging
					//ToolBug.add_UNREPEATABLE_QUERY_FROM_LOOP(sink, this.symbQuery, msg);
					throw new SISLException(SISLException.LOOPSTMT_VIOLATE_CONSTRAINT, msg);

				}
			}
		}
		else
		if(cn instanceof CfgNodeAssignBinary)
		{
			D.pr(D.D_TEMP, "Processing CfgNodeAssignBinary " + cn.toString());
			
			CfgNodeAssignBinary cnab = (CfgNodeAssignBinary) cn;			
			int iOp = cnab.getOperator();
			if(iOp == TacOperators.CONCAT)
			{
				TacPlace tpLeftOperand = cnab.getLeftOperand();
				TacPlace tpRightOperand = cnab.getRightOperand();
				TacPlace tpAssignedTo = cnab.getLeft();
				
				Variable vRightOp = null, vLeftOp = null, vLHS = null;
				
				if(tpRightOperand.isVariable()){
					vRightOp = tpRightOperand.getVariable();
					if(vRightOp.getOrig() != null)
						vRightOp = vRightOp.getOrig();
				}
				
				if(tpLeftOperand.isVariable()){
					vLeftOp = tpLeftOperand.getVariable();
					if(vLeftOp.getOrig() != null)
						vLeftOp = vLeftOp.getOrig();
				}
				
				if(tpAssignedTo.isVariable()){
					vLHS = tpAssignedTo.getVariable();
					if(vLHS.getOrig() != null)
						vLHS = vLHS.getOrig();
				}
				
				boolean bLeftR = (vLeftOp != null) && (vLHS.equals(vLeftOp));
				boolean bRightR = (vRightOp != null) && (vLHS.equals(vRightOp));
				
				if(!bLeftR && !bRightR && !PathEnumerator.m_bLR_RR_Relaxed)
				{
					// neither left not right recursive...something else
					//throw new Error(" CfgNodeAssignArray - " + cn + " violates the loop constraints");
					String msg = " Statement " + cn +" violates loop constraints \n ";
					msg +=   " Loop body stmt - " + cn + 
							" is neither immediate left- nor immediate right-recursive " + 
							" vLhs - " + vLHS + " vRightOp - " + vRightOp + " vLeftOp - " + vLeftOp + "\n";
					msg += " in the following loop \n";
					for(String s : CfgNode.getProgLines(cniLoop))
						msg += "\t\t " + s + " \n";

					msg += " \n for this sink - " + CfgNode.getProgLine(sink);
						
					D.pr(D.D_TEMP, msg);
					// dump the derivation tree for debugging
					//ToolBug.add_UNREPEATABLE_QUERY_FROM_LOOP(sink, this.symbQuery, msg);
					throw new SISLException(SISLException.LOOPSTMT_VIOLATE_CONSTRAINT, msg);
				}
				
				// if it is left recursive or right recursive demand that the other operand
				// be constant or a variable that is not changed in the rest of the loop
				Variable vAnalyze = null;
				if(bLeftR)
					vAnalyze = vRightOp;
				else
					vAnalyze = vLeftOp; 
				
				if(m_bLR_RR_Relaxed)
				{
					boolean bRightViolates = 
						(vRightOp != null && 
						 !vLHS.equals(vRightOp) &&
						 isModifiedInRestOfLoop(vRightOp, cn, cniLoop));
					
					boolean bLeftViolates = 
						(vLeftOp != null && 
						!vLHS.equals(vLeftOp) && 
						isModifiedInRestOfLoop(vLeftOp, cn, cniLoop));
					
					if(bRightViolates || bLeftViolates)
					{						
						String msg = " Statement " + cn +" violates loop constraints \n ";
						msg +=  " Loop body stmt - " + cn + 
								" uses a relaxed left-recursive and right-recursive rule but \n" +  
								(bRightViolates? (" right operand " + vRightOp +" is modified later in the loop - \n"):"") + 
								(bLeftViolates? (" left operand " + vLeftOp +" is modified later in the loop - \n"):"");

						msg += " in the following loop \n";
						for(String s : CfgNode.getProgLines(cniLoop))
							msg += "\t\t " + s + " \n";
						msg += " \n for this sink - " + CfgNode.getProgLine(sink);
							
						D.pr(D.D_TEMP, msg);
						// dump the derivation tree for debugging
						//ToolBug.add_UNREPEATABLE_QUERY_FROM_LOOP(sink, this.symbQuery, msg);
						throw new SISLException(SISLException.LOOPSTMT_VIOLATE_CONSTRAINT, msg);						
					}
				}
				else
				{
					if(vAnalyze != null && isModifiedInRestOfLoop(vAnalyze, cn, cniLoop))
					{
						bPass = false;
						// neither left not right recursive...something else
						
						String msg = " Statement " + cn +" violates loop constraints \n ";
						msg +=   (" Loop body stmt - " + cn + 
								" is " + (bLeftR ? " left-recursive " : " right-recursive ") +  
								" but second operand is modified later in the loop - " + vAnalyze + "\n");
						msg += " in the following loop \n";
						for(String s : CfgNode.getProgLines(cniLoop))
							msg += "\t\t " + s + " \n";
						msg += " \n for this sink - " + CfgNode.getProgLine(sink);
							
						D.pr(D.D_TEMP, msg);
						// dump the derivation tree for debugging
						//ToolBug.add_UNREPEATABLE_QUERY_FROM_LOOP(sink, this.symbQuery, msg);
						throw new SISLException(SISLException.LOOPSTMT_VIOLATE_CONSTRAINT, msg);
					}
				}
					
			}						
		}
/*		else
		if(cn instanceof CfgNodeAssignRef)
		{
			D.pv(D.D_TEMP, "Processing CfgNodeAssignRef " + cn.toString());

			tpLeft = cn.getLeft();
			ArrayList<Variable> r2rename = cn.getRightToRename();
			
		}
*/
		else
		if(cn instanceof CfgNodeAssignSimple)
		{
			D.pr(D.D_TEMP, "Processing CfgNodeAssignSimple " + cn.toString());

			tpLeft = cn.getLeft();
			CfgNodeAssignSimple cnas = (CfgNodeAssignSimple) cn;
			
			TacPlace tpTmp = null;
			tpRight = cnas.getRight();
			if(!tpRight.isConstant() && !tpRight.isLiteral() && 
					(isModifiedInRestOfLoop((Variable)tpRight, cn, cniLoop)))
			{
				bPass = false;
				//throw new Error(" CfgNodeAssignSimple - " + cn + " violates the loop constraints");
				String msg = " Statement " + cn +" violates loop constraints \n ";
				msg += " right hand side variable " + tpRight + " is modified after use in the following loop \n";
				for(String s : CfgNode.getProgLines(cniLoop))
					msg += "\t\t " + s + " \n";
				msg += " \n for this sink - " + CfgNode.getProgLine(sink);
					
				D.pr(D.D_TEMP, msg);
				// dump the derivation tree for debugging
				//ToolBug.add_UNREPEATABLE_QUERY_FROM_LOOP(sink, this.symbQuery, msg);
				throw new SISLException(SISLException.LOOPSTMT_VIOLATE_CONSTRAINT, msg);				
			}
		}
		else
		if(cn instanceof CfgNodeAssignUnary)
		{
			D.pr(D.D_TEMP, "Processing CfgNodeAssignUnary " + cn.toString());
			CfgNodeAssignUnary cnau = (CfgNodeAssignUnary) cn; 
			
			tpRight = cnau.getRight();
			if(!tpRight.isConstant() && !tpRight.isLiteral() && 
					(isModifiedInRestOfLoop((Variable)tpRight, cn, cniLoop)))
			{
				bPass = false;
				//throw new Error(" CfgNodeAssignUnary - " + cn + " violates the loop constraints");
				String msg = " Statement " + cn +" violates loop constraints \n ";
				msg += " right hand side variable " + tpRight + " is modified after use in the following loop \n";
				for(String s : CfgNode.getProgLines(cniLoop))
					msg += "\t\t " + s + " \n";
				msg += " \n for this sink - " + CfgNode.getProgLine(sink);
					
				D.pr(D.D_TEMP, msg);
				// dump the derivation tree for debugging
				//ToolBug.add_UNREPEATABLE_QUERY_FROM_LOOP(sink, this.symbQuery, msg);
				throw new SISLException(SISLException.LOOPSTMT_VIOLATE_CONSTRAINT, msg);				
			}
		}
		else
		{
			/*
			boolean bLetGo = true;
			if(!cn.equalsX(sink) &&
					(	cn instanceof CfgNodeCallBuiltin ||
						cn instanceof CfgNodeCall ||
						cn instanceof CfgNodeCallPrep ||
						cn instanceof CfgNodeCallRet || 
						cn instanceof CfgNodeCallUnknown
					)
				)
				bLetGo = false;
				
			if(!bLetGo)
			{
				String msg = " Statement " + cn +" is not allowed in a loop \n ";
				msg += CfgNode.getProgLines(cniLoop) + "\n";
				msg += " \n for this sink - " + CfgNode.getProgLine(sink);
					
				D.pr(D.D_TEMP, msg);
				// dump the derivation tree for debugging
				//ToolBug.add_UNREPEATABLE_QUERY_FROM_LOOP(sink, this.symbQuery, msg);
				throw new SISLException(SISLException.LOOPSTMT_VIOLATE_CONSTRAINT, msg);				
			}*/
		}
/*		else
		if(cn instanceof CfgNodeCallBuiltin)
		{
			D.pv(D.D_DT, "Processing CfgNodeCallBuiltin " + cn.toString());

			CfgNodeCallBuiltin cncb = (CfgNodeCallBuiltin) cn;
			// temp variable holds the return value
			tpLeft = cncb.getTempVar();
			
			if(isSummarized(cncb))
			{
				D.pv(D.D_TEMP, " Summarized function - " + cncb.getFunctionName());
				SISLException sisl = new SISLException(SISLException.BAIL_OUT, 
						" Unhandled summarized function - " + 
						cncb.getFunctionName() + " in " + CfgNode.getProgLineCode(cncb));
				ToolBug.add_UNCLASSIFIED_YET(sisl);
				throw sisl;
				//System.exit(1);
			}
		}
		else
		if(cn instanceof CfgNodeCall)
		{
			D.pv(D.D_DT, "Processing CfgNodeCall " + cn.toString());
			CfgNodeCall cnc = (CfgNodeCall) cn;
			// temp variable holds the return value
			// tpLeft = cnc.getTempVar();			
			tpLeft = null;
		}
		else
		if(cn instanceof CfgNodeCallPrep)
		{			
			// create dummy nodes to assign formal arguments to actual arguments
			CfgNodeCallPrep cncp = (CfgNodeCallPrep) cn;
			List<TacActualParam> actuals = cncp.getParamList();
			ArrayList<TacPlace> actualsPlaces = new ArrayList<TacPlace>();
			
			boolean bInDepGraph = isActualDependency(cn);
			if(!bInDepGraph)
			{
				D.pr(" detected that this call - " + cn + " is not in the dep graph...only return...not expanding formal to actual");
			}
			else
			{
			
			
			
			List<TacFormalParam> formals = cncp.getCallee().getParams();
			
			TacPlace left = null;
			TacPlace right = null;
			
			int index = 0;
			// actuals may not be same as formals 
			// get the default node if thats the case 
			D.pv(D.D_TEMP, " processing a call prep node " + cn);
			D.pv(D.D_TEMP, " actuals - ");
			for(TacActualParam tap : actuals)
				D.pv(D.D_TEMP, " \t actual - " + tap.toString());
			D.pv(D.D_TEMP, "formals - ");
			for(TacFormalParam tfp : formals)
			{
				D.pv(D.D_TEMP, "\t formal - " + tfp.toString());
				if(tfp.hasDefault())
					D.pv(D.D_TEMP, "\t\t has default - " + tfp.getDefaultCfg());
			}

			int iFormals = formals.size();
			int iActuals = actuals.size();

			for(TacActualParam tap : actuals)
				actualsPlaces.add(tap.getPlace());

			if(iFormals != iActuals)
			{	
				fillupDefaultValues(formals, actuals, actualsPlaces);
			}
			
			for(TacFormalParam formal : formals)
			{
				children = new ArrayList<DerivationNode>(); 
				
				TacPlace rightTmp = null;
				Variable vLeft = formal.getVariable();
				vLeft.setFormal(true);
				
				right = actualsPlaces.get(index++);	
				if(right.isVariable() && right.getVariable().isPhiLhs())
				{
					rightTmp = getPhiLhsMap(cn, cf, right);
					if(rightTmp != null)
						right = rightTmp;
				}
				
				dn = createDerivationNode(vLeft);
				DerivationNode c = createDerivationNode(right);
				if(rightTmp != null)
					c.setPhiMapFor(rightTmp.getVariable());
				
				c.setParent(dn);
				dn.setChild(c);
				dn.setCfgNode(cncp);
				
				dn.setActual2Formal(true);
				
				D.pi(D.D_DT, " set actual2formal adding derivation node = " + dn.toString() + " cfg node - " + cncp);
				dnList.add(dn);
			}
			}
			tpLeft = null;
		}
		else
		if(cn instanceof CfgNodeCallRet)
		{
			D.pr(D.D_TEMP, "Processing CfgNodeCallRet " + cn.toString());
			CfgNodeCallRet cncr = (CfgNodeCallRet) cn; 
			D.pr(D.D_TEMP, "Return variable for method - " + cncr.getFileName() + ":" + 
					cncr.getOrigLineno() + " " + 
					cncr.getCallNode().getCallee().getRetVar().getName());
			cncr.setRetVar(cncr.getCallNode().getCallee().getRetVar());
			D.pr(D.D_TEMP, " predec = " + cncr.getSuccessor(0));
			
			// introduce a dummy variable that assigns the return value of the 
			// called function to temporary variable in the current scope 			
			tpLeft = cncr.getTempVar();
			dn = createDerivationNode(tpLeft);
			dn.setCfgNode(cncr);
			dn.setReturnAssign();

			TacPlace tp = cncr.getCallNode().getCallee().getRetVar();
			DerivationNode c = createDerivationNode(tp);
			c.setParent(dn);
			dn.setChild(c);
			
			D.pr(D.D_TEMP, " adding return assignment - " + tpLeft + " = " + tp );
			dnList.add(dn);
			
			tpLeft = null;
		}
		else

		if(cn instanceof CfgNodeCallUnknown)
		{
			D.pr(D.D_TEMP, "Unhalded CfgNodeCallUnknown" + cn.toString());
			
		}
		else
		{
			D.pr(D.D_TEMP, " cfgToDN not handled - " + cn.toString());
		}
		
		if(tpLeft != null)		
		{
			dn = createDerivationNode(tpLeft, op);

			dn.setCfgNode(cn);
			if(children != null)
			{
				for(DerivationNode c : children)
				{
					c.setParent(dn);
					dn.setChild(c);
				}
			}
			
			dnList.add(dn);
		}*/

		return bPass;
	}

	private boolean isModifiedInRestOfLoop(Variable vAnalyze, CfgNode cn,
			ArrayList<CfgNode> cniLoop) 
	{
		boolean bModified = false; 
		boolean bSkipped = false; 
		
		if(vAnalyze == null)
			return false; 
		
		if(vAnalyze.isTemp())
			return false;
		
		if(vAnalyze.getOrig() != null)
			vAnalyze = vAnalyze.getOrig();
		
		D.pr(D.D_TEMP, " Checking if variable - " + vAnalyze +  " is modified after - " + cn );
		int iStmts = cniLoop.size();
		for(int i = 0; i < iStmts; i++)
		{
			CfgNode cfgNext = null;
			// skip stmts until cn;
			if(!bSkipped)
			{
				while(i < iStmts)
				{
					cfgNext = cniLoop.get(i);
					if(!cfgNext.equalsX(cn))
					{
						i++; continue;
					}
					else
					{
						i++; break;
					}
				}
			
				bSkipped = true;
				if(i >= iStmts)
				{
					D.pr(D.D_TEMP, "Loop does not modify the variable " + vAnalyze);
					break;
				}
				else
				{
					D.pr(D.D_TEMP, " Need to analyze stmts starting - " + 
							cniLoop.get(i) + " in loop body - " + cniLoop);
				}
			}			
			
			cfgNext = cniLoop.get(i);
			D.pr(D.D_TEMP, " analyze this - " + cfgNext);
			Variable vLHS = cfgNext.getLeft();
			if(vLHS == null)
				continue; 
			
			if(vLHS.getOrig() != null)
				vLHS = vLHS.getOrig();
			
			if(vLHS.equals(vAnalyze))
			{
				bModified = true; 
				break;
			}
		}
		
		return bModified;
	}

	public boolean isNestedLoop(CfgNodeIf cni)
	{
		if(this.loopIterStopsAt == null || 
				this.loopIterStopsAt.size() == 0 ||
				isLoopIterStops(cni))
			return false; 
		
		return true;
	}

}

class PathDecision {
	CfgNodeIf cni;

	public PathDecision(CfgNodeIf cni2, boolean thenForbid, boolean elseForbid) {
		this();
		this.cni = cni2;
		this.bElseForbidden = elseForbid;
		this.bThenForbidden = thenForbid;
	}

	public static int getForbiddenStatus(ArrayList<PathDecision> pathAnalysis,
			CfgNode node) {

		int iReturn = PathEnumerator.NOT_INITED;
		for (PathDecision pd : pathAnalysis) {
			if (pd.isPath(node)) {
				if (pd.isElseForbidden() && pd.isThenForbidden())
					iReturn = PathEnumerator.ALL_PATH;
				else if (pd.isElseForbidden())
					iReturn = PathEnumerator.ELSE_PATH;
				else if (pd.isThenForbidden())
					iReturn = PathEnumerator.THEN_PATH;
				else
					iReturn = PathEnumerator.NO_PATH;
				break;
			}
		}

		// D.pv(D.D_TEMP, " Forbidden status for node - " + node + " is - " +
		// PathEnumerator.getStatus(iReturn));
		return iReturn;
	}

	public PathDecision() {
		this.bElseForbidden = false;
		this.bThenForbidden = false;
		this.iLastPath = PathEnumerator.NOT_INITED;
	}

	/**
	 * @return the cni
	 */
	public CfgNodeIf getCni() {
		return cni;
	}

	public static ArrayList<PathDecision> dup(
			ArrayList<PathDecision> pathAnalysis) {
		ArrayList<PathDecision> ap = new ArrayList<PathDecision>();
		if (pathAnalysis == null)
			return ap;

		for (PathDecision pd : pathAnalysis)
			ap.add(pd.dup());

		return ap;
	}

	private PathDecision dup() {
		PathDecision pd = new PathDecision();
		pd.bElseForbidden = this.bElseForbidden;
		pd.bThenForbidden = this.bThenForbidden;
		pd.cni = this.cni;
		pd.iLastPath = this.iLastPath;
		return pd;
	}

	public boolean isPath(CfgNode cni) {
		return this.cni.equals(cni);
	}

	/**
	 * @param cni
	 *            the cni to set
	 */
	public void setCni(CfgNodeIf cni) {
		this.cni = cni;
	}

	/**
	 * @return the bElseForbidden
	 */
	public boolean isElseForbidden() {
		return bElseForbidden;
	}

	/**
	 * @param elseForbidden
	 *            the bElseForbidden to set
	 */
	public void setElseForbidden(boolean elseForbidden) {
		bElseForbidden = elseForbidden;
	}

	/**
	 * @return the bThenForbidden
	 */
	public boolean isThenForbidden() {
		return bThenForbidden;
	}

	/**
	 * @param thenForbidden
	 *            the bThenForbidden to set
	 */
	public void setThenForbidden(boolean thenForbidden) {
		bThenForbidden = thenForbidden;
	}

	/**
	 * @return the iLastPath
	 */
	public int getLastPath() {
		return iLastPath;
	}

	/**
	 * @param lastPath
	 *            the iLastPath to set
	 */
	public void setLastPath(int lastPath) {
		iLastPath = lastPath;
	}

	boolean bElseForbidden;
	boolean bThenForbidden;
	int iLastPath;
}