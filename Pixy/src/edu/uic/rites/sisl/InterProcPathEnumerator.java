package edu.uic.rites.sisl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
//import java.util.concurrent.CopyOnWriteArrayList;

import at.ac.tuwien.infosys.www.pixy.analysis.dep.DepGraph;
import at.ac.tuwien.infosys.www.pixy.analysis.dep.DepGraphNode;
import at.ac.tuwien.infosys.www.pixy.conversion.Cfg;
import at.ac.tuwien.infosys.www.pixy.conversion.TacFunction;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNode;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeCall;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeCallPrep;

/**
 * @author prithvi
 *
 */
/**
 * @author prithvi
 *
 */
/**
 * @author prithvi
 *
 */
/**
 * @author prithvi
 *
 */
/**
 * @author prithvi
 *
 */
/**
 * @author prithvi
 *
 */
public class InterProcPathEnumerator 
{
	private static final int RECURSION_UPPER_BOUND = 1000;
	private LinkedHashMap<ArrayList<ControlFlow>, TacFunction> pathsInFns;
	private TacFunction entryPoint; 
	private TacFunction exitPoint;
	private ArrayList<ControlFlow> interPaths;
	private LinkedHashMap<ArrayList<TacFunction>, TacFunction> invokeOrder;
	private static int stepCounter;
	private LinkedHashMap<Cfg, ArrayList<CfgNode>> allFnDepen;
	private DepGraph depGraph;
	
	public InterProcPathEnumerator(
			LinkedHashMap<ArrayList<ControlFlow>, TacFunction> pathsInFns,
			TacFunction entryPoint, TacFunction exitPoint, 
			LinkedHashMap<ArrayList<TacFunction>, TacFunction> invokeOrder, 
			LinkedHashMap<Cfg, ArrayList<CfgNode>> allFnDepen, DepGraph depGraph) 
	{
		this.entryPoint = entryPoint; 
		this.exitPoint = exitPoint; 
		this.pathsInFns = pathsInFns; 
		this.invokeOrder = invokeOrder;
		this.allFnDepen = allFnDepen;
		this.depGraph = depGraph;
	}

	public ArrayList<ControlFlow> enumerate()
	throws SISLException
	{
		D.pv(D.D_TEMP, " inter flow called ");
		ArrayList<ControlFlow> tmp = new ArrayList<ControlFlow>();
		tmp.add(new ControlFlow());
		
		InterProcPathEnumerator.stepCounter = 0;
		D.pr(" Transition_Look: starting inter-procedural flow enumeration - Entry method - " + 
				this.entryPoint.getName() + " exit method - " + this.exitPoint.getName());

		IPETracker.resetMe();
		this.interPaths = recurseInterProcPaths(tmp, this.entryPoint);
		
//		if(this.interPaths.size() <= 1)
//		{
//			for(ControlFlow cf : this.interPaths)
//				D.pr(D.D_TEMP, " interprocess flow -\n\t" + cf.toString());
//			return this.interPaths; 
//		}
		/*
		CopyOnWriteArrayList<ControlFlow> uniqPaths = 
			new CopyOnWriteArrayList<ControlFlow>();
		uniqPaths.addAll(this.interPaths);
		
		D.pr("making unique........"  + this.interPaths.size());
		this.interPaths.clear();
		
		//FIX THIS LOOP ---- doesnt terminate.... understanding of CopyOnWrite is not good :(
		ArrayList<ControlFlow> processed = new ArrayList<ControlFlow>();
		int i = 0 ;
		for(ControlFlow cf : uniqPaths)
		{
			if(!processed.contains(cf))
				processed.add(cf);
			else
				continue; 
			
			for(ControlFlow cfRest : uniqPaths)
			{
				if(processed.contains(cfRest) || ControlFlow.compareForSameNodes(cfRest, cf))
					continue; 
				
				boolean bDup = ControlFlow.compareForSameNodes(cf, cfRest);
				if(bDup)
				{
					i++;
					D.pr("Duplicates found...." + i); 
					processed.add(cfRest);
				}
			}
			this.interPaths.add(cf);
			/*
			D.pr("dups....check");
			
			boolean bT = uniqPaths.remove(cf);
			if(!bT)
				continue;
			D.pv(D.D_TEMP, " checking dups for path - " + cf + 
					" removed from list - "+ bT + " size = " + uniqPaths.size());
			
			// see if remaining ControlFlow are replica of cf;
			for(ControlFlow cfRest : uniqPaths)
			{
				D.pr("1");
				boolean bDup = ControlFlow.compareForSameNodes(cf, cfRest);
				if(bDup)
				{
					bT = uniqPaths.remove(cfRest);
					D.pr(D.D_TEMP, " Removed duplicate path \n " + cfRest + 
							" removed - " + bT + " size = " + uniqPaths.size());
				}
				else
				{
					D.pr(D.D_TEMP, " not dups - path - \n " + cfRest);
				}
			}
		//	
			D.pr("2");
			//this.interPaths.add(cf);
		}
		*/
		D.pr("Transition_Look:  reporting the inter-proc flows for the sink " +
				" --- computed in steps - (" + InterProcPathEnumerator.stepCounter + ")");
		if(this.interPaths != null)
		for(ControlFlow cf : this.interPaths)
//			D.pr(" interprocess flow -\n\t" + cf.toString());
			D.pr(" interprocess flow -\n\t" + cf.toStringIR());

			
		return this.interPaths;
	}

	public ArrayList<ControlFlow> recurseInterProcPaths(
			ArrayList<ControlFlow> inPaths, TacFunction fn)
			throws SISLException
	{		
		D.pr(D.D_TEMP, " in recurseInterProcPaths.... interproc steps = " + 
				InterProcPathEnumerator.stepCounter++ + " fn = " + fn.getName());

		IPETracker ipet = null;
		// check if we reached an indirect recursion
		if(IPETracker.isEnumerating(fn))
		{
			if(this.interPaths != null)
			for(ControlFlow cf : this.interPaths)
			{
				// enumerated paths for 
				D.pr(D.D_TEMP, " Enumerated paths so far - " + CfgNode.getProgLinesSorted(cf.getNodes()));
			}
			IPETracker.dump(this);
			
			ToolBug.add_INTERPROC_INDIRECT_RECURSION(fn, this.allFnDepen, this.interPaths);
			throw new SISLException(SISLException.BAIL_OUT, "BAIL_OUT");
		}
		else
		{
			// let the tracker know that we started enumerating this fn
			ipet = IPETracker.findTracker(fn);
			if(ipet == null)
			{
				ipet = new IPETracker(fn);
				IPETracker.trackMyRecursion(ipet);
			}
			else
			{
				if(ipet.iCount > 100 )
				{
					ToolBug.add_INTERPROC_FAILURE_ZERO_PROC_FLOW(allFnDepen, fn);
					throw new SISLException(SISLException.BAIL_OUT, "BAIL_OUT");
				}
				// we came to an already processed node...reset its status to as processing
				ipet.setProcessing();
				ipet.iCount++;
			}
			D.pr(D.D_TEMP, " Did not find a processing entry for - " + fn.getName() + 
					" now starting " + ipet.iCount);
		}
//		
//		if((++(InterProcPathEnumerator.stepCounter)) >= InterProcPathEnumerator.RECURSION_UPPER_BOUND)
//		{
//			String msg = " Inter-proc flow enumeration failed for statements - ( ";
//			for(Map.Entry<Cfg, CopyOnWriteArrayList<CfgNode>> en : this.allFnDepen.entrySet())
//			{
//				Cfg c = en.getKey();
//				CopyOnWriteArrayList<CfgNode> inNodes = en.getValue();
//				TacFunction tf = Cfg.getFunction(c.getHead());
//				String loc = tf.getFileName() + " : " + tf.getName();
//
//				msg += " \n\t\t - Function : " + loc + " stmts - " + CfgNode.getProgLines(inNodes);
//			}
//			msg += " ) ";
//			throw new SISLException(SISLException.INFINITE_RECURSION_INTER_PATH_ENUM, 
//					msg);	
//		}

		// increase the number of fns this path has traversed
		for(ControlFlow cf : inPaths)
		{
			D.pr(D.D_TEMP, " Incrementing the paths - " + cf.iNumberOfFnsTraversed + " for " + cf );
			cf.iNumberOfFnsTraversed++;
		}
		
		ArrayList<ControlFlow> interWithFn = new ArrayList<ControlFlow>();
		ArrayList<ControlFlow> intraPaths = getIntraPathsIn(fn);
		
		if(intraPaths == null)
		{
			ToolBug.add_INTERPROC_FAILURE_ZERO_PROC_FLOW(allFnDepen, fn);
			throw new SISLException(SISLException.BAIL_OUT, "BAIL_OUT");
			//throw new Error(" no nodes in intraPaths of " + fn.getName());
			// let SQL parser catch if this is a problem
//			interWithFn = inPaths;
		}

		for(ControlFlow intra : intraPaths)
		{
			D.pr(D.D_TEMP, " now processing intra path in function " + fn.getName());
			for(CfgNode cn : intra.getAll())
				D.pv(D.D_TEMP, "\t\t\t " + cn);
				
			ArrayList<ControlFlow> tmpOut = deepCopy(inPaths);
			addInterProcNodesInBetween(tmpOut, intra.getNodesBetween());
			
			D.pv(D.D_TEMP, "incoming paths ");
			for(ControlFlow in : inPaths)
			{
				D.pv(D.D_TEMP, " \t path - ");
				for(CfgNode cn : in.getAll())
					D.pv(D.D_TEMP, " \t\t\t node - " + cn);
			}

			
			ArrayList<CfgNode> visited = new ArrayList<CfgNode>();
			
			while(tmpOut.size() != 0)
			{
				ArrayList<ControlFlow> tmp = makeList(tmpOut.remove(0));
				ArrayList<ControlFlow> tmpo = new ArrayList<ControlFlow>(tmp);
				//tmpo.addAll(tmp);
				
				int index = 0;
				
				for(CfgNode cn : intra.getAll())
				{
					// somehow equality testing of two Cfg Nodes just doesnt work
					// creating an ad-hoc method...
					// following doesnt return true, even when visited contains cn.
					boolean bFound = false;
					for(CfgNode processed : visited)
					{
						if(processed.equalsX(cn))
						{
							D.pr(D.D_TEMP, ".......................skipping........." + cn);
							bFound = true;
							break;
						}
					}
					if(bFound) continue; 
					
					D.pr(D.D_TEMP, "############ adding a node to visited - " + cn + " current visited - " + visited);					
					visited.add(cn);
					
					D.pr(D.D_TEMP, "############ processing node  - " + cn);
					index = 0;
					for(ControlFlow fl : tmp)
					{
						D.pr(D.D_TEMP, "\t !!!!Flow  index = " + index + " tmp size - " + tmp.size() + " and \t\t tmp - " + fl.getProgStatement());
//						if(index < tmp.size())
						{
							tmpo.get(index).addPhiLhsSupport(cn, intra);
							tmpo.get(index).addNodeIfAbsent(cn);
//							D.pr("\t Now processing - \t\t " + tmpo.get(index));
							TacFunction invoked = expandsInterEnumeration(cn); 
							//boolean isExitNode = cn.getOrigLineno() == intra.getIntraExit().getOrigLineno();
							//if(invoked != null ) 
							//	D.pr(" intra exit node - " + intra.getIntraExit() + " current node - " + cn + " is Exit - " + isExitNode);
							if(invoked != null)
							{
								ArrayList<ControlFlow> toInvoked = new ArrayList<ControlFlow>();
								toInvoked.add(tmpo.get(tmp.indexOf(fl)));
								ArrayList<ControlFlow> expand = 
									recurseInterProcPaths(toInvoked, invoked);
								
								// keep the indices same
								int ind = tmp.indexOf(fl);
								boolean b1 = tmpo.remove(fl);
								//boolean b2 = tmp.remove(fl);
								//index--;
								//boolean b3 = false; 
								//if(!b1)
								//	b3 = tmpo.remove(tmpo.get(ind));
								//if(b1 != b2)
								//{
								//	String msg = " index = " + ind + " b1 = " + b1 + " b2 = " + b2 + " b3 = " + b3;
								//	D.pr(D.D_TEMP, " " + msg);
								//}
								// remember the newly created paths
								tmpo.addAll(expand);
							}
//							index++;
						}
					}
					
					tmp = new ArrayList<ControlFlow>(tmpo);
				}
				
				interWithFn.addAll(tmp);
				D.pr(D.D_TEMP, " in loop 1");
			}
			D.pr(D.D_TEMP, " returning 2");
		}

		D.pr(" ======================================== inter with fn returned - " + 
				fn.getName() + " processed - " + ipet.iCount);

		ipet.setProcessed();
		return interWithFn;
	}

	private void addInterProcNodesInBetween(ArrayList<ControlFlow> tmpOut,
			ArrayList<CfgNode> nodesBetween) {
		
		Cfg cfg = nodesBetween.get(0).getEnclosingFunction().getCfg();
		for(ControlFlow cf : tmpOut)
		{
			cf.addInterProcInBetween(cfg, nodesBetween);
		}
		
	}

	private ArrayList<ControlFlow> deepCopy(ArrayList<ControlFlow> inPaths) {
		ArrayList<ControlFlow> deep = new ArrayList<ControlFlow>();
		for(ControlFlow cf : inPaths)
			deep.add(ControlFlow.deepCopy(cf));
		return deep;
	}

	private TacFunction expandsInterEnumeration(CfgNode cn) 
	{		
		TacFunction cnHost = null;
		try
		{
			cnHost = cn.getEnclosingFunction();
		}
		catch(Exception e)
		{
			D.pf(D.D_TEMP, " exception while determining if this node expands inter proc paths - " + cn);
			return null;
		}
		
		TacFunction tfCalled = null;
		if(cn instanceof CfgNodeCall)
		{
			CfgNodeCall cnc = (CfgNodeCall) cn ;
			tfCalled = cnc.getCallee();

			// check if this node is really present in the dependency graph
			boolean bFound = isActualDependency(cnc);
			if(!bFound)
			{
				D.pr(D.D_TEMP, " not extending paths as this call node is not real dependency.  " + cn);
				return null;
			}

		}

		// if cn calls a function
		if(tfCalled != null)
		{			
			for(Map.Entry<ArrayList<TacFunction>, TacFunction> e : this.invokeOrder.entrySet())
			{
				if(e.getValue().equals(cnHost))
				{
					if(e.getKey().contains(tfCalled))
					{
//						// check if tfCalled is being expaned
//						boolean bRecursion = IPETracker.isEnumerating(tfCalled);
//						if(bRecursion)
//							throw new Error(" hail recursion!!!");
						
						D.pr(D.D_TEMP, " expanding inter-flow analysis with paths from " + 
								tfCalled.getName());
						return tfCalled;
					}
				}	
			}
		}
		
		D.pv(D.D_TEMP, " for node - " + cn + " did not find inter-flow expansion - ");
		return null;
	}

	private ArrayList<ControlFlow> makeList(ControlFlow memb) 
	{
		ArrayList<ControlFlow> newList = new ArrayList<ControlFlow>();
		newList.add(memb);
		return newList;
	}


	private ArrayList<ControlFlow> getIntraPathsIn(TacFunction fn) 
	{
		for(Map.Entry<ArrayList<ControlFlow>, TacFunction> e : this.pathsInFns.entrySet())
		{
			if(e.getValue().equals(fn))
			{
				return e.getKey();
			}	
		}
		return null;
	}

	
	private boolean isActualDependency(CfgNodeCall cnc) 
	{
		if(cnc == null)
			return false; 
		
		CfgNode cnPred = cnc.getPredecessor();
		if(cnPred != null)
		{
			if(!(cnPred instanceof CfgNodeCallPrep))
			{
				List<CfgNode> preds =cnc.getPredecessors();
				if(preds != null)
				for(CfgNode c : preds)
				{
					if(c instanceof CfgNodeCallPrep)
					{
						cnPred = c;
						break;
					}
				}
			}
		}
		
		boolean bFound = false;
		// ensure that there was a call to invoked in the dep graph. 
		// because of extending the cfg nodes, we have spurious calls
		// in longer run need to correct the algo.
		// int iExitL = exitT.getOrigLineno();
		List<CfgNode> allRealDeps = this.depGraph.getAllCfgNodes(this.depGraph.getNodes(), true);
		D.pr(D.D_PATH, " InterProc all nodes in depGraph - " + allRealDeps);
		D.pr(D.D_TEMP, "InterPRoc check Checking if this exit was present in the dep nodes - " + cnc);
		// unfortunately following doesnt work/ equality in CfgNode? 
		//if(allRealDeps.contains(cn)) 
		for(CfgNode cnd : allRealDeps)
		{
			if(cnd.equalsX(cnc) || cnd.equalsX(cnPred))
			{								
				bFound = true;
				break;
			}			
			// check if its predecessor is prepare node and it is in the dep
			
		}
		
		
		
		return bFound;
	}

	/* 
	 * @param depGraph: dependency graph for sink in sp
	 * @param sp  
	 * @return A list of control flows that compute and execute query at sink point sp, 
	 * guided by the dependency graph. 
	 * @throws SISLException If path enumeration fails 
	 */
	public static ArrayList<ControlFlow> enumeratePaths(DepGraph depGraph, SinkPath sp) 
	throws SISLException
	{
		TimeProfiler.start();
		
		TimeProfiler.ms();

		CfgNode sink = depGraph.getRoot().getCfgNode();
		ArrayList<TacFunction> fnsInvoked = new ArrayList<TacFunction>();
    	
		List<DepGraphNode> ssaNodes = depGraph.getNodes();//getSSANodes();

		// get the functions traversed by the dependency graph
//	    	CopyOnWriteArrayList<CfgNode> cfgNodesTraversed = DepGraph.getAllCfgNodesExtended(ssaNodes);
    	ArrayList<CfgNode> cfgNodesTraversed = DepGraph.getAllCfgNodesExtended(ssaNodes);    	
		ArrayList<Cfg> cfgFuncTraversed = DepGraph.getFunctions(cfgNodesTraversed);
    	
		// include special dummy nodes for formals as well. see bForcedProcess variable 
		// and its documentation
		DepGraph.addAlwaysProcessNodes(cfgFuncTraversed, cfgNodesTraversed);
		TimeProfiler.me(D.D_INTRA, 1);
		
		TimeProfiler.ms();

		LinkedHashMap<Cfg, ArrayList<CfgNode>> 
			allFnDepen = new LinkedHashMap<Cfg, ArrayList<CfgNode>>();
		
		D.pr(" Transition_Look2: Enumerating paths for sink - " + sink + "\n \t \t dependencies...........");
		
		// for each CFG traversed in this dependency graph, identify statements in the dep graph 
		for(Cfg cfg : cfgFuncTraversed)
		{
			ArrayList<CfgNode> depNodes = DepGraph.getNodesInDepd(cfg, cfgNodesTraversed);
			allFnDepen.put(cfg, depNodes);
			TacFunction tf = Cfg.getFunction(cfg.getHead());
			if(!fnsInvoked.contains(tf))
			{
				fnsInvoked.add(tf);
				D.pr(" \n \t \t \t in function - " + tf.getName() + " : " 
						+ CfgNode.getProgLines(depNodes));
			}
		}

		// figure out caller-callee relationships
		LinkedHashMap<ArrayList<TacFunction>, TacFunction> invokeOrder = 
			getInvokeOrder(allFnDepen, fnsInvoked, sink);
		
		TimeProfiler.me(D.D_INTRA, 2);
		
		TimeProfiler.ms();
		TacFunction entryPoint = null;
		// getEntryPoint(allFnDepen, fnsInvoked, sink, invokeOrder);
		
		// identify functions that are top level callers in the caller-callee relations
		ArrayList<TacFunction> entryPoints = getEntryPointAll(allFnDepen, fnsInvoked, sink, invokeOrder);
		TacFunction exitPoint = sink.getEnclosingFunction();
		
		LinkedHashMap<TacFunction, ArrayList<TacFunction>> inOrder = 
			new LinkedHashMap<TacFunction, ArrayList<TacFunction>>();
		
		//if(invokeOrder.size() < 10)
			//throw new SISLException(SISLException.BAIL_OUT, "BAIL");
		D.pr(D.D_TEMP, " size of invorder - " + invokeOrder.size() + " sink hoster - " + exitPoint.getName());
		//if(invokeOrder.size() > 0)
		{
			String str = " exit - " + exitPoint.getName();
			str += " Entry functions - [ " ; 
			for(TacFunction t : entryPoints)
				str += " " + t.getName();
			str += " ] ";
			D.pr(D.D_TEMP, str);
			
			for(Map.Entry<ArrayList<TacFunction>, TacFunction> e : invokeOrder.entrySet())
				inOrder.put(e.getValue(), e.getKey());
		
			ArrayList<TacFunction> toExpand = new ArrayList<TacFunction>();
			if(entryPoints.size() != 1)
			{
				toExpand.addAll(entryPoints);
				str = "";
	
				while(toExpand.size() > 0)
				{
					TacFunction inv = toExpand.remove(0);
					
					ArrayList<TacFunction> invoked = inOrder.get(inv);
					if(invoked == null || invoked.size() == 0)
						continue;
					
					str += inv.getName() + " ----invokes--> ";
	
					if(invoked != null && invoked.size() != 0)
					for(TacFunction tf : invoked)
					{
						str += " " + tf.getName();
						toExpand.add(tf);
					}
					str += " \n";
				}
				D.pr(D.D_TEMP, str);
				entryPoint = resolveEntryPoint(entryPoints, inOrder, exitPoint);
				if(entryPoint == null)
				{
					ToolBug.addUNRESOLVED_ENTRY_POINT(inOrder, entryPoints, exitPoint, str);
					throw new SISLException(SISLException.BAIL_OUT, "BAIL_OUT");
				}
				D.pr(D.D_TEMP, "----------- resolved entry point as " + entryPoint.getName());
			}
			else
			{
				entryPoint = entryPoints.get(0);
			}
			
			for(Map.Entry<ArrayList<TacFunction>, TacFunction> e : invokeOrder.entrySet())
			{	
				
				D.pr(D.D_TEMP, " Transition_Look: Function - " + e.getValue().getName() + " invokes - ");
					for(TacFunction inv : e.getKey())
				{
					D.pr(D.D_TEMP, " \t\t\t Function - " + inv.getName());
						if(exitPoint.equals(inv))
						D.pr(D.D_TEMP, " \t\t\t\t $$$$$$$$$$$$$$$ hosts Sql sink ");
				}
				if(exitPoint.equals(e.getValue()))
					D.pr(D.D_TEMP, " $$$$$$$$$$$$$$$ hosts Sql sink ");
			}
			
		}
//			if(1 == 1)
	//		return null;
		
		TimeProfiler.me(D.D_INTRA, 3);
		
		LinkedHashMap<ArrayList<ControlFlow>, TacFunction> pathsInFns = 
			new LinkedHashMap<ArrayList<ControlFlow>, TacFunction>();
		ArrayList<ControlFlow> flows = null;
		for(Map.Entry<Cfg, ArrayList<CfgNode>> en : allFnDepen.entrySet())
		{
			Cfg c = en.getKey();

			D.pr(" Transition_Look: Now getting paths in fn - " + Cfg.getFunction(c.getHead()).getName());
			ArrayList<CfgNode> inNodes = en.getValue();			
			flows = getPathsInCfg(c, inNodes, invokeOrder, sink, entryPoint, depGraph);
			pathsInFns.put(flows, Cfg.getFunction(c.getHead()));
		}
		
			
		for(Map.Entry<ArrayList<ControlFlow>, TacFunction> e : pathsInFns.entrySet())
		{
			TacFunction tf = e.getValue();
			D.pr(D.D_TEMP, " Transition_Look: flows in function - " + tf.getName());
			int i = 0;
			for(ControlFlow cf : e.getKey())
				D.pr(D.D_TEMP, " Flow " + (i++) + " \n \t\t " + CfgNode.getProgLines(cf.getAll()));
		}

		TimeProfiler.end(D.D_INTRA);
		
		TimeProfiler.start();
		//
		InterProcPathEnumerator	ippe = new InterProcPathEnumerator(pathsInFns, 
				entryPoint, exitPoint, invokeOrder, allFnDepen, depGraph);
		ArrayList<ControlFlow> interPaths = ippe.enumerate();
		D.pr("Transition_Look: returned total paths - " + (interPaths == null? 0 : interPaths.size()));

		TimeProfiler.end(D.D_INTER);
		return interPaths;
	}

	/*
	 * @param c: CFG that contains inNodes we want to find flows in 
	 * @param inNodes: CFG nodes that also in depGraph
	 * @param invokeOrder: map containing list of functions invoked by each function
	 * @param sink: SQL query execution statement
	 * @param entryPoint: first function in the call sequence
	 * @param depGraph: dependency graph for the sink
	 * 
	 * @return a list of control flows found in inNodes of cfg c
	 */
	private static ArrayList<ControlFlow> getPathsInCfg(Cfg c, ArrayList<CfgNode> inNodes, 
			LinkedHashMap<ArrayList<TacFunction>, TacFunction> invokeOrder, CfgNode sink, 
			TacFunction entryPoint, DepGraph depGraph) 
			throws SISLException
	{
		TacFunction inFunction = Cfg.getFunction(c.getHead());
		D.pd(D.D_TEMP, " Function -  " + inFunction.getName());
//		for(CfgNode stmt : inNodes)
//		{
//			printHelper(stmt);
//		}

//		ArrayList<CfgNode> succs = new ArrayList<CfgNode>();	
		ArrayList<ControlFlow> flows = PathEnumerator.reuseOld(c, inNodes);
		if(flows == null)
		{
			PathEnumerator pe = new PathEnumerator(c, inNodes, invokeOrder, sink, entryPoint, depGraph);
			flows = pe.enumerate();
			PathEnumerator.addPaths2Reuse(inNodes, flows);
		}
		
		return flows;
		//	while()
	}

	private static boolean isInCallingSequence(TacFunction exitPoint,
			TacFunction potentialEntry,
			LinkedHashMap<TacFunction, ArrayList<TacFunction>> inOrder) 
	{
		ArrayList<TacFunction> reachable = new ArrayList<TacFunction>();
		reachable.add(potentialEntry);

		ArrayList<TacFunction> worklist = new ArrayList<TacFunction>();
		worklist.add(potentialEntry);

		while(worklist.size() != 0)
		{
			TacFunction t = worklist.remove(0);
			List<TacFunction> called = inOrder.get(t);
			if(called != null && called.size() != 0)
			{
				for(TacFunction c : called)
				{
					if(!reachable.contains(c))
					{
						worklist.add(c);
						reachable.add(c);
					}
				}

			}
		}
		
		return reachable.contains(exitPoint);
	}

	
	private static TacFunction resolveEntryPoint(ArrayList<TacFunction> entryPoints,
			LinkedHashMap<TacFunction, ArrayList<TacFunction>> inOrder,
			TacFunction exitPoint) 
	{
		// resolve in favor of first entry point that reaches sink
		for(TacFunction potentialEntry : entryPoints)
		{
			boolean bReachesSink = isInCallingSequence(exitPoint, potentialEntry, inOrder);
			if(bReachesSink)
			{
				return potentialEntry; 
			}
		}
		return null;
	}

	
	private static ArrayList<TacFunction> getEntryPointAll(
			LinkedHashMap<Cfg, ArrayList<CfgNode>> allFnDepen,
			ArrayList<TacFunction> fnsInvoked, CfgNode sink, 
			LinkedHashMap<ArrayList<TacFunction>, TacFunction> invokeOrder) 
//	throws SISLException
	{
		ArrayList<TacFunction> fns = new ArrayList<TacFunction>(fnsInvoked);
		
		for(Map.Entry<Cfg, ArrayList<CfgNode>> en : allFnDepen.entrySet())
		{
			ArrayList<CfgNode> inNodes = en.getValue();
			//TacFunction tfCurrent = Cfg.getFunction(inNodes.get(0));
			// remove fns from fnsInvoked that are called in current cfg
			// finally we will end with the top level caller
				
			ArrayList<TacFunction> invoked = removeFnsInvokedIn(inNodes, fns, sink);
			if(invoked != null && invoked.size() > 0)
				fns.removeAll(invoked);
		}

//		TacFunction tfEntry = fnsInvoked.get(0);
/*		boolean bResolveEntry = false; 
		if(fnsInvoked.size() != 1)
		{
			
			String msg = " Possible starters - ( ";

			for(TacFunction tf : fnsInvoked)
			{
				D.pd(D.D_TEMP, " \t\t\t Entry method - " + tf.getName());
				msg += tf.getName() + ", ";
				// break in favor of _main ---> as only reachable sinks 
				// are being analyzed
				// TODO: there is problem in the code that causes these 
				// multiple flows to be identified...
				if(tf.getName().equals("_main"))
				{
					tfEntry = tf;
					bResolveEntry = true; 
					break;
				}
				else
				{
					// otherwise remove this Cfg and its dep
					//allFnDepen.remove(tf.getCfg());
				}
			}
			msg += " ) ";
			
			/* still not resolved
			if(!bResolveEntry)
			{
				for(TacFunction tf : fnsInvoked)
				{
					
				}
			}*/
/*			
			if(!bResolveEntry)
			{
				msg += " \n Details -------------------------- ";
				for(Map.Entry<Cfg, CopyOnWriteArrayList<CfgNode>> en : allFnDepen.entrySet())
				{
//				CopyOnWriteArrayList<CfgNode> inNodes = en.getValue();
					ArrayList<CfgNode> inNodes = new ArrayList<CfgNode>(en.getValue());
					TacFunction tfCurrent = Cfg.getFunction(inNodes.get(0));
					msg += " \n function - " + tfCurrent.getName() + " stmts - \n \t " + 
						CfgNode.getProgLinesSorted(inNodes);
				}

			//	throw new SISLException(SISLException.NOT_REACHABLE_FROM_MAIN, 
			//			msg);
			}
		}
*/		
		return new ArrayList<TacFunction>(fns);
	}

	
	private static ArrayList<TacFunction> removeFnsInvokedIn(
			ArrayList<CfgNode> inNodes,
			ArrayList<TacFunction> fnsInvoked,
			CfgNode sink) 		
	{	
		TacFunction tfCurrent = Cfg.getFunction(inNodes.get(0));
		
		ArrayList<TacFunction> invoked = new ArrayList<TacFunction>();

		for(CfgNode cn : inNodes)
		{
			if(cn instanceof CfgNodeCall)
			{
				CfgNodeCall cnc = (CfgNodeCall) cn ;
				TacFunction tfCallee = cnc.getCallee();
				if(fnsInvoked.contains(tfCallee) && !tfCurrent.equals(tfCallee))
				{
					if(!tfCallee.getName().equals("_main"))
					{
						invoked.add(tfCallee);
						//fnsInvoked.remove(tfCallee);
					}
					D.pf(D.D_TEMP, " removed function from list - " + tfCallee.getName());
				}
			}
		}
		
		return invoked;
	}

	
	private static LinkedHashMap<ArrayList<TacFunction>, TacFunction> getInvokeOrder(
			LinkedHashMap<Cfg, ArrayList<CfgNode>> allFnDepen,
			ArrayList<TacFunction> fnsInvoked, CfgNode sink)
	{
		LinkedHashMap<ArrayList<TacFunction>, TacFunction> invokeOrder = 
			new LinkedHashMap<ArrayList<TacFunction>, TacFunction>();

		ArrayList<TacFunction> fnsInvokedT = new 
			ArrayList<TacFunction> (fnsInvoked);
		
		ArrayList<TacFunction> invoked = null;
		for(Map.Entry<Cfg, ArrayList<CfgNode>> en : allFnDepen.entrySet())
		{
			Cfg c = en.getKey();
			ArrayList<CfgNode> inNodes = en.getValue();
			invoked = removeFnsInvokedIn(inNodes, fnsInvokedT, sink);
			if(invoked != null && invoked.size() > 0)
				invokeOrder.put(invoked, Cfg.getFunction(c.getHead()));
		}
		
		return invokeOrder;
	}
}

class IPETracker
{
	TacFunction tf; 
	int iStatus; 
	int iCount; 
	
	private static ArrayList<IPETracker> allInterProcs; 
	
	public IPETracker(TacFunction tf)
	{
		this.tf = tf;
		this.iStatus = -1;
		
		if(allInterProcs == null)
			allInterProcs = new ArrayList<IPETracker>();
	}
	
	public static void resetMe() {
		allInterProcs = null;
	}

	public void setProcessing() {
		this.iStatus = -1;
	}

	public static void dump(InterProcPathEnumerator ippe) {

		String str = " InterProcTracker found indirect recursion \n";
		str += " status of all enumerated functions - ";
		for(IPETracker ipet : IPETracker.allInterProcs)
		{
			str += " \t " + ipet + " \n";
		}
		
		D.pr(D.D_TEMP, str);
		//throw new Error(str);
	}

	public static void trackMyRecursion(IPETracker ipet) {
		allInterProcs.add(ipet);
	}

	public static IPETracker findTracker(TacFunction tf)
	{
		if(allInterProcs == null)
			return null;
		
		for(IPETracker ipet : allInterProcs)
		{
			if(ipet.tf.equals(tf))
				return ipet; 
		}
		return null;
	}
	
	public void addInterProc(TacFunction tf)
	{
		IPETracker ipet = IPETracker.findTracker(tf);
		if(ipet == null)
		{
			ipet = new IPETracker(tf);
		}
		
		ipet.iCount ++;
	}
	
	public void setProcessed()
	{
		this.iStatus = 1;
	}
	
	public static boolean isEnumerating(TacFunction tf)
	{
		IPETracker ipet = findTracker(tf);
		if(ipet == null)
			return false; 
		
		if(ipet.iStatus == -1)
			return true; 
		
		return false; 
	}
	
	public String toString()
	{
		String str = "";
		str += " IPETracker stat - " + this.tf.getName();
		str += " Status = " + (this.iStatus == -1 ? " Processing " :  " Processed");
		return str; 
	}

}