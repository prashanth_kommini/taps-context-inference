/**
 * 
 */
package edu.uic.rites.sisl;

// import at.ac.tuwien.*;
import at.ac.tuwien.infosys.www.phpparser.*;
import at.ac.tuwien.infosys.www.pixy.Dumper;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.*;
import edu.uic.rites.sisl.*;

import java.util.*;

import at.ac.tuwien.infosys.www.pixy.conversion.*;
/**
 * @author prithvi
 *
 */
public abstract class DerivationNode 
{
	public static final int TERMINAL = 1;
	public static final int NONTERMINAL = 2;
	
	public static int nodeCounter;
	public int nodeNumber;
	
	
	protected CfgNode progNode;
	protected ArrayList<DerivationNode> children;
	protected DerivationNode leftSibling; 
	protected DerivationNode rightSibling; 
	protected DerivationNode parent;
		
	protected TacPlace id;
	/**
	 * @param id the id to set
	 */
	public void setId(TacPlace id) {
		this.id = id;
	}

	protected int type; 
	protected String operator; 

	// query offsets 
	private int iBeginOffset;
	private int iEndOffset; 
	
	protected boolean isNumber; 
	public boolean isNumberLiteral(){ return isNumber; }
	public void setNumberLiteral() { isNumber = true; }

	private boolean isData;
	public void setData(){ isData = true;}
	public boolean isData(){return isData;}
	public int getBeginOffset() { return iBeginOffset; }
	public int getEndOffset() { return iEndOffset; }
	
	public String getOperator(){ return operator; }
	public void setOperator(String op){ operator = op;}
	
	abstract DerivationNode dup();
	public void setType(int type){ type = type; }
	public void setCfgNode(CfgNode cfn) { progNode = cfn;}
	public void setImmPreds(DerivationNode pred) { leftSibling = pred;}
	public void setImmSuccs(DerivationNode succ) { rightSibling = succ;}
	public void setParent(DerivationNode parent){ this.parent = parent;}
	public void setChildren(ArrayList<DerivationNode> children) {this.children = children;}
	public void setChild(DerivationNode child)
	{
		if(children == null)
		{
			children = new ArrayList<DerivationNode>();
		}
		children.add(child);
	}
	
	public int getType() {return type;}
	public CfgNode getCfgNode() { return progNode; }
	public DerivationNode getImmPred() { return leftSibling; } 
	public DerivationNode getImmSucc() { return rightSibling; }
	public DerivationNode getParent() { return parent ; }
	public ArrayList<DerivationNode> getChildren() { return children; } 
	public boolean isNT() { return (type == NONTERMINAL) 
		//&& 
		//(children != null && !children.isEmpty())
		; }
	public TacPlace getId(){ return id; }
	
    // returns a name that can be used in dot file representation
    public String dotName() 
    {
    	D.pv(D.D_DT, " Dot name --- for cfg -  " + this.progNode);

    	String str = ((type == NONTERMINAL)? "NONT": "TERM") + "\\n";
    	str += " number = " + this.nodeNumber + " \\n";
    	
    	str += Dumper.escapeDot(this.id.toString(), 0);
    	if(operator != null)
    		str += " \\n Operator: " + operator;

    	if(this.phiValue != null)
    		str += " \\n Phi lhs = " + this.phiValue;

    	//str += " \\n number : " + isNumber; 
    	
    	if(this.progNode != null)
    	{
    		str += "\\n" + this.progNode.getFileName() + 
    				" (" + this.progNode.getOrigLineno() + ")";
    	}
    	
/*    	if(this.progNode != null && this.progNode.getPlaceHolderMaps() != null)
    	{
    		str += "\\n ? maps - { ";
    		for(TacPlace tp : progNode.getPlaceHolderMaps())
    			str += tp.toString() + " , ";	
    		str += "}";
    	}
 */
    	if(this.id.isVariable() && ! this.id.getVariable().isTemp())
    	{
	    	if(this.questionMaps != null)
	    	{
	    		str += "\\n " + this.getArgsVar() + " = { ";
	    		for(TacPlace tp : this.questionMaps)
	    			str += tp.toString() + " , ";	
	    		str += "}";
	
	    	}
    	}
    	
    	return str;
    }

    public String dotNameShort() 
    {
    	D.pv(D.D_DT, " Dot name for cfg -  " + this.progNode);
    	String str = ((type == NONTERMINAL)? "NONT": "TERM") + "\\n";
    	str += " number = " + this.nodeNumber + " \\n";
        str += Dumper.escapeDot(this.id.toString(), 0);
    	if(operator != null)
    		str += " \\n Operator: " + operator;

    	if(this.phiValue != null)
    		str += " \\n Phi lhs = " + this.phiValue;
    	
    	if(this.id.isVariable() && ! this.id.getVariable().isTemp())
    	{
	    	if(this.questionMaps != null)
	    	{
	    		str += "\\n " + this.getArgsVar() + " = { ";
	    		for(TacPlace tp : this.questionMaps)
	    			str += tp.toString() + " , ";	
	    		str += "}";
	
	    	}
    	}    	
    //	str += " \\n number : " + isNumber; 
    	if(this.progNode != null)
    	{
    		String fileName = this.progNode.getFileName();
        	str += "\\n" + fileName.substring(fileName.lastIndexOf('/')+1) + 
        	" (" + this.progNode.getOrigLineno() + ")";

//        	if(this.progNode.getPlaceHolderMaps() != null)
//	    	{
//	    		str += "\\n ? maps - { ";
//	    		for(TacPlace tp : progNode.getPlaceHolderMaps())
//	    			str += tp.toString() + " , ";	
//	    		str += "}";
//	    	}        
        	
	    	if(this.progNode.getActual2FormalArgs() != null)
	    	{
	    		str += "\\n args to pass - { ";
				for (Map.Entry<TacPlace, TacPlace> a2f : progNode.getActual2FormalArgs().entrySet())
				{
					str += a2f.getKey().toString() + " -> " + a2f.getValue() + " , ";
				}
				str += " } ";
	    	}

	    	if(this.progNode.getReturnValue() != null)
	    	{
	    		str += "\\n return value to receive { ";
	    		for(Map.Entry<TacPlace, TacPlace> returnV : progNode.getReturnValue().entrySet())
	    			str += returnV.getKey().toString() + " <-- " + returnV.getValue().toString();
	    		str += " } ";
	    	}
    	}
    	
        return str;
    }

    public String toString(String pad)
    {
    	String str = pad + ((type == NONTERMINAL)? "NT": "T") + "\t";
    	str += id.toString() + "\n";	
    	str += pad + " ==== Cfg node - " + ((progNode != null)?(progNode.toString()):("null")) + "\n";
    	if(leftSibling != null)
    	{
    		str += pad + " ++++ left sibling = " + leftSibling.getId() + "\n";
    	}
    	
    	if(rightSibling != null)
    	{
    		str += pad + "++++ right sibling = " + rightSibling.getId() + "\n";
    	}

		str += pad + "........ parent = " + (parent != null ? parent.getId() : "null") + "\n";
    	if(children != null)
    	{
    		for(DerivationNode dn : children)
    		{
    			str += pad + " child: " + dn.toString(pad + "$$$$$$$$") + "\n";
    		}
    	}

    	return str;
    }
    public String toString()
    {
    	String str = ((type == NONTERMINAL)? "NT": "T") + "\t";
    	str += id.toString()  + "\n";	
    	str += " ==== Cfg node - " + ((progNode != null)?(progNode.toString()):("null")) + "\n";
    	if(leftSibling != null)
    	{
    		str += " ++++ left sibling = " + leftSibling.getId();
    	}
    	
    	if(rightSibling != null)
    	{
    		str += "++++ right sibling = " + rightSibling.getId();
    	}

    	if(parent != null)
    		str += "........ parent = " + parent.getId() + "\n";
    	if(children != null)
    	{
    		for(DerivationNode dn : children)
    		{
    			str += " \t child: " + dn.toString();
    		}
    	}

    	str += " \t number : " + isNumber;
    	
    	return str;
    }

    //abstract DerivationNode createDerivationNode(TacPlace tp);
    
    public void setBranches(ArrayList<DerivationNode> dnl, ArrayList<CfgNode> cfgs)
    throws SISLException
	{
		D.pr(D.D_TEMP, "Setting the branches for node - " + this.id);
	
		if(DerivationTree.visitedIds.contains(this))
		{
			CfgNode cn = this.getCfgNode();
			String str =  " Bug in PhiLHs map - --------- ";
			str += (cn == null? "null" : cn.toString()); 
			if(cn != null)
				str = CfgNode.getProgLine(cn);
			
			str += "\n originated from this flow - \n";
			for(String strC : CfgNode.getProgLines(cfgs))
			{
				str += " \t " + strC;
			}
			
			
			D.pr(D.D_TEMP, "Trying to set branches for a node that has already been visited " + str);
			SISLException sisl = new SISLException(SISLException.BAIL_OUT, 
					str); 
			ToolBug.add_UNCLASSIFIED_YET(sisl);
			throw sisl;		
		}
		else
		{
			DerivationTree.visitedIds.add(this);
		}
			
		ArrayList<DerivationNode> children = this.children;
		if(children != null)
		{
			for(DerivationNode c : children)
				D.pv(D.D_TEMP, " \t\t child - " + c.getId());
			
			for(DerivationNode child : children)
			{
				child.setParent(this);

				//D.pd(D.D_TEMP, "getting DN for - " + child.getId());
				DerivationNode dnchild = getDfn(child.getId(), dnl);
				D.pd(D.D_TEMP, "DN for " + child.getId() + " .....found....." + 
						(dnchild != null ? (dnchild.getId()): " null "));
				
				// TODO: better handling
				// member variables are represented as _special.member in the CFG
				// need to add proper support. until then, avoid infinite recursion here
				if(dnchild != null && dnchild.getId() != null && dnchild.getId().equals(this.getId()))
				{
					D.pv(D.D_TEMP, " breaking the recursion - " + child.getId() + " = " + this.getId());
					return;
				}
					
				if(dnchild == null)
				{
					// null for literals and costants
					// set its parent
				}
				else
				{
					child.setCfgNode(dnchild.getCfgNode());
					child.setOperator(dnchild.getOperator());
					if(dnchild.isActual2Formal())
							child.setActual2Formal(true);
					if(dnchild.isReturnAssign())
							child.setReturnAssign();
					
					ArrayList<DerivationNode> oldChildren = dnchild.getChildren();
					if(oldChildren != null)
					{
						ArrayList<DerivationNode> newChildren = new ArrayList<DerivationNode>();
						
						for(DerivationNode tmpChild : oldChildren)
						{
							DerivationNode replica = tmpChild.dup();
							replica.setParent(child);
							newChildren.add(replica);
						}
						
						child.setChildren(newChildren);
					}
					//dnl.remove(dnchild);
				}
				
				D.pv(D.D_TEMP, " child is NT ? " + child.isNT());
				if(child.isNT())
				{
					D.pv(D.D_TEMP, " setting the branches of child " + child.getId());
					child.setBranches(dnl, cfgs);
				}
			}
		}				
	}

	public static DerivationNode getDfn(TacPlace id, ArrayList<DerivationNode> dnl)
	{
		CfgNode cn = null;
		DerivationNode dnRet = null;
		for(DerivationNode dn : dnl)
		{
			if(dn.getId().equals(id))
			{
				dnRet = dn;
				break;
			}
		}

		D.pv(D.D_DT, "$$$$$$$$$$$$ matched - " + id + " with - " + 
				((dnRet == null)? ".......NULL.....":dnRet.getId()));

		return dnRet;
	}
	
	public ArrayList<DerivationNode> getLeavesInOrder()
	{
		ArrayList<DerivationNode> leaves = new ArrayList<DerivationNode> ();
		
		if(children != null)
		{
			for(DerivationNode child : children)
			{
				if(child.isNT())
					leaves.addAll(child.getLeavesInOrder());
				else
					leaves.add(child);
			}
		}
		return leaves;
	}
	
	
	// declare all variables that dont have any children as leaves.
	public void adjustLeafStatus()
	{
		if(children != null && !children.isEmpty())
		{
			for(DerivationNode child : children)
				child.adjustLeafStatus();
		}
		else
			this.type = this.TERMINAL;
	}

	public ArrayList<DerivationNode> getBfOrder()
	{
		ArrayList<DerivationNode> nodes = new ArrayList<DerivationNode>();
		
		//nodes.add(this);
		if(children != null)
		{
			nodes.addAll(children);
			
			for(DerivationNode child : children)
				nodes.addAll(child.getBfOrder());
		}
		
		return nodes;
	}

	public ArrayList<DerivationNode> getInOrder()
	{
		ArrayList<DerivationNode> nodes = new ArrayList<DerivationNode>();
		
		if(children != null && children.size() >= 1)
		{
			nodes.addAll(children.get(0).getInOrder());
		}
		
		nodes.add(this);
		
		if(children != null && children.size() > 1)
		{
			for(DerivationNode child : children)
				if(!child.equals(children.get(0)))
					nodes.addAll(child.getInOrder());
		}
		
		return nodes;
	}

	
	public void setQueryOffsets(int begin, int end)
	{
		iBeginOffset = begin; 
		iEndOffset = end;
	}
	
	public boolean containsQueryOffset(int offset)
	{
		boolean bRet = false; 
		if(offset >= iBeginOffset && offset <= iEndOffset)
			bRet = true;
		
		D.pd(D.D_DT, bRet + " ::: contains query - node_begin = " + iBeginOffset + 
				" node_end = " + iEndOffset +
				" query offset - " + offset + " " + " dn = " + this.dotName());
		return bRet; 
	}

	public boolean embedsTokenWithCode(int tokenBegin, int tokenEnd)
	{
		boolean bRet = false; 
		if(tokenBegin >= iBeginOffset && tokenEnd <= iEndOffset && 
				(iEndOffset - iBeginOffset) > (tokenEnd - tokenBegin))
			bRet = true; 

		D.pd(D.D_DT, "embedsWithCode???? " + bRet + " token begin - " + tokenBegin + " token end - " + 
				tokenEnd + " in node: begin - " + iBeginOffset + " end - " + 
				iEndOffset + " node - " + this.dotName());
		return bRet;
	}

	
	public boolean embedsToken(int tokenBegin, int tokenEnd)
	{
		boolean bRet = false; 
		if(tokenBegin >= iBeginOffset && tokenEnd <= iEndOffset)
			bRet = true; 

		D.pd(D.D_DT, "embedsToken???? " + bRet + " token begin - " + tokenBegin + " token end - " + 
				tokenEnd + " in node: begin - " + iBeginOffset + " end - " + 
				iEndOffset + " node - " + this.dotName());
		return bRet;
	}
	
	public void markDataContributions(int tokenBegin, int tokenEnd)
	{
		boolean begins = false, ends = false, contained = false; 
		
		if(tokenBegin > iBeginOffset && tokenBegin <= iEndOffset)
			begins = true;
		
		if(tokenEnd >= iBeginOffset && tokenEnd <= iEndOffset)
			ends = true; 
		
		if((tokenBegin < iBeginOffset && tokenEnd > iEndOffset) || 
				(tokenBegin == iBeginOffset && tokenEnd > iEndOffset)) 
				//|| 
//				(tokenEnd == iEndOffset && tokenBegin < iBeginOffset))
			contained = true; 
		
		// if this node contains partial value of token, then, 
		// it also contains some code.
		if(begins || ends)
			this.isData = false; 
		
		// pure data
		if(contained || embedsToken(tokenBegin, tokenEnd))
			this.isData = true;
		
		// to enable the data subtree finding algorithm...
		if(this.id.isLiteral())
		{
			String str = ((Literal)this.id).getStringValue();
			str = str.trim();
			if(str.equals("'") || str.equals("\""))
				this.isData = false;
		}
		

		D.pd(D.D_TEMP, "Node - " + this.dotName() + " begin = " + iBeginOffset + " end - " + 
				iEndOffset + " token begin - " + tokenBegin + " token end - " + tokenEnd);
		D.pd(D.D_TEMP, "is data = " + isData);
	}
	
	private boolean bDataSubtree;
	public void setDataSubtree(){ bDataSubtree = true; }
	public boolean getDataSubtree() { return bDataSubtree; }
	
	private boolean bBorrow; 
	public void setBorrowee(){ bBorrow = true;}
	public boolean isBorrowee() { return bBorrow;}
	
	private boolean bNewNode; 
	public void setNewNode(){ bNewNode = true;}
	public boolean isNewNode() { return bNewNode;}

	private boolean bPlaceholderNode;
	public void setQuestionMark() { bPlaceholderNode = true;}
	public boolean isQuestionMark() { return bPlaceholderNode; }
	
	private String donateToken;
	public Literal getDonated()
	{
		if(donateToken == null)
		{
			throw new Error("getDonated() must be preceeded by isLeftDoner()");
		}
		Literal l = new Literal(donateToken);
		if(l == null)
			throw new Error(" null literal donation - what a miser!!!");
		
		return l;
	}
	
	public boolean isLeftDoner(int tokenBegin, int tokenEnd)
	throws SISLException
	{
		boolean bLeftDoner = false; 
		if(this.type != this.TERMINAL && 
			!(this.id instanceof Literal))
			throw new Error(" A non terminal non-literal cannot be a donor !!!" + this.toString());

		if(tokenBegin >= this.iBeginOffset && tokenBegin <= this.iEndOffset)
		{
			bLeftDoner = true; 
		}
		D.pvt(D.D_DT, "tok begin = " + tokenBegin + " tok end = " + tokenEnd + 
				" node begin = " + iBeginOffset + " node end = " + iEndOffset);
		
		D.pvt(D.D_DT, " checking if this id is a left doner - " + id);
		if(! (id instanceof Literal))
		{
			D.pr(D.D_TEMP, " debug - the isLeftDoner received a variable instead of literal - " + id);
			return false; 
		}
		
		Literal lt = (Literal) id; 
		String stOld = lt.getStringValue();
		D.pv(D.D_DT, " old literal string - " + stOld + " length = " + stOld.length());
		String stNew = null;
		D.pd(D.D_DT, "Left doner = " + bLeftDoner + " node - " + this.toString());
		
		if(bLeftDoner)
		{
			int iEnd = tokenBegin - iBeginOffset;
			//if(this.isFirstLeaf())
			//	iEnd = iEnd - (" PIPER_LOOP_BEGIN ".length());
			
			D.pr(D.D_TEMP, " left doner end offset - " + iEnd);
			D.pr(D.D_TEMP, " old - " + stOld);
			stNew = stOld.substring(0, iEnd);
			donateToken = stOld.substring(iEnd);
			if(donateToken.charAt(0) == '\'')
				donateToken = (new StringBuffer(donateToken)).deleteCharAt(0).toString();
		}
		else
		{
			int iBegin = tokenEnd - iBeginOffset;
			D.ps(D.D_DT, " right doner begin offset - " + iBegin);
			if(iBegin + 1 < stOld.length())
			{
				stNew = stOld.substring(iBegin + 1, stOld.length());
				donateToken = stOld.substring(0, iBegin + 1);
			}
			else
			{
				stNew = "";
				donateToken = stOld;
			}
			
			int lastChar = donateToken.length() - 1;
			if(donateToken.charAt(lastChar) == '\'')
				donateToken = (new StringBuffer(donateToken)).deleteCharAt(lastChar).toString();
		}
		
		D.ps(D.D_DT, " new literal string = " + stNew + " donated token = " + donateToken);
		Literal ltNew = new Literal(stNew);
		this.id = ltNew;
		return bLeftDoner;
	}
	
	
	// set if this node was created as a formal to actual map
	protected boolean actual2formal; 
	public boolean isActual2Formal(){ return actual2formal; } 
	public void setActual2Formal(boolean b) { actual2formal = b; }
	
	// set if this node was created for receiving the return of CfgCallRet 
	protected boolean returnAssign; 
	public boolean isReturnAssign() { return returnAssign; }
	public void setReturnAssign() { returnAssign = true; }
	
	
	protected ArrayList<TacPlace> questionMaps; 
	public void resetQuestionMap() { questionMaps = null; }
	public ArrayList<TacPlace> getQuestionMaps(){ return questionMaps; }
	public void addQuestionMaps(TacPlace arg)
	{
		if(this.questionMaps == null)
			this.questionMaps = new ArrayList<TacPlace>();
		
		if(arg != null)
			this.questionMaps.add(arg);
		else
			throw new Error(" add question map requested adding a null variable");
 	}
	
	public void addQuestionMapsUnique(TacPlace tacPlace)
	{
		if(this.questionMaps == null)
			this.questionMaps = new ArrayList<TacPlace>();
		
		if(tacPlace != null)
		{
			boolean bFound  = false; 
			for(TacPlace tp : this.questionMaps)
			{
				if(tp.equals(tacPlace))
				{
					if(!(tp.toString().endsWith("__args") && tacPlace.toString().endsWith("__args")))
					{
						D.pv(D.D_TEMP, " allowing duplicate qmaps args - " + tp + " and " + tacPlace);
						continue; 
					}
					else
					{
						bFound = true;
						break;
					}
				}
			}
			D.pv(D.D_TEMP, " existing question maps adding unique - " + this.questionMaps + 
					" added - " + !bFound + " arg - " + tacPlace);
			if(!bFound)
				this.questionMaps.add(tacPlace);
		}
		else
			throw new Error(" add question map requested adding a null variable");
 	}
	
	public void addAllQuestionMapsUnique(ArrayList<TacPlace> args)
	{
		if(args != null)
			for(TacPlace arg : args)
				addQuestionMapsUnique(arg);
 	}
	
	public void initPlaceholderArgs()
	{
		if(!this.isNT())
			return; 
		
		Variable var = this.id.getVariable();
		
		String name = "";
		//if(var.isFormal())
		//	name = var.getSymbolTable().getName();
		
		Variable vOrig = var.getOrig();
		if(vOrig == null)
			vOrig = var; 
		
		name +=  Variable.makeArgsName(vOrig, true);
		argsVar = var.dup(name);
		
		if(children != null)
			for(DerivationNode dn : children)
				dn.initPlaceholderArgs();
	}

	protected Variable argsVar; 
	public Variable getArgsVar() { return argsVar; }
	public void setArgsVar(Variable v){ this.argsVar = v;}
	/**
	 * @return
	 */
	public String getLocation()
	{
		if(this.progNode == null)
		{
			return "...not specified...";
		}
		else
		{
			return this.progNode.getFileName() + " : " + 
				this.progNode.getOrigLineno(); 
		}
	}
	public void setNodeCounter() 
	{
		this.nodeNumber = ++ this.nodeCounter;
	
	}
	
	public void setNodeCounter(int c) 
	{
	
		this.nodeNumber = c;
	}
	
	// if this arg is set, args computation
	// must merge the args with this 
	// used in places where alternate values for 
	// same variable may reach in diff contrl paths
	protected Variable phiValue;
	public void setPhiMapFor(Variable tpTmp) {
		// TODO Auto-generated method stub
		this.phiValue = tpTmp;
	}
	public Variable getPhiMapFor(){ return phiValue; }
	
	private boolean bCameFromLoop;
	public boolean isFromLoop(){ return bCameFromLoop;}
	public void setFromLoop(){ bCameFromLoop = true;}
	
	private boolean bFirstLeaf; 
	public boolean isFirstLeaf(){ return bFirstLeaf;}
	public void setFirstLeaf(){bFirstLeaf = true;}
	
	private boolean bLastLeaf; 
	public boolean isLastLeaf(){ return bLastLeaf;}
	public void setLastLeaf(){bLastLeaf = true;}
	
	private boolean bNeededNewNode;
	public boolean getAssembledToken() { return bNeededNewNode; }
	public void setAssembledToken(boolean needNewNode) {
		bNeededNewNode = needNewNode;
	}
}
