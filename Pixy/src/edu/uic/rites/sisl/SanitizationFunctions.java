/**
 * 
 */
package edu.uic.rites.sisl;

import java.util.*;
/**
 * @author Mali
 *
 */
public class SanitizationFunctions{
	public final static ArrayList<String> builtinSanitFunctions = new ArrayList<String>(Arrays.asList("htmlentities", "htmlspecialchars", "strip_tags", "str_replace", "preg_replace", 
															"addslashes", "trim", "ltrim", "rtrim"));
	public final static ArrayList<String> wpSanitFunctions = new ArrayList<String>(Arrays.asList("wp_pre_kses_less_than", "convert_chars", "wp_specialchars", "_wp_specialchars", 
																"wp_strip_all_tags", "convert_invalid_entities", "wp_filter_kses", "wp_kses", "wp_kses_post", 
																"wp_kses_data", "wp_rel_nofollow", "wp_kses_allowed_html", "esc_html", "esc_html__", "esc_html_e", 
																"esc_textarea", "esc_attr", "esc_attr__", "esc_attr_e", "esc_js", "esc_url", "esc_url_raw", 
																"urlencode_deep", "esc_sql", "like_escape", "validate_file", "balanceTags", "force_balance_tags", 
																"tag_escape", "sanitize_email", "sanitize_file_name", "sanitize_html_class", "sanitize_key",
																"sanitize_meta", "sanitize_mime_type", "sanitize_option", "sanitize_sql_orderby", "sanitize_post_field",
																"sanitize_text_field", "sanitize_title", "sanitize_title_for_query", "sanitize_title_with_dashes", 
																"sanitize_user"));

	public final static ArrayList<String> wpdbSanitFunctions = new ArrayList<String>(Arrays.asList("insert", "update", "prepare", "get_var", "escape", "escape_by_ref", "esc_like"));
}