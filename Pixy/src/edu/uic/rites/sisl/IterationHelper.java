/**
 * 
 */
package edu.uic.rites.sisl;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import at.ac.tuwien.infosys.www.pixy.analysis.dep.DepGraphNode;
import at.ac.tuwien.infosys.www.pixy.conversion.Variable;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNode;

/**
 * @author prithvi
 *
 */
public class IterationHelper {

	private DepGraphNode dgn;
	private ArrayList<CfgNode> forSameLine;
	
	private LinkedHashMap<Variable, ArrayList<Variable>> phiDef;
	private Variable renamed;
	
	public IterationHelper(DepGraphNode dgn, ArrayList<CfgNode> forSameLine,
			LinkedHashMap<Variable, ArrayList<Variable>> phiDef,
			Variable renamed) {
		this.dgn = dgn;
		this.forSameLine = forSameLine; 
		this.phiDef = phiDef;
		this.renamed = renamed;
	}

	public void setDgn(DepGraphNode dgn) {
		this.dgn = dgn;
	}


	public DepGraphNode getDgn() {
		return dgn;
	}


	public void setImpactedNodes(ArrayList<CfgNode> forSameLine) {
		this.forSameLine = forSameLine;
	}


	public ArrayList<CfgNode> getImpactedNodes() {
		return forSameLine;
	}


	public void setPhiDef(LinkedHashMap<Variable, ArrayList<Variable>> phiDef) {
		this.phiDef = phiDef;
	}


	public LinkedHashMap<Variable, ArrayList<Variable>> getPhiDef() {
		return phiDef;
	}


	public void setRenamed(Variable renamed) {
		this.renamed = renamed;
	}


	public Variable getRenamed() {
		return renamed;
	}

}
