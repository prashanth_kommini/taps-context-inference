/**
 * 
 */
package edu.uic.rites.sisl;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
//import java.util.concurrent.CopyOnWriteArrayList;

import at.ac.tuwien.infosys.www.pixy.analysis.dep.DepGraphNode;
import at.ac.tuwien.infosys.www.pixy.analysis.dep.DepGraphNormalNode;
import at.ac.tuwien.infosys.www.pixy.analysis.dep.DepGraphOpNode;
import at.ac.tuwien.infosys.www.pixy.conversion.Cfg;
import at.ac.tuwien.infosys.www.pixy.conversion.TacFunction;
import at.ac.tuwien.infosys.www.pixy.conversion.Variable;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNode;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNodeBasicBlock;

/**
 * @author prithvi
 *
 */
public class ControlFlow
{
	public boolean isProcessed(){ return this.processed; }
	public void setProcessed(boolean b){ this.processed = b; }
	public ArrayList<CfgNode> getNodes() { return nodes;}
	
	/* 
	 * @param cn CFG node to be added to the this ControlFlow 
	 * check to see if this node has already been added to the flow, add if not.
	 */
	public boolean addNode(CfgNode cn)
	{ 
		boolean bAdded = true;
		String loc = cn.getAbsFileName() + ":";
		int line = cn.getOrigLineno();
		
		D.pv(D.D_TEMP, " trying to update current path with cn - " + cn);
		for(CfgNode present : nodes)
		{
			String locP = present.getAbsFileName() + ":";
			int lineP = present.getOrigLineno();
			
			if(line == -1 && lineP == -1 && loc.equals(locP) && cn.toString().equals(present.toString()))
			{
				D.pv(D.D_TEMP, "\t found already existing node - " + present + " for node " + cn + 
						" string representations \n \t\t " + cn.toString() + 
						" \n\t\t " + present.toString());
				bAdded = false; 
				break;
			}
			else
			{
				if(present.equals(cn))
				{
					D.pv(D.D_TEMP, "\t equals with " + present + " returned true");
					bAdded = false; 
					break;
				}
			}
		}
		
		D.pv(D.D_TEMP, "\t " + !bAdded + " node - " + cn + " already prseent in this flow - " + this.nodes );
		if(bAdded)
		{
			D.pv(D.D_TEMP, "\t adding it now...");
			nodes.add(cn);			
	
		}
		
		return bAdded; 
	}
	
	public void addNode(int index, CfgNode cn){ nodes.add(index, cn);}
	private boolean bLoopBody;
	private ArrayList<CfgNode> nodes; 
	private boolean processed; 
	private IterationHelper itObj;
	
	public ControlFlow()
	{
		this.nodes = new ArrayList<CfgNode>();
		this.iNumberOfFnsTraversed = 0;
	}
	
	public ControlFlow addAll(List<CfgNode> nodes)
	{
		if(nodes != null)
			for(CfgNode cn : nodes)
				this.nodes.add(cn);
		
		return this; 
	}
	
	public ControlFlow addAll(ControlFlow cf)
	{
		this.nodes.addAll(cf.getAll());
		return this; 
	}
	
	public ArrayList<CfgNode> getAll(){ return this.nodes;}
	
	
	public void makeUnique()
	{
		ArrayList<CfgNode> cowal = new ArrayList<CfgNode>();
		for(CfgNode cn : this.nodes)
		{
			if(!cowal.contains(cn))
				cowal.add(cn);
		}
	    
		nodes = cowal;
	    return;
	}
	
	public String toString()
	{
		makeUnique();
		String str = "\n \t\t....Control Flow....( in fns - " + 
			this.iNumberOfFnsTraversed + " ) begin \n";
//		for(CfgNode cn : this.nodes)
	//		str += pref + cn.toString() + "\n";
		
		str += this.getProgLines() + "\n";

		if(this.phiLhsMapSupport != null)
		{
			str += " \t\t\t phiLhs map support - " + " \n";
			for(Map.Entry<CfgNode, LinkedHashMap<Variable, Variable>> e : this.phiLhsMapSupport.entrySet())
			{
				CfgNode cn = e.getKey();
				LinkedHashMap<Variable, Variable> map = e.getValue();
				for(Map.Entry<Variable, Variable> m : map.entrySet())
					str += "\t\t var " + m.getKey() + " to " + m.getValue() + " at cn - " + cn;
			}
		}

		

		str += "....Control Flow....end \n";
		return str; 
	}

	public String toStringIR()
	{
		makeUnique();
		String str = "\n \t\t....Detailed Control Flow....( in fns - " + 
			this.iNumberOfFnsTraversed + " ) begin \n";
		for(CfgNode cn : this.nodes)
			str += "\n\t" + cn.toString() + "\n";
		
//		str += this.getProgLines() + "\n";
//
//		if(this.phiLhsMapSupport != null)
//		{
//			str += " \t\t\t phiLhs map support - " + " \n";
//			for(Map.Entry<CfgNode, LinkedHashMap<Variable, Variable>> e : this.phiLhsMapSupport.entrySet())
//			{
//				CfgNode cn = e.getKey();
//				LinkedHashMap<Variable, Variable> map = e.getValue();
//				for(Map.Entry<Variable, Variable> m : map.entrySet())
//					str += "\t\t var " + m.getKey() + " to " + m.getValue() + " at cn - " + cn;
//			}
//		}

		str += "....Control Flow....end \n";
		return str; 
	}
	
	
	private String getProgLines()
	{
		String str = " Lines = [ ";
		ArrayList<String> lines = new ArrayList<String>();
		
		for(CfgNode tcn : this.nodes)
		{
			String st = "";			
			if(tcn.getFileName() != null)
				st += tcn.getFileName();
			st += " : " + tcn.getOrigLineno() + ",  ";
			if(!lines.contains(st))
			{
				lines.add(st);
			}
		}
		Collections.sort(lines);
		for(String l : lines)
			str += l;
		
		str += " ] ";
		
		return str;

	}
	
	
	public void setLoopBody(){ this.bLoopBody = true;}

	public boolean isLoopBody() {
		return this.bLoopBody;
	}
//	public void setLoopBody(LinkedHashMap<ArrayList<CfgNode>, LinkedHashMap<Variable, ArrayList<Variable>>> aid)
//	{
//		setLoopAid(aid);
//		bLoopBody = true;
//	}
//	
//	private LinkedHashMap<ArrayList<CfgNode>, LinkedHashMap<Variable, ArrayList<Variable>>> loopAid; 
//	public void setLoopAid(LinkedHashMap<ArrayList<CfgNode>, LinkedHashMap<Variable, ArrayList<Variable>>> aid)
//	{
//		loopAid = aid;
//	}
	
//	public LinkedHashMap<ArrayList<CfgNode>, LinkedHashMap<Variable, ArrayList<Variable>>> getLoopAid() {
//		return loopAid;
//	}
	
	public void setIterationHelper(IterationHelper itObj) {
		this.itObj = itObj;
		bLoopBody = true;
	}
	public IterationHelper getIterationHelper() {
		return itObj;
	}
	
	public static ArrayList<ControlFlow> deepCopy(ArrayList<ControlFlow> paths) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public static ControlFlow deepCopy(ControlFlow path) {
		// TODO Auto-generated method stub
		
		ControlFlow cf = new ControlFlow();
		cf.nodes = new ArrayList<CfgNode>();
		cf.nodes.addAll(path.getNodes());
		cf.itObj = path.getIterationHelper();

		if(path.phiLhsMapSupport != null)
		{
			LinkedHashMap<CfgNode, LinkedHashMap<Variable, Variable>> phiLhsToRhs  = 
				new LinkedHashMap<CfgNode, LinkedHashMap<Variable, Variable>>();
			phiLhsToRhs.putAll(path.phiLhsMapSupport);
			cf.phiLhsMapSupport = phiLhsToRhs;
		}

		cf.intraExitNode = path.intraExitNode;
		cf.nodesBetween = path.nodesBetween;
		cf.interProcNodes = path.interProcNodes;
		cf.iNumberOfFnsTraversed = path.iNumberOfFnsTraversed;
		return cf;
	}
	
	public void addNodeIfAbsent(CfgNode cn) {
	
		if(this.getAll().contains(cn))
			return;
		addNode(cn);
	}

	
	private LinkedHashMap<CfgNode, LinkedHashMap<Variable, Variable> >phiLhsMapSupport;	
	public LinkedHashMap<CfgNode, LinkedHashMap<Variable, Variable> > getPhiLhsSupport()
	{
		return this.phiLhsMapSupport;
	}
	
	public void addPhiLhsSupport(CfgNode cn, Variable vPhiLhs, Variable vActiveRhs)
	{
		if(cn instanceof CfgNodeBasicBlock)
		{
			CfgNodeBasicBlock cnbb = (CfgNodeBasicBlock) cn;
			cn = cnbb.getContainedNodes().get(0);
		}
		
		if(this.phiLhsMapSupport == null)
		{
			this.phiLhsMapSupport = new 
				LinkedHashMap<CfgNode, LinkedHashMap<Variable, Variable> >();
			
			LinkedHashMap<Variable, Variable> phiLhsToRhs  = 
				new LinkedHashMap<Variable, Variable> ();
			phiLhsToRhs.put(vPhiLhs, vActiveRhs);		
			this.phiLhsMapSupport.put(cn, phiLhsToRhs);
		}
		else
		{
		//if(this.phiLhsMapSupport.get(cn) != null)
		//	this.phiLhsMapSupport.remove(cn);
			LinkedHashMap<Variable, Variable> current = this.phiLhsMapSupport.get(cn);
			if(current != null)
			{
				Variable rhs = current.get(vPhiLhs);
				if(rhs != null)
					current.remove(vPhiLhs);
				current.put(vPhiLhs, vActiveRhs);
				this.phiLhsMapSupport.remove(cn);
				this.phiLhsMapSupport.put(cn, current);
			}
			else
			{
				LinkedHashMap<Variable, Variable> phiLhsToRhs  = 
					new LinkedHashMap<Variable, Variable> ();
				phiLhsToRhs.put(vPhiLhs, vActiveRhs);		
				this.phiLhsMapSupport.put(cn, phiLhsToRhs);			
			}
		}
	}
	
	public Variable getPhiLhsToRhsMap(CfgNode cn, Variable vPhiLhs) {
		
		if(this.phiLhsMapSupport == null) return null;
		
		LinkedHashMap<Variable, Variable> phiMap = this.phiLhsMapSupport.get(cn);
		if(phiMap == null)
			return null;
		
		return phiMap.get(vPhiLhs);
	}
	public void addPhiLhsSupport(CfgNode cn, ControlFlow intra) {
		
		LinkedHashMap<CfgNode, LinkedHashMap<Variable, Variable>> phiS = intra.getPhiLhsSupport();
		if(phiS != null)
		{
			LinkedHashMap<Variable, Variable> ma = phiS.get(cn);
			if(ma != null)
			{
				for(Map.Entry<Variable, Variable> m : ma.entrySet())
					this.addPhiLhsSupport(cn, m.getKey(), m.getValue());
			}
		}
	}
	
	public String getProgStatement() {
		
		String str = "";
		ArrayList<Integer> dumped = new ArrayList<Integer>();
		
		for(CfgNode c : this.nodes)
		{
			String file = c.getFileName();
			String absFile = c.getAbsFileName();
			int iLine = c.getOrigLineno();
			if(!dumped.contains(iLine))
				dumped.add(iLine);
			else
				continue; 
			
			if(iLine != -1)
			{
				str += "\t\t " + file + " : " + iLine;
				try{
					str += getLineNumber(absFile, iLine) ;
				}catch(Exception e){//str += " failed to read \n";
				}
				str += "\n";
			}
		}
		
		return str;
	}
	
	public static String getLineNumber(String absFile, int line) {
	
		String str = "";
		// better packaging - utils
		BufferedReader bis = Transformation.getInStream(absFile); 
		int i = 1; 
		try
		{
			while(i < line)
			{
				i++;
				bis.readLine();
			}
			str = bis.readLine();

			try{
				bis.close();
			}
			catch(Exception e)
			{
				D.pr(D.D_TEMP, " Exception while closing teh file " + absFile + e.getStackTrace());
			}
		}
		catch(Exception e)
		{
			D.ps(D.D_TEMP, " could not read line " + line + 
					" from file " + absFile  + e.getMessage());
		}
		
		return str;
	}

	public static boolean compareForSameNodes(ControlFlow flow1, ControlFlow flow2) {
		D.pv(D.D_TEMP, " Comparing cf " + flow1 + " and - " + flow2);
		return compareForSameNodes(flow1.getAll(), flow2.getAll());
	}
	
	public static boolean compareForSameNodes(ArrayList<CfgNode> nodesIn1, ArrayList<CfgNode> nodesIn2) {
		
		boolean bDups = true;
//		ArrayList<CfgNode> nodesIn1 = flow1.getAll();
//		ArrayList<CfgNode> nodesIn2 = flow2.getAll();
		
		// check if all nodes in cfRest are contained in cf
		for(CfgNode cn : nodesIn1)
		{
			// these are the temp nodes added during
			// the phi resolution process.
			if(cn.getOrigLineno() == -1) 
				continue;
			
			boolean contains = nodesIn2.contains(cn);
			D.pv(D.D_TEMP, "\t flow1 node - " + cn + " in flow2 - " + contains);
			if(!contains)
			{
				bDups = false;
				break;
			}
		}
		
		// if so, check if all nodes of cf are also in cfRest
		// set equivalence test
		if(bDups)
		{
			// check if all nodes in cfRest are contained in cf
			for(CfgNode cn : nodesIn2)
			{
				if(cn.getOrigLineno() == -1) 
					continue;
				
				boolean contains = nodesIn1.contains(cn);
				D.pv(D.D_TEMP, "\t flow2 node - " + cn + " in flow1 - " + contains);
				if(!contains)
				{
					bDups = false;
					break;
				}
			}			
		}
			
		D.pv(D.D_TEMP, " flow1 and flow2 are same - " + bDups);
		return bDups;
	}
	
	CfgNode intraExitNode; 
	public void setIntraExit(CfgNode exitNode) {
		this.intraExitNode = exitNode; 
	}
	public CfgNode getIntraExit() {
		return this.intraExitNode; 
	}

	private ArrayList<CfgNode> nodesBetween;
	public ArrayList<CfgNode> getNodesBetween(){ return this.nodesBetween;}
	public void setNodesBetween(ArrayList<CfgNode> nodesBetween) {
		this.nodesBetween = nodesBetween;
	}
	
	// captures cfg nodes used in inter-proc fashion
	LinkedHashMap<Cfg, ArrayList<CfgNode>> interProcNodes; 
	public void addInterProcInBetween(Cfg cfg, ArrayList<CfgNode> nodesBetween2) {
		if(this.interProcNodes == null)
			this.interProcNodes = new LinkedHashMap<Cfg, ArrayList<CfgNode>>() ; 

		this.interProcNodes.put(cfg, nodesBetween2);
	}
	
	public ArrayList<CfgNode> getInterProcInBetween(TacFunction tf) 
	{
		return this.interProcNodes.get(tf.getCfg());
	}
	
	public ArrayList<CfgNode> getAllInterProcInBetween() 
	{
		ArrayList<CfgNode> all = new ArrayList<CfgNode>();
		D.pr(D.D_TEMP, " getting all cfg nodes in the path");
		for(ArrayList<CfgNode> inCfg : this.interProcNodes.values())
		{
			D.pr(D.D_TEMP, " \n Cfg nodes\t\t " + CfgNode.getProgLinesSorted(inCfg));
			all.addAll(inCfg);
		}
		return all;
	}

	public int iNumberOfFnsTraversed;
}
