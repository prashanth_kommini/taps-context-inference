/**
 * 
 */
package edu.uic.rites.sisl.hpe;

import java.util.ArrayList;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNode;

/**
 * @author Maliheh
 *
 */
public class Path {

	ArrayList<CfgNode> path = new ArrayList<CfgNode>();
	
	public Path addNode(CfgNode node){
		
		path.add(node);
		return this;
	}
	
	public ArrayList<CfgNode> getPath(){
		return path;
	}
	
	
	public CfgNode getLastNode(){
		if(path!= null){
			return path.get(path.size()-1);
		}
		return null;
	}
	
	public String toString(){
		return path.toString();
	}
}