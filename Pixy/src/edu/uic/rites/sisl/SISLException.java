package edu.uic.rites.sisl;

public class SISLException extends Exception {

	/*
	 * Methods invoked in the dep graph, do not form a calling order...
	 * e.g., f1 --> f2 --> f3 and f4 doesnt get invoked from either f1, f2, f3 and 
	 * doesnt invoke any of f1, f2, f3....
	 * potentially a problem in the path enumeration. 
	 */
	public static final int NO_SINGLE_INVOKE_ENTRY = 1;
	
	/*
	 * Path enumeration could not be done, and enumeration within a function
	 * did not terminate
	 */
	public static final int INFINITE_RECURSION_INTRA_PATH_ENUM = 2;

	/*
	 * Phi Lhs maps are used for finding the reaching definition of phi variables
	 * q1 = Phi(q2, q3)
	 * phi lhs maps q1 to either q2 or q3.
	 * this error is raised when tool does not currently support adjusting the RHS
	 * of specific CfgNodes
	 */
	public static final int TOOL_BUG_PHILHS_UNHANDLED = 3;

	/*
	 * Inter-process path enumeration hit the recursion limit
	 */
	public static final int INFINITE_RECURSION_INTER_PATH_ENUM = 4;

	/*
	 * enumerated flow doesnt contain call from _main method.
	 */
	public static final int NOT_REACHABLE_FROM_MAIN = 5;


	/*
	 * static analysis found potential statments whose semantics
	 * may change because of transformation
	 */
	public static final int FOUND_STATIC_VIOLATIONS_IN_TRANSFORM = 6;

	/*
	 * Found callbacks that use same data as queries or use modified partial queriess
	 */
	public static final int FOUND_CALLBACK_VIOLATIONS_IN_TRANSFORM = 7;

	public static final int FOUND_STATIC_WARNINGS = 8;

	public static final int INTERNAL_TOOL_BUG = 9;

	public static final int NONREPEATABLE_PARTIAL_QUERY_FROM_LOOP = 10;
	public static final int LOOPSTMT_VIOLATE_CONSTRAINT = 11;

	
	
	// just get out of there
	public static final int BAIL_OUT = 1000;



	private int type;
	private String msgDetails;
		
	public SISLException(int type, String string) {
		this.setType(type); 
		this.msgDetails = string;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getType() {
		return type;
	}

	public String getMessage()
	{
		String str = super.getMessage();
		str += "\n " + getSISLMessage() + " \n "; 
		return str;
	}

	
	public String getSISLMessage() {
		switch(this.type){
		
		case NO_SINGLE_INVOKE_ENTRY: 
			return "Flow formed by following statements does not have a single start function " +
					" \n " + this.msgDetails + 
					"\n (hint: a tool bug or infeasible flow) \n \t "; 
		
		case NOT_REACHABLE_FROM_MAIN:
			return "Flow formed by following statements does not start from main function (unreachable?)" +
			" \n " + this.msgDetails + 
			"\n (hint: a potential tool bug) \n \t "; 
			
			
		case INFINITE_RECURSION_INTRA_PATH_ENUM:
			return "Intra-process path enumeration failed " + 
				"\n " + this.msgDetails + 
				" \n (hint : a tool bug mostly, check for infeasible path enumeration)";
			
		case TOOL_BUG_PHILHS_UNHANDLED: 
			return "Tool Bug : Pending implementation - " + 
				"\n " + this.msgDetails;

		case INFINITE_RECURSION_INTER_PATH_ENUM:
			return "Inter-process path enumeration failed " + 
				"\n " + this.msgDetails +  
				" \n (hint : a tool bug mostly, check for infeasible path enumeration)";

		case FOUND_STATIC_VIOLATIONS_IN_TRANSFORM:
			return "Static analysis detected potentially semantics changing transformation - " +
				"\n " + this.msgDetails ;//+ 
//				"\n (hint : To ignore these warnings, use -F flag in command line)";

		case FOUND_CALLBACK_VIOLATIONS_IN_TRANSFORM:
			return "Static analysis detected violating callbacks - " +
				"\n " + this.msgDetails ;//+ 
//				"\n (hint : To ignore these warnings, use -F flag in command line)";

		case FOUND_STATIC_WARNINGS:
			return "Static transformation did not exact following changes (use -F flag to ignore these warnings)- " +
				"\n " + this.msgDetails;
			
		case BAIL_OUT:
			return " " + this.msgDetails;
		default: return " no specialized message for type - " + type + " -- " + this.msgDetails;
		
		}
	}
	
	
}
