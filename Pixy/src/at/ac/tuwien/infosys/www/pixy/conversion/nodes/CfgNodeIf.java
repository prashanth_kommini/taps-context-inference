package at.ac.tuwien.infosys.www.pixy.conversion.nodes;

import at.ac.tuwien.infosys.www.phpparser.*;
import at.ac.tuwien.infosys.www.pixy.conversion.Constant;
import at.ac.tuwien.infosys.www.pixy.conversion.TacActualParam;
import at.ac.tuwien.infosys.www.pixy.conversion.TacPlace;
import at.ac.tuwien.infosys.www.pixy.conversion.Variable;

import java.util.*;


//*********************************************************************************
//CfgNodeIf ***********************************************************************
//*********************************************************************************


public class CfgNodeIf
extends CfgNode {

    private TacPlace leftOperand;
    private TacPlace rightOperand;  // may only be Constant.TRUE or Constant.FALSE
    private int op;
 
//CONSTRUCTORS ********************************************************************    

    public CfgNodeIf (TacPlace leftOperand, TacPlace rightOperand, int op, ParseNode node) {
        super(node);
        // make sure that right operand is valid (i.e. true or false)
        if (!(rightOperand == Constant.TRUE || rightOperand == Constant.FALSE)) {
            throw new RuntimeException(
                "SNH: illegal right operand for if node at line " + 
                node.getLinenoLeft());
        }
        this.leftOperand = leftOperand;
        this.rightOperand = rightOperand;
        this.op = op;
    }

    public TacPlace getLeftOperand() {
        return this.leftOperand;
    }
    
    public TacPlace getRightOperand() {
        return this.rightOperand;
    }
    
    public int getOperator() {
        return this.op;
    }
    
    public List<Variable> getVariables() {
        List<Variable> retMe = new LinkedList<Variable>();
        if (this.leftOperand instanceof Variable) {
            retMe.add((Variable) this.leftOperand);
        } else {
            retMe.add(null);
        }
        return retMe;
    }
    
//  SET ****************************************************************************

    public void replaceVariable(int index, Variable replacement) {
        switch (index) {
        case 0:
            this.leftOperand = replacement;
            break;
        default:
            throw new RuntimeException("SNH");
        }
    }

    // SISL 
    // index 1: for true edge
    // index 0: for false edge (else edge)

	public CfgNode getThen() 
	{ 
		CfgNode cn = null;
        if (this.outEdges[1] != null) {
            cn = this.outEdges[1].getDest();
        }
        return cn;
	}
	public CfgNode getElse()
	{
		CfgNode cn = null;
        if (this.outEdges[0] != null) {
            cn = this.outEdges[0].getDest();
        }
        return cn;
	}

    public ArrayList<Variable> getRightToRename() 
    { 
    	ArrayList<Variable> v = new ArrayList<Variable>();
    	if(leftOperand.isVariable())
    			v.add((Variable)leftOperand);
    	if(rightOperand.isVariable())
    			v.add((Variable)rightOperand);
    	
    	return v;
    }
		
    public void rightRename(Variable from, Variable to, boolean setOrig)
    {
    	if(leftOperand.equals(from))
    	{
    		if(setOrig) to.setOrig(from);
    		leftOperand = to;
    	}
    	if(rightOperand.equals(from))
    	{
    		if(setOrig) to.setOrig(from);
    		rightOperand = to;
    	}
    }
}


