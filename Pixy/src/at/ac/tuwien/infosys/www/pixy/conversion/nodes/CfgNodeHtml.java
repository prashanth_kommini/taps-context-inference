package at.ac.tuwien.infosys.www.pixy.conversion.nodes;

import at.ac.tuwien.infosys.www.phpparser.*;
import at.ac.tuwien.infosys.www.pixy.conversion.Literal;
import at.ac.tuwien.infosys.www.pixy.conversion.TacActualParam;
import at.ac.tuwien.infosys.www.pixy.conversion.TacPlace;
import at.ac.tuwien.infosys.www.pixy.conversion.Variable;

import java.util.*;

import edu.uic.rites.sisl.D;


public class CfgNodeHtml 
extends CfgNode {

	private TacPlace place;
		    
		// CONSTRUCTORS ********************************************************************    

		    public CfgNodeHtml(TacPlace place, ParseNode node) {
		        super(node);
		        this.place = place;
		    }

		//  GET *****************************************************************************
		    
		    public TacPlace getPlace() {
		        return this.place;
		    }
		    
		    public List<Variable> getVariables() {
	            List<Variable> retMe = new LinkedList<Variable>();

		        if (this.place instanceof Variable) {
		            retMe.add((Variable) this.place);
		            return retMe;
		        }else {
		          retMe.add(null);
		           return retMe;//Collections.emptyList();
		        }
		    }

		    public void replaceVariable(int index, Variable replacement) {
		        switch (index) {
		        case 0:
		            this.place = replacement;
		            break;
		        default:
		            throw new RuntimeException("SNH");
		        }
		    }
		    
		

		    public ArrayList<Variable> getRightToRename()
		    {
		        if (this.place instanceof Variable) {
		            ArrayList<Variable> retMe = new ArrayList<Variable>();
		            retMe.add((Variable) this.place);
		            return retMe;
		        } else {
		            return null;
		        }    	
		    } 
		    
		//  SET ****************************************************************************
		    /*
		     * 

		    public ArrayList<Variable> getRightToRename()
		    {
		        if (this.place instanceof Variable) {
		            ArrayList<Variable> retMe = new ArrayList<Variable>();
		            retMe.add((Variable) this.place);
		            return retMe;
		        } else {
		            return null;
		        }    	
		    }

		 // SISL
		    public void rightRename(Variable from, Variable to, boolean setOrig)
		    {
		    	D.pr(D.D_TEMP, "SSA!!!!!!!!!! builtin !!! method - echo " + "renaming " + 
		    			" " + from.getName() + " to " + to.getName());

		        if (this.place instanceof Variable) {
		        	Variable v = this.place.getVariable();
		    		if(v.equals(from))
		    		{
		    			if(setOrig) to.setOrig(from);
		    			this.place = to;
		    		}
		    	}
		    }
		*/


	}

	