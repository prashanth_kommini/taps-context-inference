package at.ac.tuwien.infosys.www.pixy.conversion.nodes;

import at.ac.tuwien.infosys.www.phpparser.*;
import at.ac.tuwien.infosys.www.pixy.Dumper;
import at.ac.tuwien.infosys.www.pixy.MyOptions;
import at.ac.tuwien.infosys.www.pixy.Utils;
import at.ac.tuwien.infosys.www.pixy.conversion.*;

import java.util.concurrent.*;

import java.util.*;

import edu.uic.rites.sisl.ControlFlow;
import edu.uic.rites.sisl.D;

public abstract class CfgNode {

	protected final ParseNode parseNode;

	protected List<CfgEdge> inEdges;
	// index 0: for false edge (or normal edge)
	// index 1: for true edge
	protected CfgEdge[] outEdges;
	
	// number of this cfg node in reverse postorder (speeds up the analysis
	// if used by the worklist); -1 if uninitialized
	private int reversePostOrder;
	
	// this can be one of the following:
	// - the enclosing basic block, if there is one (CfgNodeBasicBlock)
	// - a function's CfgNodeEntry, if this cfg node is member of one of this
	//   function's default param cfgs
	// - null, if neither of the above applies
	private CfgNode enclosingNode;

	// function that contains this cfgNode;
	// note: you can't just set this in the constructor, since it
	// might change during include file resolution
	private TacFunction enclosingFunction = null;

// CONSTRUCTORS ********************************************************************    

	CfgNode() {
		this(null);
	}

	CfgNode(ParseNode parseNode) {
		this.parseNode = parseNode;
		this.inEdges = new ArrayList<CfgEdge>();
		this.outEdges = new CfgEdge[2];
		this.outEdges[0] = this.outEdges[1] = null;
		this.reversePostOrder = -1;
		this.enclosingNode = null;
		
		// SISL 
		this.imDominates = new ArrayList<CfgNode>();
		this.dominates = new ArrayList<CfgNode>();
		this.domF = new ArrayList<CfgNode>();
		this.bRenamed = false;
	}
	 
		
// GET *****************************************************************************    

	// returns
	// - the enclosing basic block, if it is enclosed in one
	// - the entry node of the function default cfg, if it is inside such a cfg
	// - itself otherwise
	public CfgNode getSpecial() {
		CfgNode retMe;
		
		retMe = this.getEnclosingBasicBlock();
		if (retMe != null) {
			return retMe;
		}
		
		retMe = this.getDefaultParamEntry();
		if (retMe != null) {
			return retMe;
		}
		
		return this;
	}
	
	// can return null!
	public ParseNode getParseNode() {
		return this.parseNode;
	}

	public List<CfgEdge> getInEdges() {
		return this.inEdges;
	}

	public CfgEdge[] getOutEdges() {
		return this.outEdges;
	}

	public CfgEdge getOutEdge(int index) {
		return this.outEdges[index];
	}

	public CfgNode getSuccessor(int index) {
		if (this.outEdges[index] != null) {
			return this.outEdges[index].getDest();
		} else {
			return null;
		}
	}
	
	public List<CfgNode> getSuccessors() {
		List<CfgNode> successors = new LinkedList<CfgNode>();
		if (this.outEdges[0] != null) {
			successors.add(this.outEdges[0].getDest());
			if (this.outEdges[1] != null) {
				successors.add(this.outEdges[1].getDest());
			}
		}
		return successors;
	}

	// returns the unique predecessor if there is one;
	// throws an exception otherwise
	public CfgNode getPredecessor() {
		List<CfgNode> predecessors = this.getPredecessors();
		if (predecessors.size() != 1) {
			throw new RuntimeException("SNH: " + predecessors.size());
		}
		return predecessors.get(0);
	}
	
	public List<CfgNode> getPredecessors() {
		List<CfgNode> predecessors = new LinkedList<CfgNode>();
		for (Iterator iter = this.inEdges.iterator(); iter.hasNext();) {
			CfgEdge inEdge = (CfgEdge) iter.next();
			predecessors.add(inEdge.getSource());
		}
		return predecessors;
	}

	public int getOrigLineno() {
		// in some cases, this method is currently not very useful because
		// it returns "-2" (i.e., the line number of the epsilon node), especially
		// for constructs such as $x = "hello $world";
		// PhpParser needs to be improved to overcome this problem
		if (this.parseNode != null) {
			return this.parseNode.getLinenoLeft();
		} else {
			return -1;
		}
	}
	
	public String getAbsFileName()
	{
		if(this.parseNode != null)
			return this.parseNode.getFileName();
		else
			return null;
	}
	
	public String getFileName() {
		if (this.parseNode != null) {
			// Prithvi : change
			// return this.parseNode.getFileName();
			
			StringBuffer sb = new StringBuffer(this.parseNode.getFileName());
			int end = sb.lastIndexOf("/");
			if(end >= 0)
				return sb.substring(end + 1).toString();
			else
				return sb.toString();
			
		} else {
			return "<file name unknown>";
		}
	}
	
	public String getLoc() {
		if (!MyOptions.optionB && !MyOptions.optionW) {
			return this.getFileName() + ":" + this.getOrigLineno();
		} else {
			return Utils.basename(this.getFileName()) + ":" + this.getOrigLineno();
		}
	}
	
	public TacFunction getEnclosingFunction() {
		if (this.enclosingFunction == null) {
			D.pv(D.D_TEMP, this.getFileName());
			D.pv(D.D_TEMP, this.toString() + ", " + this.getOrigLineno());
			throw new RuntimeException("SNH");
		}
		return this.enclosingFunction;
	}
	
	// returns a list of Variables referenced by this node; an empty list
	// if there are none; can also contain null values (placeholders);
	// targeted at the replacement of $GLOBALS, so you
	// should take a look at the actual implementations before using it
	// for something else
	public abstract List<Variable> getVariables();
	
	// returns a list of all USED (read) variables in this node;
	// required by taint graph generation
	//public abstract Set<TacPlace> getUsedPlaces(CfgNode cfgNodeX);
	
	public int getReversePostOrder() {
		return this.reversePostOrder;
	}

	// returns either null or the enclosing basic block
	public CfgNodeBasicBlock getEnclosingBasicBlock() {
		if (this.enclosingNode == null) {
			return null;
		}
		if (this.enclosingNode instanceof CfgNodeBasicBlock) {
			return (CfgNodeBasicBlock) this.enclosingNode;
		} else {
			return null;
		}
	}
	
	// returns either null or the entry node of the corresponding function
	// (if this node belongs to the default cfg of a function's formal parameter)
	public CfgNodeEntry getDefaultParamEntry() {
		if (this.enclosingNode == null) {
			return null;
		}
		if (this.enclosingNode instanceof CfgNodeEntry) {
			return (CfgNodeEntry) this.enclosingNode;
		} else {
			return null;
		}
	}
	
// SET *****************************************************************************    
	
	// replaces the variable with the given index in the list returned by getVariables
	// by the given replacement variable
	public abstract void replaceVariable(int index, Variable replacement);
	
	public void setOutEdge(int index, CfgEdge edge) {
		this.outEdges[index] = edge;
	}
	
	public void setReversePostOrder(int i) {
		if (i == Integer.MAX_VALUE) {
			throw new RuntimeException("Integer Overflow");
		}
		this.reversePostOrder = i;
	}
	
	public void setEnclosingBasicBlock(CfgNodeBasicBlock basicBlock) {
		this.enclosingNode = basicBlock;
	}

	public void setDefaultParamPrep(CfgNodeEntry callPrep) {
		D.pv(D.D_TEMP, " set default param entry called for " + 
				this + " prep - " + callPrep);
		this.enclosingNode = callPrep;
	}
	
	public void setEnclosingFunction(TacFunction function) {
		this.enclosingFunction = function;
	}
	
// OTHER ***************************************************************************

	public void addInEdge(CfgEdge edge) {
		this.inEdges.add(edge);
	}

	// removes the edge coming in from the given predecessor 
	public void removeInEdge(CfgNode predecessor) {
		for (Iterator iter = this.inEdges.iterator(); iter.hasNext();) {
			CfgEdge inEdge = (CfgEdge) iter.next();
			if (inEdge.getSource() == predecessor) {
				iter.remove();
			}
		}
	}

	public void clearInEdges() {
		this.inEdges = new LinkedList<CfgEdge>();
	}
	
	public void clearOutEdges() {
		this.outEdges[0] = this.outEdges[1] = null;
	}
	
	public String toString() {
		return Dumper.makeCfgNodeName(this);
	}
	
	// SISL 
	public Variable getLeft() { return null;}
	public ArrayList<Variable> getRightToRename() { return null;}
	public void setLeft(Variable vNew) { };
	public void rightRename(Variable from, Variable to, boolean setOrig) { };

	
	protected ArrayList<CfgNode> imDominates;
	public ArrayList<CfgNode> getImdDominated(){return imDominates;}
	public void setImdDominated(ArrayList<CfgNode> imdD){imDominates = imdD;}
	
	private ArrayList<CfgNode> dominates; 
	public void setDominates(ArrayList<CfgNode> imdD) {dominates = imdD;}
	public List<CfgNode> getDominates(){return dominates;}
	
	public List<CfgNode> getSuccessorsNoBlocks() 
	{
		List<CfgNode> successors = new LinkedList<CfgNode>();
		if (this.outEdges[0] != null) 
		{
			CfgNode cfn = (this.outEdges[0].getDest());
			successors.add(getFirstSuccs_SISL(cfn));
		}
 
		if (this.outEdges[1] != null) 
		{
			CfgNode cfn = (this.outEdges[1].getDest());
			cfn = this.outEdges[1].getDest();
			successors.add(getFirstSuccs_SISL(cfn));
		}

		if(successors.size() == 0)
		{
			// check if this is the last node in a Basic block
			// it doesnt connect to the successor...so
			// take the successor of the enclosing node
			if( enclosingNode != null && 
				enclosingNode instanceof CfgNodeBasicBlock)
			{
				List<CfgNode> suc = enclosingNode.getSuccessors();
				if(suc != null && suc.size() > 0)
				for(CfgNode c : suc)
				successors.add( getFirstSuccs_SISL(c) );
			}
		}
		return successors;
	}
	
	private CfgNode getFirstSuccs_SISL(CfgNode cfn)
	{
		if (cfn instanceof CfgNodeBasicBlock) 
		{
			CfgNodeBasicBlock bb = (CfgNodeBasicBlock) cfn;
			return (bb.getContainedNodes().get(0));
		}
		else
		{
			return (cfn);
		}
	}

	private ArrayList<CfgNode> domF;
	public void setDominanceFrontier(ArrayList<CfgNode> domF){this.domF = domF;}
	public ArrayList<CfgNode> getDominanceFrontier(){return this.domF;}

	private boolean bRenamed;
	public boolean isRenamed () {return bRenamed; }
	public void setRenamed() {bRenamed = true;}
	
	// each cfg node stores the phi function that might be inserted before it 
	// only initialized if there is a phi, otherwise null
	// TODO : better abstractions
	// each cfg node may require insertion of more than one phi function
	// linked hash map - for each variable there is a phi function
	// ArrayList<CfgNode> tells us which predecessors are giving phi arguments
	// and ArrayList Variable stores the actual phi arguments coming from predecessors 
	// after renaming
//	private LinkedHashMap<Variable, LinkedHashMap<ArrayList<CfgNode>, ArrayList<Variable>>>
//		phiEntries;
//	private LinkedHashMap<Variable, LinkedHashMap<CfgNode, Variable>> phiEntries;
	private LinkedHashMap<Variable, ArrayList<Variable>> phiEntries;
	
	public void addPhiNode(Variable vLhs, ArrayList<Variable> vars)
	{
		if(phiEntries == null)
		{
//			LinkedHashMap<CfgNode, Variable>>();

			phiEntries = new LinkedHashMap<Variable, 
				ArrayList<Variable>>();
		}
//		LinkedHashMap<CfgNode, Variable> predsVars = new LinkedHashMap<CfgNode, Variable>();
//		for(CfgNode cfn : preds)
//			predsVars.put(cfn, null);
		phiEntries.put(vLhs, vars);
	}
	
//	public LinkedHashMap<Variable, LinkedHashMap<CfgNode, Variable>>
	public LinkedHashMap<Variable, ArrayList<Variable>>
		getPhiNodes(){return phiEntries;}
	
	public void 
//		setPhiNodes(LinkedHashMap<Variable, LinkedHashMap<CfgNode, Variable>> 
		setPhiNodes(LinkedHashMap<Variable, ArrayList<Variable>>
		newLi){phiEntries = newLi;}
	
	
	// each CFgNode holds a mapping between used variables and what they map to 
	// in original code 
	private LinkedHashMap<Variable, Variable> ssaToOrig; 
	public void addSSAVar(Variable fromSSA, Variable toOrig)
	{
		if(ssaToOrig == null)
		{
			ssaToOrig = new LinkedHashMap<Variable, Variable>();
		}
		ssaToOrig.put(fromSSA, toOrig);
	}
	
	public LinkedHashMap<Variable, Variable> getSSA2Orig(){ return ssaToOrig;}
	
	private ArrayList<TacPlace> placeholderMap; 
	public ArrayList<TacPlace> getPlaceHolderMaps(){ return placeholderMap; }

	public void addPlaceHolderMap(TacPlace questionMap)
	{
		if(placeholderMap == null)
		{
			placeholderMap = new ArrayList<TacPlace>();
		}
		placeholderMap.add(questionMap);
	}

	
	// SISL 
	// store actual query args to formal query args mapping
	private LinkedHashMap<TacPlace, TacPlace> actual2formalArgs;
	public LinkedHashMap<TacPlace, TacPlace> getActual2FormalArgs() { return actual2formalArgs;}
	public void addActual2FormalArgs(TacPlace actualArgs, TacPlace formalArgs)
	{
		if(actual2formalArgs == null)
			actual2formalArgs = new LinkedHashMap<TacPlace, TacPlace>();
		
		actual2formalArgs.put(actualArgs, formalArgs);
	}

	
	// store return values from the function calls 
	private LinkedHashMap<TacPlace, TacPlace> returnValue;
	public LinkedHashMap<TacPlace, TacPlace> getReturnValue(){ return returnValue; }
	public void addReturnValue(TacPlace receiver, TacPlace returnValue)
	{
		if(this.returnValue == null)
			this.returnValue = new LinkedHashMap<TacPlace, TacPlace>();
		this.returnValue.put(receiver, returnValue);
	}

	LinkedHashMap<Variable, CfgNode> phiContributors;
	public LinkedHashMap<Variable, CfgNode> getPhiContributors() 
	{
		return phiContributors;
	}
	public void addPhiContributors(Variable v, CfgNode by) 
	{
		if(phiContributors == null)
			phiContributors = new LinkedHashMap<Variable, CfgNode>();
		phiContributors.put(v, by);
	}

	public LinkedHashMap<Variable, CfgNode> getContributedToPhi() 
	{
		return toPhiDefinitions;
	}
	LinkedHashMap<Variable, CfgNode> toPhiDefinitions;
	public void addContributedTo(Variable v, CfgNode to) 
	{
		if(toPhiDefinitions == null)
			toPhiDefinitions = new LinkedHashMap<Variable, CfgNode>();
		toPhiDefinitions.put(v, to);
	}

	/* tag CFG nodes that are part of the loop body */
	private boolean bInLoop;
	
	/**
	 * @return the loopBody
	 */
	public boolean isInLoop() {
		return bInLoop;
	}
	
	/**
	 * @param loopBody the loopBody to set
	 */
	public void setInLoop() {
		this.bInLoop = true;
	}
	
	ArrayList<CfgNode> loopBodyCfg;
	public void setLoopBody(Cfg cfgBody) {
		this.loopBodyCfg = new ArrayList<CfgNode>();
		this.loopBodyCfg.addAll(cfgBody.getAll());
	}
	public ArrayList<CfgNode> getLoopBody() {
		return this.loopBodyCfg;
	}
	
	ArrayList<CfgNode> thenBody;
	Cfg thenBodyCfg;
	public void setThenBody(Cfg cfgBody) {
		//this.thenBody = new ArrayList<CfgNode>();
		//this.thenBody.addAll(cfgBody.getAll());
		this.thenBodyCfg = cfgBody;
	}
	public ArrayList<CfgNode> getThenBody() {
	//	return this.thenBody;
		if(this.thenBodyCfg != null)
			return this.thenBodyCfg.getAll();
		return new ArrayList<CfgNode>();
	}

	ArrayList<CfgNode> elseBody;
	Cfg elseBodyCfg;

	public void setElseBody(Cfg cfgBody) {
		//this.elseBody = new ArrayList<CfgNode>();
		//this.elseBody.addAll(cfgBody.getAll());
		this.elseBodyCfg = cfgBody;
	}

	/*
	private boolean bSwitch;
	public void setSwitchElse() {
		D.pv(D.D_TEMP, " For switch statements, currently, it is not possible to make " +
				"the fobidding decision correctly...fall in...");
		this.bSwitch = true;
	}
	public boolean isSwitchElse()
	{
		return bSwitch;
	}*/
	
	public ArrayList<CfgNode> getElseBody() {
	//	return this.elseBody;
		if(this.elseBodyCfg != null)
			return this.elseBodyCfg.getAll();
		
		ArrayList<CfgNode> l = new ArrayList<CfgNode>();
		if(elseBody != null && elseBody.size() != 0)
			l.addAll(elseBody);
		
		return l;
	}
	
	public static String getProgLines(Collection<CfgNode> nodes)
	{
		String str = " Lines = [ ";
		ArrayList<String> lines = new ArrayList<String>();
		
		for(CfgNode tcn : nodes)
		{
			String st = "";			
			if(tcn.getFileName() != null)
				st += tcn.getFileName();
			st += " : " + tcn.getOrigLineno() + ",  ";
			if(!lines.contains(st))
			{
				lines.add(st);
			}
		}
		Collections.sort(lines);
		for(String l : lines)
			str += l;
		
		str += " ] ";
		
		return str;
	}

	public static String getProgLineCode(CfgNode node)
	{
		if(node == null)
			return "null";
		
		int iLine = node.getOrigLineno();
		String fileName = node.getFileName();
		if(fileName == null || fileName.equals("<Unknown File Name>") || iLine < 0)
			return null;
		
		String absFile = node.getAbsFileName();
		StringBuffer sb = new StringBuffer(absFile);
		
		int iLast = sb.lastIndexOf("/");
		if(iLast > 0)
			sb = new StringBuffer(sb.substring(0, iLast - 1));
		
		iLast = sb.lastIndexOf("/");
		if(iLast > 0)
			sb = new StringBuffer(sb.substring(iLast));
		
		String fn = Cfg.getFunction(node).getName();
		
		String str = "";
		str += sb.toString() + "/" + fileName + " " + fn + " " ;
		str += " : " + iLine;
		
		String fa = ControlFlow.getLineNumber(absFile, iLine);

		str += fa ;
		str += "";		
		return str;
	}

	
	public static String getProgLine(CfgNode node)
	{
		String str = " Lines = [ ";
		if(node != null)
		{
			if(node.getFileName() != null)
				str += node.getFileName();
			str += " : " + node.getOrigLineno();
		}
		str += " ] ";
		return str;
	}

	//	 SISL
	public void addOutEdge(int index, CfgEdge edge) {
		this.outEdges[index] = edge;
	}
	
	/*
	 * fored processing is needed for special dummy that nodes are created 
	 * for formal parameters and irrespective of dependency must be included for 
	 * computing derivation trees; 
	 * initially: formal = formal; 
	 * while creating derivation tree: formal = actual ; 
	 */
	private boolean bForcedProcess;
	public void setAlwaysProcess(){ bForcedProcess = true; }
	public boolean isAlwaysProcess(){ return bForcedProcess; }

	public static String getProgLinesSorted(ArrayList<CfgNode> nodes) {
		//ArrayList<String> files = new ArrayList<String>();
		if(nodes == null)
			return "";
		
		LinkedHashMap<String, ArrayList<String>> file2Stmts = 
			new LinkedHashMap<String, ArrayList<String>> ();
		
		for(CfgNode tcn : nodes)
		{
			String file = tcn.getFileName();			
			String line = CfgNode.getProgLineCode(tcn);
			if(line == null)
				continue; 
			
			if( !file2Stmts.containsKey(file))
			{
				ArrayList<String> lines = new ArrayList<String>();
				lines.add(line);
				file2Stmts.put(file, lines);
			}
			else
			{
				ArrayList<String> existing = file2Stmts.get(file); 
				if(!existing.contains(line))
					existing.add(line);
			}
		}
		for(Map.Entry<String, ArrayList<String>> en : file2Stmts.entrySet())
		{
			Collections.sort(en.getValue());
		}
		ArrayList<String> files = new ArrayList<String> (file2Stmts.keySet());
		Collections.sort(files);
		
		String str = "\n\t\t { \n";
		for(String file : files)
		{
			str = str + "\n\t\t\t" + file + ":" + file2Stmts.get(file);
		}
		str += "\n\t\t } ";
		
		return str;
	}

	// let the caller do the formatting
	public static ArrayList<String> getProgLines(List<CfgNode> nodes) {

		if(nodes == null)
			return new ArrayList<String>();
		
		LinkedHashMap<String, ArrayList<String>> file2Stmts = 
			new LinkedHashMap<String, ArrayList<String>> ();
		
		for(CfgNode tcn : nodes)
		{
			String st = "";		
			String file = tcn.getFileName();			
			int iLine = tcn.getOrigLineno();
			if(iLine < 0 || file == null || file.equalsIgnoreCase("<File Name Unknown>"))
				continue;

			String line = CfgNode.getProgLineCode(tcn);
			if(line == null)
				continue; 
			
			if( !file2Stmts.containsKey(file))
			{
				ArrayList<String> lines = new ArrayList<String>();
				lines.add(line);
				file2Stmts.put(file, lines);
			}
			else
			{
				ArrayList<String> existing = file2Stmts.get(file); 
				if(!existing.contains(line))
					existing.add(line);
			}
		}

		for(Map.Entry<String, ArrayList<String>> en : file2Stmts.entrySet())
		{
			Collections.sort(en.getValue());
		}
		ArrayList<String> files = new ArrayList<String> ();
		ArrayList<String> lines = new ArrayList<String> ();
		for(ArrayList<String> inFile : file2Stmts.values())
			lines.addAll(inFile);
		
		return lines;
	}

	
	private boolean actualDependency;
	public void setActualDependency() {
		this.actualDependency = true;
	}
	public boolean getActualDependency() {
		return this.actualDependency;
	}

	public boolean equalsX(CfgNode cn)
	{
		if(cn == null)
			return false; 
		
		int iL1 = this.getOrigLineno();
		int iL2 = cn.getOrigLineno();
		
		String sFile1 = this.getFileName();
		String sFile2 = this.getFileName();
		
		String toStr1 = this.toString();
		String toStr2 = cn.toString();
		
//		D.pr("Comparing " + this + " with - " + cn);
		//D.pr(D.D_TEMP, " \t iL1 = " + iL1 + " iL2 = " + iL2 + " equals = " + (iL1 == iL2));
		//D.pr(D.D_TEMP, " \t sFile1 = " + sFile1 + " sFile2 = " + 
		//		sFile2 + " equals = " + (sFile1.equals(sFile2)));
//		D.pr(D.D_TEMP, " \t toStr1 = " + toStr1 + " toStr2 = " + 
//				toStr2 + " equals = " + (toStr1.equals(toStr2)));
		
		return toStr1.equals(toStr2);
	}

	public static boolean containsX(List<CfgNode> visited, CfgNode from) {
		if(visited == null)
			return false; 
		
		for(CfgNode cn : visited)
			if(cn.equalsX(from))
				return true;
		
		return false;
	}

	public static boolean containsX(Set<CfgNode> visited, CfgNode next) {
		if(visited == null)
			return false; 
		
		for(CfgNode cn : visited)
			if(cn.equalsX(next))
				return true;
		
		return false;
	}

	public String getLocNew() {
		if (!MyOptions.optionB && !MyOptions.optionW) {
			return this.getAbsFileName() + "_" + this.getOrigLineno();
		} else {
			return Utils.basename(this.getAbsFileName()) + "_" + this.getOrigLineno();
		}
	}

}

