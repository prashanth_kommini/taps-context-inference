package at.ac.tuwien.infosys.www.pixy.conversion;

import java.util.*;

import edu.uic.rites.sisl.D;


public class Variable 
extends TacPlace {

    // EFF: check for unnecessary fields / methods
    
    private String name;
    private SymbolTable symbolTable; // the symbol table this variable belongs to;
    private boolean isSuperGlobal;   // member of the superglobals symboltable?
    private boolean isLocal;
    private boolean isGlobal;   // local variable of the main function?
                                // (temporaries excluded);
                                // hence, globals and superglobals are different
    
    // a variable is either local or global or superglobal

    // array properties; array elements can also be arrays, if they have
    // elements themselves
    private boolean isArray;
    // TacPlace (=index) -> Variable
    // (only direct elements, i.e., who are one dimension deeper)
    private Map<TacPlace,Variable> elements;
    // "shortcut": contains all elements with a literal last index;
    // (only direct elements)
    private List<Variable> literalElements;

    // array element properties
    private boolean isArrayElement;
    private Variable enclosingArray;
    private Variable topEnclosingArray;
    private TacPlace index;     // last index for multidimensions
    private List<TacPlace> indices;   // all indices
    private boolean hasNonLiteralIndices;

    // additional information for variable variables
    private TacPlace dependsOn;
    
    // list of array elements whose LAST index is this variable
    private List<Variable> indexFor;
    
    // is this a temporary variable?
    private boolean isTemp;
    
    // is this an object member variable?
    private boolean isMember;
    
    // is this a function return variable?
    private boolean isReturnVariable;

    //mali
    private String className; //for objects

// *********************************************************************************
// CONSTRUCTORS ********************************************************************    
// *********************************************************************************

    public Variable(String name, SymbolTable symbolTable) {
        this.name = name;
        this.symbolTable = symbolTable;
        this.isSuperGlobal = false;
        if (symbolTable.isMain()) {
            this.isLocal = false;
            this.isGlobal = true;
        } else {
            this.isLocal = true;
            this.isGlobal = false;
        }
        
        this.isArray = false;
        this.elements = null;
        this.literalElements = null;

        this.isArrayElement = false;
        this.enclosingArray = null;
        this.topEnclosingArray = null;
        this.index = null;
        this.indices = null;

        this.dependsOn = null;
        this.indexFor = new LinkedList<Variable>();
        
        this.isTemp = false;
        this.isMember = false;
        this.isReturnVariable = false;
    }

    public Variable(String name, SymbolTable symbolTable, boolean isTemp) {
        this(name, symbolTable);
        this.isTemp = isTemp;
        if (isTemp) {
            this.isGlobal = false;
        }
    }

    
// *********************************************************************************
// GET *****************************************************************************    
// *********************************************************************************
    
    public String getName() {
        return this.name;
    }

    public SymbolTable getSymbolTable() {
        return this.symbolTable;
    }

    public boolean isSuperGlobal() {
        return this.isSuperGlobal;
    }

    
    
    public boolean isArray() {
        return this.isArray;
    }

    public List<Variable> getElements() {
        if (this.elements == null) {
            // System.out.println("WARNING: unchecked call to Variable.getElements()");
            return Collections.emptyList();
        } else {
            return new LinkedList<Variable>(this.elements.values());
        }
    }
    
    // returns all elements recursively (i.e., the whole array tree, without
    // the root)
    public List<Variable> getElementsRecursive() {
        List<Variable> retMe = new LinkedList<Variable>();
        Collection<Variable> directElements = this.elements.values();
        retMe.addAll(directElements);
        for (Variable directElement : directElements) {
            if (directElement.isArray()) {
                retMe.addAll(directElement.getElementsRecursive());
            }
        }
        return retMe;
    }
    
    public Variable getElement(TacPlace index) {
        if (this.elements == null) {
            return null;
        }
        return (Variable) this.elements.get(index);
    }
    
    public List<Variable> getLiteralElements() {
        return this.literalElements;
    }
   
    
    public boolean isArrayElement() {
        return this.isArrayElement;
    }

    public Variable getEnclosingArray() {
        return this.enclosingArray;
    }

    public Variable getTopEnclosingArray() {
        return this.topEnclosingArray;
    }
    
    public TacPlace getIndex() {
        return this.index;
    }
    
    public List<TacPlace> getIndices() {
        if (this.indices == null) {
            return new LinkedList<TacPlace>();
        } else {
            return new LinkedList<TacPlace>(this.indices);
        }
    }
    
    // does this array element have non-literal indices?
    public boolean hasNonLiteralIndices() {
        return this.hasNonLiteralIndices;
    }


    
    public TacPlace getDependsOn() {
        return this.dependsOn;
    }

    public List<Variable> getIndexFor() {
        return this.indexFor;
    }


    public String toString() {
        return this.symbolTable.getName() + "." + this.name;
    }
    
    public boolean isTemp() {
        return this.isTemp;
    }
    
    public boolean isMember() {
        return this.isMember;
    }
    
    public boolean isReturnVariable() {
        return this.isReturnVariable;
    }
    
    public boolean isLocal() {
        return this.isLocal;
    }
    
    public boolean isGlobal() {
        return this.isGlobal;
    }
    
    public boolean belongsTo(SymbolTable symTab) {
        return (symTab == this.symbolTable);
    }
    
    // is this a variable variable (such as "$$x")?
    public boolean isVariableVariable() {
        if (this.dependsOn == null) {
            return false;
        } else {
            return true;
        }
    }
    
    public boolean isArrayElementOf(Variable array) {
        // if we don't even have an enclosing array: can't be true 
        if (this.enclosingArray == null) {
            return false;
        }
        if (this.enclosingArray.equals(array)) {
            // found it!
            return true;
        }
        // walk upwards recursively
        return this.enclosingArray.isArrayElementOf(array);
    }


    public String getClassName(){
        return this.className;
    }
// *********************************************************************************
// SET *****************************************************************************    
// *********************************************************************************
   
    void setIsSuperGlobal(boolean isSuperGlobal) {
        this.isSuperGlobal = isSuperGlobal;
        if (isSuperGlobal) {
            this.isLocal = false;
            this.isGlobal = false;
        }
    }

    void setIsArray(boolean isArray) {
        if (isArray == true && this.isArray == false) {
            this.isArray = isArray;
            this.elements = new LinkedHashMap<TacPlace,Variable>();
            this.literalElements = new LinkedList<Variable>();
        } else {
            System.out.println("Warning: unchecked call to Variable.setIsArray()");
        }
    }

    
    void addElement(Variable variable) {
        if (!variable.isArrayElement()) {
            throw new RuntimeException("SNH");
        }
        this.elements.put(variable.getIndex(), variable);
        if (variable.getIndex() instanceof Literal) {
            this.literalElements.add(variable);
        }
    }

    
    void setArrayElementAttributes(Variable enclosingArray, TacPlace index) {
        
        this.isArrayElement = true;
        this.indices = new LinkedList<TacPlace>();
        this.hasNonLiteralIndices = enclosingArray.hasNonLiteralIndices();
        this.enclosingArray = enclosingArray;
        if (enclosingArray.isArrayElement()) {
            this.topEnclosingArray = enclosingArray.getTopEnclosingArray();
            this.indices.addAll(enclosingArray.getIndices());
        } else {
            this.topEnclosingArray = enclosingArray;
        }
        this.index = index;
        this.indices.add(index);
        if (!(index instanceof Literal)) {
            this.hasNonLiteralIndices = true;
        }
        
        if (index.isVariable()) {
            ((Variable) index).addIndexFor(this);
        }
        
        // if the enclosing array is a temporary, then this
        // one also has to be a temporary
        if (enclosingArray.isTemp()) {
            this.isTemp = true;
        }
        
        // if the enclosing array is a global, then this
        // one also has to be a global
        if (enclosingArray.isGlobal()) {
            this.isGlobal = true;
        }
    }

    void setDependsOn(TacPlace dependsOn) {
        this.dependsOn = dependsOn;
    }
    
    void setSymbolTable(SymbolTable symbolTable) {
        this.symbolTable = symbolTable;
        // this.resetHashCode();
    }
    
    void addIndexFor(Variable var) {
        this.indexFor.add(var);
    }
    
    void setIsMember(boolean isMember) {
        this.isMember = isMember;
    }
    
    void setIsReturnVariable(boolean isReturnVariable) {
        this.isReturnVariable = isReturnVariable;
    }
    
    void setIsGlobal() {
        this.isGlobal = true;
        this.isLocal = false;
    }
    
    void setIsLocal() {
        this.isLocal = true;
        this.isGlobal = false;
    }

    //mali:
    void setClassName(String cn){
        this.className = cn;
    }
  
// *********************************************************************************
// OTHER ***************************************************************************    
// *********************************************************************************
    
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Variable)) {
            return false;
        }
        Variable comp = (Variable) obj;
        if (!this.symbolTable.equals(comp.getSymbolTable())) {
            return false;
        }
        return (this.name.equals(comp.getName()));
    }

    // EFF: hashcode caching
    public int hashCode() {
        
        int hashCode = 17;
        hashCode = 37*hashCode + this.name.hashCode();
        hashCode = 37*hashCode + this.symbolTable.hashCode();
        return hashCode;
    }

    
    // SISL
//    private String ssaName;
//	public void setSSAName(String strSSA) { this.ssaName = strSSA; }
//	public String getSSAName() { return ssaName;}

	public Variable dup(String ssaName)
	{
		if(ssaName.equals(this.name))
			throw new Error(" requested to dup variable name - " + ssaName);
		
		Variable vDup = new Variable(ssaName, symbolTable);
		
	    vDup.isSuperGlobal = this.isSuperGlobal;
	    vDup.isLocal = this.isLocal;
	    vDup.isGlobal = this.isGlobal;
	    vDup.isArray = this.isArray;
	    
	    if(this.elements != null)
	    {
		    LinkedHashMap<TacPlace,Variable> elements = new LinkedHashMap<TacPlace,Variable>();
		    elements.putAll(this.elements);
		    vDup.elements = elements;
	    }
	    
	    if(this.literalElements != null)
	    {
		    ArrayList<Variable> literalElements = new ArrayList<Variable>();
		    literalElements.addAll(this.literalElements);
		    vDup.literalElements = literalElements;
	    }
	    
	    // array element properties
	    vDup.isArrayElement = this.isArrayElement;
	    vDup.enclosingArray = this.enclosingArray;		    
	    vDup.topEnclosingArray = this.topEnclosingArray;
	    
	    if(this.isArrayElement)
	    {
	    	
	    	System.out.println("ssa name - " + ssaName);
	    	int iBegin = ssaName.lastIndexOf("_SSA");
    		int iEnd = ssaName.indexOf("[", iBegin);
	    	if( iBegin >= 0 && iEnd >= 0)
	    	{
		    	String suffix = ssaName.substring(iBegin, iEnd);
		    	if(this.enclosingArray != null)
		    	{
			    	String name = Variable.makeSSAName(this.enclosingArray, suffix);
			    	Variable vNewEnclose = this.enclosingArray.dup(name);
			    	vDup.enclosingArray = vNewEnclose;
		    	}
		    	if(this.topEnclosingArray != null)
		    	{
			    	String name = Variable.makeSSAName(this.topEnclosingArray, suffix);
			    	Variable vNewEnclose = this.enclosingArray.dup(name);
			    	vDup.enclosingArray = vNewEnclose;
		    	}
	    	}
	    }
	    
	    vDup.index = this.index;
	    
	    if(this.indices != null)
	    {
		    ArrayList<TacPlace> indices = new ArrayList<TacPlace>();
		    indices.addAll(this.indices);
		    vDup.indices = indices;
	    }
	    
	    vDup.hasNonLiteralIndices = this.hasNonLiteralIndices;

	    vDup.dependsOn = this.dependsOn;
	    
	    if(this.indexFor != null)
	    {
		    ArrayList<Variable> indexFor = new ArrayList<Variable>();
		    indexFor.addAll(this.indexFor);
		    vDup.indexFor = indexFor; 
	    }
	    
	    vDup.isTemp = this.isTemp;
	    
	    vDup.isMember = this.isMember;
	    
	    vDup.isReturnVariable = this.isReturnVariable;	
	    
	    vDup.setOrig(((this.getOrig() == null) ? this : this.getOrig()));
	    
	    vDup.setFormal(this.isFormal());
	    
	    return vDup;
	}
	
	private Variable varOrig;
	public void setOrig (Variable v) {varOrig = v;} 
	public Variable getOrig() {return varOrig;}
	
	private String sqlName; 
	public String getSQLName(){ return sqlName; }
	
	public void setSQLName()
	{
		ArrayList<Character> banned = new ArrayList<Character>();
		banned.add('_'); banned.add('$');
		banned.add('['); banned.add(']');

		String str = this.name;
		sqlName = "";
		for(Character c : str.toCharArray())
		{
			if (banned.contains(c))
				sqlName += "X";
			else
				sqlName += c;
		}
	}

    // SISL 
    private boolean bFormal;
    public boolean isFormal() { return bFormal; }
	/**
	 * @param b
	 */
	public void setFormal(boolean b){this.bFormal = b;}

	/*
	 * return a unique name for holding the ? - args mapping
	 */
	public static String makeArgsName(Variable v,  boolean addArgs) {
		String name = "";
		SymbolTable st = v.getSymbolTable();
	
		D.pd(D.D_TEMP, "!!!!!!!!!!!! variable name - " + v.getName() + " symbol table name - " + v.getSymbolTable().getName());
		if(v.isSuperGlobal())
		{
			D.pd(D.D_TEMP, " superglobal ");		
			name = v.getName();
			if(addArgs) 
				name += "__args";
		}
		else
		if(v.isMember())
		{
			TacPlace tp = v.getObjectPlace();
			Variable vO = v.getVariablePlace();
			D.pd(D.D_TEMP, " special handling for member " + v + " obj name " + tp + " var name " + vO);

			name = vO.getName();
			if(tp != null)
			{
				String namePre = makeArgsName(tp.getVariable(), false);
				D.pd(D.D_TEMP, " object name - " + namePre);
				name = namePre + "->" + name;
			}
			
			if(addArgs)
				name += "__args";
		}
		else
		if(v.isArrayElement())
		{
			name = getArrayElementName(v, addArgs);
			D.pd(D.D_TEMP, " renaming an array element variable " + v + " to " + name);
		}
		else
		if(v.isArray())
		{
			name = getArrayVarName(v, addArgs);
			D.pd(D.D_TEMP, " renaming an array variable " + v + " to " + name);
		}
		else
		if(v.isLocal() || v.isGlobal())
		{
			D.pd(D.D_TEMP, " local or global ");
			name = v.getName();
			if(addArgs)
				name += "__args";
		}
		else
		if(v.isVariableVariable())
		{
			D.pd(D.D_TEMP, " variablevariable");
			throw new Error(" unhandled variablevariable naming");
		}
		else
		{
			String str = " unhandled naming for variable - " + v + " \n"; 
			str += " \t is array = " + v.isArray() + " \n";
			str += " \t is global " + v.isGlobal() + "\n";
			str += " is formal " + v.isFormal() + " \n";
			str += " is return " + v.isReturnVariable() + "\n";
			if(v.isTemp())
			{
				String s1 = v.getName(); 
				if(addArgs)
					s1 += "__args"; 
				
				return s1;
			}
			throw new Error(str);
		}

		if(v.isTemp() && !name.startsWith("$") )
			name = "$" + name;
		return name;
	}
	
	private static String getArrayElementName(Variable v, boolean addArgs)
	{
		String nameT = "";
		D.pd(D.D_TEMP, " array element ");
		List<TacPlace> indices = v.getIndices();
		for(TacPlace index : indices)
			D.pd(D.D_TEMP, " \t \t \t index - " + index);
		
		Variable vE = v.getEnclosingArray();
		D.pd(D.D_TEMP, " enclosing array - " + vE);
		
		Variable vET = v.getTopEnclosingArray();
		D.pd(D.D_TEMP, " top enclosing array - " + vE);
//		nameT = makeArgsName(vET, false);
		nameT = vET.getName();
		if(addArgs)
			nameT += "__args";
		for(TacPlace index : indices)
		{
			if(index.isVariable())
			{
				Variable vi = index.getVariable();
				nameT += "[" + Variable.makeArgsName(vi, false) + "]";
			}
			else
			{
				nameT += "[" + index + "]";
			}
		}

		return nameT;
	}

	private static String getArrayVarName(Variable v, boolean addArgs)
	{		
		String nameT = "";
		D.pd(D.D_TEMP, " array element ");
		List<TacPlace> indices = v.getIndices();
		for(TacPlace index : indices)
			D.pd(D.D_TEMP, " \t \t \t index - " + index);
		
		Variable vE = v.getEnclosingArray();
		D.pd(D.D_TEMP, " enclosing array - " + vE);
		
		Variable vET = v.getTopEnclosingArray();
		D.pd(D.D_TEMP, " top enclosing array - " + vE);
//		nameT = makeArgsName(vET, false);
		nameT = v.getName();
		if(vET != null)
			nameT = vET.getName();
		if(addArgs)
			nameT += "__args";

		return nameT;
	}

	private boolean bPhiLhs;
	private TacPlace objectplace;
	private Variable variablePlace; 
	public boolean isPhiLhs() {return bPhiLhs;}
	public void setPhiLhs() { bPhiLhs = true;}
	
	public void setObjectPlace(TacPlace leftPlace)
	{
		this.objectplace = leftPlace;
	}
	public TacPlace getObjectPlace(){ return this.objectplace;}
	public Variable getVariablePlace() { return this.variablePlace;}
	public void setVariablePlace(Variable v)
	{
		this.variablePlace = v;
	}

	public static String makeSSAName(Variable v, String string, Integer counter) {
		return makeSSAName(v, string+counter);
	}
	
	public static String makeSSAName(Variable v, String suffix) {

		String name = "";
		SymbolTable st = v.getSymbolTable();
	
		D.pd(D.D_TEMP, "!!!!!!!!!!!! variable name - " + v.getName() + " symbol table name - " + v.getSymbolTable().getName());
		if(v.isSuperGlobal())
		{
			D.pd(D.D_TEMP, " superglobal ");		
			name = v.getName();
			name += suffix;
		}
		else
		if(v.isMember())
		{
			TacPlace tp = v.getObjectPlace();
			Variable vO = v.getVariablePlace();
			D.pd(D.D_TEMP, " special handling for member " + v + " obj name " + tp + " var name " + vO);

			if(vO != null)
				name = vO.getName();
			
			if(tp != null)
			{
				String namePre = makeSSAName(tp.getVariable(), suffix);
				D.pd(D.D_TEMP, " object name - " + namePre);
				name = namePre + "->" + name;
			}
			
			name += suffix;
		}
		else
		if(v.isArrayElement())
		{
			name = getArrayElementSSAName(v, suffix);
			D.pd(D.D_TEMP, " renaming an array element variable " + v + " to " + name);
		}
		else
		if(v.isArray())
		{
			name = getArrayVarSSAName(v, suffix);
			D.pd(D.D_TEMP, " renaming an array variable " + v + " to " + name);
		}
		else
		if(v.isLocal() || v.isGlobal())
		{
			D.pd(D.D_TEMP, " local or global ");
			name = v.getName();
			name += suffix;
		}
		else
		if(v.isVariableVariable())
		{
			D.pd(D.D_TEMP, " variablevariable");
			throw new Error(" unhandled variablevariable naming");
		}
		if(v.isTemp())
		{
			name = v.getName() + suffix;
		}
		else
		{
			String str = " unhandled naming for variable - " + v + " \n"; 
			str += " \t is array = " + v.isArray() + " \n";
			str += " \t is global " + v.isGlobal() + "\n";
			str += " is formal " + v.isFormal() + " \n";
			str += " is return " + v.isReturnVariable() + "\n";
			D.pr(D.D_TEMP, " -\n\n\n " + str + "\n\n\n");
			name = v.getName() + suffix;
//			throw new Error(str);
		}

		return name;
	}

	private static String getArrayElementSSAName(Variable v, String suffix)
	{
		String nameT = "";
		D.pd(D.D_TEMP, " array element ");
		List<TacPlace> indices = v.getIndices();
		for(TacPlace index : indices)
			D.pd(D.D_TEMP, " \t \t \t index - " + index);
		
		Variable vE = v.getEnclosingArray();
		D.pd(D.D_TEMP, " enclosing array - " + vE);
		
		Variable vET = v.getTopEnclosingArray();
		D.pd(D.D_TEMP, " top enclosing array - " + vE);
//		nameT = makeArgsName(vET, false);
		nameT = vET.getName();
		nameT += suffix;
		
		for(TacPlace index : indices)
		{
			if(index.isVariable())
			{
				Variable vi = index.getVariable();
				D.pr(D.D_TEMP, " \n\n\n\n\n\n\n\nArray indices unhandled " + v + " index - " + vi + "\n\n\n\n\n\n\n\n");
//				nameT += "[" + Variable.makeArgsSSAName(vi, "", counter) + "]";
//				nameT += "[" + Variable.makeArgsSSAName(vi, "", counter) + "]";
				nameT += "[" + vi + "]";
			}
			else
			{
				nameT += "[" + index + "]";
			}
		}

		return nameT;
	}

	private static String getArrayVarSSAName(Variable v, String suffix)
	{		
		String nameT = "";
		D.pd(D.D_TEMP, " array element ");
		List<TacPlace> indices = v.getIndices();
		for(TacPlace index : indices)
			D.pd(D.D_TEMP, " \t \t \t index - " + index);
		
		Variable vE = v.getEnclosingArray();
		D.pd(D.D_TEMP, " enclosing array - " + vE);
		
		Variable vET = v.getTopEnclosingArray();
		D.pd(D.D_TEMP, " top enclosing array - " + vE);
//		nameT = makeArgsName(vET, false);
		nameT = v.getName();
		if(vET != null)
			nameT = vET.getName();

		nameT += suffix;

		return nameT;
	}

}


