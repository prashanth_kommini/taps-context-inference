package at.ac.tuwien.infosys.www.pixy.analysis.dep;

import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNode;

import at.ac.tuwien.infosys.www.pixy.Dumper;
import at.ac.tuwien.infosys.www.pixy.MyOptions;
import at.ac.tuwien.infosys.www.pixy.conversion.BuiltinFunctions;
import at.ac.tuwien.infosys.www.pixy.conversion.TacOperators;
import at.ac.tuwien.infosys.www.pixy.conversion.nodes.CfgNode;

import java.util.concurrent.*;

public class DepGraphSSANode {
//extends DepGraphNode {
/*
    private CfgNode cfgNode;
    private String name;
    public boolean bSSAed ;

    public String dotName(){return "";}
    public String comparableName(){return "";}
    public String dotNameShort(){return "";}
    public String dotNameVerbose(boolean isModelled){return "";}

    public CfgNode getCfgNode() {
        return this.cfgNode;
    }

    public DepGraphSSANode(CfgNode cfgN)
    {
    	cfgNode = cfgN;
    	bSSAed = false;
    }
    
//  ********************************************************************************
    
    public int getLine() {
        return this.cfgNode.getOrigLineno();
    }
    
//  ********************************************************************************
    
    public int hashCode() {
        int hashCode = 17;
        hashCode = 37*hashCode + this.cfgNode.hashCode();
        return hashCode;
    }
    
//  ********************************************************************************
    
    public boolean equals(Object compX) {

    	boolean bRet = true; 
        
        if (!(compX instanceof DepGraphSSANode)) {
            bRet = false;
        }
        else
        {
        	DepGraphSSANode comp = (DepGraphSSANode) compX;

	        if (!this.cfgNode.equals(comp.cfgNode)) {
	            bRet = false;
	        }
        }
        
    	//System.out.println("Equals comparison returned - " + bRet);
	    return bRet;
/*    	boolean bRet = true; 
        if (compX == this) {
            return true;
        }
        if (!(compX instanceof DepGraphSSANode)) {
            return false;
        }
        DepGraphSSANode comp = (DepGraphSSANode) compX;

        //
        //if (!this.place.equals(comp.place)) {
        //    return false;
        //}
        
        if (!this.cfgNode.equals(comp.cfgNode)) {
            return false;
        }

    	System.out.println("Equals comparison returned - " + bRet);

        return bRet;
    }
    
    
    // SSA function 
    public String toString()
    {
    	return " associated CFG node - " + cfgNode.toString();
    }
    
    private CopyOnWriteArrayList<CfgNode> dominates;
    private CopyOnWriteArrayList<CfgNode> immediateDominates; 
	public void addDominates(CopyOnWriteArrayList<CfgNode> dominates)
	{
		this.dominates = dominates;
	}

	public CopyOnWriteArrayList<CfgNode> getDominates()
	{
		return dominates; 
	}
	
	public void setImmediateDominates(CopyOnWriteArrayList<CfgNode> cni)
	{
		this.immediateDominates = cni;
	}
    public boolean equalsX(DepGraphNode compX) {

    	if(! equals(compX))
    		return false; 
    	
    	DepGraphNode dgn = (DepGraphNode) compX; 
    	if(this.getLocationCounter() == dgn.getLocationCounter())
    		return true;
		return false; 
    }
*/
}
