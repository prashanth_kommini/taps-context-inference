package at.ac.tuwien.infosys.www.pixy.conversion.nodes;

import at.ac.tuwien.infosys.www.phpparser.*;
import at.ac.tuwien.infosys.www.pixy.conversion.TacActualParam;
import at.ac.tuwien.infosys.www.pixy.conversion.TacPlace;
import at.ac.tuwien.infosys.www.pixy.conversion.Variable;

import java.util.*;

import edu.uic.rites.sisl.D;

// *********************************************************************************
// CfgNodeCall *********************************************************************
// *********************************************************************************

// a function call
public class CfgNodeCallBuiltin
extends CfgNode {

    // name of the called builtin function
    private String functionName;
    
    // parameter list
    private List<TacActualParam> paramList;
    
    // temporary variable to hold the return value
    private Variable tempVar;

    
// CONSTRUCTORS ********************************************************************    

    public CfgNodeCallBuiltin(String functionName, 
            List<TacActualParam> paramList, TacPlace tempPlace, ParseNode node) {
        
        super(node);
        this.functionName = functionName.toLowerCase();
        this.paramList = paramList;
        this.tempVar = (Variable) tempPlace;
    }

// GET *****************************************************************************
    
    public String getFunctionName() {
        return this.functionName;
    }

    public List<TacActualParam> getParamList() {
        return this.paramList;
    }

    public Variable getTempVar() {
        return this.tempVar;
    }
    
    public List<Variable> getVariables() {
        List<Variable> retMe = new LinkedList<Variable>();
        for (Iterator iter = this.paramList.iterator(); iter.hasNext();) {
            TacActualParam param = (TacActualParam) iter.next();
            TacPlace paramPlace = param.getPlace();
            if (paramPlace instanceof Variable) {
                retMe.add((Variable) paramPlace);
            } else {
                retMe.add(null);
            }
        }
        return retMe;
    }

// SET *****************************************************************************
    
    public void replaceVariable(int index, Variable replacement) {
        TacActualParam param = (TacActualParam) this.paramList.get(index);
        param.setPlace(replacement);
    }

    public ArrayList<Variable> getRightToRename() 
    { 
    	D.pv(D.D_TEMP, "!!!!!!!! builtin ....");
    	ArrayList<Variable> v = new ArrayList<Variable>();
    	List<TacActualParam> params = this.getParamList();
    	for(TacActualParam tap : params)
    	{
    		if(tap.getPlace().isVariable())
    			v.add((Variable)tap.getPlace());
    	}
    	
    	v.add(this.tempVar);
    	return v;
    }
    
    public void rightRename(Variable from, Variable to, boolean setOrig)
    {
    	D.pv(D.D_TEMP, "SSA!!!!!!!!!! builtin !!! method - " +  this.getFunctionName() + "renaming " + 
    			" " + from.getName() + " to " + to.getName());

    	List<TacActualParam> params = this.getParamList();
    	for(TacActualParam tap : params)
    	{
    		if(tap.getPlace().equals(from))
    		{
    			if(setOrig) to.setOrig(from);
    			tap.setPlace(to);
    		}
    	}
    	
    	if(this.tempVar.equals(from))
    	{
    		if(setOrig) to.setOrig(this.tempVar);
    		this.tempVar = to;
    	}
    }

}

