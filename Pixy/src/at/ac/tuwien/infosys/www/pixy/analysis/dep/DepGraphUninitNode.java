package at.ac.tuwien.infosys.www.pixy.analysis.dep;


// special node representing uninitialized variables
public class DepGraphUninitNode 
extends DepGraphNode {
    
    public DepGraphUninitNode() {

    	// SISL : move to super...OOP is broken
        this.locationCounter = this.lc_shared++;

    }

    // returns a name that can be used in dot file representation
    public String dotName() {
        return "<uninit>";
    }

    public String comparableName() {
        return dotName();
    }
    
    public String dotNameShort() {
        return dotName();
    }
    
    public String dotNameVerbose(boolean isModelled) {
        return dotName();
    }
    
    public int getLine() {
        return -1;
    }
    
    public boolean equalsX(DepGraphNode compX) {

    	if(! equals(compX))
    		return false; 
    	
    	DepGraphNode dgn = (DepGraphNode) compX; 
    	if(this.getLocationCounter() == dgn.getLocationCounter())
    		return true;
		return false; 
    }

}
