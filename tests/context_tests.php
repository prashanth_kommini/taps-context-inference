<?php

function sanitize_key($str){
	return str_replace( "something", "nothing",$str);
}

function sanitize_post($str){
	return str_replace("nothing", "something", $str);
}

function f1($str){
	return sanitize_key($str);
}

$rcdata_test1 = $_GET['rcdata_test1']. "rcdatatest1";
$rcdata_test2 = f1($_GET['rcdata_test2']);
// $rcdata_test2 = $_GET['rcdata_test2'];
$pcdata_test1 = $_GET['pcdata_test1']. "pcdatatest1";
$pcdata_test2 = sanitize_post($_GET['pcdatatest2']);
// $pcdata_test2 = $_GET['pcdata_test2'];
$tagname_test1 = $_GET['tagname_test1']."tagnametest1";//$_GET['tagname_test1']. 
$tagname_test2 = sanitize_post($_GET['tagname_test2']);
// $tagname_test2 = $_GET['tagname_test2'];
$attr_name_test1 = $_GET['attr_name_test1'] . "attrnametest1";
$attr_name_test2 = sanitize_post($_GET['attr_name_test2']);
// $attr_name_test2 = $_GET['attr_name_test2'];
$quoted_attr_val_test1 = $_GET['quoted_attr_val_test1'] . "quotedattrvaltest1";
$quoted_attr_val_test2 = sanitize_post($_GET['quoted_attr_val_test2']);
// $quoted_attr_val_test2 = $_GET['quoted_attr_val_test2'];
$unquoted_attr_val_test1 = $_GET['unquoted_attr_val_test1'] . "unquotedattrvaltest1"; 
$unquoted_attr_val_test2 = strip_tags($_GET['unquoted_attr_val_test2']); 
// $unquoted_attr_val_test2 = $_GET['unquoted_attr_val_test2']; 
$flag = true;
$xmlns_url = "http://www.w3.org/1999/xhtml";
$html_open = "<html xmlns=\"$xmlns_url\" dir=\"ltr\" lang=\"en-US\">";
$profile_url = "http://gmpg.org/xfn/11";
$head = "<head profile=\"$profile_url\">
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />
<title> Testing</title>
<link rel='index' title=\'$quoted_attr_val_test1\' href='http://localhost/misc/wordpress' />
<meta name=\"generator\" content=\"WordPress 2.9.2\" />
</head>";

$div_header = "<hr />
<$tagname_test1 $attr_name_test1=\"$quoted_attr_val_test2\" role=\"banner\">
	<div id=\"headerimg\">
		<div class=\"description\">Just another WordPress weblog</div>
		<textarea>$rcdata_test1</textarea>
		<script>$rcdata_test2</script>
	</div>
</$tagname_test1>";
// Replace first div tag's id attribute with $get_test
$div_content = "<hr />
<div id=\"content\" $attr_name_test2=\"narrowcolumn\" role=$unquoted_attr_val_test1>
	<div class=\"post-1 post entry category-uncategorized\" id=\"post-1\">
		<small>April 10th, 2016 <!-- by admin --></small>
		<div class=\"entry\">
			<p>Welcome to WordPress. This is your first $pcdata_test1. Edit or delete it, then start blogging!</p>
		</div>
	</div>
	
	<div class=\"navigation\">
		<div class=$unquoted_attr_val_test2></div>
		<div class=\"alignright\"></div>
	</div>

</div>";

$div_footer = "<hr />
<div id=\"footer\" role=\"contentinfo\">
	<$tagname_test2>
		Testing is proudly powered by
		<a href=\"http://wordpress.org/\">$pcdata_test2</a>

	</$tagname_test2>
</div>
";
// $div_page = "<div id=\"page\">". $div_header.$div_footer . "</div>";
$div_page = "<div id=\"page\">". $div_header . $div_content . $div_footer . "</div>";
$body = "<body class=\"home blog logged-in\">". $div_page . "</body>";
$full_page = $head . $body;

if($flag)
	echo $full_page;
else
	echo "Flag is False\n";