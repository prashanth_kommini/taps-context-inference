<?php

require_once "class-tag.php";

class Body extends Tag{

	public $text;

	public function __construct($text){
		parent::__construct($text);
	}

	public function get_body(){
		return parent::get_whole_text();
	}
}