<?php

require_once "class-tag.php";

class DivFooter extends Tag{

	public $text;

	public function __construct($text){
		parent::__construct($text);
	}

	public function get_div_footer(){
		return parent::get_whole_text();
	}
}