<?php

require_once "class-tag.php";

class DivHeader extends Tag{

	public $text;

	public function __construct($text){
		parent::__construct($text);
	}

	public function get_div_header(){
		return parent::get_whole_text();
	}
}