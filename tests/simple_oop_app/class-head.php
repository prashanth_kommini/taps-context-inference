<?php

require_once "class-tag.php";

class Head extends Tag{

	public $text;

	public function __construct($text){
		parent::__construct($text);
	}

	public function get_head(){
		return parent::get_whole_text();
	}
}