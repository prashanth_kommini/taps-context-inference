<?php

require_once "class-tag.php";
class DivContent extends Tag{

	public $text;

	public function __construct($text){
		parent::__construct($text);
	}

	public function get_div_content(){
		return $this->text;
	}
}