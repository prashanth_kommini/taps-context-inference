<?php

require_once "class-tag.php";

class Div extends Tag{

	public $text;

	public function __construct($text){
		parent::__construct($text);
	}

	public function get_div(){
		return parent::get_whole_text();
	}
}