<?php

require_once "class-tag.php";

class HTMLPage{

	public $text;

	public function __construct($text){
		$this->text = $text;
	}
	
	public function get_html_page(){
		return $this->text;
	}
}