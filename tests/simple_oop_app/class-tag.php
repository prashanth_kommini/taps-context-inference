<?php

class Tag {

	public $text;

	function __construct($text){
		$this->text = $text;
	}

	public function get_whole_text(){
		return $this->text;
	}
}